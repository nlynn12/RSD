// Conveyorbelt 

leg_size = 40;
leg_height_front = 1030;
leg_height_back = 1030;
leg_pose_front = 540;
leg_pose_back = leg_pose_front + leg_size + 1060;

belt_height = 70; 
belt_length = 2080;
belt_width = 196;
belt_rot = atan((850 - 490) / 1060);
belt_mount_height = 420 - tan(belt_rot) * (540+leg_size);

motor_x = 300; 
motor_y = 150;
motor_z = 200; 
motor_mounting_dist = leg_pose_back + 40 + 60;
motor_mount_height = belt_mount_height + tan(belt_rot) * motor_mounting_dist + belt_height/2 - motor_z/2; // consider the effect of rotation on belt_height as negible

front_y = 300;
front_y_mount_offset_from_belt = 60;
front_x = 200;
front_z = belt_height;

safety_dist = 10;

// generation of the figure
scale([0.001, 0.001, 0.001]){
union(){
	// Belt
	translate([0, leg_size-safety_dist, belt_mount_height]){
		rotate([0, - belt_rot, 0]){
			cube([belt_length, belt_width+2*safety_dist, belt_height], center=false);
		}
	}

	// Legs
	// front right
	translate([leg_pose_front-safety_dist/2, -safety_dist/2, 0]){
		cube([leg_size+safety_dist/2, leg_size+safety_dist/2, leg_height_front+safety_dist], center=false);
	}
	// front left
	translate([leg_pose_front-safety_dist/2, belt_width+leg_size-safety_dist/2, 0]){
		cube([leg_size+safety_dist, leg_size+safety_dist, leg_height_front+safety_dist], center=false);
	}
	// back right
	translate([leg_pose_back-safety_dist/2, -safety_dist/2, 0]){
		cube([leg_size+safety_dist, leg_size+safety_dist, leg_height_back+safety_dist], center=false);
	}
	// back left
	translate([leg_pose_back-safety_dist/2, belt_width+leg_size-safety_dist/2, 0]){
		cube([leg_size+safety_dist, leg_size+safety_dist, leg_height_back+safety_dist], center=false);
	}

	// motor
	translate([motor_mounting_dist, belt_width+leg_size-safety_dist, motor_mount_height-safety_dist]){
		rotate([0, - belt_rot, 0]){
			cube([motor_x+2*safety_dist, motor_y+2*safety_dist, motor_z+2*safety_dist], center=false);
		}
	}	

	// frontend
	translate([0, leg_size-front_y_mount_offset_from_belt-safety_dist, belt_mount_height]){
		rotate([0, - belt_rot, 0]){
			cube([front_x, front_y+2*safety_dist, front_z], center=false);
		}
	}	

 
}
}