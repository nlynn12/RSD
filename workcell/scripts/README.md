 This folder contains the scripts used to generate some of the stl models used in the workcell.
 Currently the following models are generated:
 * Conveorbelt. (conveorbelt.scad)
 * Foot, on which the robot is mounted. (foot.scad)
 
 The .stl models are genrated using OpenSCAD.
 Get OpenSCAD on Ubuntu using: 'sudo apt-get install openscad'.
 
 ---
 
 OpenSCAD is a simple scripting language used to generate 3D models.
 
 
 For more info see their [homepage](http://www.openscad.org/) or use their [CheatSheet](http://www.openscad.org/cheatsheet/index.html).