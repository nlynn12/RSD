// script for generating the base the robot stands on

base_height = 800;
mounting_base_size = 300;
base_radius = 100;

stand_size = 1000;
stand_height = 10;


scale([0.001,0.001,0.001]){
	union(){
		// stand
		translate([0,0,base_height/2]){
			cube([mounting_base_size, mounting_base_size, base_height], center=true);
		}
		// mounting base
		translate([0,0,stand_height/2]){
			cube([stand_size, stand_size, stand_height], center=true);
		}
	}
}