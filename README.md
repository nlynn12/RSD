# The other group
[other group gitlab](https://gitlab.com/madsherlock/RSD2016-team2)
# Wiki
The groups wiki can be found on [GitLab](https://gitlab.com/nlynn12/RSD/wikis/home) and direct links to individual pages can be found here
* [Meeting Log](https://gitlab.com/nlynn12/RSD/wikis/meetinglog)
* [ROS on Multiple Machines](https://gitlab.com/nlynn12/RSD/wikis/ros-master)
* [Communication](https://gitlab.com/nlynn12/RSD/wikis/communication)
* [VPN](https://gitlab.com/nlynn12/RSD/wikis/vpn)
* [SCRUM](https://gitlab.com/nlynn12/RSD/wikis/scrum)
* [Robotics](https://gitlab.com/nlynn12/RSD/wikis/robotics)
   * [Safety](https://gitlab.com/nlynn12/RSD/wikis/robo_safety)
   * [Vision](https://gitlab.com/nlynn12/RSD/wikis/robo_vision)
* [Frobomind](https://gitlab.com/nlynn12/RSD/wikis/frobomind)
   * [Vision](https://gitlab.com/nlynn12/RSD/wikis/frobo_vision)
   * [Laser Range Scanner](https://gitlab.com/nlynn12/RSD/wikis/frobo_scanner)
   * [Raspberry Pi](https://gitlab.com/nlynn12/RSD/wikis/raspberrypi)


# Group contract RSD Group 1

**Tools**
* Framework:      SCRUM
* Sprint:         Trello
* Backlog:        Undecided
* Report:         LaTeX
* Code sharing:   Gitlab
* Text sharing:   Gitlab .MD format
* General Data:   Matlab
* Calender:       Undecided

**Goals**
* Uphold good SCRUM standard
* Complete 24h stress test
* Good documentation of code and hardware
* Clear, descriptive, simple and coherent report
* Independent clear and functional modules with clear I/O

**Expectations**
* Meetings: 2 weekly meetings (Wednesday 8-12 and Friday 8-12)
   * Nicolai informs:
       * Not present on Wednesdays.
       * On holiday in week 46 and 47 (moved workload to week 42)
*	Expected to follow Friday class 8-12
*	Expected to spent at least 16 hours a week
*	Participate actively in meetings and workload 
*	Add transparency (keep group in the loop) and follow up on given tasks
*	Do the SCRUM role assigned
*	Code standard: Comments, long descriptive, variables
*	English is spoken and written at all times in group.
*	Treat it as a job like cancelation policy (tell SCRUM master). 

Should any of these expectations be broken to such a degree that it compromises the goals it should be taken up at a group meeting as soon as possible to act and take further action. As a final result multiple warnings will result in exclusion of the group.

Please read the [Course Description](http://fagbesk.sam.sdu.dk/study/fagbasen/fagprint.shtml?fag_id=32979&print=1)
