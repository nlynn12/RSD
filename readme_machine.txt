
#Make sure your known hosts file is clear
nano ~/.ssh/known_hosts

#Then ROS is using an old algorithm for host keys and therefore we need to force this at first connection.
#Connect and login to each.
ssh -oHostKeyAlgorithms='ssh-rsa' rsd-group1@rsd-group1-pi1.projectnet.wlan.sdu.dk
ssh -oHostKeyAlgorithms='ssh-rsa' rsd-group1@rsd-group1-pi1.projectnet.wlan.sdu.dk

#add the following to the .bashrc. Remeber to change the hostname
export ROS_HOSTNAME=<hostname>.projectnet.wlan.sdu.dk
export ROS_MASTER_URI=http://<hostname>.projectnet.wlan.sdu.dk:11311


#In the launch file add the machine tag in order to specify which pi the node should run on.
<machine name="pi1" address="rsd-group1-pi1.projectnet.wlan.sdu.dk" user="rsd-group1" env-loader="~/ros_env.sh" password="rsd-group1" />
<machine name="pi2" address="rsd-group1-pi2.projectnet.wlan.sdu.dk" user="rsd-group1" env-loader="~/ros_env.sh" password="rsd-group1" />

#Then add on the node tag which machine it should run on. 
<node name="sick_tim310s01" pkg="sick_tim" type="sick_tim310s01" respawn="false" output="screen" machine="pi2">




