# Structure of report files
* Images be placed in `img/` folder
* All tex files belonging to appendices be placed in `appendices/` folder
* All tex files belonging to preface be placed in `preface/` folder
* All remaining tex files be placed in `src/` folder

# Guide for writing the report

### References and Citations
##### Bibliography
By default the `bibliography.bib` is excluded from the git, so when changes has been made to this file, it must be force added by
```bash
$ git add -f bibliography.bib
``` 

When citing a reference it is simply done inline by `\cite{key}`. 

##### References
Inline references to figures, tables, etc. must follow this standard:

1. The following should be abbreviated when they appear in running text _unless_
they come at the beginning of a sentence: Chap., Sect., Fig.; e.g. The results
are depicted in Fig. 5. Figure 9 reveals that . . . .
**Please note:** Equations should usually be referred to solely by their number in
parentheses: e.g. (14). However, when the reference comes at the beginning of
a sentence, the unabbreviated word “Equation” should be used: e.g. Equation
(14) is very important. However, (15) makes it clear that . . . .
2. If abbreviations of names or concepts are used throughout the text, they
should be defined at first occurrence, e.g. Plurisubharmonic (PSH) Func-
tions, Strong Optimization (SOPT) Problem. **See next section**


### Acronyms and Abbreviations
The package `glossaries` is included in the preamble and is used to automatically format acronyms and abbreviations correct according to the standard.

**Usage**

The acronyms and abbreviations are defined in the `glossary.tex` file, by `\newacronym{ann}{ANN}{artificial neural network}`. Please note that the last entry is always written with small letters.

There's four commands that you should be familiar with:

| Command | Outputs (first use) | Outputs (otherwise) |
| ------- | ------------------- | ------------------- |
| `\gls{ann}` | artificial neural network (ANN) | ANN |
| `\Gls{ann}` | Artificial neural network (ANN) | ANN |
| `\glspl{ann}` | artificial neural networks (ANNs) | ANNs |
| `\Glspl{ann}` | Artificial neural networks (ANNs) | ANNs |

The document is set up to re-state every glossary item again after each chapter, and a complete list of glossary items are output in the preface of the report.

### Italic and Roman Type in Math Mode
* In math mode LATEX treats all letters as though they were mathematical
or physical variables, hence they are typeset as characters of their own in
italics. However, for certain components of formulas, like short texts, this
would be incorrect and therefore coding in roman is required. Roman should
also be used for subscripts and superscripts in formulas where these are
merely labels and not in themselves variables.
* Please ensure that physical units and abbreviations
are always set in roman type. To ensure this use the `\mathrm` command:
`\mathrm{Hz}`.
* Chemical symbols and formulas should be coded for **roman**
* Familiar foreign words and phrases, e.g. et al., a priori, in situ, bremsstrah-
lung, eigenvalues should be coded for **roman**.



