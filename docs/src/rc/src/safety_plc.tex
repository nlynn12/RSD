In every industrial application involving heavy and/or dangerous machinery there must be installed sufficient safety measures to ensure the safety of its operators. This section will therefore present the safety assessment performed whereafter the technical solution is presented. The system diagram of the safety system can be seen in Fig. \ref{fig:rc_safety_system_diagram}.

\begin{figure}[h!]
 \centering
 %\resizebox {\columnwidth} {!} {
 \begin{tikzpicture}[
    >=stealth,
    scale=0.8, every node/.style={scale=0.8}
 ]
 \node[data, minimum width=8cm, align=center, inner xsep=10pt, inner ysep=10pt] at(0,-4.5) (1) {\textbf{Safety PLC} \nodepart{second} \ \\ \ \\ \ \\ \ \\ \ \\ \ \\};
 \node[draw, rectangle, minimum width=3.5cm, minimum height=0.5cm, align=center] at(0,-3.8) (2) {Light Curtain};
 \node[draw, rectangle, minimum width=3.5cm, minimum height=0.5cm, align=center] at(0,-4.6) (3) {3 State Stack Light};
 \node[draw, rectangle, minimum width=3.5cm, minimum height=0.5cm, align=center] at(0,-5.4) (4) {Emergency Button};
 \node[draw, rectangle, minimum width=3.5cm, minimum height=0.5cm, align=center] at(0,-6.2) (5) {Pulse Start Button};
 
 \node[draw, rectangle, minimum width=3.5cm, minimum height=1.5cm, align=center] at(-5,-9) (6) {Robot};
 \node[draw, rectangle, minimum width=3.5cm, minimum height=1.5cm, align=center] at(0,-9) (7) {Frequency Converter};
 \node[draw, rectangle, minimum width=3.5cm, minimum height=1.5cm, align=center] at(5,-9) (8) {Control PLC};
 
 \coordinate (center) at (0,-7.5);
 \draw[very thick] (1) -- (center) node[midway,right] {HW Line};
 \draw[->, very thick] (center) -| (6);
 \draw[->, very thick] (center) -- (7);
 \draw[->, very thick] (center) -| (8);

\end{tikzpicture}
%}
\caption[Illustration of the safety system diagram.]{The figure illustrates the safety system diagram within the robot cell.}
 \label{fig:rc_safety_system_diagram}
\end{figure}

\subsection{Safety Assessment}
Industrial robots are inherently unsafe as they are capable of moving large payloads at high speeds. Many industrial robots are them selves heavy and can do severe damage to both environment and personnel. The robot arm used in this project is no exception and therefore a safety assessment is required to limit these risks.

According to the ISO 12100 standard \cite{dseniso12100} risk reduction can be performed using an iterative method as can be seen in Fig. \ref{fig:riskflowchart}. The risk analysis is performed by evaluating the severeness and probability of risks, by use of the risk matrix shown in Fig. \ref{fig:riskmatrix}.
The steps performed in the safety assessment and the reasoning are as follows:

		\begin{enumerate}\itemsep-3pt
			\item[\textbf{Step 1}] Initial Robot\\
				Inherently unsafe. High Risk %Very Likely, Significant
			\item[\textbf{Step 2}] Tool mount\\
				Risk increased due to longer reach and sharp tool. Risk High. % Very Likely, Severe
			\item[\textbf{Step 3}] Added fence\\
				Added fence preventing access to robot leaving one side open. Risk High. % Likely, Severe,
			\item[\textbf{Step 4}] Added light curtain\\
				The light curtain will emergency stop the robot when disrupted. Risk Med. High. % Possible, Severe
				
			\item[\textbf{Step 5}] Added visual indicators\\
				The light curtain can be bypassed by crawling under or over it. A tower light, and a chain is added for indicating working state. Risk Med. High. % Unlikely, Severe
			
		\end{enumerate}

		% Define block styles
\tikzstyle{decision} = [diamond, draw, text width=4.5em, text badly centered, node distance=3cm, inner sep=0pt]
\tikzstyle{block} = [rectangle, draw, text width=8em, text centered, minimum height=4em]
\tikzstyle{line} = [draw, -latex']
\tikzstyle{cloud} = [draw, ellipse, node distance=3cm, minimum height=2em]
    


		\begin{figure}[h!]
			\centering
			\begin{tikzpicture}[node distance = 2cm, auto,scale=0.7, every node/.style={scale=0.7}]
			    % Place nodes
			    \node [cloud] (init) {Start};
			    \node [block, below= 0.5cm of init] (limit) {Determination\\of the limits of\\ the machinery}; 
			    \node [block, below= 0.5cm of limit] (hazard) {Hazard\\identification}; 
			    \node [block, below= 0.5cm of hazard] (riskest) {Risk estimation};
			    \node [block, below= 0.5cm of riskest] (riskeva) {Risk evaluation};
			    \node [decision, below= 1cm of riskeva] (decide) {Has the risk been adequately reduced?};
			    \node [cloud, right= 1cm of decide] (end) {End};
			    \node [block, dashed, below = 1cm of decide] (riskred) {Risk reduction\\(see ISO 12100)};
			    
			    % Draw edges
			    \path [line] (init) -- (limit);
			    \path [line] (limit) -- (hazard);
			    \path [line] (hazard) -- (riskest);
			    \path [line] (riskest) -- (riskeva);
			    \path [line] (riskeva) -- (decide);
			    \path [line] (decide) -- node {yes}(end);
			    \path [line] (decide) -- node {no}(riskred);
			    \path [line, dashed] (riskred) -- ++(-3,0) |- (limit);
			\end{tikzpicture}
			\caption{ISO 12100 flowchart for risk reduction.}
			\label{fig:riskflowchart}
		\end{figure}
		


		\begin{figure}[h!]
			\centering
			\includegraphics[width=0.8\textwidth]{img/rc/riskmatrix}
			\caption{Risk assessment matrix.}
			\label{fig:riskmatrix}
		\end{figure}		
		



\subsection{Safety Installation}
The safety installation consist of the following parts:
	\begin{itemize}\itemsep-3pt
	\item SICK FLEXI soft CPU1 (PLC).
	\item SICK FLEXI soft XTIO (Port expander).
	\item SICK M4000 standard A/P (Light curtain).
	\item ISO standard emergency button.
	\item Pulse start button.
	\item 3-state Stack light.
	\end{itemize}
	
The SICK \gls{cpu} is a central component of the installation and controls the behavior of the system. 
An overview of the connected inputs and outputs can be seen in Fig. \ref{fig:safetydigram}.


The safety cpu uses simple logic signals (0-24V) to trigger and reset the emergency signal \verb|Q4| which is set high (24V) when the robot cell is operational.
If either the E-stop, pin \verb|I5|, or the light curtain, \verb|I7|, \verb|I8|, connections goes low it will trigger the emergency stop hence setting the "safety" signal, pin \verb|Q4|, low. 
Using active low will ensure that an unintentional open circuit in the safety setup triggers the emergency stop. 
The emergency-stop signal is connected to the robot's and the frequency converter's external stop and to the PLC all discussed in this chapter. The red indicator of the stack light will indicate an emergency stop.


To reset the system the release conditions must be met i.e. releasing the stop button or resetting the light curtain. 
The light-curtain is reset by a pulse signal to pin 1 (\cite{sickm4000} p. 51). 
This pulse signal is provided via the start button by the operator. 
When the release conditions are met the yellow indicator of the stack light is flashing.

To restart the application the start button must be pressed after the release conditions have been met. 
The green light of the indicator will indicate an active state of the cell. 
Note that the machinery will not necessarily be running when the green light is on but might be on standby. 
The light coding has been chosen according to the DS/EN 60073 standard (\cite{dsen60073}, Table 8), regarding state of equipment. 
The same standard also specifies a coding for "safety of persons" which was not seen fit to the application. 
The reasons for not choosing "safety of persons" is listed below:

	\begin{itemize}\itemsep-3pt
	\item The robot should never be running under unsafe conditions. Hence the need for physical and optical fences.
	\item The stack light is mounted outside the robot cell ie. indicating to people who are inherently safe. 
	\item The red light would be on when the robot is running which according to DS/EN 60073 requires immediate response from the operator (\cite{dsen60073}, Table 6) and for others to leave the area. This is clearly not the case for people outside the cell.
	\end{itemize}



A block diagram of the logic behaviour of the safety CPU can be seen in Fig. \ref{fig:safetylogic}.

	\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\textwidth]{img/rc/safetyDiagram}
	\caption{Overview of electronic safety installation.s}
	\label{fig:safetydigram}
	\end{figure}

	\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{img/rc/safetyLogic}
	\caption{Block diagram of logic implemented on a SICK CPU.}
	\label{fig:safetylogic}
	\end{figure}
