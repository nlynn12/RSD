To pick the bricks from the conveyor belt the Kuka KR6 R700 sixx robot arm and the Kuka KRC C4 compactrobot controller are used.
The Kuka KR6 R700 sixx will henceforth be mentioned as ``robot arm'' and the Kuka KRC C4 compact as ``robot controller''. 
The robot arm is placed in the  workcell as showed on Fig \ref{fig:robotcell}.


\begin{figure}[h]
\centering
\begin{subfigure}[t]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/rc/DSC_4631_mod}
        \caption{The real robot workcell.}
        \label{fig:real_wc}
    \end{subfigure}
    \qquad
    \begin{subfigure}[t]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/rc/wc_rw}
        \caption{The workcell used for collision checking, kinematics and path planning.}
        \label{fig:rw_wc}
    \end{subfigure}
\caption{Robot workcell.}
\label{fig:robotcell}
\end{figure}


The robot controller was delivered with pre installed \gls{rsi} 3.3. 
The RSI  interface enables direct configuration of the sensors via \gls{xml} messages send via Ethernet \gls{udp}. 
A standard \gls{udp} server was setup on the PC site to receive and answer the \gls{rsi} messages send from the robot controller.

The \gls{rsi} is configured by modifying 3 files from in the standard "Ethernet" example given in the \gls{rsi} 3.3 datasheet. 
The first file is the .RSI file which is modified in \gls{rsi} visual. 
The \gls{rsi} visual can only be used on windows and enables the design of a ladder diagram using various components. 
This project uses a Ethernet block for communication and absolute coordinate block to control the robot and set up limits for the robot. 
The important parameters for the Ethernet is the precision and the timeout. 
A precision of 3 is chosen to be able to get a proper resolution on the joint configurations. 
The "Timeout" parameter is set to 100 which specifies that 100 unanswered messages in a row will terminate the Ethernet connection and thereby the robot program. 
This relative high number was chosen to ensure stability as delays in the program can occur. 
These delays have shown to be non critical and thereby more tolerable than the entire program terminating. 
The ladder diagram is displayed on figure \ref{fig:RSIVisual}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{img/rc/RSIVisual}
	\caption{Ladder diagram for RSI connections.}
	\label{fig:RSIVisual}
\end{figure}

The second file which had to be modified was the config \gls{xml} file. 
This file specifies the IP address and port of the external \gls{udp} server and communication protocol. 
The \gls{ip} and port is set to \verb|192.168.100.50:49000|. 
The communication protocol is defined by linking the objects specified in the ladder diagram with a tag in the \gls{xml} file. 
These tags can then be found on the PC site and their values can be read. 
The same concept is applied for sending messages to the robot controller. 
Two standard entries are made in the \gls{xml} protocol. 
The first one is the \verb|DEF_AIPos| which is and incremental counter linked to the message.
The AIPos is unique for the message has to be answered within 4 ms for \verb|IPO_FAST| mode or 12 ms for \verb|IPO| mode. 
The second one is the \verb|DEF_Delay| which keeps track of the number of Ethernet delays in a row.

The third file is the \verb|.src| file which is the main robot program. 
It starts by moving the robot arm to a specified home position in a point to point movement and then starts the \gls{rsi} context which is specified in the \verb|.rsi| file. 
The program then starts the \gls{rsi} execution with with absolute robot movements and in \verb|IPO_FAST| mode. 
\verb|IPO_FAST| mode was chosen as IPO mode could not function when the gripper connections were added.

With these files the \gls{rsi} is defined. They are placed in the \gls{rsi} folder on the robot controller as specified in the setup guide. 
The robot arm will move between the joint configurations in the 4ms time slot given by the \gls{udp} communication. 
Therefore the joint configurations have to be relatively close to not violate torque/speed/acceleration limits of the robot arm. 


\subsection{Gripper} \label{subsection:gripper}
To grasp the LEGO\reg bricks the HGPC-16A parallel pneumatic gripper from Festo is used. 
The HGPC-16A parallel pneumatic gripper will henceforth be mentioned as ``gripper''. 
The gripper is mounted on the robot sixth axis by a custom designed flange, as seen in Fig. \ref{fig:rc_flange_gripper_fingers}.

\begin{figure}[h!]
 \centering
 \includegraphics[width=0.5\linewidth]{img/rc/flangev3gripperfingerv2}
 \caption[Illustration of custom designed flange and gripper fingers]{This figure depicts the custom designed flange and gripper fingers. 
 The design of the flange is inspired from the Danish Technological Institute.}\label{fig:rc_flange_gripper_fingers}
\end{figure}

The robot arm has 3 built in 5/2 valves, which can be used to control the direction of the air pressure to the gripper. 
The air is connected at the base of the robot and led through the robot arm to the internal 5/2 valves. 
The output of this valve is connected to the gripper as specified in the datasheet.

The 5/2 valves were configured and connected to the desired I/O ports through Work Visual. 
In RSI Visual the digital inputs and outputs for the gripper are connected to the Ethernet block so it can be controlled from the PC. 
In this case digital output 7 and 10 are used to control the gripper. 
The output from the Ethernet connection to the gripper is a bit, which is connected to digital output 7 and inverted before it is linked to digital output 10. 
This is done so the gripper can be controlled with one bit as the 2 digital outputs have to be opposite to open or close the gripper.


 

  
\subsection{Kuka Interface}
To keep a modular interface between the robot and the rest of the higher level system a "Kuka interface" was created and run on a PC.
This Kuka interface keeps the communication alive with the robot controller by sending and receiving data every 4ms.
The Kuka interface also receives joint configurations from the higher level system. 
These joint configurations are then adjusted into smaller steps which are sent to the robot so no constraints are violated.

The connection to the higher level system is done via \gls{tcp}. 
The Kuka interface creates a \gls{tcp} socket and follows a simple protocol to set and get joint configuration and gripper status.
The higher level system is then responsible for maintaining a \gls{ros} interface that forwards the commands to the Kuka interface.

The basis for the Kuka interface was provided in the course, but this was modified and adjusted to meet the requirements of the project.
This is in terms of safety, monitoring and functionality where the interface was expanded.
The add-ons include monitoring if connection to the robot is established or if it suddenly has been disrupted.
Thus the user can immediately be informed if the connection is lost and the higher level program execution can be safely stopped.
Functionality to maintain a \gls{ros} topic of the robots current configuration was added together with control of the gripper actuation.


\subsection{Robot Motion Planning}
In this application it is required to move the robot to poses that are generated by a camera hence the need for an \gls{ik} solver for finding suitable robot configurations is needed. 
Furthermore a robot path planner is needed to find feasible paths to the various poses avoiding collisions on the way. 
The \verb|C++| library used to implement these functionalities is RobWork \cite{joergensen10}, developed at \gls{sdu}.

For inverse kinematics a simple Jacobian \gls{ik} solver has been applied. 
To expand the possible solutions it is seeded with a number of random configurations to find other potentially better solutions.
The different starting configurations of the IK solver were calculated on maximum available number of threads on the \gls{cpu}. 
The resultant list of \gls{ik} solutions is then expanded with the ambiguities of each solution due to some of the joints being able to rotate more than 360 degrees.
From the candidate list of solutions the configuration closest to the current one is chosen.

The motion planner is a simple RRTConnect algorithm. 
RRTConnect was chosen because the planner is needed only when moving the last segment from above the conveyor belt to the approach and further to the pick position which can often be done directly in configuration space. 
The rest of the motions can be defined via way-points that are guaranteed to be collision free by the integrator when defining the action list. 
The action list used can bee seen in listing \ref{app:lst_sorting_bricks} and \ref{app:lst_deliver_bricks}, in App. \ref{app:actionlib_seq}, where the actions \verb|Move2Brick| and \verb|Move2Point| are using the motion planner. 
The action list is explained in further details in Sec. \ref{sec:process_control}. 
The motion planner is made available through services in a \gls{ros} node.


\subsection{LEGO\reg Brick Clearance and Grasp Planning}
When grasping a LEGO\reg brick it is of high importance to prevent collisions between the gripper fingers and nearby bricks that lie close to the target.
To do this a set of simple algorithms was developed to sort bricks depending on clearance and decide on how to pick each brick.

It was decided to grasp all bricks at their centre, but keep the option of rotation open.
That is the long LEGO\reg bricks (two-by-four and two-by-six) can be grasped from two angles, with the fingers perpendicular to the brick and its corresponding 180 degrees rotated counter part.
In case of the square two-by-two brick, it can be grasped from all four directions, rotations of multiples of 90 degrees.
The grasping algorithm will hence, when calculating how to grasp a brick, take all the possible options into account and choose the grasp that requires least movement in joint-space.

Knowing that all bricks only will get grasped at its centre also makes it easier when it comes to compute grasp clearance.
The clearance algorithm is based on bounding spheres.
Bounding spheres was chosen due to its simplicity in implementation.
The size of every bounding sphere is already known and only depending on which of the three types of LEGO\reg brick it is.
Furthermore the centre of the LEGO\reg brick, which is also the centre of the bounding sphere, is directly given by the camera.
The clearance can hence quickly be calculated considering the distance between two LEGO\reg bricks in 3D space.
Using bounding spheres to approximate rectangles does cause big false positive regions, especially when compared to Object Aligned Bounding Boxes.
For this project they where however still used since they ensure that the clearance needed is present and a 100 percent pick rate of all packable bricks is not needed.

All bricks in the camera view are sorted depending on clearance.
The bricks with to little clearance or of a brick type that is not needed in the overall order are then removed from the list.
The best brick, in term of clearance, is then passed on to the grasp planner and grasped by the robot.

