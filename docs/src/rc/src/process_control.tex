The process control node is pure software, why a state diagram is presented instead of a system diagram. This state diagram is depicted in Fig. \ref{fig:rc_process_control_state}, where the state transitions are controlled by the \gls{mes} system. As it can be seen from Fig. \ref{fig:rc_process_control_state} the number of states are significantly reduced compared to the number present in the robot cell's \gls{mes} client.

\begin{figure}[h!]
\centering
\vspace*{2cm}
\begin{tikzpicture}[
->,>=stealth',
shorten >=1pt,
auto,
node distance=5cm,
transform shape,
scale=0.9, every node/.style={scale=0.9}
]

  \node[state,initial,accepting,minimum size=2cm] (1) {Idle};
  \node[state,minimum size=2cm, align=center] (2) [below left of=1] {Deliver\\Bricks};
  \node[state,minimum size=2cm, align=center] (3) [below right of=1] {Sort\\Bricks};

\begin{pgfinterruptboundingbox}
  \path[every node/.style={font=\sffamily\scriptsize}]

	(1)	edge [bend left] node[right] {MESclient.state == SortBricks} (3)
		edge [loop above] node [above] {} (1)
    (2)	edge [bend left] node[left] {deliver.isDone() == True} (1)
		edge [loop left] node[left] {deliver.isDone() == False} (2)
	(3) edge [bend left] node[below,align=center] {order.isComplete() == True \&\& \\MESclient.state == DeliverBricks} (2)
		edge [loop right] node [right] {order.isComplete() == False} (3);
\end{pgfinterruptboundingbox}
\end{tikzpicture}
 \vspace*{36pt}
 \caption[Illustration of the process control node's state diagram]{This figure illustrates the state diagram of the process control node, in which the state transitions are controlled by the \gls{mes} system.}
 \label{fig:rc_process_control_state}
\end{figure}

The process control node is designed in such a manner that the actual process flow in the active states; Sort Bricks and Deliver Bricks, can be changed easily without the need for recompiling the source code. We denote the controlling software ActionLib, for action library, and we dedicate the idea to Danish Technological Institute, who refined the idea of skill based execution of robot tasks \cite{andersen2014definition}. In our ActionLib we do not differ from primitives and skills, why we simply denote each entry an action.

The core idea behind this is that the operator must be able to change the behaviour of the robot cell, in such a manner that no programming skills is required. We have achieved a proof of concept of this by parsing a \gls{ros} configuration file, also known as a YAML file, in which a sequence of actions is defined, to a \verb|C++| program converting the YAML to actions. This list of actions then forms the complete behaviour of the robot cell, and can be changed with little to no programming skills in the current revision. Each action has two attributes, which we denote the parameters and blocking, describing the input parameters for the action and describing if the action should run to completion before the next action is called or not. A complete list of the ActionLib's actions, its behaviour and required input parameters are given in Tab. \ref{tab:actionlib_actions}

\begin{sidewaystable}[h!]
 \centering
 \begin{tabular}{lp{8cm}p{8cm}}
  \toprule
  \textbf{Action}	& \textbf{Input Parameter(s)} & \textbf{Behaviour}\\
  \midrule
  \verb|Move2Pose| 	& A 6D vector; $\bm{T}_{\mathrm{tcp}}$ & Solves the IK problem to end and path-plans from $\bm{Q}_{\mathrm{curr}}$ to $\bm{Q}_{\mathrm{end}}$.\\
  \verb|Move2Point| 	& A 6D vector; $\bm{Q}_{\mathrm{end}}$ & Path-plans from $\bm{Q}_{\mathrm{curr}}$ to $\bm{Q}_{\mathrm{end}}$  \\
  \verb|MovePoint2Pose| & Two 6D vectors; $\bm{Q}_{\mathrm{start}}$ and $\bm{T}_{\mathrm{tcp}}$ & Solves the IK problem and path-plans $\bm{Q}_{\mathrm{start}}$ to $\bm{Q}_{\mathrm{end}}$.\\
  \verb|MoveP2P| 	& Two 6D vectors; $\bm{Q}_{\mathrm{start}}$ and $\bm{Q}_{\mathrm{end}}$ & Path plans from $\bm{Q}_{\mathrm{start}}$ to $\bm{Q}_{\mathrm{end}}$. \\
  \verb|MoveJ| 		& A 6D vector; $\bm{Q}_{\mathrm{end}}$  &  Moves linear (C-space) from $\bm{Q}_{\mathrm{curr}}$ to $\bm{Q}_{\mathrm{end}}$.\\
  \verb|Gripper| 	& Boolean; Open: 0, Close: 1 & Opens or closes the gripper.\\
  \verb|ConveyorBelt| 	& Integer; Stop: 0 Forward: 1-3, Backwards: 4-6 & Controls conveyor belt.\\
  \verb|Wait| 		& Double; Time & Will wait for x amount of time. \\
  \verb|DetectBricks| 	& N/A & Searches for a LEGO\reg brick of interest and stops the conveyor belt if found.\\
  \verb|Move2Brick| 	& A 6D vector; $\bm{T}_{\mathrm{brick}}$ & Plans to a grasp pose of a LEGO\reg brick.\\
  \bottomrule
 \end{tabular}
 \caption[Table showing the ActionLib's implemented actions]{The table shows the complete list of actions implemented in the ActionLib, where input parameters and the individual actions' behaviours are described.}
 \label{tab:actionlib_actions}
\end{sidewaystable}

\subsection{Creation of Actions from ROS Configuration Files}
The creation of actions is done by parsing the YAML file to a \verb|C++| program which contains the actual definition of the behaviour for each possible action. The raw data in the YAML file is simply an array of entries where each entry has the following three attributes; the type of the action, a list of input parameters, and a boolean telling the controlling peace of software if the action should run to completion or not. These attributes are the only required to instantiate each action.

The creation of each action is done by reading the YAML file iteratively, parsing the three attributes to the constructor of the \verb|Action| class and pushing the resulting instance onto a list of type \verb|Action|s, as seen in Fig. \ref{fig:rc_actionlib_action_executor_function}.

Each instance of the \verb|Action| class is in need of an actual execution function, that defines its full behaviour. The behaviours of the actions naturally differs from each other, why each action must have their own executor function. This is achieved by defining the executor function for each action with their respective names, and linking them to a common executor function inside the \verb|Action| class, at instantiation. This idea is depicted in Fig. \ref{fig:rc_actionlib_action_executor_function} where an instance of the action \verb|MoveJ| is being created.

\begin{figure}[h!]
 \centering
 \begin{tikzpicture}[
    >=stealth,
    scale=0.8, every node/.style={scale=0.8}
 ]
 
 \node[data, minimum width=4.5cm, minimum height=5cm, align=left, inner xsep=10pt, inner ysep=10pt] at(0,0.3) (1) {
    \textbf{YAML File} \nodepart{second}
    \verb|Sequence:|\\
     \ \ \ \ \ \ \ $\vdots$\\
    \verb|- Action:|\\
    \verb|  Type: 'MoveJ'|\\
    \verb|  Parameters: ['0.00', '-2.006', '1.57', '0.00', '1.92', '0.87']|\\
    \verb|  Blocking: 0|\\
    \ \ \ \ \ \ \ $\vdots$
 };
 
 
 \node[data, minimum width=4.5cm, minimum height=5cm, align=center, inner xsep=10pt, inner ysep=10pt] at(-3.12,-5.3) (7) {\textbf{List of Action Executor Functions} \nodepart{second} \ \\ \ \\ \ \\\ \\\ \\ \ \\ \vspace{-0.2cm}\hspace{1.9cm}$\vdots$};
 \node[draw, rectangle, minimum width=3.5cm, minimum height=0.5cm, align=center] at(-4.5,-4.5) (8) {Type};
 %\node[draw, rectangle, minimum width=3.5cm, minimum height=0.5cm, align=center] at(-3.12,-5.3) (4) {Parameters};
 \node[draw, rectangle, minimum width=3.5cm, minimum height=0.5cm, align=center] at(-2.12,-6.1) (9) {\verb|MoveJ()|};
 
 \node[data, minimum width=4.5cm, minimum height=5cm, align=center, inner xsep=10pt, inner ysep=10pt] at(4.46,-5.3) (2) {\textbf{Action Instance} \nodepart{second} \ \\ \ \\ \ \\\ \\\ \\ \ \\};
 \node[draw, rectangle, minimum width=3.5cm, minimum height=0.5cm, align=center] at(4.46,-4.5) (3) {Type};
 \node[draw, rectangle, minimum width=3.5cm, minimum height=0.5cm, align=center] at(4.46,-5.3) (4) {Parameters};
 \node[draw, rectangle, minimum width=3.5cm, minimum height=0.5cm, align=center] at(4.46,-6.1) (6) {Executor Function};
 \node[draw, rectangle, minimum width=3.5cm, minimum height=0.5cm, align=center] at(4.46,-6.9) (10) {Blocking};
 
 \draw (-6.2,-0.15) -- (-7.5,-0.15);
 \draw[->] (-7.5,-0.15) |- (8);
 \draw (-7.5,-2.7) -- (1.2,-2.7);
 \draw[->] (1.2,-2.7) |- (3);
 
 \draw[dashed] (-6,-5.2) -- (-1, -5.2);
 \draw[->] (8) -| (9);
 
 \draw[->] (9) -- (6) node[left=2.45cm, above] {\scriptsize{\verb|std::bind()|}};
 
 \draw[->] (6.5, -0.65) -- (8,-0.65) |- (4);
 
 \draw[->] (-3.5, -1.15) -- (7.5,-1.15) |- (10);
 
 \node[data, minimum width=9cm, minimum height=5cm, align=left, inner xsep=10pt, inner ysep=10pt] at(-2.25,-11) (11) {\textbf{Process Sequence} \nodepart{second} \ \\ \ \\ \ \\ \ \\ \ \\\ \\ \ \\\vspace{-0.2cm}$\vdots$};
 \node[draw, rectangle, minimum width=3.5cm, minimum height=0.5cm, align=center] at(-2.25,-10) (12) {\verb|Gripper|};
 \node[draw, rectangle, minimum width=3.5cm, minimum height=0.5cm, align=center] at(-2.25,-10.8) (13) {\verb|DetectBricks|};
 \node[draw, rectangle, minimum width=3.5cm, minimum height=0.5cm, align=center] at(-2.25,-11.6) (14) {\verb|Move2Brick|};
 \node[draw, rectangle, minimum width=3.5cm, minimum height=0.5cm, align=center] at(-2.25,-12.4) (15) {\verb|MoveJ|};
 
 \draw[->] (2) |- (15);
  

\end{tikzpicture}
 \caption[Illustration of construction of actions in ActionLib]{This figure illustrates the creation of an arbitrary action from a \gls{ros} configuration file (YAML), where the three attributes from the YAML file is parsed to the control software of the ActionLib. Furthermore it illustrates the linking of the action's executor function (in this case: \lstinline|MoveJ()|) to the common Action class function}
 \label{fig:rc_actionlib_action_executor_function}
\end{figure}

\subsection{Communication with Peripherals through ROS}
All actions' behaviours are heavily dependent on the \gls{ros} environment where topics and services are used to perform manipulation with external peripherals like the gripper, robot, etc. Instead of including all these dependencies in the \verb|Action| class, a singleton class is created denoted the communication handler, which each individual action instance can use to communicate through \gls{ros}. This ensures that if multiple actions uses one or more common \gls{ros} services or topics, these will only be instantiated once. This is an important part of the implementation as to many instantiations of the same topics would potentially overflow the system with messages, which is unwanted.
