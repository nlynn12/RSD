To detect the LEGO\reg bricks an Omron FQ2-S1 smart camera is used. 
The camera is interfaced via Omron's Touch Finder software, where the communication protocol is set to \gls{udp} and the \gls{ip} and port is set as desired.

\noindent The camera system is divided into two parts:
\begin{enumerate}
 \item A detection that locates the bricks in the view using the Omron Touch Finder software, and
 \item A calibration mapping the spatial coordinates $[u,v]$ and angle of the brick in the image to the \gls{3d} position and rotation.
\end{enumerate}




\subsection{Lego Brick Detection}
The camera's built-in imaging software is used to detect the bricks in the camera view.
Each of the three types of LEGO\reg bricks are detected and labelled such that they can be picked by 
the robot, depending on the order received from the \gls{mes} server.
For this to be possible a scene with one inspection item is created for each of the three types of bricks.
This is needed because the camera only supports one inspection item for each scene captured by the camera. 
To create more inspection items in the same scene a more advanced version of the camera is needed. 
In the sense of this smart camera, a scene refers to a specified sequence of imaging processing (image adjustment, inspection, etc.) used on a given image (see the manual for detailed description).
The three scenes used to detect the bricks follow the same processing scheme, but with different parameters.
At runtime, the camera is then set to switch between the three scenes in order to detect all three different bricks. 

First the image is preprocessed.
The image is first linearly corrected to account for some of the skew.
Afterwards the image is mapped to \gls{hsv} space and filtered for the respective colour of the brick (blue, red or yellow).
When this is done edge detection is performed on the image and the image is eroded and dilated as needed to remove the background noise.
Edge detection was performed to get a set of good feature values to use in the further detection process.
Lastly the inspection item Shape Search is used to locate all the bricks of the respective type in the image.
This algorithm searches for the shape of the type of brick and matches the outer edge of each brick to the ones detected.
Thereafter the output of the shape search is send to the connected computer for further processing.
The parameters are the $[u,v]$ position of the LEGO\reg bricks, the angle of rotation of the brick and the correlation, that is the quality of the shape match given as a percentage.
The output is from there converted into a point and rotation in \gls{3d} space and used for further grasp-planning etc.


\subsection{Robot to Camera Calibration}
When a brick coordinate is found it is sent to the connected PC via \gls{udp} where a transformation from the robot base to the brick is calculated from the image coordinates. 
This transformation is calculated by mapping the coordinates given by the camera to a plane in \gls{3d}. 
This becomes a mapping from the pixel coordinates $u,v$ and rotation to a \gls{3d} translation and rotation. 
This transformation is then published on a \gls{ros} topic.

The mapping of the \gls{2d} coordinates to a \gls{3d} transformation is performed in a two step way.
First the $[u,v]$ coordinates are used to be mapped to a \gls{3d} transform (translation only) $\bm{P}_r$ and standard rotation $\bm{R}_o$ which aligns the LEGO\reg brick with the plane.
Secondly the rotation in \gls{3d} is modified by applying the rotation of the brick given by the camera as a rotation of the LEGO\reg brick around the normal of the plane of the conveyor belt.

To find the \gls{2d} to \gls{3d} mapping a set of \gls{2d} camera coordinates was recorded together with the corresponding configuration of the robot.
The robot configurations were used to calculate the tcp position of the robot which are equivalent to $\bm{P}_r$, considering the translation only.
Mathematically the two coordinate systems can then be related using equation \ref{eq:original_mapping}.

\begin{equation}
\bm{P}_r =
\bm{P}_o
+ u \cdot
\bm{P}_u
+ v \cdot
\bm{P}_v
\label{eq:original_mapping}
\end{equation}

Where $\bm{P}_o$ is the position in \gls{3d} that corresponds to the \gls{2d} $[0,0]$ position of the camera. 
$\bm{P}_u$ and $\bm{P}_v$ is the direction in \gls{3d} space corresponding to the axis direction of the $u$ and $v$ axis of the camera on the \gls{3d} plane to which the LEGO\reg bricks are mapped.

For equation \ref{eq:original_mapping} to work, it is assumed that the skew is negligible small.
This is the case when the camera is parallel to the plane or when skew is otherwise corrected for.
In the current implementation the skew is largely reduced by the camera software using the linear correction before any vision algorithms are performed.
The slight angle of the camera to conveyor is hence accounted for.
However, the algorithm does not account for radial distortion.
This is however not a problem since the camera has a narrow view which automatically ensures less distortion.

In order to find the \gls{3d} vectors needed for the mapping, equation \ref{eq:original_mapping} is rearranged to give the system of linear equations in matrix form as seen in equation \ref{eq:calibration}.

\begin{eqnarray}
\bm{P}_r =
\left[
\begin{tabular}{ccc}
 $\bm{P}_o$ & $\bm{P}_u$ & $\bm{P}_v$ 
\end{tabular}
\right]
\cdot
\left[
\begin{tabular}{c}
 1 \\
 $u$ \\
 $v$ \\
\end{tabular}
\right]
\\
\left[
\begin{tabular}{ccc}
 $\bm{P}_o$ & $\bm{P}_u$ & $\bm{P}_v$ 
\end{tabular}
\right]
=
\bm{P}_r \cdot
\left[
\begin{tabular}{c}
 1 \\
 $u$ \\
 $v$ \\
\end{tabular}
\right]^+
\label{eq:calibration}
\end{eqnarray}

Where $\bm{x}^+$ denotes the Moore-Penrose pseudo-inverse of the matrix $\bm{x}$.
The least squares solution is then found stacking the measured \gls{2d} and \gls{3d} coordinates, $[u,v]$ and $\bm{P}_r$ respectively.

Given the matrix for mapping a \gls{2d} to a \gls{3d} point in space, the rotation of such can also be found.
In order to find the rotation $\bm{R}_o$ that aligns the LEGO\reg brick with the \gls{3d} plane, it was decided to arrange the rotation such that the $z$-axis of the LEGO\reg brick goes into the conveyor and the $x$- and $y$-axis go along the $u$- and $v$-axis of the camera respectively.
The \gls{3d} rotation matrix is then given by equation \ref{eq:base_rotation}.
The decision on how to arrange the coordinate axis with respect to a LEGO\reg brick was purely done for convenience and simplicity.
The coordinate system of a LEGO\reg brick is, using this convention, aligned in the same way as the frame of the gripper mounted on the robot and hence simplifies the grasp planning.

\begin{equation}
\bm{R}_o =
\left[
\begin{tabular}{ccc}
$\widehat{\bm{P}_u}$ & $\widehat{\bm{P}_v}$ & $\widehat{\bm{P}_u \times \bm{P}_v}$
\end{tabular}
\right]
\label{eq:base_rotation}
\end{equation}

In order to find the actual rotation of the located LEGO\reg brick, the final rotation of the LEGO\reg brick can be found by multiplying $\bm{R}_o$ with the rotation of the LEGO\reg brick around the $z$-axis, $\bm{R}_z$.
The exact angle by which to rotate the LEGO\reg brick is given by the cameras Shape Search algorithm.
The cameras Shape Search algorithm is thought the LEGO\reg bricks shape such that it aligns with the base pose of the predefined base rotation.
Thus the rotation from the camera can be plugged directly into the algorithm.

Finally the \gls{3d} transform,  $\bm{T}_{\mathrm{LEGO}}$, for each LEGO\reg brick can be found using equation \ref{eq:lego_trasnform}.

\begin{equation}
\bm{T}_{\mathrm{LEGO}} = 
\left[
\begin{tabular}{c|c}
$\bm{R}_o \cdot \bm{R}_z$ & $\bm{P}_r$ \\ \hline
0 0 0 & 1
\end{tabular}
\right]
\label{eq:lego_trasnform}
\end{equation}


\subsubsection{Calibration Performance}

The calibration of the camera to the robot was performed using 20 uniformly distributed points in the camera view.

Figure \ref{fig:cameraCoordsDist} shows the $[u,v]$ coordinates of the brick poses.

\begin{figure}[h]
\centering
\includegraphics[width=0.8 \linewidth]{img/rc/uv_calib_points}
 \caption{Distribution of the samples in the cameras view.}
 \label{fig:cameraCoordsDist}
\end{figure}



To find the error of the mapping from camera coordinates to a point in \gls{3d} each of the measured \gls{2d} points was mapped to \gls{3d} using the found mapping.
The euclidean difference between the actual pose calculated from the robot and the result of the mapping from \gls{2d} to \gls{3d} was found.
This re-projection error was then noted down and visualised in figure \ref{fig:histReproError} and \ref{fig:cameraReproError}.
The mean error was also found to be $0.00105791\ m$ and the maximum error $0.00197753\ m$.

\begin{figure}[h]
\centering
\includegraphics[width=0.8 \linewidth]{img/rc/hist_repro_error}
 \caption{Histogram of re-projection error in meters.}
 \label{fig:histReproError}
\end{figure}

Figure \ref{fig:cameraReproError} shows the error of each point (on the $[u,v]$ plane) where a ten-thousandth of the radius of the circle corresponds to the re-projection error in meters.

\begin{figure}[h]
\centering
\includegraphics[width=0.8 \linewidth]{img/rc/uv_repro_error}
 \caption[Re-projection error of each coordinate in the camera frame]{Re-projection error of each coordinate in the camera frame. A ten-thousandth of the radius of the circle corresponds to the re-projection error in meters.}
 \label{fig:cameraReproError}
\end{figure}

It is seen in figure \ref{fig:histReproError} that the error is not normally distributed around the fitted plane.
However, considering figure \ref{fig:cameraReproError} then the absence of systematic error confirms that the model used should cover the variation presented in the data.
The model used to perform the mapping was, considering this and the low error, deemed good enough to be used for the final system.



