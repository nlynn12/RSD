The \gls{hmi} is build as a simple, easy to use, web interface, and is created as a \gls{spa} using AngularJS \cite{angularjs}. The \gls{hmi} is packaged in a \gls{ros} node which launches its self and its dependent package called roswww \cite{roswww}. This package launches an \gls{http} server which serves the \gls{spa} \gls{hmi}. Please note that the roswww package from the \gls{ros} repository is yet only available for the Indigo version of \gls{ros}, why it has been cloned from the repository and customised to work with the latest \gls{ros} version, Kinetic. The customised package is delivered as a separate package in the \gls{ros} workspace.

When the \gls{http} server serves the \gls{hmi} and a client accesses the server site, a WebSocket from is created between the client and a new server site handled by the rosbridge \cite{rosbridge_suite}. This WebSocket connection is created by use of the roslibjs \cite{roslibjs} library, which is a core JavaScript library. Due to the power of the WebSocket protocol, messages to and from the \gls{hmi} does not suffer from latency otherwise caused by the nature of the \gls{http} protocol.

In addition to the abovementioned dependencies, the \gls{hmi} is dependent on the following:

\begin{itemize}\itemsep-3pt
 \item nya-bs-select
 \item angular-ripple
 \item angular-animate
 \item font-awesome
 \item ngDialog
 \item Google Web Fonts
 \item underscoreJS
 \item bootstrap
 \item ngStorage
 \item virtualjoystick
\end{itemize}

\section{Validation of Dependent ROS Topics and Services}
The \gls{hmi} is dependent on several \gls{ros} topics and services, why the defined behaviour cannot be guaranteed if one or more of the topics or services is missing. Therefore the \gls{hmi} is required to validate that all the dependent services and topics is online. 
\begin{figure}[h!]
 \includegraphics[trim={0 5cm 0 0},clip,width=1.0\linewidth]{img/hmi/hmi_validate_false}
 \caption{Validation of dependent ROS topics and services failed}
 \label{fig:hmi_validate_false}
\end{figure}
\begin{figure}[h!]
 \includegraphics[trim={0 5cm 0 0},clip,width=1.0\linewidth]{img/hmi/hmi_validate_true}
 \caption{Validation of dependent ROS topics and services succeeded}
 \label{fig:hmi_validate_true}
\end{figure}

\noindent All the services and topics that the \gls{hmi} uses are listed in the launch file of the \gls{hmi} \gls{ros} package and put on the parameter server on launch. The \gls{hmi} then uses the \gls{ros} master \gls{api} to retrieve a list of available topics and services. These two lists are then compared and if any of the dependent topics or services is not available the operator will be warned through the \gls{hmi} as shown in Fig. \ref{fig:hmi_validate_false}. If all services and topics are available the validation succeeds and the dialog will close automatically, hence no external action is required, as depicted in Fig. \ref{fig:hmi_validate_true}.

\section{Overall Equipment Effectiveness}
At the main page of the \gls{hmi} the operator is presented with the \gls{oee}, and its submeasurements; availability, performance and quality, as depicted in Fig. \ref{fig:hmi_oee_time}. Here it is also shown that the operator can choose to see what the \gls{oee} has been over several different time intervals.

\begin{figure}[h!]
 \includegraphics[trim={0 5cm 0 0},clip,width=1.0\linewidth]{img/hmi/hmi_oee_time}
 \caption[OEE presentation in the HMI]{The figure illustrates the presentation of the \gls{oee} parameters, where the operator can choose which interval is relevant}
 \label{fig:hmi_oee_time}
\end{figure}
\begin{figure}[h!]
 \includegraphics[trim={0 5cm 0 0},clip,width=1.0\linewidth]{img/hmi/hmi_oee_settings}
 \caption[OEE settings dialog in the HMI]{The figure illustrates the \gls{oee} settings dialog in which the operator can choose the sample rate and set the target count for the action production process.}
 \label{fig:hmi_oee_settings}
\end{figure}

As of this revision, the \gls{oee} is not linked to the responsible \gls{ros} topics and services, why the values of the performance is not correct. The up and down time, i.e. availability, is measured by client side timers, and of this revision, the operator must manually tell the \gls{hmi} that the system is currently down or running. Furthermore the quality measurement is not supported by the current system as there is no way the system can verify that the order is assembled correctly.

Figure \ref{fig:hmi_oee_settings} depicts the settings dialog for the \gls{oee} where the target count and the sample rate can be chosen. It is furthermore possible to delete the \gls{oee} history.

All parameters and settings of the \gls{oee} are stored in the local storage of the client's browser. Please note that this storage will terminate itself after 48 hours.

\section{Monitoring and Controlling the Robot Cell}
The \gls{hmi} has a dedicated page for the robot cell, which allows the operator to watch parameters specific for the robot cell. It furthermore enables the operator to manually control the robot cell, where the operator can control; the robot in configuration space, the conveyor belt, and the gripper. The manual control is depicted in Fig. \ref{fig:hmi_rc_manual}.
\begin{figure}[h!]
 \includegraphics[trim={0 5cm 0 0},clip,width=1.0\linewidth]{img/hmi/hmi_rc_manual}
 \caption[Manual control of the robot cell in the HMI]{The figure shows the manual control page in which the operator can control the robot, gripper and conveyor.}
 \label{fig:hmi_rc_manual}
\end{figure}
\newpage
\subsection{Error Dialogs Presented to the Operator}
If the safety system is activated by an external event, the operator will be warned by the error dialog shown in Fig. \ref{fig:hmi_rc_emergencystop}. This error dialog is inadmissible and can only be resolved by following the steps presented in the dialog.
\begin{figure}[h!]
 \includegraphics[trim={0 5cm 0 0},clip,width=1.0\linewidth]{img/hmi/hmi_rc_emergencystop}
 \caption[Emergency stop error dialog]{Error dialog presented to the operator when the emergency stop is active}
 \label{fig:hmi_rc_emergencystop}
\end{figure}

If the system looses its connection with the robot it will warn the operator by showing the error dialog in Fig. \ref{fig:hmi_rc_lostconnection}, which also is inadmissible by the operator. To recover from the error the operator must follow the steps presented in the dialog.
\begin{figure}[h!]
 \includegraphics[trim={0 5cm 0 0},clip,width=1.0\linewidth]{img/hmi/hmi_rc_lostconnection}
 \caption[Robot connection lost error dialog]{Error dialog presented to the operator when the system looses the connection to the robot}
 \label{fig:hmi_rc_lostconnection}
\end{figure}
\newpage
\section{Controlling the Mobile Robot}

The \gls{hmi} for the mobile robot consisted solely of a manual control for it. Figure \ref{fig:mr_manualcontrol} shows the virtual joystick designed to control the robot. It works with both touchscreens and regular screens using an external mouse.

It works by measuring the $x$ and $y$ positions of the joystick and comparing them to the previous position to control the linear and angular velocity of the robot. In order to avoid false linear movements when trying to rotate in place, a 5$^\circ$ dead zone is implemented. Finally, when the user releases the joystick the program publishes zero linear and zero angular velocity that forces the robot to stop.

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=0.7\linewidth]{img/hmi/hmi_mr_manualControl}
		\caption{Screenshot of the manual control for the mobile robot.}
		\label{fig:mr_manualcontrol}
	\end{center}
\end{figure}