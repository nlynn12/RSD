The navigation of the mobile robot in the robot cell and drone cell areas is controlled using images obtained with a USB camera and the odometry parameters given by the frobomind controller.
This sensor information is processed in order to obtain the velocity commands needed to navigate.

The navigation along these areas is done following a line drawn on the floor that can be detected using the camera.
This line describes a complete circuit which includes a set of crossroads used to enter on the different docking stations and exit the circuit.
When a crossroad is detected, the robot follows the line a fixed distance to place the wheel axis on top of the crossroad. 
In this position, the robot can turn and continue following the line or dock on the workcell.

By including the odometry to the system it has been possible to perform precise turns and linear movements, that are needed to perform a correct docking on the workcells.

\subsection{Image Processing for Line and Crossroad Detection}
The environment of the robot in this operation area includes a yellow line on the floor that allows the robot to follow a fixed path.
In order to follow the line, the frames from the camera are processed using a binarization on the HSV colour space followed by an analysis of the moments of the items found on the image.


The binarization on the HSV color space of the image can precisely filter the pieces of tape found on the image, making easy the identification of the line to follow.
In this implementation, the position of the camera limits the vision range of the robot making impossible to detect more than one line to follow in the closest visible area in front of the robot.
It can then be assumed that the robot will always follow the line visible on the bottom of the image.

Under this conditions, a portion of the image corresponding with the bottom of the binarized image is extracted and analyzed.
This reduces problems sourced in the wrong detection of crossroads or other elements visible further away from the robot.

Once this binarized area of the image is extracted, it is processed using the find contours algorithm \cite{SuzukiA85} implemented in the openCV library\cite{itseez2015opencv,itseez2014theopencv} that generates a list of the contours of all the items in this image.
The moments of all this items are then processed to find the line on the image and process its offset from the center of the image.
The item with the largest area on the list is defined as the line to follow, and the horizontal position of its centroid is used to correct the robot trajectory and follow the line.
The position error of the robot on the line is defined then as the distance of this centroid from the middle of the image.

This process is represented on Figure \ref{lineFollowerVision}, where the binarization and result of the moment analyses are shown.
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{img/mr/image_processing.png}
	\caption[Image processing for line following navigation of the mobile robot]{Image processing. From left to right the original image, the image after binarization and the selection of the image processed to obtain the centroid of the line are shown. On the top set of images, the line to follow is shown without intersections, in the bottom, a intersection is analyzed to show the difference between the areas of the detected objects, used to detect crossroads. The feedback used to control the robot movement is the deplacement of the center of the line from the center of the image. In some cases, multiple objects can be seen on the image, the one with the largest area is defined as the line to follow.}
	\label{lineFollowerVision}
\end{figure}

To detect intersections, the area of the line obtained using image moments can be used.
As the crossroads will be seen as a big object on the image, a threshold on the area of the object has been set to differentiate between the line and a crossroad.
Doing this has shown a very good performance without adding any extra computation on this node.


Therefore, using a simple, fast and cheap algorithm it is possible to detect a line on the floor and follow it.
This same algorithm has been used in a correction of the robot orientation performed at the beginning of the docking behavior to center the line on the image before going backwards.



\subsection{Control system for line following, turning and docking}
Along the different behaviors defined to navigate in this area, the robot's odometry and the image processing described before have been combined to perform different actions.
This way, the robot processes the images when following the line, but uses only odometry to turn and combines both when docking and to be placed on top of the crossroad.

To control the robot when it is following the line, the same control described in figure \ref{fig:pidwallfollowing} has been used with some modifications.
The input to the system has been changed from a distance to the wall to a distance to the center of the line, being this one obtained using images from a camera as described before.
The parameters for this controller have been tuned accordingly to the system in order to obtain a fast response and minimum offset over the line.

When an intersection is found, the robot continues on the line for a determined distance calculated with the odometry values from Frobomind.
The total traveled distance is controlled using a PID to make a precise approximation to the crossroad.

A similar control system is used to go backwards during the docking.
Defining a new distance to travel from a crossroad to the docking station, the robot is aligned with the line on the floor using the algorithm described in the previous section.
Once the robot is aligned, it starts moving backwards using the same PID controller used to position the robot on the crossroads and travels the defined distance to the dock.
Using the line following algorithm, a small correction of the trajectory of the robot is done, providing a more precise docking. 

In the case of the turns performed on intersections, four different cases where defined: turning $90^\circ, -90^\circ, 180^\circ$ or $-180^\circ$.
This definitions make possible to turn left and right on the intersections, as well as turning complete $180^\circ$ right or left to avoid hitting obstacles placed inside the robot workcell.
In this four cases, a simple PID has been used to control the turns, setting as a reference a certain yaw angle and using the actual angular position of the robot as a feedback.



\subsection{ROS node and state machine}
The line following operation uses two main ROS nodes to handle the camera images and process them together with the position, orientation and velocity values processed by frobomind to control the robot along the different actions to perform during the line following.
One of this nodes is used exclusively to manage the USB camera, getting images from the camera using the openCV library and publishing them using the CVbridge utility from ROS.
The second node computes this images together with odometry values given by frobomind to control the robot in a certain sequence of actions.

This way, the camera node publishes images at 30fps with the minimum resolution given by the camera, 480x240 pix.
Managing images with this small resolution improves drastically the system performance by both using less memory in the publications and reducing the need of processing the image to filter small elements and noise from the image.
Thanks to this, it is possible to compute real time images on the line follower node, improving the control over the system.

The structure of the line follower node is described in \ref{alg:lineFollower}. 
Being based on a single thread, the node subscribes to the robot odometry given by frobomind and the camera image in order to update the sensor information of the robot.
The robot is controlled by publishing the desired velocity commands to Frobomind.
The node also advertises a service that is called by the top module with new orders and has a service client to comunicate the end of a mission to the top module.

\begin{algorithm}[!h]
	\caption{Line follower action}\label{alg:lineFollower}
	\begin{algorithmic}[1]
		\Procedure{Line follower}{}		
		\State \textit{advertiseServices()}
		\State \textit{subscribeTopics()}
		\While{1}	
		\State \textit{missionService.listenForMission()}
		\State \textit{imageSubscriber.getImage()}
		\State \textit{odometrySubscriber.getOdometry()}
		\If{mission received}
		\State \textit{missionControl(image, robot\_position)}
		\State \textit{commandPublisher.publish(cmd\_vel)}
		\EndIf
		\If{mission finished}
		\State \textit{clientService.notifyMissionEnded(mission result)}
		\State \textit{reset\_node()}
		\EndIf 
		\EndWhile	
		\EndProcedure
%		\Function{getOdometry()}{}
%		\If{New odometry value available}
%		\State \textit{$updated\_odometry \rightarrow robot\_position$}
%		\EndIf
%		\EndFunction
%		\Function{getImage()}{}
%		\If{New image available}
%		\State \textit{$updated\_image \rightarrow image$}
%		\EndIf
%		\EndFunction
		\Function{listenForMission()}{}
		\If{New command received}
		\State \textit{$current\_path \leftarrow new\_mission$}		
		\State \textit{activate\_node()}
		\EndIf
		\EndFunction
		
	\end{algorithmic}
\end{algorithm}	

The orders given by the top module are translated in paths to follow by the robot in the advertised service's callback, being these paths a list of actions to do in each intersection while navigating.
Each top module mission corresponds with a single path for the robot, except for the mission used to make the robot go back to the waypoint navigation area, in which case, this path will depend on the current position of the robot.
In the same callback of this service, the line follower node is activated, setting the node variables to start a new mission.

When the node is active and controlling the mobile platform, the system performs different actions set on the selected path, traveling to the different docking stations on the workcells, the drone area, or the waypoint navigation area.

This way, a state machine (fig. \ref{lineFollower:stateMachine}) has been designed, including different behaviors to perform by the robot during navigation.

When this node starts the navigation, it reads the list of commands and sets the first behavior to follow.
When this action is finished, a new command is taken from the list to be performed until this list is empty or the stop command is read.

If the stop state is reached following this list, the mission is finished successful.
In case the stop state is reached after a line has been lost, the mission finishes as well but with an error, that will be sent to the top module.

The first command in each path is usually to follow the line.
When this behavior is running, the robot will follow the line until an intersection is found.

In the case an intersection is detected, the robot will continue following the line a fixed distance and will read the next behaviour from the command list.

If a line is lost, the robot turns at constant angular speed and zero linear speed up to 45$^\circ$ to the left.
If the line is found again, the robot will return to line following.
In the case of getting to the 45º of rotation, the line is considered lost, and the robot stops with an error.
This behavior is to be developed in a more complete system, but it has shown to be useful for the application solving some problems when the robot can not see the line in the transitions between waypoint navigation and line following.



\tikzstyle{circularBox} = [ellipse, text width=5em, minimum height=1cm,text centered, draw=black]
\begin{figure}[H]
\centering
	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick,scale=0.7, every node/.style={scale=0.7}]
	
	\node [initial,state] (read_mission) [circularBox] {Read mission command};
	\node [state] (follow_line) [circularBox, below of=read_mission] {Follow line};
	\node [state] (turn) [circularBox, above of=read_mission, xshift=-8em]{Turn on intersection};
	\node [state] (odom_bwd) [circularBox, above of=read_mission, xshift=8em] {Dock on workcell};
	\node [state] (find_line) [circularBox, right of=follow_line,xshift=2em] {Find line};
	\node [state] (odom_fwd) [circularBox, left of=follow_line, xshift=-2em] {Align with crossroad};
	\node [state] (stop) [circularBox, right of=read_mission, xshift=5em] {Mission finished: stop};
	
	
	\path (read_mission) 	edge	[bend left = 20] 	node [left, pos=0.5] {cmd: r,i,l, e}(turn)
	edge					node [left, pos=0.5] {cmd: f}(follow_line)
	edge	[bend left ] 	node [right, pos=0.2] {cmd: b}(odom_bwd)
	%edge	[bend left]		node [right, pos=0.5] {command: x}(find_line)
	edge				 	node [below]{cmd: s}(stop)
	(follow_line)		edge 	[bend left] 	node [below] {found intersection}(odom_fwd)
	edge	[bend left]	node [above] {line lost}(find_line)
	(turn)			edge	[bend left ]	node [left, pos=0.8] {finished}(read_mission)
	(odom_bwd)		edge	[bend left = 20]		node [right, pos=0.3] {finished}(read_mission)
	(find_line)		edge	[bend left]	node [below] {line found}(follow_line)
	edge	[bend right]	node [right, pos=0.5] {line not found}(stop)
	(odom_fwd)		edge	[bend left = 20]		node [left, pos=0.5, text width=7em] {stopped on crossroad}(read_mission);
	
	\end{tikzpicture}
	\caption[State machine of line follower node] {The mission selected for the robot is read and different operations are activated in base of the commands of the order. The turning behaviour includes different commands that correspond with different angles to turn: l = 90$^\circ$, e = 180$^\circ$, r = -90$^\circ$, i = -180$^\circ$; being positive values twists to the left and negative to the right.}
	\label{lineFollower:stateMachine}
\end{figure}