The charging area consist of a wooden box with one entrance and three charging stations inside. The mobile robots then have to navigate from the entrance to their respective charging station and back again. Since the box is well defined it was decided to use the mounted LIDAR for navigating inside the box by wall following. Furthermore is the IMU used for making a 180 degrees turn when going out of the box. 

\subsection{Box Navigation}
Figure \ref{fig:boxnavigation} shows a sketch of the charging area with the desired behaviour of the mobile robot. The dashed line shows the route from the starting point, indicated by the cross, to charging station 1. Group 2 uses charging station 3 and charging station 2 is not used. The distance-measures is the desired distance the mobile robot should keep from the box wall while navigating. When going back to the entrance the mobile robot will drive backwards until a clearance of 20 cm is reached before making the 180 degrees turn and again follow the dashed line out of box. This will ensure the mobile robot will not enter the red area that is reserved for group 2's mobile robot. An important part, when entering the charging area, is that the mobile robot front should face towards the entrance such that when driving forwards it will enter the box. This is because the behaviour assumes this and will fail if this criteria is not for filled. 

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{img/mr/box_navigation}
	\caption[Sketch of the charging area]{Sketch of the charging area. The dashed line is the route of the mobile robot from entrance to charging station one. The distance measure is the desired distance the mobile robot should keep from the wall. The red area is the navigation area used by group 2.}
	\label{fig:boxnavigation}
\end{figure}

\subsection{ROS Node and State machine}
Navigation in the charging area is implemented as a ROS node were it communicates with the top module node that decides when to go charge and when to leave the charging station. Figure \ref{fig:statemachineboxnavigation} shows the state machine diagram of the node and below is a description of each state.

\begin{figure}[h!]
\centering
\vspace*{1.5cm}
\begin{tikzpicture}[
->,>=stealth',
shorten >=1pt,
auto,
node distance=2.5cm and 3.2cm,
transform shape,
scale=0.7, every node/.style={scale=0.7}
]

  \node[state,initial,accepting,minimum size=3cm] (1) {Charging};
  \node[state,minimum size=3cm, align=center] (2) [right = of 1] {Reverse};
  \node[state,minimum size=3cm, align=center] (3) [right = of 2] {Turn $180^{\circ}$};
  \node[state,minimum size=3cm, align=center] (4) [below = of 3] {Follow wall\\30cm\\(left)};
  \node[state,minimum size=3cm, align=center] (5) [below = of 2] {Turn $90^{\circ}$\\right};
  \node[state,minimum size=3cm, align=center] (6) [below = of 1] {Follow wall\\60cm\\(left)};
  \node[state,minimum size=3cm, align=center] (7) [below = of 6] {Idle};
  \node[state,minimum size=3cm, align=center] (8) [below = of 5] {Enter Box};
  \node[state,minimum size=3cm, align=center] (9) [below = of 4] {Follow wall\\60cm\\(right)};
  \node[state,minimum size=3cm, align=center] (10) [below = of 9] {Turn $90^{\circ}$\\left};
  \node[state,minimum size=3cm, align=center] (11) [below = of 7] {Follow wall\\30cm\\(right)};
  

\begin{pgfinterruptboundingbox}
  \path[every node/.style={font=\sffamily\scriptsize}]

	(1)	edge [bend left] node[below=7pt, align=center] {\tiny top module order ==\\\tiny STOP\_CHARGING} (2)
		edge [loop above] node [above] {\tiny no top module order} (1)
	(2)	edge [bend left] node[below=7pt, align=center] {\tiny front\_distance > 20cm} (3)
		edge [loop above] node [above] {\tiny front distance < 20cm} (2)
	(3)	edge [bend left] node[above, align=center,rotate=270] {\tiny turn angle > $180^\circ$} (4)
		edge [loop above] node [above] {\tiny turn angle < $180^\circ$} (3)
	(4)	edge [bend left] node[above=7pt, align=center] {\tiny front distance > 50cm} (5)
		edge [loop above] node [above, xshift=-10pt] {\tiny front distance < 50cm} (4)
	(5)	edge [bend left] node[above=7pt, align=center] {\tiny front distance > 120cm} (6)
		edge [loop above] node [above] {\tiny front distance < 120cm} (5)
	(6)	edge [bend right] node[above, align=center, rotate=90] {\tiny left distance > 80cm} (7)
		edge [loop above] node [above] {\tiny left distance < 80cm} (6)
	(7)	edge [bend left] node[below=7pt, align=center] {\tiny  top module order == \\\tiny GO\_CHARGE} (8)
		edge [loop above] node [above,xshift=10pt] {\tiny no top module order} (7)
	(8)	edge [bend left] node[below=7pt, align=center] {\tiny front distance > 160cm} (9)
		edge [loop above] node [above] {\tiny front distance < 160cm} (8)
	(9)	edge [bend left] node[above, align=center,rotate=270] {\tiny front distance < 25cm} (10)
		edge [loop above] node [above] {\tiny front distance < 25cm} (9)
	(10)	edge [bend left] node[above=7pt, align=center] {\tiny front distance > 120cm} (11)
		edge [loop above] node [above,xshift=-10pt] {\tiny front distance < 120cm} (10)
	(11)	edge [bend left] node[above, align=center, rotate=90] {\tiny front distance < 12cm} (1)
		edge [loop above] node [above] {\tiny front distance > 12cm} (11);
\end{pgfinterruptboundingbox}
\end{tikzpicture}
 \vspace*{36pt}
 \caption[State diagram of box navigation]{State machine diagram of the box navigation node. It communicates with top module node the decides when go charge and when to leave the charging station.}
 \label{fig:statemachineboxnavigation}
\end{figure}

\begin{itemize}
	\item \textbf{Charging}: Start state where the node waits for the top module node to give a command. 
	\item \textbf{Reverse}: When getting the command \texttt{STOP\_CHARGING} from the top module node will the mobile robot reverse slowly ($0.1m/s$) until the distance in front is larger than $20cm$. This gives enough room for the 180 degrees turn. 
	\item \textbf{Turn 180}: Uses the mounted IMU for turning 180 degrees with $1 rad/s$. 
	\item \textbf{Follow wall 30 cm left}: Follows the wall with a desired distance of 30 cm and with a velocity of $0.3m/s$ until the distance in front is less than $50cm$. 
	\item \textbf{Turn 90 right}: Turns 90 degrees with $1 rad/s$ until the distance in front is more than $120 cm$.
	\item \textbf{Follow wall 60 cm left}: Follows the wall with a desired distance of 60 cm and with a velocity of $0.3m/s$ until the distance to the left is more than $80 cm$. This insures that the mobile robot will stop when it is outside the box. 
	\item \textbf{Idle}: Waiting for the top module node to give a command.
	\item \textbf{Enter Box}: Drives forward with a velocity of $0.3m/s$ until the distance to the back wall is less than $160cm$. In this state the node simply assumes the robot is placed in front of the entrance and facing such that it will enter the box when driving forward. 
	\item \textbf{Follow wall 60 cm right}: Follows the wall with a desired distance of 60 cm and with a velocity of $0.3m/s$ until the distance in front is less than $25cm$. 
	\item \textbf{Turn 90 left}: Turns 90 degress with $1 rad/s$ until the distance in front is more than $120cm$.
	\item \textbf{Follow wall 30 cm right}: Follow the wall with a desired distance of 60 cm and with velocity of $0.1m/s$ until the distance in front is less than $12cm$. The slower speed insures a good docking into the charging station. 
\end{itemize}


\subsection{Wall Following}
The wall following behaviour is done by using two cascaded PID controllers where the first controls the distance to the wall and outputs the desired angle the mobile robot should travel in. The second one then controls the desired travelling angle and outputs an angular velocity. This is illustrated in figure \ref{fig:pidwallfollowing}.

\begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{img/mr/PID_wall_following}
	\caption[Control diagram of cascaded PID controller]{Control diagram of the cascaded PID controller for wall following. One PID controller is used for wall distance while the other is used for travelling angle. The output for the mobile robot is angular velocity. }
	\label{fig:pidwallfollowing}
\end{figure}

