#ifndef STATISTICS_H
#define STATISTICS_H

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>

#include "ros/ros.h"
#include <ros/macros.h>
#include <string>
#include <ostream>

class statistics{
public:
  void initStatsCSV(){
    filename = boost::posix_time::to_simple_string(ros::WallTime::now().toBoost());
    statfile.open(filename + ".csv", std::ofstream::out | std::ofstream::trunc);
    statfile << "IKtime,IKsucces,PLtime,PLsucces,EXtime,EXsucces\n";
    statfile.close();  
  }
  
  void resetStats(){
    ik_start = 0; ik_end = 0; ik_succes = false;
    planner_start = 0; planner_end = 0; planner_succes = false;
    executor_start = 0; executor_end = 0; executor_succes = false;
  }
  
  void writeStatsCSV(){
    statfile.open(filename + ".csv", std::ofstream::out | std::ofstream::app);
    statfile << ik_end-ik_start << "," << ik_succes << ","
	     << planner_end-planner_start << "," << planner_succes << ","
	     << executor_end-executor_start << "," << executor_succes << "\n";
    statfile.close();
  }
  
  void printStats(){
    ROS_INFO("IK Time: %f succes: %s", ik_end-ik_start, ik_succes ? "true" : "false");
    ROS_INFO("Planner Time: %f Succes: %s", planner_end-planner_start, planner_succes ? "true" : "false");
    ROS_INFO("Executor Time: %f succes: %s", executor_end-executor_start, executor_succes ? "true" : "false");
  }
  
  std::ofstream statfile;
  std::string filename;
  double ik_start;
  double ik_end;
  bool ik_succes;
  double planner_start;
  double planner_end;
  bool planner_succes;
  double executor_start;
  double executor_end;
  bool executor_succes;
}statData;

#endif //STATISTICS_H
