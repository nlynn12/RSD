#ifndef KUKA_EXECUTOR_H
#define KUKA_EXECUTOR_H
/*
 * Code written by jeben12
 */

#include <rw/math/Math.hpp>
#include <rw/trajectory.hpp>
#include <rw/rw.hpp>
#include <iostream>

#include <ros/ros.h>
#include <ros/console.h>

class robot_executor
{
public:
    robot_executor();
    robot_executor ( ros::NodeHandle &n );
    bool executePath ( rw::trajectory::QPath path );
    bool printPathtoLUA ( rw::trajectory::QPath &path );
    bool getIsMoving();
    bool getRobotConfiguration ( rw::math::Q &robotconfig );
    bool getQueueSize ( int &queueSize );
    bool stopRobot();
    bool getSafety();

private:
    ros::ServiceClient isMovingClient;
    ros::ServiceClient getConfigurationClient;
    ros::ServiceClient setConfigurationClient;
    ros::ServiceClient getQueueSizeClient;
    ros::ServiceClient stopRobotClient;
    ros::ServiceClient getSafetyClient;

};

#endif // KUA_EXECUTOR_H