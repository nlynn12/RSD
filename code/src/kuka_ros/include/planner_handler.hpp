#ifndef PLANNER_HANDLER_H
#define PLANNER_HANDLER_H

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "robot_path_planner.hpp"
#include "kuka_executor.hpp"
#include <cstdlib>
#include <fstream>
#include <kuka_ros/movePoint2Pose.h>
#include <kuka_ros/moveP2P.h>
#include <kuka_ros/move2Pose.h>
#include <kuka_ros/move2Point.h>
#include <kuka_ros/legoBrickPickUp.h>

#include <rw/math/Transform3D.hpp>
#include <rw/math/Q.hpp>
#include <rw/trajectory/Path.hpp>


class planner_handler
{
public:
    
    struct PoseQPair {
          rw::math::Transform3D<> trans;
          rw::math::Q q;
          bool ikSuccess;
      };

      
    planner_handler(ros::NodeHandle &n);
    bool Move2Pose(kuka_ros::move2Pose::Request &req, kuka_ros::move2Pose::Response &res);
    bool MoveP2P(kuka_ros::moveP2P::Request &req, kuka_ros::moveP2P::Response &res);
    bool MovePoint2Pose(kuka_ros::movePoint2Pose::Request &req, kuka_ros::movePoint2Pose::Response &res);
    bool Move2Point(kuka_ros::move2Point::Request &req, kuka_ros::move2Point::Response &res);
    bool PickUpLego(kuka_ros::legoBrickPickUp::Request &req, kuka_ros::legoBrickPickUp::Response &res);
  protected:
      
      
      
    enum states {IDLE, IKSOLVE_FOR_LEGO, IKSOLVE_FROM_CURRENT, IKSOLVE, PATHPLAN, PATHPLAN_FROM_CURRENT, EXECUTE} state;
    robot_path_planner planner;
    robot_executor executor;
    rw::math::Transform3D<double> RequestTF;
    rw::math::Q start;
    rw::math::Q goal;
    rw::trajectory::QPath path;
    int type;
    
    
    bool plan();
    
    void generateGraspPoses(const rw::math::Transform3D<> &basepose, const int bricktype, std::vector< PoseQPair > &poses);
    
};

#endif //PLANNER_HANDLER_H