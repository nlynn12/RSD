#ifndef ROBOT_PATH_PLANNER_H
#define ROBOT_PATH_PLANNER_H
/*
 * Code written by jeben12
 */
#include <rw/invkin/JacobianIKSolver.hpp>
#include <rw/kinematics/FixedFrame.hpp>
#include <rw/kinematics/Kinematics.hpp>
#include <rw/kinematics/MovableFrame.hpp>
#include <rw/loaders/WorkCellLoader.hpp>
#include <rw/models/SerialDevice.hpp>
#include <rw/models/WorkCell.hpp>
#include <rw/math/Math.hpp>
#include <rw/math/Q.hpp>
#include <rw/rw.hpp>
#include <rw/math/MetricFactory.hpp>
#include <rw/invkin/PieperSolver.hpp>
#include <rw/invkin/ClosedFormIKSolverUR.hpp>
#include <rw/invkin/JacobianIKSolver.hpp>

#include <rwlibs/pathplanners/rrt/RRTPlanner.hpp>
#include <rwlibs/pathplanners/rrt/RRTQToQPlanner.hpp>
#include <rwlibs/proximitystrategies/ProximityStrategyFactory.hpp>

#include <ros/console.h>
#include <ros/param.h>
#include <ros/ros.h>

#include <iostream>
#include <vector>
#include <list>

#define NSAMPLES 32 //TODO Move these to ros param
#define MAXTIME 10
#define extend 0.01

class robot_path_planner
{
public:
    robot_path_planner();
    robot_path_planner(ros::NodeHandle &n);

    bool planPointToPoint(const rw::math::Q &start, const rw::math::Q &goal, rw::trajectory::QPath &path);

    /**
     * @brief checkCollisions
     * @param q
     * @return Returns true if not in collision.
     */
    bool checkCollisions(const rw::math::Q &q);
    bool checkCollisions(const rw::math::Q &q, rw::kinematics::State &state);

    bool validateStartGoalStates(const rw::math::Q &start, const rw::math::Q &goal);

    bool solveIK(const rw::math::Transform3D<double>, rw::math::Q &q);

    rw::math::Q get_configuration();
    
//private:
    rw::models::WorkCell::Ptr rw_wc;
    rw::kinematics::State state;
    rw::models::SerialDevice::Ptr sdevice;

protected:
    // general functionality
    // taken from RW ambiguity solver..
    std::vector< rw::math::Q > expandQ(const std::list<rw::math::Q> &config) const;

    // planners
    rw::proximity::CollisionDetector::Ptr _detector;
    rw::pathplanning::PlannerConstraint _constraint;
    
    // Create sampler for RRTConnect
    rw::pathplanning::QSampler::Ptr _sampler;
    rw::math::QMetric::Ptr _metric;
   
    // Create planner
    rw::pathplanning::QToQPlanner::Ptr _planner;
    rw::invkin::JacobianIKSolver::Ptr _iksolver; //sdevice, state

    // used for ambiguity
    rw::math::Q _lower;
    rw::math::Q _upper;
    std::vector<size_t> _indices;


};

#endif // ROBOT_PATH_PLANNER_H
