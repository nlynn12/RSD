Kuka ROS
====

This folder contains the code first supplied by Jens Cortsen.
This is a ROS node made to communicate with the [Kuka Server](../code/robotcell/kuka_server).
All robot control goes through this ROS node to communicate with the Kuka.

To launch, run: roslaunch kuka_ros kukaros.launch 

Kuka Services & Topics
-----

### Services

- /KukaNode/GetConfiguration | returns float32[6] q.
- /KukaNode/GetQueueSize | returns int32 queueSize.
- /KukaNode/GetSafety | returns bool safetyBreached.
- /KukaNode/IsMoving | returns bool isMoving.
- /KukaNode/SetConfiguration | recieves float32[6] q, float32[6] speed # 1-80, float32[6] acc	 # 1-40 (not implemented yet).
- /KukaNode/StopRobot
- /KukaNode/ActuateValve | recives bool flowForward
- /KukaNode/GetValve | returns bool[2] valve
- /KukaNode/IsConnected | returns bool isConnected


### Topics

- /KukaNode/IsConnected | returns bool data
- /KukaNode/GetConfiguration | returns float32[6] q.


Running the Kuka ROS Node
-----------

First you need to make sure that you've set up this project.
To do so follow [this guide](../code/).
If you've already followed the setting up part, but this code is new in your project, just do:

```sh
cd .../RSD/code/
catkin_make
```

Now the ros node is build.
Also make sure that you've build and are running the server communicating with the Kuka Robot.
To do so follow the Build & Run Instructions of [the Kuka server guide](../code/robotcell/kuka_server/).

Once the server is up and running it will have output:
```sh
UDP Server+Client is running..
TCP Server is running..
```

You can now open the ros node:
```sh
roslaunch kuka_ros kukaros.launch 
```

This will start the rosnode and if everything goes right the server will also output (IP may vary)
```sh
Got connection from: 192.168.100.50,48572
```
To show that it has established the TCP connection to the ROS Node.

## Moving the Robot from CLI 

To test if the robot works you can call the services from the command line interface.
To do so, first run the Kuka ROS node as described above.

Also go into the Kuka HMI controller, set it into 'Auto' ('A'), acitvate the motors ('I') and run the program 'KRC:/R1/Program/RSIJointControl'.

Now you are ready to test the robot:


```sh
# go home
rosservice call /KukaNode/SetConfiguration [0,-1.57,1.57,0,1.57,0] [1,1,1,1,1,1] [0,0,0,0,0,0]
# move a little
rosservice call /KukaNode/SetConfiguration [0.1,-1.47,1.67,0.1,1.67,0.1] [1,1,1,1,1,1] [0,0,0,0,0,0]

rosservice call /KukaNode/GetConfiguration
```

/KukaNode/SetConfiguration parameters are [pose] [speed] [acc].
Acceleration is not used, but must be supplied.
Pose is the actual joint position and speed values goe from 0 to 80.

Please not that if the actual pose of the robot may be a little of due to calibration errors on startup :worried:

Running the KukaClient Node
-----------
The KukaClient node takes care of pathplanning. 
#### Starting Up Everything
2. Assuming that you have run catkin make
3. Run the RSI code
4. `roslaunch kuka_ros kukaros`
5. `roslaunch kuka_ros kukaClient`

#### Move the robot to a pose
Now you should be able to call the service with a TCP pose:

`rosservice call /rc/kukaClient/requestPosePickup "{msg_x: 0.040, msg_y: -0.530, msg_z: -0.013, msg_roll: -1.846, msg_pitch: 0.540, msg_yaw: 2.994}"`

