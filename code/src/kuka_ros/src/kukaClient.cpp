/*
 * Copyright 2016 Jens-Jakob Bentsen <jeben12@student.sdu.dk>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
#include "ros/ros.h"
#include "planner_handler.hpp"

int main ( int argc, char **argv )
{
    // ROS setup
    ros::Time::init();
    ros::init ( argc, argv, "giveMeAname" );
    ros::NodeHandle n ( ros::this_node::getName() );

    planner_handler ph ( n );

    // Create service handlers
    ros::ServiceServer move2Pose = n.advertiseService ( "move2Pose", &planner_handler::Move2Pose, &ph );
    ros::ServiceServer moveP2P = n.advertiseService ( "moveP2P", &planner_handler::MoveP2P, &ph );
    ros::ServiceServer movePoint2Pose = n.advertiseService ( "movePoint2Pose", &planner_handler::MovePoint2Pose, &ph );
    ros::ServiceServer move2Point = n.advertiseService ( "move2Point", &planner_handler::Move2Point, &ph );
    ros::ServiceServer pickUpLegoBrick = n.advertiseService ( "pickUpLego", &planner_handler::PickUpLego, &ph );

    // Sleep rate
    ros::Rate r ( 1000 );

    // Set loop rate
    while ( ros::ok() ) {
        ros::spinOnce();
        r.sleep();
    }

    return 0;
}
