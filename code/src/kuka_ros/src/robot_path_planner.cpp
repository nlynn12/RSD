/*
 * Copyright 2016 Jens-Jakob Bentsen <jeben12@student.sdu.dk>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "robot_path_planner.hpp"

robot_path_planner::robot_path_planner() {}

robot_path_planner::robot_path_planner ( ros::NodeHandle &n ) :
    _metric ( rw::math::MetricFactory::makeEuclidean< rw::math::Q >() )
{

    //Load WorkCell
    std::string wcFile;
    if ( !n.getParam ( "wcFilePath", wcFile ) ) {
        ROS_ERROR ( "Missing ros parameter 'wcFilePath'" );
    }
    rw_wc = rw::loaders::WorkCellLoader::Factory::load ( wcFile );

    //Controll if WorkCell is properly loaded
    if ( rw_wc.isNull() ) {
        RW_THROW ( "Could not load workcell!" );
    } else {
        ROS_INFO_STREAM ( "Loaded workcell: " << rw_wc->getName() );
    }

    // Get default state
    state = rw_wc->getDefaultState();

    // find a device by name
    sdevice = rw_wc->findDevice<rw::models::SerialDevice> ( "KukaKr6" );

    // get device name
    std::string sdevicename = sdevice->getName();
    ROS_INFO_STREAM ( "Loaded Robot: " << sdevicename );

    // you could specify the name in the launch file...
    rw::kinematics::Frame::Ptr tcp = rw_wc->findFrame ( "HGPC16A.TCP" );
    if ( tcp == NULL ) {
        throw ( "TCP frame not found in wc." );
    }
    _detector = new rw::proximity::CollisionDetector ( rw_wc, rwlibs::proximitystrategies::ProximityStrategyFactory::makeDefaultCollisionStrategy() );
    _iksolver = new rw::invkin::JacobianIKSolver ( sdevice, &*tcp, state ); //sdevice, state

    _constraint = rw::pathplanning::PlannerConstraint::make ( &*_detector,sdevice,state );

    // Create sampler for RRTConnect
    _sampler = rw::pathplanning::QSampler::makeConstrained ( rw::pathplanning::QSampler::makeUniform ( sdevice ), _constraint.getQConstraintPtr() );

    // Create planner
    _planner = rwlibs::pathplanners::RRTPlanner::makeQToQPlanner ( _constraint, _sampler, _metric, extend, rwlibs::pathplanners::RRTPlanner::RRTConnect );

    // kidnaped from RW Ambiguity
    const std::vector<rw::models::Joint*>& joints = sdevice->getJoints();
    size_t index = 0;
    BOOST_FOREACH ( const rw::models::Joint* joint, joints ) {
        if ( dynamic_cast<rw::models::RevoluteJoint*> ( joints[index] ) ) {
            for ( int i = 0; i<joint->getDOF(); i++ ) {
                _indices.push_back ( index );
                ++index;
            }
        } else {
            index += joint->getDOF();
        }
    }

    _lower = sdevice->getBounds().first;
    _upper = sdevice->getBounds().second;

}


bool robot_path_planner::planPointToPoint ( const rw::math::Q &start,const rw::math::Q &goal, rw::trajectory::QPath &path )
{
    ROS_DEBUG_STREAM ( "Planner Called" );

    // Crate Pathcontainer
    rw::trajectory::QPath thepath;

    // Validate Start And Goal Joint States
    bool statesAreValid = validateStartGoalStates ( start, goal );

    if ( statesAreValid ) {

        bool plannerSucces = _planner->query ( start,goal,thepath,MAXTIME );
        path = thepath;
        if ( plannerSucces ) {

            ROS_DEBUG ( "Planning Succesfull" );
            return true;

        } else {
            ROS_DEBUG ( "Planning Failed" );
        }

    } else {
        ROS_DEBUG ( "Failed due to invalid States" );
        return false;
    }

    return false;
}

bool robot_path_planner::validateStartGoalStates ( const rw::math::Q &start, const rw::math::Q &goal )
{
    // Check Preconditions for planning
    if ( !checkCollisions ( start ) ) {
        ROS_DEBUG ( "Error: Robot Start State was in collision" );
        return false;
    }

    if ( !checkCollisions ( goal ) ) {
        ROS_DEBUG ( "Error: Robot Goal State was in collision" );
        return false;
    }

    return true;
}

bool robot_path_planner::checkCollisions ( const rw::math::Q &q )
{
    rw::kinematics::State testState = state;
    return checkCollisions ( q, testState );
}

bool robot_path_planner::checkCollisions ( const rw::math::Q &q, rw::kinematics::State &state )
{
    rw::proximity::CollisionDetector::QueryResult data;
    bool colFrom;

    sdevice->setQ ( q,state );
    colFrom = _detector->inCollision ( state,&data );
    if ( colFrom ) {
        ROS_DEBUG_STREAM ( "Configuration in collision: " << q );
        ROS_DEBUG_STREAM ( "Colliding frames: " );
        rw::kinematics::FramePairSet fps = data.collidingFrames;
        for ( rw::kinematics::FramePairSet::iterator it = fps.begin(); it != fps.end(); it++ ) {
            ROS_DEBUG_STREAM ( ( *it ).first->getName() << " " << ( *it ).second->getName() );
        }
        return false;
    }
    return true;
}

bool robot_path_planner::solveIK ( const rw::math::Transform3D<double> transform, rw::math::Q &q )
{
    bool succes = false;

    ROS_DEBUG_STREAM ( "Starting IK solver." );
    const int startPoses = 100;
    std::list<rw::math::Q> iklistresults;
//     #pragma omp parallel for shared(iklistresults)
    for ( int i = 0; i < startPoses; i++ ) {
        // Find solution from ranq
        ROS_DEBUG_STREAM ( "Starting loop." );
        rw::kinematics::State tmpState = state;
        rw::math::Q ranq;
        if ( i > 0 ) {
            ranq = rw::math::Math::ranQ ( sdevice->getBounds() );
        } else {
            ranq = q;
        }
        ROS_DEBUG_STREAM ( "Starting IK solving." );
        sdevice->setQ ( ranq, tmpState );
        std::vector< rw::math::Q > qsol = _iksolver->solve ( transform, tmpState );
        // collision check q
        ROS_DEBUG_STREAM ( "Checking collisions." );
        for ( int c = 0; c < static_cast<int> ( qsol.size() ); c++ ) {
            if ( checkCollisions ( qsol[c], tmpState ) ) {
                iklistresults.push_back ( qsol[c] );
            }
        }
    }

    ROS_DEBUG_STREAM ( "Expanding Qs." );
    // expand q's for ambigouty
    std::vector<rw::math::Q> ikresults = expandQ ( iklistresults );

    //std::vector<rw::math::Q> ikresults = _invkin.solve(transform,state);
    rw::math::Q currentQ = q;
    rw::math::Q nearestQ ( sdevice->getDOF(), 0.0 );
    double distance;

    ROS_DEBUG ( "IK solutions: %d", ( int ) ikresults.size() );
    if ( ikresults.size() == 0 ) {
        return false;
    }
    double nearestDistance = -1;
    for ( size_t i = 0; i < ikresults.size(); i++ ) {
        distance = _metric->distance ( ikresults[i],currentQ );
        if ( nearestDistance > distance || nearestDistance < 0 ) {
            succes = true;
            nearestQ = ikresults[i];
            nearestDistance = distance;
        }

    }
    q = nearestQ;
    return succes;
}


std::vector< rw::math::Q > robot_path_planner::expandQ ( const std::list< rw::math::Q > &config ) const
{
    std::vector< rw::math::Q > res1 ( config.cbegin(), config.cend() );
    std::vector< rw::math::Q > res2;
    const double pi2 = 2*rw::math::Pi;


    for ( auto index : _indices ) {
        res2.clear();
        for ( auto q : res1 ) {

            double d = q ( index );
            while ( d>_lower ( index ) ) {
                d -= pi2;
            }
            while ( d<_lower ( index ) ) {
                d += pi2;
            }
            while ( d <_upper ( index ) ) {
                rw::math::Q tmp ( q );
                tmp ( index ) = d;
                res2.push_back ( tmp );
                d += pi2;
            }
        }
        res1 = res2;
    }
    return res1;
}
