/*
 * Copyright 2016 Jens-Jakob Bentsen <jeben12@student.sdu.dk>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "planner_handler.hpp"
#include "statistics.hpp"

#include <vector>

#include <rw/math/Vector3D.hpp>
#include <rw/math/RPY.hpp>
#include <rw/math/MetricUtil.hpp>


planner_handler::planner_handler ( ros::NodeHandle &n )
{
    planner = robot_path_planner ( n );
    executor = robot_executor ( n );
    start = rw::math::Q ( 6, 0.0 );
    goal = rw::math::Q ( 6, 0.0 );
}


bool planner_handler::Move2Pose ( kuka_ros::move2Pose::Request &req, kuka_ros::move2Pose::Response &res )
{
    rw::math::Transform3D<double> tf ( rw::math::Vector3D<double> ( req.poseGoal[0], req.poseGoal[1], req.poseGoal[2] ),
                                       rw::math::RPY<double> ( req.poseGoal[3],req.poseGoal[4],req.poseGoal[5] ).toRotation3D() );
    RequestTF = tf;

    state = IKSOLVE_FROM_CURRENT;
    res.acceptedRequest = true;
    return plan();
}

bool planner_handler::MoveP2P ( kuka_ros::moveP2P::Request &req, kuka_ros::moveP2P::Response &res )
{
    for ( unsigned i = 0; i < 6; i++ ) {
        start[i] = req.qStart[i];
        goal[i] = req.qGoal[i];
    }
    state = PATHPLAN;
    res.acceptedRequest = plan();
    return true;
}

bool planner_handler::MovePoint2Pose ( kuka_ros::movePoint2Pose::Request &req, kuka_ros::movePoint2Pose::Response &res )
{
    rw::math::Transform3D<double> tf ( rw::math::Vector3D<double> ( req.poseGoal[0], req.poseGoal[1], req.poseGoal[2] ), rw::math::RPY<double> ( req.poseGoal[3],req.poseGoal[4],req.poseGoal[5] ).toRotation3D() );
    RequestTF = tf;
    for ( unsigned i = 0; i < 6; i++ ) {
        start[i] = req.qStart[i];
    }
    state = IKSOLVE;
    res.acceptedRequest = plan();
    return true;
}

bool planner_handler::Move2Point ( kuka_ros::move2Point::Request &req, kuka_ros::move2Point::Response &res )
{
    for ( unsigned i = 0; i < 6; i++ ) {
        goal[i] = req.qGoal[i];
    }
    state = PATHPLAN_FROM_CURRENT;
    res.acceptedRequest = plan();
    return true;

}

bool planner_handler::PickUpLego(kuka_ros::legoBrickPickUp::Request &req, kuka_ros::legoBrickPickUp::Response &res)
{
    rw::math::Transform3D<double> tf ( rw::math::Vector3D<double> ( req.poseGoal[0], req.poseGoal[1], req.poseGoal[2] ),
                                       rw::math::RPY<double> ( req.poseGoal[3],req.poseGoal[4],req.poseGoal[5] ).toRotation3D() );
    for ( unsigned i = 0; i < 6; i++ ) {
        start[i] = req.qStart[i];
    }
    RequestTF = tf;
    type = req.type;
    
    state = IKSOLVE_FOR_LEGO;
	
    res.acceptedRequest = plan();
    
    //bool ret = plan();
    
    for ( unsigned i = 0; i < 6; i++ ) {
        res.qGoal[i] = goal[i];
    }    
    return true;
}

void planner_handler::generateGraspPoses ( const rw::math::Transform3D< double >& basepose, const int bricktype, std::vector< planner_handler::PoseQPair >& poses ) {
    poses.clear();
    planner_handler::PoseQPair element;
    element.trans = basepose;
    poses.push_back(element);
    if(bricktype > 2 || bricktype < 0){
        ROS_ERROR_STREAM("Undefined brick type!");
        return;
    }
    // the 180 deg flipped works for all 3 types
    element.trans = basepose * rw::math::Transform3D<>(rw::math::RPY<>(180.0*rw::math::Deg2Rad, 0.0, 0.0).toRotation3D());
    poses.push_back(element);
    // for the blue add the quarters as well
    if(bricktype == 0){
        element.trans = basepose * rw::math::Transform3D<>(rw::math::RPY<>(90.0*rw::math::Deg2Rad, 0.0, 0.0).toRotation3D());
        poses.push_back(element);
        element.trans = basepose * rw::math::Transform3D<>(rw::math::RPY<>(270.0*rw::math::Deg2Rad, 0.0, 0.0).toRotation3D());
        poses.push_back(element);
    }
}


bool planner_handler::plan()
{
    bool succes = false;
    while ( state != IDLE ) {
        switch ( state ) {
            case IDLE:
                statData.resetStats();
                break;
            case IKSOLVE_FOR_LEGO:
                goal = start; // Here we add the the current robot state as seed to the IKsolver
                
                {
                    std::vector< planner_handler::PoseQPair > possibleGraspPosese;
                    generateGraspPoses(RequestTF, type, possibleGraspPosese);
                    ROS_DEBUG_STREAM("Lego pose has " << possibleGraspPosese.size() << " possible grasp poses.");
                    
                    statData.ik_start = ros::Time::now().toSec();
                    statData.ik_succes = true;
                    for(size_t i = 0; i < possibleGraspPosese.size(); i++){
						planner.sdevice->setQ ( start, planner.state );
                        possibleGraspPosese[i].q = goal;
                        bool ik_succes = planner.solveIK ( possibleGraspPosese[i].trans, possibleGraspPosese[i].q );
                        possibleGraspPosese[i].ikSuccess = ik_succes;
                        statData.ik_succes = statData.ik_succes & ik_succes;
                    }
                    statData.ik_end = ros::Time::now().toSec();
                    
                    int idx_bestSolution = -1;
                    double shortest_path = 0;
                    
                    for(size_t i = 0; i < possibleGraspPosese.size(); i++){
                        if(possibleGraspPosese[i].ikSuccess){
                            double distance = rw::math::MetricUtil::dist2<rw::math::Q>(start, possibleGraspPosese[i].q);
                            if(distance < shortest_path || idx_bestSolution < 0){
                                shortest_path = distance;
                                idx_bestSolution = static_cast<int>(i);
                            }
                        }
                    }
                    ROS_DEBUG_STREAM("Planner found solution: " << idx_bestSolution << " to be the best grasp.");
                    if(idx_bestSolution < 0){
                        ROS_ERROR_STREAM ( "IK FAILED" );
                        state = IDLE;
                        break;
                    }
                    goal = possibleGraspPosese[idx_bestSolution].q;
                }
                
                
                state = PATHPLAN;
                break;
                
            case IKSOLVE_FROM_CURRENT:
                executor.getRobotConfiguration ( start );
            case IKSOLVE:
                planner.sdevice->setQ ( start, planner.state );
                goal = start; // Here we add the the current robot state as seed to the IKsolver
                
                statData.ik_start = ros::Time::now().toSec();
                statData.ik_succes = planner.solveIK ( RequestTF, goal );
                statData.ik_end = ros::Time::now().toSec();
                
                if ( statData.ik_succes ) {
                    ROS_DEBUG_STREAM ( "IK FOUND: " << goal );
                    state = PATHPLAN;
                } else {
                    ROS_ERROR_STREAM ( "IK FAILED" );
                    state = IDLE;
                }
                break;
                
            case PATHPLAN_FROM_CURRENT:
                executor.getRobotConfiguration ( start );
                
            case PATHPLAN:
                statData.planner_start = ros::Time::now().toSec();
                statData.planner_succes = planner.planPointToPoint ( start, goal, path );
                statData.planner_end = ros::Time::now().toSec();
                
                if ( statData.planner_succes ) {
                    state = EXECUTE;
                } else {
                    state = IDLE;
                    statData.printStats();
                }
                break;
                
            case EXECUTE:
                statData.executor_start = ros::Time::now().toSec();
                statData.executor_succes = executor.executePath ( path );
                statData.executor_end = ros::Time::now().toSec();
                statData.printStats();

                succes = statData.executor_succes;
                
                state = IDLE;
                break;
                
            default:
                state = IDLE;
                break;
        }
    }
    return succes;
}