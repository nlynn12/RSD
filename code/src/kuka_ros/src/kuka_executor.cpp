/*
 * Copyright 2016 Jens-Jakob Bentsen <jeben12@student.sdu.dk>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
#include "kuka_executor.hpp"
#include <kuka_ros/getIsMoving.h>
#include <kuka_ros/setConfiguration.h>
#include <kuka_ros/getConfiguration.h>
#include <kuka_ros/getQueueSize.h>
#include <kuka_ros/stopRobot.h>
#include <kuka_ros/getSafety.h>

//TODO jointSpeed should not be at a hardcoded value as it is.
//TODO Can the RobWork Q - constructor take a float array as input?
//TODO Error expeptions/Handling

robot_executor::robot_executor()
{

};

robot_executor::robot_executor ( ros::NodeHandle &n )
{
    getConfigurationClient = n.serviceClient<kuka_ros::getConfiguration> ( "/rc/KukaNode/GetConfiguration" );
    getQueueSizeClient = n.serviceClient<kuka_ros::getQueueSize> ( "/rc/KukaNode/GetQueueSize" );
    getSafetyClient = n.serviceClient<kuka_ros::getSafety> ( "/rc/KukaNode/GetSafety" );
    isMovingClient = n.serviceClient<kuka_ros::getIsMoving> ( "/rc/KukaNode/IsMoving" );
    setConfigurationClient = n.serviceClient<kuka_ros::setConfiguration> ( "/rc/KukaNode/SetConfiguration" );
    stopRobotClient = n.serviceClient<kuka_ros::stopRobot> ( "/rc/KukaNode/StopRobot" );
}

bool robot_executor::getRobotConfiguration ( rw::math::Q &robotconfig )
{
    kuka_ros::getConfiguration srv;

    if ( getConfigurationClient.call ( srv ) ) {
        for ( unsigned i = 0; i < 6; i++ ) {
            robotconfig[i] = srv.response.q[i];
        }
        return true;
    }
    return false;
}

bool robot_executor::executePath ( rw::trajectory::QPath path )
{
    //Carefull, with the types (srv takes in floats and ints are defined)
    int qSize = path.at ( 0 ).size();
    int pathSize = path.size();
    int jointSpeed = 40.0;
    int jointAcceleration = 60.0; // Not Implemented on serverside

    //Create the request "package / object"
    kuka_ros::setConfiguration srv;

    for ( int i = 0; i < pathSize; i++ ) {
        for ( int j = 0; j < qSize; j++ ) {
            srv.request.q[j] = path[i][j];
            srv.request.speed[j] = jointSpeed;
            srv.request.acc[j] = jointAcceleration;
        }

        // Send the actual request
        if ( !setConfigurationClient.call ( srv ) ) {
            ROS_ERROR ( "Failed to call setConfiguration service" );
            return false;
        } else {
            ROS_INFO_STREAM ( "Moving to: " << path[i] );
        }
        usleep ( 9000 ); // 8000 is too fast, so 9000 it is.
    }

    return true;
}

bool robot_executor::printPathtoLUA ( rw::trajectory::QPath &path )
{
    // Print out the Path Vector
    for ( unsigned int i = 0; i< path.size(); i++ ) {
        rw::common::Log::infoLog() << "setQ({" << path[i][0] << ","<<path[i][1]<< ","<<path[i][2]<< ","<<path[i][3]<< ","<<path[i][4]<< ","<<path[i][5]<< "})" << std::endl;
    }
    return true;
}

bool robot_executor::getIsMoving()
{
    kuka_ros::getIsMoving srv;

    if ( isMovingClient.call ( srv ) ) {
        return srv.response.isMoving;
    } else {
        ROS_ERROR ( "Failed to call isMoving service" );
        return false; //If not responding what should we do? considder changing design to take boolean argument.
    }
}

bool robot_executor::getQueueSize ( int &queueSize )
{
    kuka_ros::getQueueSize srv;

    if ( getQueueSizeClient.call ( srv ) ) {
        queueSize = srv.response.queueSize;
        return true;
    } else {
        ROS_ERROR ( "Failed to call getQueueSize service" );
        return false;
    }
}

bool robot_executor::stopRobot()
{
    kuka_ros::stopRobot srv;

    if ( stopRobotClient.call ( srv ) ) {
        return true;
    } else {
        ROS_ERROR ( "Failed to call stopRobot service" );
        return false;
    }
}

bool robot_executor::getSafety()
{
    kuka_ros::getSafety srv;

    if ( getSafetyClient.call ( srv ) ) {
        return srv.response.safetyBreached;
    } else {
        ROS_ERROR ( "Failed to call getSafety service" );
        return false; //If not responding what should we do? considder changing design to take boolean argument.
    }
}
