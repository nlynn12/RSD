/*
 * Copyright 2016 Lukas Schwartz <lschw12@student.sdu.dk>  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

// Includes
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "kuka_ros/getConfiguration.h"
#include "kuka_ros/setConfiguration.h"
#include "kuka_ros/getQueueSize.h"
#include "kuka_ros/getIsMoving.h"
#include "kuka_ros/getIsConnected.h"
#include "kuka_ros/stopRobot.h"
#include "kuka_ros/getSafety.h"
#include "kuka_ros/actuateValve.h"
#include "kuka_ros/getValve.h"
#include "kuka_ros/robotConfiguration.h"
#include "std_msgs/String.h"
#include <iostream>
#include <thread>
#include <list>
#include <mutex>
#include <string>
#include "PracticalSocket.h"

// Defines
#define TCP_IP_SERVER           "192.168.100.50"
#define TCP_PORT_SERVER         49002
#define DEBUG                   true
#define DEF_PRECISION           5

// Global variables
bool shutdownindicator = false;
std::list< std::string > message_queue;
std::mutex message_protection;
TCPSocket *client = NULL;

// Functions

// stalling call that waits for reply of the server
size_t getReply(std::string &msg, const std::string &search = ""){
//    bool replied = false;
    size_t idx;
//    while(!replied){
       while(message_queue.size() == 0){
	usleep(1);
       }	 

       message_protection.lock();
	bool target_found = false;
	int size = message_queue.size();
        if(size == 0){
            std::cout << "No messages on lock" << std::endl;
            message_protection.unlock();
            return std::string::npos;
        }
	for(std::list< std::string >::iterator it = message_queue.begin(); it != message_queue.end(); it++){
		idx = it->find(search);
		if(search == "" || idx != std::string::npos){
			target_found = true;
			msg = *it;
			message_queue.erase(it);
			break;
		}
	}
	if(size > 0 && !target_found){
		idx = std::string::npos;
	}
//        if(message_queue.size() > 0){
//            replied = true;
//            msg = message_queue.front();
//            message_queue.pop_front();
//        }
        message_protection.unlock();
	return idx;
//    }
#if DEBUG == true
    std::cout << "Popped: " << msg << std::endl;
#endif
}


bool isDigitsOnly(const std::string &str)
{
    return str.find_first_not_of("0123456789") == std::string::npos;
}

bool setConfigurationCallback(kuka_ros::setConfiguration::Request &req, kuka_ros::setConfiguration::Response &res)
{
    // Add Q
    std::stringstream str;
    str << "SetConfiguration:Q{";

    for(unsigned int i=0; i<req.q.size(); i++)
    {
        str << std::fixed << std::setprecision(DEF_PRECISION) << req.q[i];

        if(i<req.q.size()-1)
            str << ", ";
    }
    str << "},V{";

    // Add Speed
    for(unsigned int i=0; i<req.speed.size(); i++)
    {
        str << std::fixed << std::setprecision(DEF_PRECISION) << req.speed[i];

        if(i<req.speed.size()-1)
            str << ", ";
    }
    str << "}";

    client->send(str.str());

#if DEBUG == true
        std::cout << str.str() << std::endl;
#endif

    return true;
}

bool getConfigurationCallback(kuka_ros::getConfiguration::Request &req, kuka_ros::getConfiguration::Response &res)
{
    std::string msg;
    size_t idx;
    client->send("GetConfig");
    idx = getReply(msg, "GetConfig:");
    if(idx == std::string::npos){
        ROS_ERROR("Error: Got no message containing the data!");
        return false;
    }    
    if(msg.empty())
    {
        ROS_ERROR("Error: Got empty msg!");
        return false;
    }
    else
    {
#if DEBUG == true
            std::cout << "Get config: " << msg << std::endl;
#endif

//        if(msg.find("GetConfig:") == std::string::npos){
//            ROS_ERROR("Error: Did not get the correct reply!");
//            return false;
//        }
        // Isolate data
        int i = idx+6;
        while(msg[i++] != '{' && i < static_cast<int>(msg.size()));
        int k = i;
        while(msg[k++] != '}' && k < static_cast<int>(msg.size()));

        if(msg[k-1] != '}' || i >= k){
            ROS_ERROR("Error: Get Q had no end bracket!");
            return false;
        }
        msg = msg.substr(i, k-2);

	// Get data
        std::stringstream ss(msg);
        double d = 0;
        i = 0;
        while(ss >> d)
        {
            res.q[i++] = d;
            if(ss.peek() == ',' || ss.peek() == ' ')
                ss.ignore();
        }
        if(i != 6){
            ROS_ERROR("Error: Did not get the correct number of values!");
            return false;            	
        }

        return true;
    }
}

bool getQueueSizeCallback(kuka_ros::getQueueSize::Request &req, kuka_ros::getQueueSize::Response &res)
{
    client->send("GetQueueSize");
    std::string msg;
    getReply(msg);

    if(msg.empty())
    {
        ROS_ERROR("Error: Got empty msg!");
        return false;
    }
    else
    {
#if DEBUG == true
            std::cout << msg << std::endl;
#endif

        if(msg.find("GetQueueSize") == std::string::npos){
            ROS_ERROR("Error: Did not get the correct reply!");
            return false;
        }

        int i = 0;
        while(msg[i++] != '{' && i < static_cast<int>(msg.size()));
        int k = i;
        while(msg[k++] != '}' && k < static_cast<int>(msg.size()));
	std::string num = msg.substr(i, k-1);;
        if(isDigitsOnly(num))
        {
            // Handle msg
            res.queueSize = atoi(num.c_str());
            return true;
        }
        return false;
    }
}

bool stopRobotCallback(kuka_ros::stopRobot::Request &req, kuka_ros::stopRobot::Response &res)
{
    client->send("StopRobot");
    return true;
}

bool isMovingCallback(kuka_ros::getIsMoving::Request &req, kuka_ros::getIsMoving::Response &res)
{
    client->send("IsMoving");
    std::string msg;
    getReply(msg);

    if(msg.empty())
    {
        ROS_ERROR("Error: Got empty msg!");
        return false;
    }
    else
    {
#if DEBUG == true
            std::cout << msg << std::endl;
#endif

        if(msg.find("IsMoving{") == std::string::npos){
            ROS_ERROR("Error: Did not get the correct reply!");
            return false;
        }
        // Handle msg
        if(msg == "IsMoving{T}")
        {
            res.isMoving = true;
            return true;
        }
        else if(msg == "IsMoving{F}")
        {
            res.isMoving = false;
            return true;
        }
        else
            return false;
    }
}

bool isConnectedCallback(kuka_ros::getIsConnected::Request &req, kuka_ros::getIsConnected::Response &res)
{
    client->send("IsConnected");
    std::string msg;
    getReply(msg);

    if(msg.empty())
    {
        ROS_ERROR("Error: Got empty msg!");
        return false;
    }
    else
    {
#if DEBUG == true
            std::cout << msg << std::endl;
#endif

        if(msg.find("IsConnected{") == std::string::npos){
            ROS_ERROR("Error: Did not get the correct reply!");
            return false;
        }
        // Handle msg
        if(msg == "IsConnected{T}")
        {
            res.isConnected = true;
            return true;
        }
        else if(msg == "IsConnected{F}")
        {
            res.isConnected = false;
            return true;
        }
        else
            return false;
    }
}

bool safetyCallback(kuka_ros::getSafety::Request &req, kuka_ros::getSafety::Response &res)
{
    client->send("GetSafety");
    std::string msg;
    getReply(msg);

    if(msg.empty())
    {
        ROS_ERROR("Error: Got empty msg!");
        return false;
    }
    else
    {
#if DEBUG == true
            std::cout << msg << std::endl;
#endif

        if(msg.find("GetSafety{") == std::string::npos){
            ROS_ERROR("Error: Did not get the correct reply!");
            return false;
        }
        // Handle msg
        if(msg == "GetSafety{F}")
        {
            res.safetyBreached = true;
            return true;
        }
        else if(msg == "GetSafety{T}")
        {
            res.safetyBreached = false;
            return true;
        }
        else
            return false;
    }
}

bool actuateValveCallback(kuka_ros::actuateValve::Request &req, kuka_ros::actuateValve::Response &res)
{
    // Add Q
    std::string str = "ActuateValve{";
    if(req.flowForward){
        str += "T}";
    } else {
        str += "F}";
    }    

    client->send(str);

#if DEBUG == true
        std::cout << str << std::endl;
#endif

    return true;
}

bool getValveCallback(kuka_ros::getValve::Request &req, kuka_ros::getValve::Response &res)
{
    client->send("GetValve");
    std::string msg;
    getReply(msg);

    if(msg.empty())
    {
        ROS_ERROR("Error: Got empty msg!");
        return false;
    }
    else
    {
#if DEBUG == true
            std::cout << msg << std::endl;
#endif

        if(msg.find("GetValve{") == std::string::npos){
            ROS_ERROR("Error: Did not get the correct reply!");
            return false;
        }
        // Isolate data
        int i = 0;
        while(msg[i++] != '{');
        msg = msg.substr(i, msg.size()-i);
        i = 0;
        while(msg[i++] != '}');
        msg = msg.substr(0, i-1);

        // Get data
        std::stringstream ss(msg);
        int d = 0;
        i = 0;
        while(ss >> d)
        {
            res.valve[i++] = d > 0;
            if(ss.peek() == ',' || ss.peek() == ' ')
                ss.ignore();
        }

        return true;
    }
}

// Creates the tcp connection and intercepts the messages
// main purpose is to monitor for unrequested topic messages for topics
void TCPhandler(ros::Publisher *pub_isConnected, ros::Publisher *pub_getConfiguration){
    std::list< std::string > packets;
    std::string partial_packet = "";
    while(!shutdownindicator){
        std::string msg = "";
	ssize_t len;
	if(packets.size() == 0){
            try {
                len = client->recv(msg, 150);
            } catch (SocketException &err) {
                ROS_INFO("Error: %s", err.what());
                shutdownindicator = true;
                continue;
            }
            if(len < 1 || msg.empty()){
                continue;
            }
            msg = msg.substr(0, len);
#if DEBUG == true
            std::cout << "Message recieved: " << msg << std::endl;
#endif
        }
        // Implement a package seperator scheme
        // this may also be needed on the recieve side if we should spam it...
        // package seperation is based on each package being surrounded by ( DATA )
        partial_packet += msg;
        // find start and end and remove those
        int start = -1, end = -1, last_end = -1;
        for(int i = 0; i < static_cast<int>(partial_packet.size()); i++){
             if(partial_packet[i] == '('){
                 start = i;
             } else if(partial_packet[i] == ')'){
                 end = i;
             }
             if(start < end && start >= 0 && end > 0){
                 packets.push_back(partial_packet.substr(start+1, end-1));
#if DEBUG == true
                 std::cout << "Added packet: " << packets.back() << " to queue." << std::endl;
#endif
                 last_end = end;
                 start = -1;
                 end = -1;
             }
        }
        if(last_end > 0){
            partial_packet = partial_packet.substr(last_end+1, std::string::npos);
        }

        if(packets.size() == 0){
            continue;
	}
	msg = packets.front();
	packets.pop_front();
        // check for topic messages.
        size_t pos_robot_connection_lost = msg.find("PushIC"); // Is Connected
        size_t pos_robot_get_config = msg.find("PushRC:Q"); // Robot Configuiguration
	size_t pos_robot_error = msg.find("error");
        if(pos_robot_connection_lost != std::string::npos){
#if DEBUG == true
            std::cout << "Recieved Push IC request." << std::endl;
#endif
            std_msgs::Bool ros_msg;
            if(msg[pos_robot_connection_lost + 7] == 'T'){
#if DEBUG == true
                ROS_INFO("Connection to robot is established.");
#endif
                ros_msg.data = true;
            } else {
#if DEBUG == true
                ROS_INFO("Connection to robot was lost.");
#endif
                ros_msg.data = false;
            }
            pub_isConnected->publish(ros_msg);
        } else if(pos_robot_get_config != std::string::npos){
#if DEBUG == true
            std::cout << "Recieved Push RC request: " << msg << std::endl;
#endif
            kuka_ros::robotConfiguration ros_msg;
            // Isolate data
            int i = pos_robot_get_config;
            while(msg[i++] != '{' && i < static_cast<int>(msg.size()));
            int k = i;
            while(msg[k++] != '}' && k < static_cast<int>(msg.size()));
            msg = msg.substr(i, k-1);

#if DEBUG == true
            std::cout << "Reduced msg to: " << msg << std::endl;
#endif	    
            
            // Get data
            std::stringstream ss(msg);
            double d = 0;
            i = 0;
            while(ss >> d)
            {
                ros_msg.q[i++] = d;
                if(ss.peek() == ',' || ss.peek() == ' ')
                    ss.ignore();
            }
            pub_getConfiguration->publish(ros_msg);
        } else if(pos_robot_error != std::string::npos){
            // got an error...
            ROS_ERROR("Error: Got a error message from the Kuka Server.");
        }else {
            // otherwise put it on the queue
#if DEBUG == true
            std::cout << "Taking the message protection." << std::endl;
#endif	    
            message_protection.lock();
            message_queue.push_back(msg);
            message_protection.unlock();
#if DEBUG == true
            std::cout << "Released the message protection." << std::endl;
#endif	    
        }
    }
    ROS_INFO("Shutdown TCP thread communicating with the server.");
}

int main()
{
    shutdownindicator = false;
    // Setup ROS Arguments
    char** argv = NULL;
    int argc = 0;

    // Init ROS Node
    ros::init(argc, argv, "Kuka_ROS");
    ros::NodeHandle nh;
    ros::NodeHandle pNh("~");

    // Topic names
    std::string servicePrefixName, serverIP;
    int serverPort;
    pNh.param<std::string>("CmdServiceName", servicePrefixName, "/rc/KukaNode");
    pNh.param<std::string>("ServerIP", serverIP, TCP_IP_SERVER);
    pNh.param<int>("ServerPort", serverPort, TCP_PORT_SERVER);
    
    // Setup TCP Client
    ros::Rate loop_rate(1);
    while(client == NULL){
        try {
            client = new TCPSocket(serverIP, serverPort);
        } catch (SocketException &err) {
            ROS_INFO("TCP connection not established. Trying again in a moment. Error: %s",err.what());
	    delete client;
            client = NULL;
	    loop_rate.sleep();
        }
    }
    // Inform
    ROS_INFO("Successfully connected to server!");

    // Create service handlers
    ros::ServiceServer getConfigurationService = nh.advertiseService(servicePrefixName + "/GetConfiguration", getConfigurationCallback);
    ros::ServiceServer setConfigurationService = nh.advertiseService(servicePrefixName + "/SetConfiguration", setConfigurationCallback);
    ros::ServiceServer getQueueSizeService = nh.advertiseService(servicePrefixName + "/GetQueueSize", getQueueSizeCallback);
    ros::ServiceServer stopRobotService = nh.advertiseService(servicePrefixName + "/StopRobot", stopRobotCallback);
    ros::ServiceServer isMovingService = nh.advertiseService(servicePrefixName + "/IsMoving", isMovingCallback);
    ros::ServiceServer getSafetyService = nh.advertiseService(servicePrefixName + "/GetSafety", safetyCallback);
    ros::ServiceServer actuateValve = nh.advertiseService(servicePrefixName + "/ActuateValve", actuateValveCallback);
    ros::ServiceServer getValve = nh.advertiseService(servicePrefixName + "/GetValve", getValveCallback);
    ros::ServiceServer isConnectedService = nh.advertiseService(servicePrefixName + "/IsConnected", isConnectedCallback);
    ros::Publisher     isConnectedTopic = nh.advertise<std_msgs::Bool>(servicePrefixName + "/IsConnected", 1, true);
    ros::Publisher     getConfigurationTopic = nh.advertise<kuka_ros::robotConfiguration>(servicePrefixName + "/GetConfiguration", 1, true);

    {
        std_msgs::Bool ros_msg;
        ros_msg.data = false;
        isConnectedTopic.publish(ros_msg);
    }


    // start the tcp monitoring thread
    std::thread tcp_thread(TCPhandler, &isConnectedTopic, &getConfigurationTopic);
    
    // Sleep rate
    ros::Rate r(100);

    // Set loop rate
    while(ros::ok() && shutdownindicator == false)
    {
        ros::spinOnce();
        r.sleep();
    }
    
    shutdownindicator = true;
    client->closeConnection();
    tcp_thread.join();

    ROS_INFO("Program ended.");

    return 0;
}
