# Smart Camera

```sh
roslaunch smart_camera smartCamera.launch
```
Currently the camera is given the address 192.168.100.10:9600 and communicates with (you) 192.168.100.40:9600.

Please see "FQ2-S/CH Series Smart Camera User's Manual for Communications Settings" chapter 3.4 for the data protocols and what the actual functionality of the services/topics are.

## Topics

 - /rc/smartCamera/legoBricks
 
LegoBricks currently returns the number of bricks, the tranformation (translation and rotation in rad in RPY format) with respect to the robot base (T_robotbase^legobrick) and the color(/type) of the brick.
 
## Sevices

 - /rc/smartCamera/automaticMode
 - /rc/smartCamera/clearErrors
 - /rc/smartCamera/echo
 - /rc/smartCamera/getMode
 - /rc/smartCamera/getScene
 - /rc/smartCamera/get_loggers
 - /rc/smartCamera/inputStatus
 - /rc/smartCamera/resetCamera
 - /rc/smartCamera/setMode
 - /rc/smartCamera/setScene
 - /rc/smartCamera/setSearchPreferences
 - /rc/smartCamera/set_logger_level
 - /rc/smartCamera/takeContinousImage
 - /rc/smartCamera/takeSingleImage


Node the function .../echo is quite usefull for testing the connection, the camera returns the given string to the sender.
But plz keep the string short - don't write a novel...

.../takeSingleImage and takeContinousImage and the automaticMode make the camera take images and output the calculated lego transform on the legoBricks topic.
AutomaticMode is set accordingly, 0 = manual mode, 1 = full automatic (searches for all 3 colors) and 2 searches only for the colors specified with setSearchPreferences.

Most of the topics are disabled while not in manual mode (automaticMode = 0).
In manual mode you can change between the scenes or get the current scene using the services .../getScene or .../setScene.


## Calibration
The camera node currently loads the calibration data in on start and performs the calibration every time launched.
All relevant data (wc, tcp frame, etc) concerning the calibration is specified in the launch file.
The status of the calibration is then output.
If anythin with the calibration fails, this will result in no lego brick transformations being published on the relevant topic.
The loading (and saving) of the calibration data is currently not supported.

