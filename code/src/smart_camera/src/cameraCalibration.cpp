/*
 * Copyright 2016 Lukas Schwartz <lschw12@student.sdu.dk>  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#include "cameraCalibration.hpp"

#include <boost/filesystem.hpp>

#include <rw/kinematics/Frame.hpp>
#include <rw/kinematics/State.hpp>
#include <rw/models/WorkCell.hpp>
#include <rw/models/Device.hpp>
#include <rw/math/RPY.hpp>
#include <rw/loaders/WorkCellFactory.hpp>
#include <rw/loaders/WorkCellLoader.hpp>

#include <sstream>
#include <fstream>
#include <iostream>
#include <list>


#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Jacobi>
#include <eigen3/Eigen/SVD>


#define DEBUG false

CameraCalibration::CameraCalibration():
    _tcpName(""), _workcellPath(""), _deviceName("")
{}


void CameraCalibration::loadData(const std::string &filename, std::vector<CameraCoordinate> &camdata, std::vector<rw::math::Q> &robdata) const {
    if(!boost::filesystem::exists(filename)){
        throw std::runtime_error("File attempted to load data from invalid.");
    }
    // calibration data: u, v, rotation, A1, ..., A6
    std::list< rw::math::Q > tmpQlist;
    std::list< CameraCoordinate > tmpCamlist;

    std::fstream file(filename, std::fstream::in);

    int row = 0;
    while(!file.eof()){
        std::string line;
        std::getline(file, line);

        int temp_cols = 0;
        std::stringstream stream(line);
        rw::math::Q tmpQ(6);
        CameraCoordinate tmpC;
        while(! stream.eof()){
            double d;
            if(stream.peek() == ' ' || stream.peek() == ','){
                stream.ignore();
                continue;
            }
            if(temp_cols > 9){
                throw std::runtime_error("Invlaid row (" + std::to_string(row) + ") found.");
            }
            stream >> d;
            if(temp_cols == 0){
                tmpC.u = d;
            } else if(temp_cols == 1){
                tmpC.v = d;
            } else if(temp_cols == 2){
                tmpC.angle = d;
            } else {
                tmpQ[temp_cols-3] = d * rw::math::Deg2Rad;
            }
            temp_cols++;
        }
        row++;
        tmpCamlist.push_back(tmpC);
        tmpQlist.push_back(tmpQ);
#if DEBUG == true
    std::cout << "Saved the row " << row << " containing: {" << tmpC.u << ", " << tmpC.v << ", " << tmpC.angle << "}, " << tmpQ << std::endl;
#endif
    }

    camdata.clear();
    robdata.clear();
    camdata.insert(camdata.begin(), tmpCamlist.begin(), tmpCamlist.end());
    robdata.insert(robdata.begin(), tmpQlist.begin(), tmpQlist.end());
#if DEBUG == true
    std::cout << "Loaded " << row << " data points." << std::endl;
#endif
}


void CameraCalibration::loadCalibrationParameters(const std::string &filename, CalibrationDataSimple &calibParameters){
    throw std::runtime_error("Loading of calibration file not supported yet.");
}


void CameraCalibration::calibrateCamera(const std::string &dataPath, CalibrationDataSimple &calibParameters){
    std::vector<CameraCoordinate> camdata;
    std::vector<rw::math::Q> robdata;
//    rw::geometry::Plane plane;
    std::vector< rw::math::Transform3D<> > transforms;
    loadData(dataPath, camdata, robdata);
    Inference inference;
    convertToTransforms(robdata, transforms);

    // ---- Translation ----
    /// Solving the eq. P = [O U V] [1, u, v] = A x -> A = P x^(-1) = P (x^T x)^(-1) x^T
    /// where: P = the 3D point
    ///        u, v = camera cordinates
    ///        O = 3D point corresponding to [u, v] = [0, 0]
    ///        U, V = Direction corresponding to the direction of u and v in the image.
    Eigen::Matrix3Xd P, x;
    P.resize(Eigen::NoChange, camdata.size());
    x.resize(Eigen::NoChange, camdata.size());

    for(size_t i = 0; i < camdata.size(); i++){
        P.block<3,1>(0,i) = transforms[i].P().e();
        x(0,i) = 1;
        x(1,i) = camdata[i].u;
        x(2,i) = camdata[i].v;
    }

    //When A has linearly independent rows (matrix A ∗ A^{T}is invertible), A^{+} can be computed as:
    Eigen::Matrix3d A = P * x.transpose() * (x * x.transpose()).inverse();
    // This is a right inverse, as A * A^{+} = I.

    for(size_t i = 0; i < 3; i++){
        calibParameters.origo[i] = A(i,0);
        calibParameters.udir[i] = A(i,1);
        calibParameters.vdir[i] = A(i,2);
    }
    rw::math::cross(calibParameters.udir, calibParameters.vdir,calibParameters.normal); // pointing into conveyor
    calibParameters.normal /= calibParameters.normal.norm2();
#if DEBUG == true
    std::cout << "Estimated the following parameters:"
              << "\n\t- Origo : " << calibParameters.origo
              << "\n\t- U-dir : " << calibParameters.udir
              << "\n\t- V-dir : " << calibParameters.vdir
              << "\n\t- Normal: " << calibParameters.normal
              << std::endl;
    std::cout << "Finished estimating the translation." << std::endl;
#endif

    // ---- Rotation ----
    /// It is known that: R I = A, where R is the rotation needed to transform the world coordinate system,
    /// that is I, into our conveyor coordinate system A.
    /// Hence R = A. Since

    rw::math::Rotation3D<> R(calibParameters.udir / calibParameters.udir.norm2(), calibParameters.vdir / calibParameters.vdir.norm2(), calibParameters.normal / calibParameters.normal.norm2());

    calibParameters.zeroRotation = R;

#if DEBUG == true
    rw::math::RPY<> rpy_rot(calibParameters.zeroRotation);
    std::cout << "The zero rotation was found to be: " << rpy_rot[0] * rw::math::Rad2Deg << ", " << rpy_rot[1] * rw::math::Rad2Deg << ", " << rpy_rot[2] * rw::math::Rad2Deg << std::endl;
    std::cout << "Finished estimating the rotation." << std::endl;
#endif


    evaluateCalibration(transforms, camdata, calibParameters, inference);

    std::cout << "The translational part of the calibration fitted:"
              << "\n\t- mean " << inference.mean
              << "\n\t- var " << inference.variance
              << "\n\t- min " << inference.min
              << "\n\t- max " << inference.max
              << std::endl;
}


rw::math::Transform3D<> CameraCalibration::projectData(const CameraCoordinate &coordinate, const CalibrationDataSimple &calibParameters){
#if DEBUG == true
    std::cout << "Projecting [u,v,a] to Transform3D." << std::endl;
#endif
    rw::math::Transform3D<> data;
    data.P() = calibParameters.origo + coordinate.u * calibParameters.udir + coordinate.v * calibParameters.vdir;
    rw::math::RPY<> rpy(coordinate.angle, 0.0, 0.0);
    data.R() = calibParameters.zeroRotation * rpy.toRotation3D();

    return data;
}

void CameraCalibration::evaluateCalibration(const std::vector<rw::math::Transform3D<> > &samples, const std::vector<CameraCoordinate> &camdata, const CalibrationDataSimple &calib, Inference &inferenceTranslation){
#if DEBUG == true
    std::cout << "Evaluating the Calibration." << std::endl;
#endif
    inferenceTranslation.max = 0;
    inferenceTranslation.min = 0;
    inferenceTranslation.mean = 0;
    inferenceTranslation.variance = 0;
    std::string performance = "Camera Calibration reprojection error:\n";

    size_t numb = samples.size();
    for(size_t s = 0; s < numb; s++){
        rw::math::Transform3D<> prediction = projectData(camdata[s], calib);
        double dist = rw::math::MetricUtil::dist2(prediction.P(), samples[s].P());
        performance += "\tIdx ";
        if(s < 10){
            performance += "0";
        }
        performance += std::to_string(s) + ", " + std::to_string(dist) + "\n";
        if(s == 0 || dist > inferenceTranslation.max){
            inferenceTranslation.max = dist;
        }
        if(s == 0 || dist < inferenceTranslation.min){
            inferenceTranslation.min = dist;
        }
        inferenceTranslation.mean += dist;
        inferenceTranslation.variance += dist*dist;
    }
    
    std::cout << performance << std::endl;
    
    inferenceTranslation.mean /= numb;
    inferenceTranslation.variance /= numb;
    inferenceTranslation.variance -= (inferenceTranslation.mean * inferenceTranslation.mean);
#if DEBUG == true
    std::cout << "Calibration evaluation done." << std::endl;
#endif
}


void CameraCalibration::convertToTransforms(const std::vector<rw::math::Q> &angles, std::vector<rw::math::Transform3D<> > &transforms) const {
#if DEBUG == true
    std::cout << "Converting all Q's to transforms." << std::endl;
#endif
    if(_tcpName == "" || _workcellPath == "" || _deviceName == ""){
        throw std::runtime_error("Device, WC and TCP needs to be specefied before execution.");
    }
    transforms.resize(angles.size());

    // load the wc
    if(!boost::filesystem::exists(_workcellPath)){
        throw std::runtime_error("Workcell attempting to load does not exist.");
    }
    rw::models::WorkCell::Ptr wc = rw::loaders::WorkCellFactory::load(_workcellPath);
    if(wc == NULL){
        throw std::runtime_error("Workcell could not be loaded.");
    }
    // state
    rw::kinematics::State state = wc->getDefaultState();
    // device
    rw::models::Device::Ptr device = wc->findDevice(_deviceName);
    if(device == NULL){
        throw std::runtime_error("Could not find the device in the WC.");
    }
    if(device->getDOF() != angles[0].size()){
        throw std::runtime_error("Angles and DOF's of device did not match.");
    }
    // tcp
    rw::kinematics::Frame::Ptr tcp = wc->findFrame(_tcpName);
    if(tcp == NULL){
        throw std::runtime_error("Could not find tcp frame in WC.");
    }

    // find the transforms
//#pragma omp parallel for
    for(size_t i = 0; i < angles.size(); i++){
        rw::kinematics::State tmpstate = state;
        device->setQ(angles[i], tmpstate);
        transforms[i] = device->baseTframe(&*tcp, tmpstate);
    }
#if DEBUG == true
    std::cout << "Q to transform conversions done." << std::endl;
#endif
}
