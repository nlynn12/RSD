/*
 * Copyright 2016 Lukas Schwartz <lschw12@student.sdu.dk>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <ros/ros.h>

#include <string>
#include <stdexcept>
#include <list>
#include <vector>
#include <mutex>
#include <chrono>
#include <ratio>

#include <boost/thread.hpp>

#include "smart_camera/takeSingleImage.h"
#include "smart_camera/takeContinousImage.h"
#include "smart_camera/resetCamera.h"
#include "smart_camera/clrError.h"
#include "smart_camera/echo.h"
#include "smart_camera/getScene.h"
#include "smart_camera/setScene.h"
#include "smart_camera/mode.h"
#include "smart_camera/setMode.h"
#include "smart_camera/inputTransState.h"
#include "smart_camera/legoBricks.h"
#include "smart_camera/setSearchPreferences.h"
#include "smart_camera/setAutomaticMode.h"
#include "smart_camera/setDetectionTreshold.h"

#include "smartCameraProtocol.hpp"
#include "cameraCalibration.hpp"
#include "UDP.h"

#include <rw/math/RPY.hpp>
#include <rw/math/Transform3D.hpp>

#define DEBUG false

#define OUTPUT_AVG true
#define AVG_OVER 10

#define REPLY_TIMEOUT 0.2 // seconds to wait for response from camera

#define UDP_IP_SERVER "192.168.100.50" // find out which IP to use
#define UDP_IP_CLIENT "192.168.100.10" // find out which IP to use
#define UDP_PORT_SERVER 9600 // default port
#define UDP_PORT_CLIENT 9600 // default port

enum class CameraRunMode {
    MANUAL, AUTOMATIC_FULL, AUTOMATIC_PREFERENCED
};

bool shutdownIndicator = false;
CameraRunMode automaticMode = CameraRunMode::MANUAL;
udp_client_server::udp_client *_client = NULL;
std::vector< int > _searchPreferences;
std::list< rw::math::Transform3D<> > _legoPoses;
std::list< double > _legoPoseCorrelation;
bool _legoPosesUpdated = false;
std::mutex _searchPreferenceMutex, _legoPoseMutex;
int _sceneNumber = -1;
std::vector< double > _detectionTresholds = {0.0, 65.0, 82.0};



void convertToMsg ( const std::list< rw::math::Transform3D<> > &transforms, const std::list< int> &type, const std::list< double > &correlation, smart_camera::legoBricks &msg ) {
    if ( transforms.size() != type.size() && transforms.size() != correlation.size()) {
        throw std::runtime_error (std::to_string(__LINE__) + ": Did not supply equal number of transforms and types." );
    }
    int count = transforms.size();
    msg.brickCount = count;
    msg.type.resize ( count );
    msg.translation.resize ( count );
    msg.rotation.resize ( count );
    msg.correlation.resize ( count );

    std::list< rw::math::Transform3D<> >::const_iterator it_trans = transforms.cbegin();
    std::list< int >::const_iterator it_type = type.cbegin();
    std::list< double >::const_iterator it_corr = correlation.cbegin();
    for ( int i = 0; i < count; i++, it_trans++, it_type++, it_corr++) {
        rw::math::RPY<> rpy ( it_trans->R() );
        msg.type.at(i) = *it_type;
        msg.translation.at(i).x = it_trans->P() [0];
        msg.translation.at(i).y = it_trans->P() [1];
        msg.translation.at(i).z = it_trans->P() [2];
        msg.rotation.at(i).x = rpy[0];
        msg.rotation.at(i).y = rpy[1];
        msg.rotation.at(i).z = rpy[2];
        msg.correlation.at(i) = *it_corr;
    }
}


void removeLowCorrelation ( std::list< rw::math::Transform3D<> > &transforms, std::list< int> &type, std::list< double > &correlation ) {
    if(transforms.size() != type.size() && transforms.size() != correlation.size()){
        throw std::runtime_error(std::to_string(__LINE__) + ": Dimension mismatch.");
    }
    std::list< rw::math::Transform3D<> >::iterator it_tr = transforms.begin();
    std::list< int >::iterator it_ty = type.begin();
    for ( std::list< double >::iterator it_c = correlation.begin(); it_c != correlation.end(); it_c++, it_ty++, it_tr++ ) {
        if ( *it_ty >= 0 && *it_ty < static_cast<int>(_detectionTresholds.size()) && *it_c < _detectionTresholds.at(*it_ty) ) {
            it_tr = transforms.erase ( it_tr );
            it_ty = type.erase ( it_ty );
            it_c = correlation.erase ( it_c );
            it_tr--;
            it_ty--;
            it_c--;
        }
    }
}


bool messageStatus ( std::vector< std::string > &msgs, const int numbExpectedMsgs ) {
    if ( numbExpectedMsgs <= 0 ) {
        throw std::runtime_error (std::to_string(__LINE__) + ": Must expect more than 0 messages." );
    }
    msgs.resize ( numbExpectedMsgs-1 );
    char* cmsg = new char[10];
    for ( int i = 0; i < numbExpectedMsgs; i++ ) {
        size_t msgSize = -1;
        try {
            msgSize = _client->recv ( cmsg, 10 );
        } catch ( std::runtime_error &err ) {
            ROS_INFO_STREAM ( "Recived error: " << err.what() );
            return false;
        }

        if ( msgSize <= 0 ) {
            return false;
        }
        std::string msg = ( std::string ( cmsg ) ).substr ( 0,msgSize );
        if ( msg == "ER" ) {
            return false;
        }
        if ( i == numbExpectedMsgs-1 ) {
            if ( msg != "OK" )
                return false;
        } else {
            msgs.at(i) = msg;
        }
    }
    return true;
}


bool messageStatus() {
    std::vector< std::string > tmp;
    return messageStatus ( tmp, 1 );
}


void udp_server ( std::string udp_ip, int udp_port, CameraCalibration::CalibrationDataSimple *calibData ) {
    udp_client_server::udp_server *server = NULL;
    while ( !shutdownIndicator && server == NULL ) {
        try {
            server = new udp_client_server::udp_server ( udp_ip, udp_port );
        } catch ( std::runtime_error &err ) {
            ROS_INFO_STREAM ( "Could not create udp server, retrying.\nError was: " << err.what() );
            server = NULL;
            usleep ( 500000 ); // µs, wait half a second
        }
    }

    if ( calibData == NULL ) {
        ROS_INFO_STREAM ( "Calibration was not supplied and the pose topic will not be used." );
    } else {
        ROS_INFO_STREAM ( "Calibration data was supplied." );
    }

    // normal stuff
    SmartCameraProtocol protocol;
    std::vector< double > corr;
    std::vector< CameraCalibration::CameraCoordinate > cameraPoses;
    std::vector< rw::math::Transform3D<> > robotTLego;
    char* cmsg = new char[500];

#if OUTPUT_AVG == true
    std::list< SmartCameraProtocol::LEGOPose > camcoords;
#endif
    while ( !shutdownIndicator ) {
        size_t msgSize = -1;
        try {
            msgSize = server->recv ( cmsg, 500 );
        } catch ( std::runtime_error &err ) {
            ROS_ERROR_STREAM ( "Failed with: " << err.what() );
        }

        if ( msgSize <= 0 ) {
            ROS_ERROR_STREAM ( "Recieved empty frame." );
            continue;
        }

        std::list< SmartCameraProtocol::LEGOPose > poses;
        std::string msg = ( std::string ( cmsg ) ).substr ( 0,msgSize );

        try {
            protocol.disectMessage ( msg, poses );
        } catch ( std::runtime_error &err ) {
            ROS_ERROR_STREAM ( "Got Error: " << err.what() << ", on the message: " << msg );
            continue;
        }
        if ( poses.size() > 0 ) {
            /// Calculate the 3D pose
            if ( calibData != NULL ) {
                cameraPoses.clear();
                robotTLego.clear();
                corr.clear();
                cameraPoses.resize ( poses.size() );
                robotTLego.resize ( poses.size() );
                corr.resize ( poses.size() );
                int p = 0;
                for ( std::list< SmartCameraProtocol::LEGOPose >::iterator it = poses.begin(); it != poses.end(); it++, p++) {
                    // convert from LegoPose to CameraData
                    cameraPoses.at(p).u = it->x;
                    cameraPoses.at(p).v = it->y;
                    cameraPoses.at(p).angle = it->angle;
                    robotTLego.at(p) = CameraCalibration::projectData ( cameraPoses.at(p), *calibData );
                    corr.at(p) = it->correlation;
                }
            }
#if OUTPUT_AVG == true
            camcoords.push_back ( poses.front() );
            int size = static_cast<int>(camcoords.size());
            while ( size > AVG_OVER ) {
                size--;
                camcoords.pop_front();
            }
            if(camcoords.size() == AVG_OVER){
                SmartCameraProtocol::LEGOPose avg;
                for ( std::list< SmartCameraProtocol::LEGOPose >::iterator it = camcoords.begin(); it != camcoords.end(); it++ ) {
                    avg.x += it->x;
                    avg.y += it->y;
                    avg.angle += it->angle;
                    avg.correlation += it->correlation;
                }
                avg.x /= AVG_OVER;
                avg.y /= AVG_OVER;
                avg.angle /= AVG_OVER;
                avg.correlation /= AVG_OVER;

                ROS_INFO_STREAM ( "T[" << avg.x << ", " << avg.y << "], A[" << avg.angle << "], C[" << avg.correlation << "]." );
            }
#endif
//            ROS_INFO_STREAM("Recived " << poses.size() << " lego brick poses.");
//            for(std::list< SmartCameraProtocol::LEGOPose >::iterator it = poses.begin(); it != poses.end(); it++){
//                ROS_INFO_STREAM("\tT[" << (*it).x << ", " << (*it).y << "], A[" << (*it).angle << "], C[" << (*it).correlation << "].");
//            }

        }
        _legoPoseMutex.lock();
        _legoPoses.clear();
        _legoPoseCorrelation.clear();
        if(calibData != NULL && robotTLego.size() > 0){
            _legoPoses.assign ( robotTLego.begin(), robotTLego.end() );
            _legoPoseCorrelation.assign ( corr.begin(), corr.end() );
        } 
        robotTLego.clear();
        corr.clear();
        _legoPosesUpdated = true;
        _legoPoseMutex.unlock();
    }

    ROS_INFO_STREAM ( "Closed UDP server thread." );
}


bool takeSingleImageCallback ( smart_camera::takeSingleImage::Request &req, smart_camera::takeSingleImage::Response &res ) {
    res.result = false;
    if ( automaticMode != CameraRunMode::MANUAL ) {
        return false;
    }
    std::string msg;
    try {
        SmartCameraProtocol prot;
        msg = prot.constructMessage ( SmartCameraProtocol::Message::MEASURE );
        _client->send ( msg.c_str(), msg.size() );
        if ( !messageStatus() ) {
            ROS_INFO_STREAM ( "Message was rejected." );
            return false;
        }
        res.result = true;
    } catch ( std::runtime_error &err ) {
        ROS_INFO_STREAM ( "Failed with: " << err.what() );
        return false;
    }
    return true;
}


bool takeContinousImageCallback ( smart_camera::takeContinousImage::Request &req, smart_camera::takeContinousImage::Response &res ) {
    res.result = false;
    if ( automaticMode != CameraRunMode::MANUAL ) {
        return false;
    }
    std::string msg;
    try {
        SmartCameraProtocol prot;
        if ( req.status ) {
            msg = prot.constructMessage ( SmartCameraProtocol::Message::MEASURE_CONT );
        } else {
            msg = prot.constructMessage ( SmartCameraProtocol::Message::MEASURE_STOP );
        }
        _client->send ( msg.c_str(), msg.size() );
        if ( !messageStatus() ) {
            ROS_INFO_STREAM ( "Message was rejected." );
            return false;
        }
        res.result = true;
    } catch ( std::runtime_error &err ) {
        ROS_INFO_STREAM ( "Failed with: " << err.what() );
        return false;
    }
    return true;
}


bool resetCameraCallback ( smart_camera::resetCamera::Request &req, smart_camera::resetCamera::Response &res ) {
    res.result = false;
    if ( automaticMode != CameraRunMode::MANUAL ) {
        return false;
    }
    std::string msg;
    try {
        SmartCameraProtocol prot;
        msg = prot.constructMessage ( SmartCameraProtocol::Message::RESET );
        _client->send ( msg.c_str(), msg.size() );
//        if(messageStatus()){
//            ROS_INFO_STREAM("Message was rejected.");
//            return false;
//        }
        ROS_INFO_STREAM ( "It is currently not possible to verify if the sensor actually restartet..." );
        res.result = true;
    } catch ( std::runtime_error &err ) {
        ROS_INFO_STREAM ( "Failed with: " << err.what() );
        return false;
    }
    return true;
}


bool getSceneCallback ( smart_camera::getScene::Request &req, smart_camera::getScene::Response &res ) {
    res.result = false;
    if ( automaticMode != CameraRunMode::MANUAL ) {
        return false;
    }
    res.scene = -1;
    std::string msg;
    std::vector< std::string > scene;
    try {
        SmartCameraProtocol prot;
        msg = prot.constructMessage ( SmartCameraProtocol::Message::SCENE );
        _client->send ( msg.c_str(), msg.size() );
        if ( !messageStatus ( scene, 2 ) ) {
            ROS_INFO_STREAM ( "Message was rejected." );
            return false;
        }
        res.scene = std::atoi ( scene.at(0).c_str() );
        res.result = true;
        _sceneNumber = res.scene;
    } catch ( std::runtime_error &err ) {
        ROS_INFO_STREAM ( "Failed with: " << err.what() );
        return false;
    }
    return true;
}


bool setSceneCallback ( smart_camera::setScene::Request &req, smart_camera::setScene::Response &res ) {
    res.result = false;
    if ( automaticMode != CameraRunMode::MANUAL ) {
        return false;
    }
    std::string msg;
    std::vector< int > scene = {req.scene};
    try {
        SmartCameraProtocol prot;
        msg = prot.constructMessage ( SmartCameraProtocol::Message::SCENE, scene );
        _client->send ( msg.c_str(), msg.size() );
        if ( !messageStatus() ) {
            ROS_INFO_STREAM ( "Message was rejected." );
            return false;
        }
        res.result = true;
        _sceneNumber = scene.at(0);
    } catch ( std::runtime_error &err ) {
        ROS_INFO_STREAM ( "Failed with: " << err.what() );
        return false;
    }
    return true;
}


bool echoCallback ( smart_camera::echo::Request &req, smart_camera::echo::Response &res ) {
    res.result = false;
    res.echo = "";
    if ( automaticMode != CameraRunMode::MANUAL ) {
        return false;
    }
    std::string msg;
    std::vector< std::string > resp;
    try {
        SmartCameraProtocol prot;
        msg = prot.constructMessage ( SmartCameraProtocol::Message::ECHO, req.shout );
        _client->send ( msg.c_str(), msg.size() );
        if ( !messageStatus ( resp, 2 ) ) {
            ROS_INFO_STREAM ( "Message was rejected." );
            return false;
        }
        res.echo = resp.at(0);
        res.result = true;
    } catch ( std::runtime_error &err ) {
        ROS_INFO_STREAM ( "Failed with: " << err.what() );
        return false;
    }
    return true;
}


bool getCameraModeCallback ( smart_camera::mode::Request &req, smart_camera::mode::Response &res ) {
    res.result = false;
    res.mode = -1;
    if ( automaticMode != CameraRunMode::MANUAL ) {
        return false;
    }
    std::string msg;
    std::vector< std::string > mode;
    try {
        SmartCameraProtocol prot;
        msg = prot.constructMessage ( SmartCameraProtocol::Message::MODE );
        _client->send ( msg.c_str(), msg.size() );
        if ( !messageStatus ( mode, 2 ) ) {
            ROS_INFO_STREAM ( "Message was rejected." );
            return false;
        }
        res.mode = std::atoi ( mode.at(0).c_str() );
        res.result = true;
    } catch ( std::runtime_error &err ) {
        ROS_INFO_STREAM ( "Failed with: " << err.what() );
        return false;
    }
    return true;
}


bool setCameraModeCallback ( smart_camera::setMode::Request &req, smart_camera::setMode::Response &res ) {
    res.result = false;
    if ( automaticMode != CameraRunMode::MANUAL ) {
        return false;
    }
    std::string msg;
    try {
        SmartCameraProtocol prot;
        std::vector<int> mode = {req.mode};
        msg = prot.constructMessage ( SmartCameraProtocol::Message::MODE, mode );
        _client->send ( msg.c_str(), msg.size() );
        if ( !messageStatus() ) {
            ROS_INFO_STREAM ( "Message was rejected." );
            return false;
        }
        res.result = true;
    } catch ( std::runtime_error &err ) {
        ROS_INFO_STREAM ( "Failed with: " << err.what() );
        return false;
    }
    return true;
}


bool clrErrorCallback ( smart_camera::clrError::Request &req, smart_camera::clrError::Response &res ) {
    res.result = false;
    if ( automaticMode != CameraRunMode::MANUAL ) {
        return false;
    }
    std::string msg;
    try {
        SmartCameraProtocol prot;
        msg = prot.constructMessage ( SmartCameraProtocol::Message::CLEARERRORS );
        _client->send ( msg.c_str(), msg.size() );
        if ( !messageStatus() ) {
            ROS_INFO_STREAM ( "Message was rejected." );
            return false;
        }
        res.result = true;
    } catch ( std::runtime_error &err ) {
        ROS_INFO_STREAM ( "Failed with: " << err.what() );
        return false;
    }
    return true;
}


bool inputTransStateCallback ( smart_camera::inputTransState::Request &req, smart_camera::inputTransState::Response &res ) {
    res.result = false;
    res.allowed = false;
    if ( automaticMode != CameraRunMode::MANUAL ) {
        return false;
    }
    std::string msg;
    std::vector< int > protocol = {req.protocol};
    std::vector< std::string > resp;
    try {
        SmartCameraProtocol prot;
        msg = prot.constructMessage ( SmartCameraProtocol::Message::INPUTTRANSSTATE, protocol );
        _client->send ( msg.c_str(), msg.size() );
        if ( !messageStatus ( resp, 2 ) ) {
            ROS_INFO_STREAM ( "Message was rejected." );
            return false;
        }
        if ( resp.at(0) == "1" ) {
            res.allowed = true;
        }
        res.result = true;
    } catch ( std::runtime_error &err ) {
        ROS_INFO_STREAM ( "Failed with: " << err.what() );
        return false;
    }
    return true;
}


bool automaticModeCallback ( smart_camera::setAutomaticMode::Request &req, smart_camera::setAutomaticMode::Response &res ) {
    res.result = false;
    int autoMode = req.mode;
    if(autoMode == 0 && automaticMode != CameraRunMode::MANUAL){
        try {
            SmartCameraProtocol prot;
            std::string msg = prot.constructMessage ( SmartCameraProtocol::Message::MEASURE_STOP );
            _client->send ( msg.c_str(), msg.size() );
            if ( !messageStatus() ) {
                ROS_INFO_STREAM ( "Message was rejected." );
                return false;
            }
        } catch ( std::runtime_error &err ) {
            ROS_ERROR_STREAM ( "Got error: " << err.what() );
            return false;
        }
    }

    bool ret = true;
    res.result = true;
    switch ( autoMode ) {
    case 0:
        automaticMode = CameraRunMode::MANUAL;
        break;
    case 1:
        automaticMode = CameraRunMode::AUTOMATIC_FULL;
        break;
    case 2:
        automaticMode = CameraRunMode::AUTOMATIC_PREFERENCED;
        break;
    default:
        automaticMode = CameraRunMode::MANUAL;
        ROS_ERROR_STREAM ( "Unknown runmode requested." );
        res.result = false;
        ret = false;
        break;
    }


    return ret;
}


bool searchPreferencesCallback ( smart_camera::setSearchPreferences::Request &req, smart_camera::setSearchPreferences::Response &res ) {
    _searchPreferenceMutex.lock();
    _searchPreferences.resize ( req.search.size() );
#if DEBUG == true
    std::cout << "Setting search preferences to: ";
#endif
    for ( size_t i = 0; i < req.search.size(); i++ ) {
        _searchPreferences.at(i) = req.search.at(i);
#if DEBUG == true
        std::cout << _searchPreferences.at(i) << ", ";
#endif
    }
#if DEBUG == true
    std::cout << std::endl;
#endif
    _searchPreferenceMutex.unlock();
    res.result = true;
    return true;
}

bool setDetectionTresholdCallback ( smart_camera::setDetectionTreshold::Request &req, smart_camera::setDetectionTreshold::Response &res ) {
    if ( req.treshold.size() != _detectionTresholds.size() ) {
        res.result = false;
        return false;
    }
    for ( int i = 0; i < static_cast<int> ( req.treshold.size() ); i++ ) {
        _detectionTresholds.at(i) = req.treshold.at(i);
    }
    res.result = true;
    return true;
}


struct AutomaticModeData {
    // runmode properties
    int sendStep = 0, lastSend = -1, recieveStep = 0;
    bool modeshift = false;

    // used for storing/collecting data before it is send
    std::list< rw::math::Transform3D<> > legoposes, poses;
    std::list< int > legotype;
    std::list< double > legocorrelation, legocorrelationRecieved;
    bool legoposesupdated = false;

    std::list< int > scenesSend;
};

void searchmode ( AutomaticModeData &data, ros::Publisher &legoBrickTopic, const std::vector< int > &scenes ) {
    if ( scenes.size() == 0 ) {
        ROS_INFO_STREAM ( "No search preferences set." );
        return;
    }
    // fetch all possible images
    if ( !data.modeshift && static_cast<int> ( data.scenesSend.size() ) < 2 ) {
        std::vector<int> pref;
        int scene = data.sendStep;
        if ( data.sendStep >= static_cast<int> ( scenes.size() ) ) {
            scene = scenes.size() - 1 - ( data.sendStep % scenes.size() );
        }
        int sceneColor = scenes.at(scene);
        pref.push_back ( sceneColor );
        data.scenesSend.push_back ( sceneColor );
        SmartCameraProtocol prot;
        std::string msg;
        if ( data.lastSend != sceneColor ) {
            msg = prot.constructMessage ( SmartCameraProtocol::Message::SCENE, pref );
            _client->send ( msg.c_str(), msg.size() );
            if ( !messageStatus() ) {
                throw std::runtime_error ( "Camera rejected the message: " + msg );
            }
            data.lastSend = sceneColor;
        }
        msg = prot.constructMessage ( SmartCameraProtocol::Message::MEASURE );
        _client->send ( msg.c_str(), msg.size() );
        if ( !messageStatus() ) {
            throw std::runtime_error ( "Camera rejected the message: " + msg );
        }
        data.sendStep = ( data.sendStep + 1 ) % ( scenes.size() * 2 );
    }
    if ( data.legoposesupdated ) {
        data.legoposesupdated = false;
        if(data.poses.size() > 0){
            data.legoposes.insert ( data.legoposes.end(), data.poses.begin(), data.poses.end() );
            data.legotype.insert ( data.legotype.end(), data.poses.size(), data.scenesSend.front() );
            data.legocorrelation.insert ( data.legocorrelation.end(), data.legocorrelationRecieved.begin(), data.legocorrelationRecieved.end() );
            data.poses.clear();
            data.legocorrelationRecieved.clear();
        }
        data.scenesSend.pop_front();
        data.recieveStep++;
    }
    if ( data.recieveStep == static_cast<int> ( scenes.size() ) ) {
        data.recieveStep = 0;
        removeLowCorrelation ( data.legoposes, data.legotype, data.legocorrelation );
        smart_camera::legoBricks msgdata;
        convertToMsg ( data.legoposes, data.legotype, data.legocorrelation, msgdata );
        legoBrickTopic.publish ( msgdata );
        data.legoposes.clear();
        data.legotype.clear();
        data.legocorrelation.clear();
    }

}


void automaticModeControl ( ros::Publisher &legoBrickTopic ) {

    // camera img capture rate is expected to be ~10Hz, this is currently slower when switching between scenes..
    ros::Rate r ( 200 );

    CameraRunMode currentMode = CameraRunMode::MANUAL;

    // runmode properties
    const std::vector< int > fullmode = {0, 1, 2};
    std::vector< int > preferencedmode;

    AutomaticModeData automodedata;

    while ( !shutdownIndicator && ros::ok() ) {
        // make sure it does not stall without a wait, want to be saving that cpu power :)
        r.sleep();
        // make sure no step is skipped due to mode changes
        if ( currentMode != automaticMode ) {
//             ROS_INFO_STREAM("Waiting to switch mode.");
            automodedata.modeshift = true;
            automodedata.sendStep = 0;
            if ( automodedata.scenesSend.size() == 0 ) {
                automodedata.recieveStep = 0;
                automodedata.modeshift = false;
                automodedata.legoposes.clear();
                automodedata.poses.clear();
                automodedata.legotype.clear();
                automodedata.legocorrelation.clear();
                automodedata.legocorrelationRecieved.clear();
                currentMode = automaticMode;
                automodedata.legoposesupdated = false;
                automodedata.lastSend = -1;
                _searchPreferenceMutex.lock();
                preferencedmode = _searchPreferences;
                _searchPreferenceMutex.unlock();
                std::string mode = "N/A";
                if ( currentMode == CameraRunMode::MANUAL ) {
                    mode = "Manual";
                } else if ( currentMode == CameraRunMode::AUTOMATIC_FULL ) {
                    mode = "Auto Full";
                } else if ( currentMode == CameraRunMode::AUTOMATIC_PREFERENCED ) {
                    mode = "Auto Preferenced";
                }
                ROS_INFO_STREAM ( "Shifted to mode: " << mode );
            }
        } else {
            automodedata.modeshift = false;
        }
        try {
            // get the pose infrmation
            automodedata.poses.clear();
            automodedata.legocorrelationRecieved.clear();
            _legoPoseMutex.lock();
            if ( _legoPosesUpdated ) {
                automodedata.poses = _legoPoses;
                automodedata.legocorrelationRecieved = _legoPoseCorrelation;
                _legoPoses.clear();
                _legoPoseCorrelation.clear();
                automodedata.legoposesupdated = true;
                _legoPosesUpdated = false;
            } else {
                automodedata.legoposesupdated = false;
            }
            _legoPoseMutex.unlock();

            // handle the individual modes
            if ( currentMode == CameraRunMode::MANUAL ) {
                automodedata.sendStep = 0;
                if ( automodedata.poses.size() <= 0 || !automodedata.legoposesupdated ) {
                    continue;
                }
                int scene = _sceneNumber;
                // ready the data for output
                smart_camera::legoBricks data;
                automodedata.legotype.assign ( automodedata.poses.size(), scene );
                removeLowCorrelation ( automodedata.poses, automodedata.legotype, automodedata.legocorrelationRecieved );
                convertToMsg ( automodedata.poses, automodedata.legotype, automodedata.legocorrelationRecieved, data );
                legoBrickTopic.publish ( data );

            } else if ( currentMode == CameraRunMode::AUTOMATIC_FULL ) {

                searchmode ( automodedata, legoBrickTopic, fullmode );


            } else if ( currentMode == CameraRunMode::AUTOMATIC_PREFERENCED ) {
                // fetch only wanted images
                if ( automodedata.sendStep == 0 && _searchPreferences.size() == preferencedmode.size() ) {
                    _searchPreferenceMutex.lock();
                    preferencedmode = _searchPreferences;
                    _searchPreferenceMutex.unlock();
                }
                searchmode ( automodedata, legoBrickTopic, preferencedmode );

            } else {
                throw std::runtime_error ( "No valid runmode was chosen." );
            }
        } catch ( std::runtime_error &err ) {
            ROS_ERROR_STREAM ( "Got error: " << err.what() << ". Going back into manual mode." );
            automaticMode = CameraRunMode::MANUAL;
        }
    }
}

int main ( int argc, char *argv[] ) {
    shutdownIndicator = false;
    // Init ROS Node
    ros::init ( argc, argv, "SmartCamera_ROS" );
    ros::NodeHandle nh ( ros::this_node::getName() );

    // Topic names
    std::string udp_server_IP, udp_client_IP;
    std::string calibrationData_file, calibrationData_wc, calibrationData_tcp, calibrationData_robot, calibrationFile;
    int udp_server_port, udp_client_port;
    nh.param<std::string> ( "UDPServerIP", udp_server_IP, UDP_IP_SERVER );
    nh.param<std::string> ( "UDPClientIP", udp_client_IP, UDP_IP_CLIENT );
    nh.param<int> ( "UDPServerPort", udp_server_port, UDP_PORT_SERVER );
    nh.param<int> ( "UDPClientPort", udp_client_port, UDP_PORT_CLIENT );

    // if the calibration file is present (the already calculated calibration parameters), use it.
    nh.param<std::string> ( "CalibrationFile", calibrationFile, "" );
    // if calibration data is present use it for fresh calibration, otherwise skip.
    nh.param<std::string> ( "CalibrationData_file", calibrationData_file, "" );
    nh.param<std::string> ( "CalibrationData_workcell", calibrationData_wc, "" );
    nh.param<std::string> ( "CalibrationData_tcp", calibrationData_tcp, "HGPC16A.TCP" );
    nh.param<std::string> ( "CalibrationData_robot", calibrationData_robot, "KukaKr6R700" );

    CameraCalibration::CalibrationDataSimple *camCalib = NULL;
    if ( calibrationFile != "" ) {
        try {
            camCalib = new CameraCalibration::CalibrationDataSimple();
            CameraCalibration::loadCalibrationParameters ( calibrationFile, *camCalib );
        } catch ( std::runtime_error &err ) {
            ROS_INFO_STREAM ( "Failed the calibration with: " << err.what() );
            delete camCalib;
            camCalib = NULL;
        }
    } else if ( calibrationData_file != "" && calibrationData_wc != "" ) {
        try {
            camCalib = new CameraCalibration::CalibrationDataSimple();
            CameraCalibration cam;
            cam.setDeviceName ( calibrationData_robot );
            cam.setTCPName ( calibrationData_tcp );
            cam.setWcPath ( calibrationData_wc );
            cam.calibrateCamera ( calibrationData_file, *camCalib );
        } catch ( std::runtime_error &err ) {
            ROS_INFO_STREAM ( "Failed the calibration with: " << err.what() );
            delete camCalib;
            camCalib = NULL;
        }
    }

    // Setup UDP threads
    boost::thread udp_server_thread ( udp_server, udp_server_IP, udp_server_port, camCalib );

    while ( !shutdownIndicator && _client == NULL ) {
        try {
            _client = new udp_client_server::udp_client ( udp_client_IP, udp_client_port, REPLY_TIMEOUT );
        } catch ( std::runtime_error &err ) {
            ROS_INFO_STREAM ( "Could not create udp client, retrying.\nError was: " << err.what() );
            _client = NULL;
            usleep ( 500000 ); // µs, wait half a second
        }
    }

    ROS_INFO_STREAM ( "UDP server connection to: " << udp_server_IP << ":" << udp_server_port );
    ROS_INFO_STREAM ( "UDP client connection to: " << udp_client_IP << ":" << udp_client_port );

    // Create service handlers
    ros::ServiceServer takeSingleImageService = nh.advertiseService ( "takeSingleImage", takeSingleImageCallback );
    ros::ServiceServer takeContinuesImageService = nh.advertiseService ( "takeContinousImage", takeContinousImageCallback );
    ros::ServiceServer resetCameraService = nh.advertiseService ( "resetCamera", resetCameraCallback );
    ros::ServiceServer setSceneService = nh.advertiseService ( "setScene", setSceneCallback );
    ros::ServiceServer getSceneService = nh.advertiseService ( "getScene", getSceneCallback );
    ros::ServiceServer echoService = nh.advertiseService ( "echo", echoCallback );
    ros::ServiceServer modeService = nh.advertiseService ( "getMode", getCameraModeCallback );
    ros::ServiceServer setModeService = nh.advertiseService ( "setMode", setCameraModeCallback );
    ros::ServiceServer clrErrorService = nh.advertiseService ( "clearErrors", clrErrorCallback );
    ros::ServiceServer inputTransStateService = nh.advertiseService ( "inputStatus", inputTransStateCallback );
    ros::ServiceServer setDetectionTresholdService = nh.advertiseService ( "setDetectionTresholds", setDetectionTresholdCallback );


    ros::ServiceServer automaticModeService = nh.advertiseService ( "automaticMode", automaticModeCallback );
    ros::ServiceServer searchPreferencesService = nh.advertiseService ( "setSearchPreferences", searchPreferencesCallback );
    ros::Publisher     legoBricks = nh.advertise< smart_camera::legoBricks > ( "legoBricks", 1, false );


    boost::thread automaticmode_thread ( automaticModeControl, legoBricks );


    // Sleep rate
    ros::Rate r ( 100 );

    // Set loop rate
    while ( !shutdownIndicator && ros::ok() ) {
        ros::spinOnce();
        r.sleep();
    }

    shutdownIndicator = true;

    udp_server_thread.interrupt();
    automaticmode_thread.join();

    ROS_INFO_STREAM ( "Program ended." );

    return 0;
}
