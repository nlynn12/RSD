/*
 * Copyright 2016 Lukas Schwartz <lschw12@student.sdu.dk>  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#pragma once
 
#include <string>
#include <vector>
#include <list>
 

class SmartCameraProtocol {
public:
	
	enum class Message {
        CLEARERRORS, MEASURE, MEASURE_CONT, MEASURE_STOP, RESET, MODE, SCENE, ECHO, INPUTTRANSSTATE
	};

	
    struct LEGOPose {
        double x = -1, y = -1, angle = 0, correlation = 0;
        int idx = 0;
    };


	SmartCameraProtocol();
	
	std::string constructMessage(const Message msgCode, const std::vector< int > &parameters = std::vector< int >());

	std::string constructMessage(const Message msgCode, const std::string option);
	
    void disectMessage(const std::string &msg, std::list< LEGOPose > &replies);
		
	void setCR(const char cr){
		_CR = cr;
	}

	void setLF(const char lf){
		_LF = lf;
	}

	void setDEL(const char del){
		_DEL = del;
	}

	void setFF(const char ff){
		_FF = ff;
	}

	
protected:
	
	std::string getCommand(const Message msgCode);
	
	char _CR, _LF, _DEL, _FF, _SPACE;
	
};
