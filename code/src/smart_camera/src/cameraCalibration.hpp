/*
 * Copyright 2016 Lukas Schwartz <lschw12@student.sdu.dk>  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#pragma once

#include <string>
#include <vector>

#include <rw/math/Vector3D.hpp>
#include <rw/math/Q.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/geometry/Plane.hpp>


class CameraCalibration
{
public:

    struct CalibrationDataSimple {
        /// u = x, v = y directions. Vectors are scaled to account for the 'mapping'.
        /// Normal points into the conveyor.
        /// Zero rotatation is the rotation corresponding to the lego brick being parallel to v(/y direction).
        /// Rotations of the bricks is then performed as a rotation around the normal originating in the zeroRotation.
        /// Consult Lukas for questions, he designed this shit :P
        /// Currently it assumes the cmera is parallel to the conveyor (which is not the case...)!
        rw::math::Vector3D<> origo, udir, vdir, normal;
        rw::math::Rotation3D<> zeroRotation;
    };


    struct Inference {
        double mean = 0, variance = 0, max = 0, min = 0;
    };

    struct CameraCoordinate {
        double u, v, angle;
    };

    CameraCalibration();

    void setTCPName(const std::string &tcpname){
        _tcpName = tcpname;
    }

    void setDeviceName(const std::string &devname){
        _deviceName = devname;
    }

    void setWcPath(const std::string &path){
        _workcellPath = path;
    }

    static void loadCalibrationParameters(const std::string &filename, CalibrationDataSimple &calibParameters);

    void calibrateCamera(const std::string &dataPath, CalibrationDataSimple &calibParameters);

    static rw::math::Transform3D<> projectData(const CameraCoordinate &coordinate, const CalibrationDataSimple &calibParameters);

protected:

    void loadData(const std::string &filename, std::vector< CameraCoordinate > &camdata, std::vector< rw::math::Q > &robdata) const;

    /**
     * @brief convertToTransforms
     * @param angles The angle of the given joint in radiance.
     * @param transforms
     */
    void convertToTransforms(const std::vector<rw::math::Q> &angles, std::vector< rw::math::Transform3D<> > &transforms) const;

    void evaluateCalibration(const std::vector< rw::math::Transform3D<> > &samples, const std::vector< CameraCoordinate > &camdata, const CalibrationDataSimple &calib, Inference &inference);


    std::string _tcpName, _workcellPath, _deviceName;


};
