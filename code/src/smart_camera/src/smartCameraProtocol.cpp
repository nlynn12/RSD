/*
 * Copyright 2016 Lukas Schwartz <lschw12@student.sdu.dk>  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#include "smartCameraProtocol.hpp"

#include <sstream>
#include <cmath>
#include <stdexcept>

SmartCameraProtocol::SmartCameraProtocol():
	_CR('\x0D'), _LF('\x0A'), _DEL('\x7F'), _FF('\xFF'), _SPACE(' ')
{
	
}

std::string SmartCameraProtocol::getCommand(const Message msgCode){
    std::string msg;
	
    switch (msgCode) {
    case Message::CLEARERRORS:
        msg = "CLRERR";
        break;
    case Message::MEASURE:
        msg = "MEASURE";
        break;
    case Message::MEASURE_CONT:
        msg = "MEASURE /C";
        break;
    case Message::MEASURE_STOP:
        msg = "MEASURE /E";
        break;
    case Message::RESET:
        msg = "RESET";
        break;
    case Message::MODE:
        msg = "MODE";
        break;
    case Message::SCENE:
        msg = "SCENE";
        break;
    case Message::ECHO:
        msg = "ECHO";
        break;
    case Message::INPUTTRANSSTATE:
        msg = "ITS";
        break;
    default:
        throw("Invalid message code given, the message code has not been defined.");
        break;
    };
    return msg;
}

	
std::string SmartCameraProtocol::constructMessage(const Message msgCode, const std::vector< int > &parameters){
    std::string msg = getCommand(msgCode);

    for(int i = 0; i < static_cast<int>(parameters.size()); i++){
        msg += _SPACE + std::to_string(parameters[i]);
    }
    // msg += _CR;
    return msg;
}

std::string SmartCameraProtocol::constructMessage(const Message msgCode, const std::string option){
    std::string msg = getCommand(msgCode) + _SPACE + option;// + _CR;
    return msg;
}



void SmartCameraProtocol::disectMessage(const std::string &msg, std::list<LEGOPose> &replies) {
    std::stringstream ss(msg);
    double d = 0;
    int i = 0;
    int count = 0;
    LEGOPose pose;
    ss >> d;
    int numberOfPoses = static_cast<int>(d);

    while(ss >> d){
        if(i == 0){
            pose.x = d;
            pose.idx = count;
        } else if(i == 1){
            pose.y = d;
        } else if(i == 2){
            pose.angle = d * M_PI / 180.0;
        } else if(i == 3){
            pose.correlation = d;
            replies.push_back(pose);
        } else {
            throw std::runtime_error("Invalid format recieved from camera.");
        }
        i++;
        if(i == 4){
            i = 0;
        }
        if(ss.peek() == ','){
            ss.ignore();
            break;
        }
        if(ss.peek() == ' '){
            ss.ignore();
        }
    }
    if(static_cast<int>(replies.size()) != numberOfPoses){
        throw std::runtime_error("Recieved wrong number of poses.");
    }
}
	
	
	
