#!/usr/bin/env python

 # Copyright 2016 Nicolai Lynnerup <nlynn12@student.sdu.dk>
 #
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 #
 #     http://www.apache.org/licenses/LICENSE-2.0
 #
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.

import rospy
from std_msgs.msg import String

import socket
import random
import time

IDLE = 0
REQUEST = 1
SUSPEND = 2

#0 and 1 only
mobileNumber = 1


accept_new_tasks = False
at_feeder = False
at_home = False
bricks_empty = False
at_robot_cell = False


port = 21212
#hostname = socket.gethostbyname('robolab01.projectnet.wlan.sdu.dk')

#mobile commands to get from server
commandListServer_mobile = ["No_task", "New_task_del", "New_task_recv", "Wait_for_free_feeder", "Get_bricks", "Wait_for_accept_drone", "Move_to_cell_1_del", "Move_to_cell_2_del", "Empty_bricks", "Move_home", "Move_to_cell_1_recv", "Move_to_cell_2_recv", "Wait_for_accept_robot"]

#mobile commands to send to server
commandListClient_mobile = ["Idle", "Suspended", "Request_task", "Accept_del", "Reject_del", "Accept_recv", "Reject_recv", "Waiting_for_free_feeder", "Moving_to_feeder", "Waiting_for_accept_drone", "Waiting_for_accept_drone_1", "Waiting_for_accept_drone_2", "Moving_to_cell_1_del", "Moving_to_cell_2_del", "Emptying_bricks", "Bricks_empty", "Moving_home", "Complete", "Moving_to_cell_1_recv", "Moving_to_cell_2_recv", "Waiting_for_accept_robot"]


def processState(msg):
    if(msg.data == "ACCEPT_TASKS"):
        global accept_new_tasks
        accept_new_tasks = True
    if(msg.data == "REJECT_TASKS"):
        global accept_new_tasks
        accept_new_tasks = False
    if(msg.data == "AT_FEEDER"):
	global at_feeder
	at_feeder = True
    if(msg.data == "AT_HOME"):
	global at_home
	at_home = True
    if(msg.data == "BRICKS_EMPTY"):
	global bricks_empty
	bricks_empty = True
    if(msg.data == "AT_ROBOT_CELL"):
	global at_robot_cell
	at_robot_cell = True 

#Log info
def command(command ,id, comment):  
    str = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
    str += "<log_entry>"
    str += "<event>%s</event>" % commandListClient_mobile[command]
    str += "<time>%s</time>" % time.ctime() 
    str += "<cell_id>%s</cell_id>" % id
    str += "<comment>%s</comment>" % comment
    str += "</log_entry>"
    return str

#TCP/IP client
def sendStatus(string):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((hostname, port))

    sock.send(string)
    reply = sock.recv(16384)  # limit reply to 16K
    sock.close()
    return reply


def processReply(string):
    start = string.find("<event>")+7
    end = string.find("</event>")
    return string[start:end]

def findState(string):
    state = commandListServer_mobile.index(string)
    return state
    
def main():

    global accept_new_tasks
    global at_feeder
    global at_home
    global bricks_empty
    global at_robot_cell

    rospy.init_node("mes_mr_client", anonymous=True)
    state_pub = rospy.Publisher("mes_mr_client", String,queue_size=1, latch=True)
    process_node_subscriber = rospy.Subscriber("topModule/getCurrentState", String, processState)
    
    rate = rospy.Rate(1.0/3.0)
    
    init()
    
    nxt_state = REQUEST

    #Startup and go to idle
    str = command(commandListClient_mobile.index("Idle"),mobileNumber,"Idle")   
    reply = sendStatus(str)
    old_state = 3
    state = findState(processReply(reply))
    
    while not rospy.is_shutdown():
        if(state == commandListServer_mobile.index("No_task")):    
            #idle, Request new task or suspend
            if(nxt_state == IDLE):
                str = command(commandListClient_mobile.index("Idle"),mobileNumber,"Idle")
            elif(nxt_state == REQUEST):
                str = command(commandListClient_mobile.index("Request_task"),mobileNumber,"Request a new task")
            elif(nxt_state == SUSPEND):
                str = command(commandListClient_mobile.index("Suspended"),mobileNumber,"Suspend mobile")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))

        elif(state == commandListServer_mobile.index("New_task_del")):
            #Accept or reject new tak
            if(accept_new_tasks == True):
                str = command(commandListClient_mobile.index("Accept_del"),mobileNumber,"Accept new task")
            elif(accept_new_tasks == False):
                str = command(commandListClient_mobile.index("Reject_del"),mobileNumber,"Reject new task")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))

        elif(state == commandListServer_mobile.index("New_task_recv")):
            #Accept or reject new tak
            if(accept_new_tasks == True):
                str = command(commandListClient_mobile.index("Accept_recv"),mobileNumber,"Accept new task")
            elif(accept_new_tasks == False):
                str = command(commandListClient_mobile.index("Reject_recv"),mobileNumber,"Reject new task")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))

        elif(state == commandListServer_mobile.index("Wait_for_free_feeder")):
            #Wait for the feeder to become free
            str = command(commandListClient_mobile.index("Waiting_for_free_feeder"),mobileNumber,"Waiting for Task")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))
            
        elif(state == commandListServer_mobile.index("Get_bricks")):
            #Move to feeder
            str = command(commandListClient_mobile.index("Moving_to_feeder"),mobileNumber,"Speeding toward feeder")
            reply = sendStatus(str)
            testcounter = testcounter + 1
            print processReply(reply)

            ### Simulating until movement is complete
            if( at_feeder == True):
                #complete and send status to server
                at_feeder = False
                str = command(commandListClient_mobile.index("Waiting_for_accept_drone"),mobileNumber,"Waiting to get bricks")
                reply = sendStatus(str)
                print processReply(reply)
                state = findState(processReply(reply))

        elif(state == commandListServer_mobile.index("Wait_for_accept_drone")):
            #Wait for the feeder to be filled
            str = command(commandListClient_mobile.index("Waiting_for_accept_drone"),mobileNumber,"Waiting to get bricks")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))


        elif(state == commandListServer_mobile.index("Move_to_cell_1_del") or state == commandListServer_mobile.index("Move_to_cell_2_del") ):
            #Move to robot cell
            if(state == commandListServer_mobile.index("Move_to_cell_1_del")):
                str = command(commandListClient_mobile.index("Moving_to_cell_1_del"),mobileNumber,"Speeding toward cell 1")
            elif(state == commandListServer_mobile.index("Move_to_cell_2_del")):
                str = command(commandListClient_mobile.index("Moving_to_cell_2_del"),mobileNumber,"Speeding toward cell 2")
                
            reply = sendStatus(str)
            testcounter = testcounter + 1
            print processReply(reply)
		
            ### Simulating until movement is complete
            if( at_robot_cell == True):
                #complete and send status to server
                at_robot_cell = False
                str = command(commandListClient_mobile.index("Emptying_bricks"),mobileNumber,"Emptying bricks in cell")
                reply = sendStatus(str)
                print processReply(reply)
                state = findState(processReply(reply))

        elif(state == commandListServer_mobile.index("Move_to_cell_1_recv") or state == commandListServer_mobile.index("Move_to_cell_2_recv") ):
            #Move to robot cell
            if(state == commandListServer_mobile.index("Move_to_cell_1_recv")):
                str = command(commandListClient_mobile.index("Moving_to_cell_1_recv"),mobileNumber,"Speeding toward cell 1")
            elif(state == commandListServer_mobile.index("Move_to_cell_2_del")):
                str = command(commandListClient_mobile.index("Moving_to_cell_2_recv"),mobileNumber,"Speeding toward cell 2")
                
            reply = sendStatus(str)
            testcounter = testcounter + 1
            print processReply(reply)

            ### Simulating until movement is complete
            if( at_robot_cell == True):
                #complete and send status to server
                at_robot_cell = False
                str = command(commandListClient_mobile.index("Waiting_for_accept_robot"),mobileNumber,"Waiting to be full")
                reply = sendStatus(str)
                print processReply(reply)
                state = findState(processReply(reply))

        elif(state == commandListServer_mobile.index("Empty_bricks")):
            #emptying bricks in cell
            str = command(commandListClient_mobile.index("Emptying_bricks"),mobileNumber,"Emptying bricks in cell")     
            reply = sendStatus(str)
            testcounter = testcounter + 1
            print processReply(reply)

            ### Simulating until emptying is complete
            if( bricks_empty == True):
                #complete and send status to server
                bricks_empty = False
                str = command(commandListClient_mobile.index("Bricks_empty"),mobileNumber,"Finished emptying bricks")
                reply = sendStatus(str)
                print processReply(reply)
                state = findState(processReply(reply))

        elif(state == commandListServer_mobile.index("Move_home")):
            #Moving Home
            str = command(commandListClient_mobile.index("Moving_home"),mobileNumber,"Returning to box")     
            reply = sendStatus(str)
            testcounter = testcounter + 1
            print processReply(reply)

            ### Simulating until movement is complete
            if( at_home == True):
                #complete and send status to server
                at_home = False  
                str = command(commandListClient_mobile.index("Complete"),mobileNumber,"Order complete")
                #nxt_state = SUSPEND
                reply = sendStatus(str)
                print processReply(reply)
                state = findState(processReply(reply))

        
       
        elif(state == commandListServer_mobile.index("Wait_for_accept_robot")):
            #Wait for the feeder to be filled
            str = command(commandListClient_mobile.index("Waiting_for_accept_robot"),mobileNumber,"Waiting to get bricks")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))

        print "state %s " % commandListServer_mobile[state]
        
        # ONLY PUBLISH WHEN REPLY FROM MES SERVER CHANGES!
        if(state != old_state):
	  state_pub.publish(commandListServer_mobile[state])
        old_state = state;
        rate.sleep()

def init():
    if rospy.has_param("~port"):
        global port
        port = rospy.get_param("~port")
    if rospy.has_param("~hostname"):
        global hostname
        hostname = rospy.get_param("~hostname")

if __name__ == "__main__":
    main()
