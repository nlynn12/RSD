/*
 * Copyright 2016 Nicolai Lynnerup <nlynn12@student.sdu.dk>
 *                Jens-Jakob Bentsen <jeben12@student.sdu.dk>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#ifndef ACTION_HPP
#define ACTION_HPP

#include <ros/ros.h>
#include <ros/console.h>

#include <std_msgs/String.h>

#include <iostream>
#include <vector>
#include <string.h>
#include <memory>
#include <functional>
#include <cassert>
#include <algorithm>

#include <rw/rw.hpp>
#include <rw/math/Math.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/RPY.hpp>

#include "primitives.hpp"
#include "communicationhandler.hpp"
#include "brick.hpp"
#include "order.hpp"

class Action{
   private:
      Primitive _type;
      std::vector<float> _parameters;
      bool _blocking = true;
      ros::NodeHandle* _nh;
   public:
      Action(){}
      Action(Primitive, std::vector<float>, bool, ros::NodeHandle*);
      ~Action(){}
      std::function<void()> execute;
      Primitive get_type(){return _type;};
      std::vector<float> get_parameters(){return _parameters;};
      // Actions
      void test(){std::cout << "Not implemented." << std::endl;};
      void move2pose();
      void move2point();
      void move_point2pose();
      void move_p2p();
      void move_j();
      void gripper();
      void conveyor_belt();
      void wait();
      void plannerStateCallback(const std_msgs::String::ConstPtr& msg);
      void waitForRobot();
      void detectBricks();
      void move2BrickPose();
};

#endif // ACTION_HPP
