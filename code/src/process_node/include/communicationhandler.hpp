/*
 * Copyright 2016 Jens-Jakob Bentsen <jeben12@student.sdu.dk>
 *                Nicolai Lynnerup <nlynn12@student.sdu.dk>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef COMMUNICATIONHANDLER_HPP
#define COMMUNICATIONHANDLER_HPP

#include <ros/ros.h>
#include <ros/console.h>

#include "brick.hpp"

//msgs types
#include <std_msgs/String.h>
#include <std_msgs/Int16.h>
#include <smart_camera/legoBricks.h>

// Services
#include <kuka_ros/setConfiguration.h>
#include <kuka_ros/getIsMoving.h>
#include <kuka_ros/actuateValve.h>
#include <kuka_ros/move2Pose.h>
#include <kuka_ros/move2Point.h>
#include <kuka_ros/movePoint2Pose.h>
#include <kuka_ros/moveP2P.h>
#include <kuka_ros/legoBrickPickUp.h>
#include <control_plc/setConveyorSpeed.h>
#include <smart_camera/setAutomaticMode.h>


// Singleton class which holds all ROS communication
class CommunicationHandler{
   private:
      void emergencyStopCallback(const std_msgs::Int16::ConstPtr& msg);
      void cameraCallback(const smart_camera::legoBricks::ConstPtr& msg);
      ros::NodeHandle* _nh;
      
      std::vector<brick> _bricks_list;
   protected:
      CommunicationHandler(ros::NodeHandle *n); // Constructor must be protected
      bool _emergencyStopActive;
   public:
      // Accessor method for retrieval of singleton instance
      static CommunicationHandler& instance(ros::NodeHandle *n){
	 static CommunicationHandler inst(n);
	 return inst;
      }
      
      std::vector<brick> getListOfBricks() {return _bricks_list;}
      void clearListOfBricks() {_bricks_list.clear();}
      void setListOfBricks(const std::vector<brick> &bricks){ _bricks_list = bricks;}
      
      // getters
      bool isEmergencyStopActive() const {
          return _emergencyStopActive;
      }
      
      // ROS
      ros::Subscriber _emergencyStopSub;
      ros::Subscriber _cameraPosesSub;
      ros::Subscriber _mes_state_sub;
      ros::ServiceClient _setConfigurationClient;
      ros::ServiceClient _actuateValveClient;
      ros::ServiceClient _getIsMovingClient;
      ros::ServiceClient _setConveyorSpeedClient;
      ros::ServiceClient _move2PoseClient;
      ros::ServiceClient _movePoint2PoseClient;
      ros::ServiceClient _move2PointClient;
      ros::ServiceClient _moveP2PClient;
      ros::ServiceClient _cameraModeClient;
      ros::ServiceClient _pickUpLego;
      
      bool _holdCurrentBricks;
};

#endif // COMMUNICATIONHANDLER_HPP
