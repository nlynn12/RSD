/*
 * Copyright 2016 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#ifndef EXECUTOR_HPP
#define EXECUTOR_HPP

#include <ros/ros.h>
#include <yaml-cpp/yaml.h>

#include <string.h>

#include "primitives.hpp"
#include "action.hpp"

class Executor{
   private:
      ros::NodeHandle *_nh;
      std::vector<bool> _blocking_list;
      std::vector<std::vector<float>> _parameter_list;
      std::vector<Primitive> _primitive_list;
      std::vector<Action> _action_list;
   public:
      Executor(ros::NodeHandle *n) : _nh(n){std::cout << n << std::endl;};
      void readPrimitivesFromYAML(std::string);
      void createActionsFromPrimitives();
      std::vector<Action> getActionList(){return _action_list;}
};

#endif // EXECUTOR_HPP
