/*
 * Copyright 2016 Nicolai Lynnerup <nlynn12@student.sdu.dk>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#ifndef ORDER_HPP
#define ORDER_HPP

struct MESOrder{
  int red = -1;
  int yellow = -1;
  int blue = -1;
  bool isOrderComplete(){
    if(red <= 0 && yellow <= 0 && blue <= 0){
      return true;
    }else{
      return false;
    }
  }
  void setOrderToDefault(){red = -1, yellow = -1, blue = -1;}
};

extern MESOrder order;

#endif // ORDER_HPP