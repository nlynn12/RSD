#pragma once

#include <vector>
#include <list>

#include <rw/math/Transform3D.hpp>

#include "communicationhandler.hpp"
#include "brick.hpp"

class SimpleClearanceSorter {
    
public:
        
    struct BrickPose {
        rw::math::Transform3D<> brickTransform;
        int type = -1;
        brick brickPose;
        double clearance = 0;
        
        friend bool operator< (const BrickPose &i, const BrickPose &j) { return (i.clearance < j.clearance);}
        friend bool operator> (const BrickPose &i, const BrickPose &j) { return !(i < j);}
    };

    static void findBestClearance(const std::vector<brick> &bricks, const std::vector<int> &picktypes, std::vector<brick> &bestBrick);
    
    
protected:
    
    static bool isDesiredBrick(const BrickPose &brick, const std::vector<int> &picktypes);
    
    static void convertToTransform3D(const std::vector<brick> &bricks, std::list<BrickPose> &trans);
    
    static void findClearance(const std::list<BrickPose> &brick_list, std::list<BrickPose>::iterator &brick);

    static double getClearanceLevel(const int type);
  
};


