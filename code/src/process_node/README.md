Process Node
====
This Node will controll all the different components in the robot cell.

The Process will be defined by Cortsen through the mes server. This Node should proberbly also communicate with the MES.

To launch, run: `roslaunch process_node process_node.launch` 

As of right now the central processing node does nothing besides switching states.

TODO
-----

1. Include all needed services and test if functional (Conveyer, KUKA etc.)
2. Design process flow (A flowchart would be nice)
3. Implement MES communication
4. Implement MES Translation
5. (3. and 4. should maybe be in a seperate node?)
6. Create a Launch file that launches all the other nodes?


Services & Topics
-----

### Services
To get into manual mode:

`rosservice call /rc/mainProcess/requestManualMode "release: false"` 

To get into automatic mode:

`rosservice call /rc/mainProcess/requestManualMode "release: true"`

-----------