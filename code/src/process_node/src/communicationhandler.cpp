/*
 * Copyright 2016 Jens-Jakob Bentsen <jeben12@student.sdu.dk>
 *                Nicolai Lynnerup <nlynn12@student.sdu.dk>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "communicationhandler.hpp"


CommunicationHandler::CommunicationHandler(ros::NodeHandle* n) : _nh(n){
   // Set
   _holdCurrentBricks = false;    

   //Topics
   _emergencyStopSub = n->subscribe ( "/rc/controlPLC/emergencyStopActive",1, &CommunicationHandler::emergencyStopCallback, this );
   _cameraPosesSub = n->subscribe ( "/rc/smartCamera/legoBricks",1, &CommunicationHandler::cameraCallback, this );
   
   //Services
   _actuateValveClient = n->serviceClient<kuka_ros::actuateValve> ( "/rc/KukaNode/ActuateValve" );
   _setConfigurationClient = n->serviceClient<kuka_ros::setConfiguration> ( "/rc/KukaNode/SetConfiguration" );
   _getIsMovingClient = n->serviceClient<kuka_ros::getIsMoving> ( "/rc/KukaNode/IsMoving" );
   _move2PoseClient = n->serviceClient<kuka_ros::move2Pose> ( "/rc/kukaClient/move2Pose" );
   _move2PointClient = n->serviceClient<kuka_ros::move2Point> ( "/rc/kukaClient/move2Point" );
   _movePoint2PoseClient = n->serviceClient<kuka_ros::movePoint2Pose> ( "/rc/kukaClient/movePoint2Pose" );
   _moveP2PClient = n->serviceClient<kuka_ros::moveP2P> ( "/rc/kukaClient/moveP2P" );
   _pickUpLego = n->serviceClient<kuka_ros::legoBrickPickUp> ( "/rc/kukaClient/pickUpLego" );
   _setConveyorSpeedClient = n->serviceClient<control_plc::setConveyorSpeed> ( "/rc/controlPLC/setConveyorSpeed" );
   _cameraModeClient = n->serviceClient<smart_camera::setAutomaticMode> ( "/rc/smartCamera/automaticMode" );

    
}

void CommunicationHandler::emergencyStopCallback ( const std_msgs::Int16::ConstPtr& msg ){
   _emergencyStopActive = msg->data;
   ROS_INFO_STREAM ( "The emergencyStopActive changed to: " <<  _emergencyStopActive );
}

void CommunicationHandler::cameraCallback ( const smart_camera::legoBricks::ConstPtr& msg ){
   if( _holdCurrentBricks ){
      return;
   }
   
   ROS_DEBUG_STREAM ( "Recieved new message from camera" );
   _bricks_list.clear();
   for(int i = 0; i < msg->brickCount; i++){
      std::vector<float> pose = {(float)msg->translation[i].x,
                                 (float)msg->translation[i].y,
                                 (float)msg->translation[i].z,
                                 (float)msg->rotation[i].x,
                                 (float)msg->rotation[i].y,
                                 (float)msg->rotation[i].z};
      _bricks_list.emplace_back(pose, msg->type[i], msg->correlation[i]);
   }
}