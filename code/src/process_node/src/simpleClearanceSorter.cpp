#include "simpleClearanceSorter.hpp"

#include <rw/math/RPY.hpp>

#include <algorithm>

void SimpleClearanceSorter::findBestClearance ( const std::vector<brick>& bricks, const std::vector< int >& picktypes, std::vector<brick> &bestBrick ) {
    std::list< BrickPose > allBricks;
    std::list< BrickPose > desiredBricks;
    convertToTransform3D(bricks, allBricks);
    
    for(std::list< BrickPose >::iterator it = allBricks.begin(); it != allBricks.end(); it++){
        if(isDesiredBrick(*it, picktypes)){
            findClearance(allBricks, it);
            if(it->clearance > 0.0){
                desiredBricks.push_back(*it);
            }
        }
    }
    
    desiredBricks.sort(std::greater<SimpleClearanceSorter::BrickPose>());

    bestBrick.clear();
    bestBrick.resize(desiredBricks.size());
    int i = 0;
    for(std::list< BrickPose >::iterator it = desiredBricks.begin(); it != desiredBricks.end(); it++, i++){
        bestBrick[i] = it->brickPose;
        ROS_DEBUG_STREAM("Found brick #" << i << " to have clearance of " << it->clearance);
    }
}


bool SimpleClearanceSorter::isDesiredBrick ( const BrickPose& brick, const std::vector< int > &picktypes) {
    for(size_t k = 0; k < picktypes.size(); k++){
        if(brick.type == picktypes[k]){
            return true;
        }
    }
    return false;
}


void SimpleClearanceSorter::convertToTransform3D ( const std::vector<brick >& bricks, std::list< SimpleClearanceSorter::BrickPose >& trans ) {
    trans.clear();
    for(size_t i = 0; i < bricks.size(); i++){
            BrickPose tmpPose;
            rw::math::RPY<> rpy(bricks[i].pose[3], bricks[i].pose[4], bricks[i].pose[5]);
            tmpPose.brickTransform.R() = rpy.toRotation3D();
            tmpPose.brickTransform.P() = rw::math::Vector3D<>(bricks[i].pose[0], bricks[i].pose[1], bricks[i].pose[2]);
            tmpPose.type = bricks[i].type;
            tmpPose.brickPose = bricks[i];
            trans.push_back(tmpPose);
    }
}


void SimpleClearanceSorter::findClearance ( const std::list< BrickPose >& brick_list, std::list< BrickPose >::iterator &brick ) {
    double ownClearance = 0.022;
    if(ownClearance < getClearanceLevel(brick->type)){
        ownClearance = getClearanceLevel(brick->type);
    }
    rw::math::Transform3D<> invpose = rw::math::inverse<>(brick->brickTransform);
    double lowest_clearance = std::numeric_limits< double >::max();
    bool init_lowest = false;
    for(std::list< BrickPose >::const_iterator it = brick_list.cbegin(); it != brick_list.cend(); it++){
        if(it == brick){
            continue;
        }
        rw::math::Transform3D<> dist = invpose * it->brickTransform;
        double clearance = dist.P().norm2() - getClearanceLevel(it->type);
        if(clearance < lowest_clearance || !init_lowest){
            lowest_clearance = clearance;
            init_lowest = true;
        }
    }
    lowest_clearance -= ownClearance;
    brick->clearance = lowest_clearance;
}


double SimpleClearanceSorter::getClearanceLevel ( const int type ) {
    double clr = 0.0;
    if(type == 0){
        clr = 0.01132;
    } else if(type == 1){
        clr = 0.0179;
    } else if(type == 2){
        clr = 0.0253;
    } else {
        throw std::runtime_error("Undefined type asked for.");
    }
    return clr;
}



