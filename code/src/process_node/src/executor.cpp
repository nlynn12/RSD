/*
 * Copyright 2016 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#include "executor.hpp"

void Executor::readPrimitivesFromYAML(std::string ros_param){
   // Get YAML filepath from param server
   std::string yaml_path;
   if(!_nh->getParam(ros_param, yaml_path)){
      ROS_ERROR("Missing ros parameter 'yaml_path' Cannot Proceed");
      return;
   }else{
      ROS_INFO_STREAM( "Loaded Process Config file from: " << yaml_path);
   }
   
   // READ YAML FILE
  YAML::Node sequence = YAML::LoadFile(yaml_path.c_str());
  const YAML::Node& actions = sequence["Sequence"];
  
  // HACK
  _action_list.reserve(actions.size());
  
  int it_n = 0;
  for(YAML::const_iterator it = actions.begin(); it != actions.end(); ++it){
      const YAML::Node& action = *it;
      //std::cout << "Action: " << it_n << std::endl;
      
      // Get type of action
      std::string type;
      if(action["Type"]){
	 type = action["Type"].as<std::string>();
      }      
      
      std::vector<float> parameters;
      // Get Action's Parameter List
      if(action["Parameters"]){
	 YAML::Node parameter_node = action["Parameters"];
	 for(YAML::const_iterator param = parameter_node.begin(); param != parameter_node.end(); ++param){
	    //parameter_list.push_back(param->as<float>());
	    parameters.push_back(param->as<float>());
	 }
	 _parameter_list.push_back(parameters);
      }
      
      // Check if action should be blocking
      if(action["Blocking"]){
	 _blocking_list.push_back(action["Blocking"].as<int>());
      }
      
      
      // Fill list of Primitives
      if(type == "Move2Pose"){
	 _primitive_list.push_back(Primitive::Move2Pose);
      }else if(type == "Move2Point"){
	 _primitive_list.push_back(Primitive::Move2Point);
      }else if(type == "MovePoint2Pose"){
	 _primitive_list.push_back(Primitive::MovePoint2Pose);
      }else if(type == "MoveP2P"){
	 _primitive_list.push_back(Primitive::MoveP2P);
      }else if(type == "MoveL"){
	 _primitive_list.push_back(Primitive::MoveL);
      }else if(type == "MoveJ"){
	 _primitive_list.push_back(Primitive::MoveJ);
      }else if(type == "Gripper"){ 
	 _primitive_list.push_back(Primitive::Gripper);
      }else if(type == "ConveyorBelt"){
	 _primitive_list.push_back(Primitive::ConveyorBelt);
      }else if(type == "Wait"){
	 _primitive_list.push_back(Primitive::Wait);
      }else if(type == "DetectBricks"){
	 _primitive_list.push_back(Primitive::DetectBricks);
      }else if(type == "Move2Brick"){
	 _primitive_list.push_back(Primitive::Move2Brick);
      }else{
	 ROS_ERROR("Primitive in YAML file does not exist!");
      }
      
      type = "";
      //_action_list.emplace_back(_primitive_list[it_n], parameters, _blocking_list[it_n], _nh);
      it_n++;
   }
}

void Executor::createActionsFromPrimitives(){
   for(size_t i = 0; i < _primitive_list.size(); i++){
//       std::cout << _parameter_list[i].size() << std::endl;
      _action_list.emplace_back(_primitive_list[i], _parameter_list[i], _blocking_list[i], _nh);
   }
}
