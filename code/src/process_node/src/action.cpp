/*
 * Copyright 2016 Nicolai Lynnerup <nlynn12@student.sdu.dk>
 *                Jens-Jakob Bentsen <jeben12@student.sdu.dk>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "action.hpp"

#include "simpleClearanceSorter.hpp"

#include <kuka_ros/legoBrickPickUp.h>
#include <kuka_ros/move2Point.h>
#include <kuka_ros/move2Pose.h>
#include <kuka_ros/moveP2P.h>
#include <kuka_ros/movePoint2Pose.h>

#include <stdexcept>


Action::Action ( Primitive primitive, std::vector<float> params, bool block, ros::NodeHandle *n )
    : _type ( primitive ), _parameters ( params ), _blocking ( block ), _nh ( n )
{

    switch ( primitive ) {
    case Primitive::Move2Pose:
        std::cout << "Creating Move2Pose" << std::endl;
        std::cout << "   with TCP (x,y,z,r,p,y): ";
        for ( auto param:params ) {
            std::cout << param << ",";
        }
        std::cout << std::endl;
        execute = std::bind ( &Action::move2pose,this );
        break;
    case Primitive::Move2Point:
        std::cout << "Creating Move2Point" << std::endl;
        std::cout << "   with Q[6]: ";
        for ( auto param:params ) {
            std::cout << param << ",";
        }
        std::cout << std::endl;
        execute = std::bind ( &Action::move2point, this );
        break;
    case Primitive::MovePoint2Pose:
        std::cout << "Creating MovePoint2Pose" << std::endl;
        execute = std::bind ( &Action::move_point2pose, this );
        break;
    case Primitive::MoveP2P:
        std::cout << "Creating MoveP2P" << std::endl;
        std::cout << "   with Q[6]: ";
        for ( auto param:params ) {
            std::cout << param << ",";
        }
        std::cout << std::endl;
        execute = std::bind ( &Action::move_p2p, this );
        break;
    case Primitive::MoveJ:
        std::cout << "Creating MoveJ" << std::endl;
        std::cout << "   with Q[" << params.size() << "]: ";
        for ( auto param:_parameters ) {
            std::cout << param << ",";
        }
        std::cout << std::endl;
        execute = std::bind ( &Action::move_j, this );
        break;
    case Primitive::MoveL:
        std::cout << "Creating MoveL" << std::endl;
        std::cout << "   with Q[" << params.size() << "]: ";
        for ( auto param:params ) {
            std::cout << param << ",";
        }
        std::cout << std::endl;
        execute = std::bind ( &Action::test, this );
        break;
    case Primitive::Gripper:
        std::cout << "Creating Gripper" << std::endl;
        execute = std::bind ( &Action::gripper, this );
        break;
    case Primitive::ConveyorBelt:
        std::cout << "Creating Conveyor Belt" << std::endl;
        execute = std::bind ( &Action::conveyor_belt, this );
        break;
    case Primitive::Wait:
        std::cout << "Creating Wait" << std::endl;
        execute = std::bind ( &Action::wait, this );
        break;
    case Primitive::DetectBricks:
        std::cout << "Creating DetectBricks" << std::endl;
        execute = std::bind ( &Action::detectBricks, this );
        break;
    case Primitive::Move2Brick:
        std::cout << "Creating Move2Brick" << std::endl;
        execute = std::bind ( &Action::move2BrickPose, this );
        break;
    default:
        break;
    }
    std::cout << "   Blocking: " << _blocking << std::endl;
}

void Action::waitForRobot()
{
    kuka_ros::getIsMoving srv;

    ROS_DEBUG_STREAM ( "Waiting for robot to finish" );

    bool isMoving = true;
    while ( isMoving ) {
        usleep ( 100000 );

        if ( CommunicationHandler::instance ( _nh ).isEmergencyStopActive() ) {
            throw std::runtime_error ( "EStop Active." );
        }

        if ( CommunicationHandler::instance ( _nh )._getIsMovingClient.call ( srv ) ) {
            isMoving = srv.response.isMoving;
        } else {
            ROS_ERROR ( "/rc/KukaNode/IsMoving is not available" );
            break;
        }

    }
}

void Action::move2pose()
{
    kuka_ros::move2Pose srv;

    for ( size_t i=0; i < _parameters.size(); i++ ) {
        srv.request.poseGoal[i] = _parameters[i];
    }

    int errorCounter = 0;
    while ( ! CommunicationHandler::instance ( _nh )._move2PoseClient.call ( srv ) ) {
        ROS_ERROR ( "/rc/kukaClient/move2Pose is not available" );
        usleep ( 100000 );
        errorCounter++;
        if ( errorCounter > 20 ) {
            ROS_ERROR ( "ACTION CANCELED!" );
            return;
        }
    }
    if ( srv.response.acceptedRequest == 0 ) {
        throw std::runtime_error ( "Could not plan the move2pose." );
    }
    if ( _blocking ) {
        waitForRobot();
    }

}

void Action::move2point()
{
    kuka_ros::move2Point srv;

    for ( size_t i=0; i < _parameters.size(); i++ ) {
        srv.request.qGoal[i] = _parameters[i];
    }

    int errorCounter = 0;
    while ( ! CommunicationHandler::instance ( _nh )._move2PointClient.call ( srv ) ) {
        ROS_ERROR ( "/rc/kukaClient/move2Point is not available" );
        usleep ( 100000 );
        errorCounter++;
        if ( errorCounter > 20 ) {
            ROS_ERROR ( "ACTION CANCELED!" );
            return;
        }
    }
    if ( srv.response.acceptedRequest == 0 ) {
        throw std::runtime_error ( "Could not plan the move2point." );
    }
    ROS_DEBUG_STREAM ( "Moving from to POSE" );

    if ( _blocking ) {
        waitForRobot();
    }
}

void Action::move_point2pose()
{
    kuka_ros::movePoint2Pose srv;

    for ( size_t i = 0; i < 6; i++ ) {
        srv.request.qStart[i] = _parameters[i];
        srv.request.poseGoal[i] = _parameters[i+6];
    }

    int errorCounter = 0;
    while ( ! CommunicationHandler::instance ( _nh )._movePoint2PoseClient.call ( srv ) ) {
        ROS_ERROR ( "/rc/kukaClient/movePoint2Pose is not available" );
        usleep ( 100000 );
        errorCounter++;
        if ( errorCounter > 20 ) {
            ROS_ERROR ( "ACTION CANCELED!" );
            return;
        }
    }
    if ( srv.response.acceptedRequest == 0 ) {
        throw std::runtime_error ( "Could not plan the point2pose." );
    }
    ROS_DEBUG_STREAM ( "Moving from Q to POSE" );

    if ( _blocking ) {
        waitForRobot();
    }
}

void Action::move_p2p()
{
    kuka_ros::moveP2P srv;

    for ( size_t i = 0; i < 6; i++ ) {
        srv.request.qStart[i] = _parameters[i];
        srv.request.qGoal[i] = _parameters[i+6];
    }

    int errorCounter = 0;
    while ( ! CommunicationHandler::instance ( _nh )._moveP2PClient.call ( srv ) ) {
        ROS_ERROR ( "/rc/kukaClient/moveP2P is not available" );
        usleep ( 100000 );
        errorCounter++;
        if ( errorCounter > 20 ) {
            ROS_ERROR ( "ACTION CANCELED!" );
            return;
        }
    }
    if ( srv.response.acceptedRequest == 0 ) {
        throw std::runtime_error ( "Could not plan the p2p." );
    }

    ROS_DEBUG_STREAM ( "Moving from Q-start to Q-goal" );

    if ( _blocking ) {
        waitForRobot();
    }
}



void Action::move_j()
{
    kuka_ros::setConfiguration srv;

    for ( size_t i = 0; i < _parameters.size(); i++ ) {
        srv.request.q[i] = _parameters.at ( i );
        srv.request.speed[i] = 300;
        srv.request.acc[i] = 0.01;
    }

    int errorCounter = 0;
    while ( ! CommunicationHandler::instance ( _nh )._setConfigurationClient.call ( srv ) ) {
        ROS_ERROR ( "/rc/KukaNode/setConfiguration is not available" );
        usleep ( 100000 );
        errorCounter++;
        if ( errorCounter > 20 ) {
            ROS_ERROR ( "ACTION CANCELED!" );
            return;
        }
    }

    ROS_DEBUG_STREAM ( "Moving (Joint)" );

    if ( _blocking ) {
        waitForRobot();
    }

}

void Action::gripper()
{
    kuka_ros::actuateValve srv;

    srv.request.flowForward = _parameters[0];

    int errorCounter = 0;
    while ( ! CommunicationHandler::instance ( _nh )._actuateValveClient.call ( srv ) ) {
        ROS_ERROR ( "/rc/KukaNode/actuateValve is not available" );
        usleep ( 100000 );
        errorCounter++;
        if ( errorCounter > 20 ) {
            ROS_ERROR ( "ACTION CANCELED!" );
            return;
        }
    }

    ROS_DEBUG_STREAM ( "Setting valves to: " << _parameters[0] );

    if ( _blocking ) {
        usleep ( 200000 ); // sleep 200ms
    }

}

void Action::conveyor_belt()
{
    control_plc::setConveyorSpeed srv;

    srv.request.speed = _parameters[0];

    int errorCounter = 0;
    while ( ! CommunicationHandler::instance ( _nh )._setConveyorSpeedClient.call ( srv ) ) {
        ROS_ERROR ( "/rc/control_plc/setConveyorSpeed is not available" );
        usleep ( 100000 );
        errorCounter++;
        if ( errorCounter > 20 ) {
            throw std::runtime_error ( "Conveyor not available." );
        }
    }

    ROS_DEBUG_STREAM ( "Setting Conveyor Belt speed to: " << _parameters[0] );

    if ( _blocking ) {
        ROS_ERROR ( "Error in /rc/control_plc/setConveyorSpeed: Blocking set to true. This should be set to false!" );
    }

}

void Action::wait()
{
    int ms = _parameters[0];
    usleep ( ms*1000 );
}

void Action::detectBricks()
{

    ROS_INFO_STREAM ( "We want: " << order.blue << ", " << order.red << ", " << order.yellow << "!" );

    CommunicationHandler::instance ( _nh ).clearListOfBricks();

    smart_camera::setAutomaticMode cam_srv;
    control_plc::setConveyorSpeed conv_srv;

    //Turn on the camera.
    cam_srv.request.mode = true;

    int errorCounter = 0;
    while ( ! CommunicationHandler::instance ( _nh )._cameraModeClient.call ( cam_srv ) ) {
        ROS_ERROR ( "/rc/smartCamera/automaticMode is not available" );
        usleep ( 1000000 );
        errorCounter++;
        if ( errorCounter > 20 ) {
            ROS_ERROR ( "ACTION CANCELED!" );
            return;
        }
    }

    std::vector< int > picktypes;
    if ( order.blue > 0 ) {
        picktypes.push_back ( 0 );
    }
    if ( order.red > 0 ) {
        picktypes.push_back ( 1 );
    }
    if ( order.yellow > 0 ) {
        picktypes.push_back ( 2 );
    }

    std::vector< brick > brickList = CommunicationHandler::instance ( _nh ).getListOfBricks();
    //ROS_INFO_STREAM ( "Got " << brickList.size() << " bricks from camera." );
    SimpleClearanceSorter::findBestClearance ( brickList, picktypes, brickList );
    ROS_DEBUG_STREAM ( "Reduced brick list to " << brickList.size() << " bricks pickable." << "We WANT: " << order.blue << ", " << order.red << ", " << order.yellow << "!!!!!!!!!!!!!" );

    if ( brickList.size() <= 0 ) {

        bool found_bricks = false;

        while ( !found_bricks ) {
			
			if ( CommunicationHandler::instance ( _nh ).isEmergencyStopActive() ) {
				cam_srv.request.mode = false;
				CommunicationHandler::instance ( _nh )._cameraModeClient.call ( cam_srv );
				conv_srv.request.speed = 0;
				CommunicationHandler::instance ( _nh )._setConveyorSpeedClient.call ( conv_srv );
                throw std::runtime_error ( "EStop Active." );
            }

            conv_srv.request.speed = 1;
            CommunicationHandler::instance ( _nh )._setConveyorSpeedClient.call ( conv_srv );

            do {
				
				if ( CommunicationHandler::instance ( _nh ).isEmergencyStopActive() ) {
					cam_srv.request.mode = false;
					CommunicationHandler::instance ( _nh )._cameraModeClient.call ( cam_srv );
					conv_srv.request.speed = 0;
					CommunicationHandler::instance ( _nh )._setConveyorSpeedClient.call ( conv_srv );
					throw std::runtime_error ( "EStop Active." );
				}
				
                usleep ( 5000 );
                brickList = CommunicationHandler::instance ( _nh ).getListOfBricks();
                if ( brickList.size() != 0 ) {
                    //ROS_INFO_STREAM ( "Got " << brickList.size() << " bricks from camera." );
                    SimpleClearanceSorter::findBestClearance ( brickList, picktypes, brickList );
                    ROS_DEBUG_STREAM ( "Reduced brick list to " << brickList.size() << " bricks pickable." << "We WANT: " << order.blue << ", " << order.red << ", " << order.yellow << "!!!!!!!!!!!!!" );

                }
            } while ( brickList.size() <= 0 );

            conv_srv.request.speed = 0;
            CommunicationHandler::instance ( _nh )._setConveyorSpeedClient.call ( conv_srv );

            CommunicationHandler::instance ( _nh ).clearListOfBricks();

            int c = 1000;
            do {
				
				if ( CommunicationHandler::instance ( _nh ).isEmergencyStopActive() ) {
					cam_srv.request.mode = false;
					CommunicationHandler::instance ( _nh )._cameraModeClient.call ( cam_srv );
					conv_srv.request.speed = 0;
					CommunicationHandler::instance ( _nh )._setConveyorSpeedClient.call ( conv_srv );
					throw std::runtime_error ( "EStop Active." );
				}
				
				
                usleep ( 5000 );
                brickList = CommunicationHandler::instance ( _nh ).getListOfBricks();
                if ( brickList.size() != 0 ) {
                    ROS_DEBUG_STREAM ( "Got " << brickList.size() << " bricks from camera." );
                    SimpleClearanceSorter::findBestClearance ( brickList, picktypes, brickList );
                    ROS_DEBUG_STREAM ( "Reduced brick list to " << brickList.size() << " bricks pickable." );
                }
            } while ( brickList.size() <= 0 && c --> 0 );

            if ( brickList.size() > 0 ) {
                found_bricks = true;
                CommunicationHandler::instance ( _nh )._holdCurrentBricks = true;
            }
        }
    }

    CommunicationHandler::instance ( _nh )._holdCurrentBricks = true;

    CommunicationHandler::instance ( _nh ).setListOfBricks ( brickList );

    //Turn off camera
    cam_srv.request.mode = false;
    errorCounter = 0;
    while ( ! CommunicationHandler::instance ( _nh )._cameraModeClient.call ( cam_srv ) ) {
        ROS_ERROR ( "/rc/smartCamera/automaticMode is not available" );
        usleep ( 1000000 );
        errorCounter++;
        if ( errorCounter > 20 ) {
            ROS_ERROR ( "ACTION CANCELED!" );
            return;
        }
    }
}

void Action::move2BrickPose()
{
    ROS_DEBUG_STREAM ( "HAS BRICKS: " << CommunicationHandler::instance ( _nh ).getListOfBricks().size() );
    if ( CommunicationHandler::instance ( _nh ).getListOfBricks().size() <= 0 ) {
        CommunicationHandler::instance ( _nh )._holdCurrentBricks = false;
        throw std::runtime_error ( "Did not get any bricks when entered move2BrickPose." );
    }

    // calculate poses
    std::vector<brick> bricks_list = CommunicationHandler::instance ( _nh ).getListOfBricks();
    rw::math::RPY<> rpy ( bricks_list[0].pose[3], bricks_list[0].pose[4], bricks_list[0].pose[5] );
    rw::math::Transform3D<> pickup ( rw::math::Vector3D<> ( bricks_list[0].pose[0], bricks_list[0].pose[1], bricks_list[0].pose[2] ),
                                     rpy.toRotation3D() );

    int col = bricks_list[0].type;
    if ( col == 0 && order.blue > 0 ) {
        order.blue--;
        ROS_INFO_STREAM ( "Grasping a BLUE brick." );
    } else if ( col == 1 && order.red > 0 ) {
        order.red--;
        ROS_INFO_STREAM ( "Grasping a RED brick." );
    } else if ( col == 2 && order.yellow > 0 ) {
        order.yellow--;
        ROS_INFO_STREAM ( "Grasping a YELLOW brick." );
    } else {
        throw std::runtime_error ( "Unknown Brick Colour: " + std::to_string ( col ) );
    }



    rw::math::Transform3D<> retract ( rw::math::Vector3D<> ( 0.0,0.0,-0.025 ) );
    rw::math::Transform3D<> approachPose = pickup * retract;

    rw::math::Transform3D<> pickup_retract ( rw::math::Vector3D<> ( 0.0,0.0,-0.002 ) );
    rw::math::Transform3D<> pickupPose = pickup * pickup_retract;

    kuka_ros::legoBrickPickUp move2lego_approach_srv, move2lego_grasp_srv;
    rw::math::RPY<> rpy_approach = rw::math::RPY<> ( approachPose.R() );
    rw::math::RPY<> rpy_grasp = rw::math::RPY<> ( pickupPose.R() );
    for ( size_t i = 0; i < 6; i++ ) {
        move2lego_approach_srv.request.qStart[i] = _parameters[i];
        if ( i < 3 ) {
            move2lego_approach_srv.request.poseGoal[i] =  approachPose.P() [i];
            move2lego_grasp_srv.request.poseGoal[i] =  pickupPose.P() [i];
        } else {
            move2lego_approach_srv.request.poseGoal[i] =  rpy_approach[i-3];
            move2lego_grasp_srv.request.poseGoal[i] =  rpy_grasp[i-3];
        }
    }
    move2lego_approach_srv.request.type = bricks_list[0].type;
    move2lego_grasp_srv.request.type = bricks_list[0].type;

    ROS_DEBUG_STREAM ( "Moving to Approach." );

    while ( ! CommunicationHandler::instance ( _nh )._pickUpLego.call ( move2lego_approach_srv ) ) {
        ROS_ERROR ( "/rc/kukaClient/pickUpLego is not available" );
    }
    if ( move2lego_approach_srv.response.acceptedRequest == 0 ) {
        throw std::runtime_error ( "Could not move to approach pose." );
    }

    move2lego_grasp_srv.request.qStart = move2lego_approach_srv.response.qGoal;

    waitForRobot();

    ROS_DEBUG_STREAM ( "Moving to Grasp." );

    while ( ! CommunicationHandler::instance ( _nh )._pickUpLego.call ( move2lego_grasp_srv ) ) {
        ROS_ERROR ( "/rc/kukaClient/pickUpLego is not available" );
    }
    if ( move2lego_grasp_srv.response.acceptedRequest == 0 ) {
        throw std::runtime_error ( "Could not move to grasp pose." );
    }

    if ( _blocking ) {
        waitForRobot();
    }

    CommunicationHandler::instance ( _nh ).clearListOfBricks();
    CommunicationHandler::instance ( _nh )._holdCurrentBricks = false;

}

