/*
 * Copyright 2016 Nicolai Lynnerup <nlynn12@student.sdu.dk>  
 *                Jens-Jakob Bentsen <jeben12@student.sdu.dk>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */


#include <ros/ros.h>
#include <std_msgs/String.h>
#include <std_msgs/UInt32.h>

#include <vector>
#include <string>
#include <cstdlib>
#include <stdexcept>
#include <mutex>

#include "action.hpp"
#include "primitives.hpp"
#include "communicationhandler.hpp"
#include "order.hpp"
#include "executor.hpp"

// RapidXML
#include "rapidxml.hpp"

#include <process_node/requestManualMode.h>
#include <process_node/getActionList.h>

MESOrder order;

//Global Vars
enum states {IDLE, MANUAL_MODE, SORT_BRICKS, SORT_BRICKS_ERROR, DELIVER_BRICKS, FINISHED, FINISHED_DELIVER} state;
std::mutex stateProtection;
std::vector<Primitive> _primitive_list, _d_primitive_list;
std::vector<Action> _action_list, _d_action_list;

void mes_state(const std_msgs::String::ConstPtr& msg){
  std::string xml_str = msg->data;
  ROS_DEBUG( "--------- NEW REPLY ----------" );
  
  char * xml_str_parse = new char[xml_str.size() + 1];
  std::copy(xml_str.begin(), xml_str.end(), xml_str_parse);
  xml_str_parse[xml_str.size()] = '\0'; // don't forget the terminating 0
  
  rapidxml::xml_document<> doc;
  rapidxml::xml_node<> * root_node;
  
  doc.parse<0>(xml_str_parse);
  
  root_node = doc.first_node("log_entry");
  
  rapidxml::xml_node<> * child_node = root_node->first_node("event");
  std::string event = child_node->value();
  ROS_INFO_STREAM( event.c_str() );
  
  if(event.compare("Wait_for_task_accept")==0){
      ROS_DEBUG_STREAM( xml_str.c_str() );
      rapidxml::xml_node<> * order_node = root_node->first_node("order");
      for(rapidxml::xml_node<> * brick_node = order_node->first_node("brick"); brick_node; brick_node = brick_node->next_sibling()){
            ROS_INFO_STREAM( brick_node->first_attribute("type")->value() << ": " << brick_node->value());
          if(std::string(brick_node->first_attribute("type")->value()) == "red"){
              order.red = static_cast<int>(atoi(brick_node->value()));
          }else if(std::string(brick_node->first_attribute("type")->value()) == "blue"){
              order.blue = static_cast<int>(atoi(brick_node->value()));
          }else if(std::string(brick_node->first_attribute("type")->value()) == "yellow"){
              order.yellow = static_cast<int>(atoi(brick_node->value()));
          }else{
              ROS_ERROR("This brick type is currently not supported by the system");
          }
      }
  }else if(event.compare("Sort_bricks")==0){
      stateProtection.lock();
      state = SORT_BRICKS;
      stateProtection.unlock();
  }else if(event.compare("Deliver_to_mobile")==0){
      stateProtection.lock();
      state = DELIVER_BRICKS;
      stateProtection.unlock();
  }
  
  delete[] xml_str_parse;
}

bool RequestManualMode(process_node::requestManualMode::Request &req, process_node::requestManualMode::Response &res){
    if(req.release == true){
        stateProtection.lock();
        state = IDLE;
        stateProtection.unlock();
        res.acceptedRequest = true;
        return true;
        
    }else if(req.release == false && state == IDLE){
        stateProtection.lock();
        state = MANUAL_MODE;
        stateProtection.unlock();
        res.acceptedRequest = true;
        return true;
        
    }else{
        
        res.acceptedRequest = false;
        return false;
        
    }
}

void planHome(ros::NodeHandle *n)
{
    kuka_ros::move2Point srv;
    srv.request.qGoal =  {0,-1.57,1.57,0,1.57,0};

    int errorCounter = 0;
    while ( ! CommunicationHandler::instance ( n )._move2PointClient.call ( srv ) ) {
        ROS_ERROR ( "/rc/kukaClient/move2Point is not available" );
        usleep ( 100000 );
        errorCounter++;
        if ( errorCounter > 20 ) {
            ROS_ERROR ( "HOME CANCELED!" );
            return;
        }
    }
    if ( srv.response.acceptedRequest == 0 ) {
        throw std::runtime_error ( "Could not plan the move2point." );
    }
    ROS_DEBUG_STREAM ( "Moving from to Point" );

	kuka_ros::getIsMoving srvim;
    bool isMoving = true;
    while ( isMoving ) {
        usleep ( 100000 );

        if ( CommunicationHandler::instance ( n)._getIsMovingClient.call ( srvim ) ) {
            isMoving = srvim.response.isMoving;
        } else {
            ROS_ERROR ( "/rc/KukaNode/IsMoving is not available" );
            break;
        }

    }
}


bool GetActionList(process_node::getActionList::Request &req, process_node::getActionList::Response &res){
  std::vector<std::string> types_vec;
  
   /*switch (state){
      case SORT_BRICKS:
	 
	 break;
      case DELIVER_BRICKS:
	 break;
      default:
	 break;
   }
  
  for(size_t i = 0; i < sort_bricks_primitives.size(); i++){
    switch(sort_bricks_primitives[i]){
      case Primitive::Move2Pose:
	types_vec.push_back("Move2Pose");
	break;
      case Primitive::Move2Point:
	types_vec.push_back("Move2Point");
	break;
      case Primitive::MovePoint2Pose:
	types_vec.push_back("MovePoint2Pose");
	break;
      case Primitive::Wait:
	types_vec.push_back("Wait");
	break;
      case Primitive::MoveJ:
	types_vec.push_back("Move Linear in Joint Space");
	break;
      case Primitive::MoveL:
	types_vec.push_back("Move Linear in TCP Space");
	break;
      case Primitive::MoveP2P:
	types_vec.push_back("Plan and Move");
	break;
      case Primitive::Gripper:{
	std::vector<float> params(sort_bricks_action_list[i].get_parameters());
	(params[0] == 1 ? types_vec.push_back("Open Gripper") : types_vec.push_back("Close Gripper"));
	break;
      }
      case Primitive::ConveyorBelt:
	types_vec.push_back("Conveyor Belt");
	break;
	  case Primitive::DetectBricks:
	types_vec.push_back("Detect Bricks");
	break;
	  case Primitive::Move2Brick:
	types_vec.push_back("Move To Bricks");
	break;
      default:
	types_vec.push_back("N/A");
	break;
    }
  }*/
  res.types = types_vec;
  return true;
}


std::string whatAction(const Primitive action){
    std::string name = "";
     switch(action){
      case Primitive::Move2Pose:
        name = "Move2Pose";
        break;
      case Primitive::Move2Point:
        name = "Move2Point";
        break;
      case Primitive::MovePoint2Pose:
        name = "MovePoint2Pose";
        break;
      case Primitive::Wait:
        name = "Wait";
        break;
      case Primitive::MoveJ:
        name = "Move Linear in Joint Space";
        break;
      case Primitive::MoveL:
        name = "Move Linear in TCP Space";
        break;
      case Primitive::MoveP2P:
        name = "Plan and Move";
        break;
      case Primitive::Gripper:
        name = "Gripper Actuation";
        break;
      case Primitive::ConveyorBelt:
        name = "Conveyor Belt";
        break;
          case Primitive::DetectBricks:
        name = "Detect Bricks";
        break;
          case Primitive::Move2Brick:
        name = "Move To Bricks";
        break;
      default:
        name = "N/A";
        break;
    }
   return name;
}

int main(int argc, char **argv){  
  ros::Time::init();
  ros::init(argc, argv, "giveMeAname");
  ros::NodeHandle n(ros::this_node::getName());

  ros::Subscriber mes_state_sub = n.subscribe("/rc/MESClient/MESState", 1, mes_state);
  ros::Publisher currentAction = n.advertise<std_msgs::UInt32>("currentAction", 1);
  ros::Publisher currentState = n.advertise<std_msgs::String>("getCurrentState", 1);
  ros::ServiceServer requestManualMode = n.advertiseService("requestManualMode", RequestManualMode);
  ros::ServiceServer getActionList = n.advertiseService("getActionList", GetActionList);
  
  // HACK
  _action_list.reserve(30);
  _d_action_list.reserve(30);
  
  Executor sort_bricks(&n);
  sort_bricks.readPrimitivesFromYAML("sort_bricks_primitives_path");
  sort_bricks.createActionsFromPrimitives();
  _action_list = sort_bricks.getActionList();
  ROS_INFO_STREAM("Succesfully loaded " << sort_bricks.getActionList().size() << " actions");
  
  Executor deliver_bricks(&n);
  deliver_bricks.readPrimitivesFromYAML("deliver_bricks_primitives_path");
  deliver_bricks.createActionsFromPrimitives();
  _d_action_list = deliver_bricks.getActionList();
  ROS_INFO_STREAM("Succesfully loaded " << deliver_bricks.getActionList().size() << " actions");
  
  
  ros::AsyncSpinner spinner(2); // Use 2 threads
  spinner.start();
  
  
  //Main ROS Loop
  state = IDLE;
  size_t process_cnt = 0;
  std_msgs::UInt32 action_idx;
  std::string currStateStr = "IDLE";
  std::string oldStateStr = "none";
  //usleep(10000000);
  
  bool eStop = true;
  std::string error_trigger_msg = "";
  
  ros::Rate r(100);
  while( ros::ok() ){
	  
      /// lock is only required here to prevent the change of it while running
      stateProtection.lock();
      states activeState = state;
      stateProtection.unlock();
      eStop = CommunicationHandler::instance(&n).isEmergencyStopActive();
      if(eStop && (activeState == SORT_BRICKS_ERROR || activeState == SORT_BRICKS || activeState == DELIVER_BRICKS)){
          state = SORT_BRICKS_ERROR;
          activeState = SORT_BRICKS_ERROR;
          auto action = _action_list.at(process_cnt);
          std::string nameOfAction = whatAction(action.get_type());
          error_trigger_msg = "Emergency stop activated during step " + nameOfAction  + " (" + std::to_string(process_cnt) + ").";
      }
	  
    switch (activeState){	
		
	case IDLE:
		currStateStr = "IDLE";
		break;
		
		
	case MANUAL_MODE:
		currStateStr = "MANUAL_MODE";	
		break;
		
	case SORT_BRICKS:
	{
		ROS_DEBUG("SORT_BRICKS");
		currStateStr = "SORT_BRICKS";
		action_idx.data = process_cnt;
		currentAction.publish(action_idx);
		// Actual execution of action(s)
		
		if ( order.isOrderComplete() && process_cnt <= 0){
			ROS_ERROR("Order recieved was invalid.");
			state = SORT_BRICKS_ERROR; 
			continue;
		}
		
                auto action = _action_list.at(process_cnt);
                try {
                    action.execute();
                } catch (std::runtime_error &err){
                    std::string nameOfAction = whatAction(action.get_type());
                    error_trigger_msg = nameOfAction + " (" + std::to_string(process_cnt) + ").";
                    state = SORT_BRICKS_ERROR;
                    continue;
                }
		process_cnt++;
				
		if(process_cnt >= _action_list.size()){
			process_cnt = 0;
			
			if(order.isOrderComplete()){
			  ROS_INFO("ORDER IS COMPLETE!");
			  order.setOrderToDefault();
			  state = FINISHED;
			}else{
			  state = SORT_BRICKS;
			}
		}
	
		usleep(100000);
		break;
	}
	
	case DELIVER_BRICKS:
		ROS_DEBUG("DELIVER_BRICKS");
		currStateStr = "DELIVER_BRICKS";
		for(auto action:_d_action_list){
		  action.execute();
		  usleep(100000);
		}
		
		state = FINISHED_DELIVER;
		break;
		
		
	case FINISHED:
		currStateStr = "FINISHED";
		state = IDLE;
		break;
	
	case FINISHED_DELIVER:
		currStateStr = "FINISHED_DELIVER";
		state = IDLE;
		break;
		
        case SORT_BRICKS_ERROR: {
            currStateStr = "SORT_BRICKS_ERROR";
            ROS_ERROR_STREAM("Error at " << error_trigger_msg);
            ROS_ERROR_STREAM("Input wanted step to go to: ");
            int newcount;
            std::string input;
            std::cin >> input;
            state = SORT_BRICKS;
            if(input == "home"){
                newcount = 0;
                planHome(&n);
            } else if(input == "deliver"){
                newcount = 0;
                planHome(&n);
                state = DELIVER_BRICKS;
            } else {
                newcount = std::atoi(input.c_str());
            }
            int secs = 5;
            process_cnt = newcount;
            auto newaction = _action_list.at(newcount);
            std::string nameOfNewAction = whatAction(newaction.get_type());
            for(int i = secs; i > 0; i--){
                ROS_INFO_STREAM("Restating at state: " << nameOfNewAction << " (" << newcount << ") in " << i << " seconds.");
                usleep(1000000);
            }
            ROS_INFO_STREAM("Restating at state: " << nameOfNewAction << " (" << newcount << ") now.");
        }
        break;
        default:
            state = IDLE;
            break;
            
    }
    
    if ( currStateStr != oldStateStr ) {
	std_msgs::String tmp;
	tmp.data = currStateStr;
	currentState.publish ( tmp );
	oldStateStr = currStateStr;
    }

    r.sleep();
  }

  return 0;
}