# Control PLC

This document describes how to activate the ROS node connecting to the PLC that is controlling the conveyor belt ??and sending status regarding the light curtains??.

## Topics
- control_plc/IsRobotActive | sends Int32 data 

Currently IsRobotActive is 0 if ligtcurtain/e-stop is active (robot can not move) and 1 if not (robot can move).

## Services
- control_plc/setConveyorSpeed | recieves Int32 speed 

### Conveyor Speed Modes
| Value | Correspondance | 
|----|----| 
| 0 | Stop | 
| 1 | Fwd Speed 1 | 
| 2 | Fwd Speed 2 | 
| 3 | Fwd Speed 3 | 
| 4 | Back Speed 1 | 

It is recommended to stop the conveyor belt first before changing turning direction of the conveyor.

The conveyor belt is stoped if IsRobotActive==0 and the conveyor will not move before IsRobotActive==1.
There is some (like ~5 seconds) delay before the conveyor responds to the IsRobotActive signal if this control node is not connected.


## Programming the PLC (Not Needed)
`The program on the PLC should run when you put in the plug`

First the PLC has to be programmed.
To do this boot up in windows and install the PLC program we got from Jens Cortsen.
Make sure you are connected to the PLC by setting your IP to '192.168.0.X' where X is **NOT** 10.

Now open the project file ....
Ensure you are connected to the PLC be going into ?? > IP configurations and in the window press scan.
If you can find the PLC then your good to go.

Open the code project by double clicking on ?"AC500"?, this will open ?CoDeSys?.
Now go online (?? > Login), download the project to the PLC and run it (?? > Run).
The PLC is now started and will run the program till the power is shut off.

## Install Dependencies for ROS node
1. Make sure you have Python 2.7
2. Make sure you have python pip installed
3. sudo pip install pymodbus
4. sudo pip install service_identity
5. sudo apt-get install libffi-dev
6. pip install cryptography


## Running the ROS node
With the PLC running it is now time to boot up in Ubuntu (or whatever operating system you use to run the nodes in...).

In case of Ubuntu we first need to forward the port 502, with which the PLC communicates to a unprivelliged port (ports >1024).
We forward it to 8080 by running the script below.

```sh
$ sudo iptables -A PREROUTING -t nat -p tcp --dport 502 -j REDIRECT --to-port 8080
```

To check that the rule has been added run the code below and verify that you have the following redirect.

```sh
$ sudo iptables -t nat --line-numbers -n -L

num  target     prot opt source               destination 
1    REDIRECT   tcp  --  0.0.0.0/0            0.0.0.0/0            tcp dpt:502 redir ports 8080

```

Now make sure you are connected to a roscore or that you have one running on your pc.
Then run the following:

```sh
$ rosrun control_plc plc_control.py
```

Optionally you can also run it with the parameters ip or port to specify them yourself.

```sh
$ rosrun control_plc plc_control.py _ip:="192.168.0.17" _port:=8080
```

Service Call

`rosservice call /control_plc/setConveyorSpeed "speed: 4" `

Now you have the ros node running so you can set the speed of the conveyor belt and get other info from it. :smile: