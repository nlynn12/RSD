#!/usr/bin/env python

 # Copyright 2016 Lukas Schwartz <lschw12@student.sdu.dk>
 #
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 #
 #     http://www.apache.org/licenses/LICENSE-2.0
 #
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.

import rospy
from std_msgs.msg import Int16 
   
from pymodbus.server.async import StartTcpServer
#from pymodbus.server.async import ModbusTcpServer
from pymodbus.server.async import StartUdpServer
from pymodbus.server.async import StartSerialServer

from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.transaction import ModbusRtuFramer, ModbusAsciiFramer

from twisted.internet.task import LoopingCall
import threading

from control_plc.srv import *
   
if __name__=="__main__":
	rospy.init_node("control_plc", anonymous=True)
	
	#---------------------------------------------------------------------------# 
	# set ip and ports
	#---------------------------------------------------------------------------# 
	port = 8080 # custom modbus port without requirement of sudo rights
	address = "192.168.100.50" # default master address
	if rospy.has_param("~port"):
		port =  rospy.get_param("~port")
		rospy.loginfo("For not using the default port %d, add an arg e.g.: '_port:=8080'",port)
		
	if rospy.has_param("~ip"):
		address =  rospy.get_param("~ip")
		rospy.loginfo( ("For not using the default address ", address, ", add an arg e.g.: _ip:=\"0.0.0.0\" ") )
	
	
	#---------------------------------------------------------------------------# 
	# initialize your data store
	#---------------------------------------------------------------------------# 
	block = ModbusSequentialDataBlock(0, [0]*200)
	store = ModbusSlaveContext(co = block, hr = block, ir = block, di = block)
	context = ModbusServerContext(slaves=store, single=True)
	
	#---------------------------------------------------------------------------# 
	# initialize the server information
	#---------------------------------------------------------------------------# 
	# If you don't set this or any fields, they are defaulted to empty strings.
	#---------------------------------------------------------------------------# 
	identity = ModbusDeviceIdentification()
	identity.VendorName  = 'Pymodbus'
	identity.ProductCode = 'PM'
	identity.VendorUrl   = 'http://github.com/bashwork/pymodbus/'
	identity.ProductName = 'Pymodbus Server'
	identity.ModelName   = 'Pymodbus Server'
	identity.MajorMinorRevision = '1.0'
	
	
	def updating_writer(req):
		''' A worker process that runs every so often and
		updates live values of the context. It should be noted
		that there is a race condition for the update.
		
		:param arguments: The input arguments to the call
		'''
		global context
		register = 3
		slave_id = 0x00
		address  = 100
		values = [req.speed]
		print(values)
		context[slave_id].setValues(register, address, values)
		return setConveyorSpeedResponse()


	
	## Publishers
	pub_rand = rospy.Publisher(rospy.get_name() + "/emergencyStopActive", Int16,queue_size=1, latch=True)
	
	last_result = -1;
	def publishRegister():
		global context
		global pub_rand
		global last_result
		register = 3
		slave_id = 0x00
		address  = 101
		val = context[slave_id].getValues(register, address, count=1)
		#print('emergencyStopActive: ')
		#print(val[0])
		if last_result != val[0]:
			status = True
			if val[0] > 0:
				status = False
			pub_rand.publish(data=status)
			#rospy.spin()
			last_result = val[0]
	
	time_update_reg = 0.01 # 100Hz update frequency
	loop_pub = LoopingCall(f=publishRegister) 
	loop_pub.start(time_update_reg,True) # initially delay by time
	
	## Services
	s = rospy.Service(rospy.get_name() + "/setConveyorSpeed", setConveyorSpeed, updating_writer)    
	
# Not needed :)
#	def rosspinner():
#		global rospy
#		rospy.spin()
#	
#	loop_spin = threading.Thread(target=rosspinner)
#	loop_spin.start()
#		
#	
	#print('Jens gives a fuck')
	StartTcpServer(context, identity=identity, address=(address, port))
	#ModbusTcpServer(context, identity=identity, address=(address, port))
	#print('Jens has a fuck')
	#rospy.spin()


