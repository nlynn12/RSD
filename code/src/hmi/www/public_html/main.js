(function () {
    "use strict";

    // Declare app
    var app = angular.module('HMI', ['ngRoute',
                                     'ngDialog',
                                     'ngAnimate',
                                     'angularRipple',
                                     'nya.bootstrap.select',
                                     'ngSanitize',
                                     'ngStorage',
                                     'timer']);


    app
            .config(['$logProvider',
                function ($logProvider) {
                    $logProvider.debugEnabled(true);
                }
            ])
            .config(['$routeProvider',
                function ($routeProvider) {
                    $routeProvider.otherwise({
                        //redirectTo: 'routes/404/404.template.html'
                        templateUrl: 'routes/404/404.template.html'
                    });
                }
            ])
            .config(['ngDialogProvider',
                function (ngDialogProvider) {
                    ngDialogProvider.setDefaults({
                        classname: 'ngdialog-theme-default',
                        showClose: false,
                        closeByDocument: false,
                        closeByEscape: false
                    });
                }
            ])
            .animation('.reveal-animation', function () {
                return {
                    enter: function (element, done) {
                        element.css('display', 'none');
                        element.fadeIn(300, done);
                        return function () {
                            element.stop();
                        };
                    },
                    leave: function (element, done) {
                        element.css('display','none');
                        element.fadeOut(300, done);
                        return function () {
                            element.stop();
                        };
                    }
                };
            });
})();