(function () {
    "use strict";

    angular.module("HMI").factory('ROS', ['$rootScope','$interval','$window', '$log', 'Dialog', function ($rootScope, $interval, $window, $log, Dialog) {
            // Only object outside this factory
            var service = {};

            var host = $window.location.hostname;
            var port = '9090';
            var reconnect;

            // Create connection to websocket
            var ros = new ROSLIB.Ros({
                url: 'ws://' + host + ':' + port
            });

            ros.on('open', function(){
                $log.info("Connection open")
            });

            ros.on('connection', function () {
                $log.info('Connected to websocket server at ws://' + host + ':' + port);
                $interval.cancel(reconnect);
            });

            ros.on('error', function (error) {
                $log.warn('Error connecting to websocket server: ', error);
            });

            ros.on('close', function () {
                $log.warn('Connection to websocket server closed.');
                Dialog.alert("Connection Lost!", "The connection to the HTTP and Websocket servers were lost! Please check your internet connection and reload the page. If this doesn't help, contact your system administrator.", false);
                /*var attempt = 0;
                reconnect = $interval(function(){
                    $log.info("   Trying to reconnect (Attempt "+ attempt +")");
                    attempt++;
                    ros = new ROSLIB.Ros({
                        url: 'ws://' + host + ':' + port
                    });
                    $log.warn(ros);
                    if(ros.isConnected){
                        $interval.cancel(reconnect);
                    }
                },1000);*/
            });

            // Try to reconnect if connection is lost

            // Requesting a service
            service.requestService = function (name, srv_type) {
                return new ROSLIB.Service({
                    ros: ros,
                    name: name,
                    serviceType: srv_type
                });
            };

            // Requesting a 2D viewer
            service.request2DViewer = function (divID, width, height) {
                var viewer = new ROS2D.Viewer({
                    divID: divID,
                    width: width,
                    height: height
                });

                var gridClient = new ROS2D.OccupancyGridClient({
                    ros: ros,
                    rootObject: viewer.scene
                });

                gridClient.on('change', function () {
                    viewer.scaleToDimensions(gridClient.currentGrid.width, gridClient.currentGrid.height);
                });
            };

            // Publish a topic
            service.publish = function (name, msgtype, rosmsg) {
                var topic = new ROSLIB.Topic({
                    ros: ros,
                    name: name,
                    messageType: msgtype
                });

                topic.publish(rosmsg);
            };


            service.requestSubscriber = function (name, msg_type) {
                return new ROSLIB.Topic({
                    ros: ros,
                    name: name,
                    messageType: msg_type
                });
            };
            
            service.getServices = function (fn) {
                ros.getServices(fn);
            };

            service.getTopics = function (fn) {
                ros.getTopics(fn);
            };
            
            service.getParamTopics = function (fn) {
                ros.Param({ros:ros,name:"/hmi/topics"}).get(fn);
            };
            
            service.getParamServices = function (fn) {
                ros.Param({ros:ros,name:"/hmi/services"}).get(fn);
            }

            // Return object to anything injecting this factory
            return service;
        }]);
})();