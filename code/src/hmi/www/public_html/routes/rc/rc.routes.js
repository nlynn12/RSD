(function () {
    "use strict";

    angular.module('HMI').config(['$routeProvider', function ($routeProvider) {
            $routeProvider.when("/robot", {
                templateUrl: "routes/rc/rc.template.html"
            });
        }]);
})();
