(function () {
    "use strict";

    function RCCtrl($scope, $sce, $interval, $log, ROS, Dialog) {
        
        
        var emergencyStopHasBeenActiveBefore = false;
        var emergencyDialog;
        var emergencySubscriber = ROS.requestSubscriber('/rc/controlPLC/emergencyStopActive', 'std_msgs/Int16');
        emergencySubscriber.subscribe(function(msg){
            var emergencyStopActive = msg.data;
            if(emergencyStopActive){
                $log.warn("Emergency stop triggered!");
                emergencyDialog = Dialog.alert("Emergency Stop Triggered",
                                               "To resolve this, please:"+
                                               "<ol>"+
                                               "<li>Verify that no personel is present in the Robot Cell <span class='label label-danger'>Important</span></li>"+
                                               "<li>Release all emergency stops</li>"+
                                               "<li>Press once on start button</li>"+
                                               "<li>Verify that the stack light is blinking yellow</li>"+
                                               "<li>Press once again on the start button</li>"+
                                               "</ol>",false);
                emergencyStopHasBeenActiveBefore = true;
            }
            if(!emergencyStopActive && emergencyStopHasBeenActiveBefore){
                emergencyDialog.close();
            }
        });

        var isConnectedDialog;
        var isConnectedSubscriber = ROS.requestSubscriber('/rc/KukaNode/IsConnected', 'std_msgs/Bool');
        isConnectedSubscriber.subscribe(function(msg){
            var isRobotConnected = msg.data;
            if(!isRobotConnected){
                isConnectedDialog = Dialog.alert("Connection to Robot Lost!",
                                                 "<p>To resolve this error please:"+
                                                 "<ol>"+
                                                 "<li>Verify that the Kuka Server program is running</li>"+
                                                 "<li>Verify that emergency stop is not pressed on the Kuka Pad</li>"+
                                                 "<li>On the Kuka Pad: Activate Joints</li>"+
                                                 "<li>On the Kuka Pad: Reset Program</li>"+
                                                 "</ol>"+
                                                 "<p></p>",false);
            }
            if(isRobotConnected){
                if(isConnectedDialog === undefined || isConnectedDialog === "undefined" || isConnectedDialog === null){
                    // do nothing
                }else{
                    isConnectedDialog.close();
                }
            }
        });
        
        
        /**
         * Gripper
         */
        $scope.gripper = {
            open: false
        };
        
        var srv = ROS.requestService('rc/KukaNode/GetValve', '/rc/KukaNode/GetValve');
        var req = new ROSLIB.ServiceRequest({});
        srv.callService(req, function (res) {
            var valve = res.valve;
            if(valve[0] === false && valve[1] === true){
                $scope.gripper.open = true;
            }else{
                $scope.gripper.open = false;
            }
        });
        
        $scope.gripperControls = function(control){
            var srv = ROS.requestService('rc/KukaNode/ActuateValve', '/rc/KukaNode/ActuateValve');
            var req;
            if(control === 'open'){
                req = new ROSLIB.ServiceRequest({flowForward: false});
                $scope.gripper.open = true;
            }else if(control === 'close'){
                req = new ROSLIB.ServiceRequest({flowForward: true});
                $scope.gripper.open = false;
            }else{
                $log.error(control + " is not a valid argument for gripperControls()!");
            }
            
            srv.callService(req, function (res) {
                
            });
        };

        /**
         *  PLC
         */
        $scope.PLC = {
            speed: {title: 'Stopped', value: 0},
            allSpeeds: [{title: '-100%', value: 6},
                        {title: '-75%', value: 5},
                        {title: '-25%', value: 4},
                        {title: 'Stopped', value: 0},
                        {title: '25%', value: 1},
                        {title: '75%', value: 2},
                        {title: '100%', value: 3}
                    ]
        };

        $scope.updatePLCSpeed = function () {
            var srv = ROS.requestService('/rc/controlPLC/setConveyorSpeed', '/rc/controlPLC/setConveyorSpeed');
            var req = new ROSLIB.ServiceRequest({
                speed: $scope.PLC.speed.value
            });
            srv.callService(req, function () {});
        };
        
        var srv = ROS.requestService('/rc/controlPLC/setConveyorSpeed', '/rc/controlPLC/setConveyorSpeed');
        var req = new ROSLIB.ServiceRequest({
            speed: 0
        });
        srv.callService(req, function () {});

        /**
         * Robot 
         */
        // Get current configuration
        $scope.robot = {
            q: [0.00, -1.57, 1.57, 0.00, 1.57, 0.00],// Home position
            speed: 5.0,
            joints: [{name: 'Base',     min: -2.9670, max: 2.9670, q: 0.00},
                     {name: 'Shoulder', min: -3.3160, max: 0.7854, q: -1.57},
                     {name: 'Elbow',    min: -2.0940, max: 2.7230, q: 1.57},
                     {name: 'Link 1',   min: -3.2290, max: 3.2290, q: 0.00},
                     {name: 'Link 2',   min: -2.0940, max: 2.0940, q: 1.57},
                     {name: 'Link 3',   min: -6.1090, max: 6.1090, q: 0.00}],
            allSpeeds: [{title: '5.0', value: 5.0},
                        {title: '10.0', value: 10.0},
                        {title: '15.0', value: 15.0},
                        {title: '20.0', value: 20.0},
                        {title: '25.0', value: 25.0}]
            
        }
        //$scope.qFixed = [0.00, -1.57, 1.57, 0.00, 1.57, 0.00];// Home position
        //* Use the online robot (<- add or remove '/')
        var srv = ROS.requestService('rc/KukaNode/GetConfiguration', '/rc/KukaNode/GetConfiguration');
        var req = new ROSLIB.ServiceRequest({});
        srv.callService(req, function (result) {
            var Q = result.q;
            Q.forEach(function (q,key){
                //$log.info(q);
                $scope.robot.joints[key].q = q;
            });
            $scope.robot.joints.forEach(function(joint, key){
                //$log.info(joint.q);
                $scope.robot.q[key] = parseFloat(joint.q).toFixed(2);
            });
            /*$scope.joints = [];
            angular.forEach(Q,function (q,key){
                $scope.joints.push({name: 'Link '+(key-3), min: -3.14, max: 3.14, q: q});
            });
            $scope.joints[0].name = 'Base';
            $scope.joints[1].name = 'Shoulder';
            $scope.joints[2].name = 'Elbow';
            angular.forEach($scope.joints,function(joint,key){
                $scope.qFixed[key] = parseFloat(joint.q).toFixed(2);
            });
             */
        });
        /*/ //Test using offline
        $scope.joints = [
            {name: 'Base', min: -3.14, max: 3.14},
            {name: 'Shoulder', min: -3.14, max: 3.14},
            {name: 'Elbow', min: -3.14, max: 3.14},
            {name: 'Link 1', min: -3.14, max: 3.14},
            {name: 'Link 2', min: -3.14, max: 3.14},
            {name: 'Link 3', min: -3.14, max: 3.14},
        ];
        //*/
        
        function QtoFixed(joint) {
            var q = $scope.robot.q[joint];
            q = parseFloat(q).toFixed(2);
            $scope.robot.q[joint] = q;
        };

        /*
        var promise;
        $scope.jog = function (dir, joint) {
            promise = $interval(function () {
                if (dir === "Up") {
                    if ($scope.qFixed[joint] < $scope.joints[joint].max) {
                        var joint_q = parseFloat($scope.qFixed[joint]);
                        joint_q += 0.01;
                        $scope.qFixed[joint] = joint_q;
                        $log.debug("Joint: " + joint + " q-val: " + $scope.qFixed[joint]);
                    }
                } else if (dir === "Down") {
                    if ($scope.qFixed[joint] > $scope.joints[joint].min) {
                        $scope.qFixed[joint] -= 0.01;
                        $log.debug("Joint: " + joint + " q-val: " + $scope.qFixed[joint]);
                    }
                } else {
                    $log.error(dir + " is not a valid argument for jog()");
                }
                QtoFixed(joint);
            }, 100);

        };

        $scope.mouseUp = function () {
            $interval.cancel(promise);
        };
        
        /*/
        var promise;
        var ready = true;
        $scope.jog = function (dir, joint) {
            promise = $interval(function () {
                if(ready){
                    ready = false;
                    var joint_q = parseFloat($scope.robot.q[joint]);
                    if (dir === "Up") {
                        if ($scope.robot.q[joint] < $scope.robot.joints[joint].max) {
                            joint_q += 0.01;
                            //$log.debug("Joint: " + joint + " q-val: " + $scope.qFixed[joint]);
                        }
                    } else if (dir === "Down") {
                        if ($scope.robot.q[joint] > $scope.robot.joints[joint].min) {
                            joint_q -= 0.01;
                            //$log.debug("Joint: " + joint + " q-val: " + $scope.qFixed[joint]);
                        }
                    } else {
                        $log.error(dir + " is not a valid argument for jog()");
                    }
                    $scope.robot.q[joint] = joint_q;
                    QtoFixed(joint);
                    
                    var Qf32 = new Float32Array(6);
                    angular.forEach($scope.robot.q, function(q,key){
                        Qf32[key] = q;
                    });
                    
                    // Set configuration
                    var srv = ROS.requestService('rc/KukaNode/SetConfiguration', '/rc/KukaNode/SetConfiguration');
                    var req = new ROSLIB.ServiceRequest({
                        q: [Qf32[0],Qf32[1],Qf32[2],Qf32[3],Qf32[4],Qf32[5]],
                        speed: [$scope.robot.speed.value,
                                $scope.robot.speed.value,
                                $scope.robot.speed.value,
                                $scope.robot.speed.value,
                                $scope.robot.speed.value,
                                $scope.robot.speed.value],
                        acc: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
                    });
                    srv.callService(req, function () {
                        //$log.debug("Callback from KukaNode/SetConfiguration");
                    });
                    
                    
                }else{
                    //var tmpCurr = [0.00, -1.57, 1.5, 0.00, 1.50, 0.00];//[0.00, -1.57, 1.57, 0.00, 1.57, 0.00];
                    var qAccept = [false,false,false,false,false,false];
                    // Get current configuration
                    var srv = ROS.requestService('rc/KukaNode/GetConfiguration', '/rc/KukaNode/GetConfiguration');
                    var req = new ROSLIB.ServiceRequest({});
                    srv.callService(req, function (res) {
                        var qGet = res.q;
                        var offset = 10; // Percentage diff does not make sense around zero!
                        if($scope.robot.q.length === qGet.length){ //qGet.length
                            angular.forEach($scope.robot.q, function(qSet,key){
                                var dif = (offset-qSet)-(offset-qGet[key]); // qGet[i]
                                // If there is a difference calculate the percentage
                                //var test = 10.00-qSet;
                                //$log.debug(test);
                                if(dif !== 0){
                                    var difper = dif/(offset-qSet) * 100;
                                    //$log.debug(difper);
                                    if(Math.abs(difper) < 0.05){
                                        qAccept[key] = true;
                                    }else{
                                        qAccept[key] = false;
                                    }
                                }else{
                                    qAccept[key] = true;
                                }
                            });
                            if(!_.contains(qAccept,false)){
                                ready = true;
                                //$log.debug("Q accepted -> robot still ready")
                            }
                        }
                        //$log.debug("Callback from KukaNode/GetConfiguration");
                    });
                }
            }, 100);

        };

        $scope.mouseUp = function () {
            $interval.cancel(promise);
        };
        //*/
    }

    angular.module("HMI").controller("RCCtrl", RCCtrl);
})();