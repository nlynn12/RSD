(function () {
    "use strict";

    function RCOperationCtrl($scope, $log, $http, ROS, Device) {
        $scope.topics = [{name:"/init",type:'/std_msgs/String'}];
        
        $scope.subscribedTopics = [];
        var subscribers = [];
        
        $scope.isAndroid = Device.isAndroid();
        
        
        var srv = ROS.requestService('/getAllTopics', 'hmi/getAllTopics');
        var request = new ROSLIB.ServiceRequest({});

        srv.callService(request, function (result) {
            var topic_names = result.topic_names;
            var topic_types = result.topic_types;
            $scope.topics = [];
            angular.forEach(topic_names,function (t_name,key){
                $scope.topics.push({name:t_name, type: topic_types[key]});
                subscribers.push(ROS.requestSubscriber(t_name, topic_types[key]));
            });
        });
        
        // Called on select change
        $scope.updateTopics = function (){
            angular.forEach($scope.topics,function(topic,key){
                subscribers[key].unsubscribe();
            });
            angular.forEach($scope.topics,function(topic,key){
                if(_.find($scope.subscribedTopics, function(t){return t.name === topic.name;})){
                    $log.info("Subscribing to: " + topic.name);
                    subscribers[key].subscribe(log.bind(this));
                }else{
                    $log.info("Unsubscribing from: " + topic.name);
                    subscribers[key].unsubscribe();
                }
            });
        }

        function log(msg) {
            //$("#test").prepend($scope.subscribedTopics[0].name + "<br/>");
            $("#test").prepend(msg.data + "<br/>");
        };
        
        var stateService = ROS.requestService('/rc/processNode/getActionList', '/rc/processNode/getActionList');
        var request = new ROSLIB.ServiceRequest({});

        $scope.systemStates = [];
        stateService.callService(request, function (result) {
            $scope.systemStates = result.types;
        });
        
        var stateChangeSub = ROS.requestSubscriber('/rc/processNode/currentAction', 'std_msgs/UInt32');
        
        $scope.states = {
            current: 0
        };
        
        stateChangeSub.subscribe(function(result){
            $scope.states.current = result.data;
            console.log(result.data);
            console.log($scope.states.current);
        });
    }

    angular.module("HMI").controller("RCOperationCtrl", RCOperationCtrl);
})();