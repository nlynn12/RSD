(function () {
    "use strict";

    angular.module('HMI').config(['$routeProvider', function ($routeProvider) {
            $routeProvider.when("/uas", {
                templateUrl: "routes/uas/uas.template.html"
            });
        }]);
})();
