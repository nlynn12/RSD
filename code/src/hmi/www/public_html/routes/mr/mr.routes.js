(function () {
    "use strict";

    angular.module('HMI').config(['$routeProvider', function ($routeProvider) {
            $routeProvider.when("/mobilerobot", {
                templateUrl: "routes/mr/mr.template.html"
            });
        }]);
})();
