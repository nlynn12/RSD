(function () {
    "use strict";

    angular.module('HMI').config(['$routeProvider', function ($routeProvider) {
            $routeProvider.when("/", {
                templateUrl: "routes/home/home.template.html"
            });
        }]);
})();
