(function () {
    "use strict";

    function ROSValidatorCtrl($scope, $timeout, $log, ROS) {
        $scope.pTpc = _.sortBy($scope.pTpc, function (str) {return str;});
        $scope.pSrv = _.sortBy($scope.pSrv, function (str) {return str;});
        
        $scope.services = [];
        $scope.services = _.reject($scope.srv, function (str) {return (str.indexOf("rosapi") >= 0) || (str.indexOf("rosout") >= 0) || (str.indexOf("logger") >= 0);});
        //$scope.services = _.sortBy($scope.services, function (str) {return str;});

        $scope.topics = [];
        $scope.topics = _.reject($scope.tpc, function (str) {return (str.indexOf("rosapi") >= 0) || (str.indexOf("rosout") >= 0) || (str.indexOf("logger") >= 0);});
        //$scope.topics = _.sortBy($scope.topics, function (str) {return str;});
        
        var missingTpc = [];
        $scope.pTpc.forEach(function(dependent_topic){
            if($scope.topics.indexOf(dependent_topic) < 0){
                missingTpc.push(dependent_topic);
            }
        });

        var missingSrv = [];
        $scope.pSrv.forEach(function(dependent_srv){
            if($scope.services.indexOf(dependent_srv) < 0){
                missingSrv.push(dependent_srv);
            }
        });

        var timer1,
            timer2;

        if(missingTpc.length === 0 && missingSrv.length === 0){
            timer1 = $timeout(function () {
                $scope.display = true;
                $scope.validated = true;

                if($scope.validated){
                    timer2 = $timeout(function () {
                        $scope.confirm();
                    }, 2500);
                }
            },2500);
        }else{
            $scope.display = true;
            $scope.validated = false;
            (missingTpc.length !== 0 ? $log.error(missingTpc) : $log.error());
            (missingSrv.length !== 0 ? $log.error(missingSrv) : $log.error());
        }

        $scope.$on("$destroy",
            function (event) {
                $timeout.cancel(timer1);
                $timeout.cancel(timer2);
            }
        );
    }

    angular.module("HMI").controller("ROSValidatorCtrl", ROSValidatorCtrl);
})();