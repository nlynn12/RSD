(function () {
    "use strict";

    function NavCtrl($scope, $location, Device) {
        $scope.isActive = function (viewLocation) {
            return viewLocation === $location.path();
        };
        
        $scope.fullscreen = function(){
            document.body.webkitRequestFullscreen();
        };
        
        $scope.isAndroid = Device.isAndroid();
    }
    
    angular.module("HMI").controller("NavCtrl",NavCtrl);
})();