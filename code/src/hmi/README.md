## HMI ROS Node

This node contains all dependencies for the MVC GUI. By running the launch file, it launches an HTTP server which serves the GUI HTML file. This file opens a websocket connection on which data is transfered from ROS to AngularJS.

The HMI has the following external dependencies:

* ros-indigo-ros-base (basic ROS installation)
* ros-indigo-rosbridge-server [rosbridge_server](http://wiki.ros.org/rosbridge_server)
* ~~ros-indigo-roswww~~ [roswww](http://docs.ros.org/indigo/api/roswww/html/)
   * This has been replaced by a modified version of the original package so it works in both 14.04 LTS and 16.04 LTS

Simply install these dependencies by invoking `sudo apt-get install pkg-name`


### Running the HMI node

The HMI node (`hmi`) has a launch file associated with it, which launches both the HTTP and the websocket server. Simply run it by invoking:

```
roslaunch hmi hmi.launch
```

When the servers has been launched point your browser to the IP of the host with a trailing port (8085) and folder (/hmi), e.g.: [http://localhost:8085/hmi/](http://localhost:8085/hmi/). You now have full access to the complete system through the GUI.

Use with care!



### Programming in AngularJS
* Read about MVC (important software term) used in the GUI

### ROS API
The ROS interface is created with [roslibjs](http://wiki.ros.org/roslibjs) where a AngularJS factory (singleton class) is created. For correct usage, please make sure that the ROS service `ROS` is injected to the controller

#### Publishing a topic

```javascript
var twist = new ROSLIB.Message({
    linear: {x: 0.1,y: 0.2,z: 0.3},
    angular: {x: -0.1,y: -0.2,z: -0.3}
});

ROS.publish('/topicname','geometry_msgs/Twist',twist);
```

#### Subscribing to a topic

```javascript
var subscriber = ROS.requestSubscriber('/topicname', 'topic/type'); // e.g. std_msgs/String

subscriber.subscribe(log.bind(this));

...

function log(msg) {
   $log.info("Callback function received: " + msg.data);
}
```
 
#### Calling a service

```javascript
var srv = ROS.requestService('/topicname', 'service/type'); // service/type, e.g.: hmi/getalltopics

var request = new ROSLIB.ServiceRequest({
    // Insert variables for request
});

srv.callService(request, function (result) {
    // Evaluate the result gotten from the service call
});
```