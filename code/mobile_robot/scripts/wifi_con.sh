#!/bin/sh
#
# Automatically try to connect to 'MyWiFi' when WiFi is enabled
#

# the output of nmcli should be in English
LC_ALL=C

sleep 7

# loop for a while until NetworkManager is accepting commands
nmcli con up ProjektNet
exit 0
