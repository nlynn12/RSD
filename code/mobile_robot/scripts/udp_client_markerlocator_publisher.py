import socket
import sys
import rospy
from std_msgs.msg import String
from markerlocator.msg import markerpose

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print 'Socket is open.'
except socket.error, msg:
    print 'Failed to open socket. Error: ' + str(msg[0]) + ', Message: ' + msg[1]
    sys.exit()
 
host = 'robolab01.projectnet.wlan.sdu.dk';
port = 5005;
 
while(1) :
    msg = 'markerpose_7'
    markerpose_msg = markerpose()
    try :
	pub = rospy.Publisher('/markerlocator/markerpose_7', markerpose, queue_size=10)  
        rospy.init_node('python_udp_client', anonymous=True) 
        rate = rospy.Rate(2) # 2hz
        #Set the whole string
        s.sendto(msg, (host, port))
         
        # receive data from server (data, addr)
        d = s.recvfrom(1024)
        reply = d[0]
        addr = d[1]     
        
	markerpose_msg.header.stamp = rospy.get_rostime()
	markerpose_msg.order = int(reply.split(',')[0])
	markerpose_msg.x = float(reply.split(',')[1])
	markerpose_msg.y = float(reply.split(',')[2])
	markerpose_msg.theta = float(reply.split(',')[3])
	markerpose_msg.quality = float(reply.split(',')[4])
        #markerpose_msg.timestamp = float(reply.split(',')[5])
	markerpose_msg.timestamp = markerpose_msg.header.stamp
        
        rospy.loginfo(markerpose_msg)
        pub.publish(markerpose_msg)
        rate.sleep()
     
    except socket.error, msg:
        print 'Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
        sys.exit()

s.close()