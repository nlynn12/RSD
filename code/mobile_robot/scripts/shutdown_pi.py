import RPi.GPIO as GPIO
import time
import os

shutdown_pin = 12
restart_pin = 11

GPIO.setmode(GPIO.BOARD)
GPIO.setup(shutdown_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(restart_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def Shutdown(channel):
	os.system("sudo shutdown -h now")

def Reboot(channel):
	os.system("sudo reboot")

GPIO.add_event_detect(shutdown_pin, GPIO.BOTH, callback = Shutdown, bouncetime = 2000)
GPIO.add_event_detect(restart_pin, GPIO.BOTH, callback = Reboot, bouncetime = 2000)

while 1:
	time.sleep(1)
