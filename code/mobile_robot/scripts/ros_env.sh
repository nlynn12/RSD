#!/usr/bin/env bash

source /opt/ros/kinetic/setup.bash
source ~/RSD/code/mobile_robot/roswork/devel/setup.bash
export ROS_HOSTNAME=rsd-group1-pi1.projectnet.wlan.sdu.dk

exec "$@"
