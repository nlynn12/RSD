#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Vector3.h"
#include "nav_msgs/Odometry.h"
#include "std_srvs/SetBool.h"
#include "msgs/FloatArrayStamped.h"

#include "top_module/zone_state.h"
#include "top_module/zone_order.h"

#include <Eigen/Geometry>


#include <thread>
#include <mutex>

#include "../include/lineDetect.hpp"


using namespace cv;
using namespace std;
bool setOdom;

//std::mutex mtxCam;

int cols = 320;
int rows = 240;
lineDetector findlines(320,240);
ros::Publisher pub_control;
ros::Publisher odometry_reset;

//ros::Time lastTime;

std::string order;
int intoC = 0;

void imageCallback(const sensor_msgs::ImageConstPtr& msg){
    try{
        //lastTime = msg->header.stamp;

        Mat image_raw;
        image_raw = cv_bridge::toCvShare(msg, "bgr8")->image;

        if(!image_raw.empty())
            findlines.original = image_raw.clone();

//      cv::imshow("view", findlines.intersection);
//      cv::waitKey(1);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
    }


}

static void toEulerianAngle(const Eigen::Quaterniond& q, double& pitch, double& roll, double& yaw)
{
    double ysqr = q.y() * q.y();
    double t0 = -2.0f * (ysqr + q.z() * q.z()) + 1.0f;
    double t1 = +2.0f * (q.x() * q.y() - q.w() * q.z());
    double t2 = -2.0f * (q.x() * q.z() + q.w() * q.y());
    double t3 = +2.0f * (q.y() * q.z() - q.w() * q.x());
    double t4 = -2.0f * (q.x() * q.x() + ysqr) + 1.0f;

    t2 = t2 > 1.0f ? 1.0f : t2;
    t2 = t2 < -1.0f ? -1.0f : t2;

    pitch = std::asin(t2);
    roll = std::atan2(t3, t4);
    yaw = std::atan2(t1, t0);
}

void odometryCallback(const nav_msgs::Odometry msg){
    //translate rotation quaternion
    try{
        Eigen::Quaterniond q(msg.pose.pose.orientation.w, msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, msg.pose.pose.orientation.z);
        double pitch, roll, yaw;
        toEulerianAngle(q, pitch, roll, yaw);
        //Update current robot position and orientation
        findlines.x = msg.pose.pose.position.x;
        findlines.y = msg.pose.pose.position.y;
        findlines.theta = yaw;
        findlines.angularVel = msg.twist.twist.angular.z;

    }catch(const std::exception &e){
        ROS_ERROR("Something wrong with the Odometry msg");
    }

}


bool zone_activation(top_module::zone_order::Request& request, top_module::zone_order::Response& response){

    findlines.pathIndex = 0;
    findlines.active = true;
    findlines.robotFinished = false;
    response.confirmation = true;
    response.message = "Line following enabled";
    setOdom = false;
    bool resetOdom = false;

    ROS_INFO_STREAM(request.order);

    if(request.order == "C1Unload"){
        order = "xfrfebs";
        intoC = 1;
        resetOdom = true;
    }
    else if(request.order == "C1Load"){
        order = "xffrfibs";
        intoC = 2;
        resetOdom = true;
    }
    else if(request.order == "C2Load"){
        order = "xfffrfebs";
        intoC = 3;
        resetOdom = true;
    }
    else if(request.order == "C2Unload"){
        order = "xffffrfibs";
        intoC = 4;
        resetOdom = true;
    }
    else if(request.order == "DroneDoor"){
    	order = "xes";
        intoC = 5;
    }
    else if(request.order == "STOP"){
        order = "s";
        intoC = 6;
    }
    else if(request.order == "GoHome"){
        switch(intoC){
        case 1:
            order = "hhlhh";
            setOdom = true;
        break;
        case 2:
            order = "hhlhhh";
            setOdom = true;
        break;
        case 3:
            order = "hhlhhhh";
            setOdom = true;
        break;
        case 4:
            order = "hhlhhhhh";
            setOdom = true;
        break;
        case 5:
            order = "h";
       	break;
        default:
            order = "s";
        }
    }
    else{
    	order = "s";
    	findlines.pathIndex = 0;
		findlines.active = false;
		findlines.robotFinished = false;
		response.confirmation = false;
		response.message = "Unknown command";
		ROS_INFO("Unknown command received");
		return false;
    }

    if(resetOdom){
    	msgs::FloatArrayStamped odomPos;
	    odomPos.data.push_back(0);
	    odomPos.data.push_back(0);
	    odomPos.data.push_back(0);
	    odometry_reset.publish(odomPos);
    }

  ROS_INFO("Line follower received order");

  return true;
}


int main( int argc, char** argv )
{

    ros::init(argc, argv, "line_follower");

    ros::NodeHandle n;

    //findlines = new lineDetector(rows, cols);


    image_transport::ImageTransport it(n);

    //To set the parameters here
    ros::NodeHandle private_node_handle_("~");
    private_node_handle_.param("crossroad_density", findlines.crossThreshold, double(5000));
    private_node_handle_.param("display_mode", findlines.DISPLAY, int(0));
    private_node_handle_.param("TRAINING", findlines.ON_TRAINING, bool(false));
    private_node_handle_.param("stringOrder", order, std::string("s"));
    private_node_handle_.param("P_lfVelControl", findlines.lfVelControl.P, double(1));
    private_node_handle_.param("I_lfVelControl", findlines.lfVelControl.I, double(0));
    private_node_handle_.param("D_lfVelControl", findlines.lfVelControl.D, double(10));
    private_node_handle_.param("P_lfPosControl", findlines.lfPosControl.P, double(10));
    private_node_handle_.param("I_lfPosControl", findlines.lfPosControl.I, double(0));
    private_node_handle_.param("D_lfPosControl", findlines.lfPosControl.D, double(0));
    private_node_handle_.param("P_odomFordward", findlines.linearOdom.P, double(0.7));
    private_node_handle_.param("I_odomFordward", findlines.linearOdom.I, double(0));
    private_node_handle_.param("D_odomFordward", findlines.linearOdom.D, double(0));
    private_node_handle_.param("P_odomAngle", findlines.angleOdom.P, double(1));
    private_node_handle_.param("I_odomAngle", findlines.angleOdom.I, double(0));
    private_node_handle_.param("D_odomAngle", findlines.angleOdom.D, double(0));
    private_node_handle_.param("DistanceFWD", findlines.intersectionFordwardMovement, double(0.6));
    private_node_handle_.param("DistanceBWD", findlines.intersectionBackwardMovement, double(-0.2));

    string activation_string, endofoperation_string, camera_sub, odom_sub;
    private_node_handle_.param("node_activation_service_server", activation_string, std::string("/zone_activation/line_following") );
    private_node_handle_.param("node_service_client", endofoperation_string, std::string("/zone_activation/TopModule"));
    private_node_handle_.param("camera_subscriber", camera_sub, std::string("/camera/image_raw"));
    private_node_handle_.param("odometry_subscriber", odom_sub, std::string("/fmKnowledge/pose"));
    

    //Services
    ros::ServiceServer zoneSrv = n.advertiseService(activation_string, zone_activation);
    ros::ServiceClient TopSrv = n.serviceClient<top_module::zone_state>(endofoperation_string);

    //Subscribers
    image_transport::Subscriber imgsub = it.subscribe(camera_sub, 1, imageCallback);
    ros::Subscriber odomsub = n.subscribe(odom_sub, 1, odometryCallback);

    //Publishers
    pub_control = n.advertise<geometry_msgs::TwistStamped>("/fmCommand/cmd_vel", 10);
	odometry_reset = n.advertise<msgs::FloatArrayStamped>("/fmInformation/odom_reset", 1000);

    order = "s";

    findlines.active = false;
    ROS_INFO("Line following node active");

//        cv::namedWindow("view");
//        cv::startWindowThread();

    geometry_msgs::TwistStamped robotTurn;

    robotTurn.twist.linear.y = 0;
    robotTurn.twist.linear.z = 0;
    robotTurn.twist.angular.x = 0;
    robotTurn.twist.angular.y = 0;
    ros::Rate loop_rate(60);

    int count = 0;

    while (ros::ok())
    {
        ros::Time time = ros::Time::now();
        robotTurn.header.stamp = time;
		//ros::Duration timeLapse = time - lastTime;

        //Update the commands to the robot.
        if(findlines.active)
		{      
		/*          
	        if(timeLapse.toSec()>2){
	            Mat failure;
	            findlines.original = failure;
	            ROS_WARN("Not getting a new image");
	        }*/
			robotTurn.twist.linear.x = findlines.robotSpeed;
        	robotTurn.twist.angular.z = findlines.robotTurn;
			findlines.mission(order);
            pub_control.publish(robotTurn);
		}

        if(findlines.robotFinished){
        	if(setOdom){
    			msgs::FloatArrayStamped odomPos;
			    odomPos.data.push_back(4.65);
			    odomPos.data.push_back(0.8);
			    odomPos.data.push_back(3.14/2);
			    odometry_reset.publish(odomPos);
        	}

			top_module::zone_state srv;
            srv.request.status = !findlines.errorFlag;
            srv.request.message = findlines.errorMessage;
            findlines.active = false;
			order = "s";	

            if(findlines.errorFlag)
            	ROS_ERROR("Line lost");
            else
                ROS_INFO("End of path");
			if(TopSrv.call(srv)){
				findlines.robotFinished = false;
                ROS_INFO("Top module answered to line follower end of path");
			}
		}


        ros::spinOnce();

        loop_rate.sleep();
        ++count;
    }

    //cv::destroyWindow("view");
    ROS_INFO("closing line following");


    return 0;

}
