#include "lineDetect.hpp"

#define ERROR std::cout << "here " << std::endl;
////////////////////////////////////////////////////////
/// Class constructor: Initialization of openCV parameters
///
lineDetector::lineDetector(int rows, int cols){

    initialization(rows, cols);
}

lineDetector::lineDetector(int rows, int cols, std::string window){

    initialization(rows, cols);
    figure_name = window;

}

lineDetector::~lineDetector(){

}


void lineDetector::initialization(int rows, int cols){

    cvtweaks.hsvLowThreshold[0] = 85;
    cvtweaks.hsvLowThreshold[1] = 100;
    cvtweaks.hsvLowThreshold[2] = 150;
    cvtweaks.hsvHighThreshold[0] =105;
    cvtweaks.hsvHighThreshold[1] =255;
    cvtweaks.hsvHighThreshold[2] =235;
    cvtweaks.dilation_elem = 2;
    cvtweaks.dilation_size = 3;
    cvtweaks.erosion_elem = 1;
    cvtweaks.erosion_size = 3;
    cvtweaks.harris_thresh = 200;

    //Squares
    nOfRectangles = 4;
    rectangleHeight = 50;

    //Recu = new int(nOfRectangles);
    //Recv = new int(nOfRectangles);
    //Recwidth = new int(nOfRectangles);
    //Recheight = new int(nOfRectangles);
    lineCenters.reserve(nOfRectangles);
    //input = new double(2*nOfRectangles);
    //prev_input = new double(2*nOfRectangles);
    w.reserve(nOfRectangles*2);
    lastw.reserve(nOfRectangles*2);

    for(int i = 0; i<nOfRectangles; i++)
    {
        lineCenters.push_back(line_center());
        Recu[i] = 0;
        Recwidth[i] = cols;
        Recheight[i] = rectangleHeight;
        Recv[i] = rows-(i+1)*rectangleHeight;
    }

    //Setup weights
    w.push_back(1);
    w.push_back(1);
    lastw.push_back(1);
    lastw.push_back(1);

//    input.push_back(0);
//    input.push_back(0);
//    prev_input.push_back(0);
//    prev_input.push_back(0);

    for(int i = 2; i < 2*nOfRectangles; i++){
        w.push_back(0);
        lastw.push_back(0);
//        input.push_back(0);
//        prev_input.push_back(0);
    }


    std::ifstream f("/home/rsd-group1/weightData0.txt");
    if(f.good()){
        loadWeights(&w, &weight_lineNumber, 0);
//        for(int i = 0; i < w.size(); i++)
//            std::cout << w[i] << " " << std::endl;
    }
    else
        weight_lineNumber = 0;

    for(int i = 0; i < w.size(); i++){
        input[i] = 0;
        prev_input[i] = 0;

    }


    robotSpeed = 0;
    robotTurn = 0;

    figure_name = "window";
    DISPLAY = 0;

    learning_rate = 0.07;
    ON_TRAINING = false;
    crossThreshold = 5000;


    //set up pids
    lfVelControl.P = 1;
    lfVelControl.D = 10;
    lfVelControl.reference = 0;
    lfPosControl.P = 10;
    lfPosControl.I = 0;
    lfPosControl.reference = 0;

    intersectionFordwardMovement = 0.6;
    intersectionBackwardMovement = -0.2;
    linearOdom.reference = intersectionFordwardMovement;
    linearOdom.I = 0;
    linearOdom.P = 0.7;
    angleOdom.I = 0.05;
    angleOdom.P = 0.7;

    //For state machine
    operation_mode = OM_CHANGE_MODE;
    pathIndex = 0;
    active = false;
    robotFinished = false;
    errorMessage = "-";
    errorFlag = false;

}

///////////////////////////////////////////////////////////////
/// state machine used during operation in line following area
///
void lineDetector::mission(std::string pathToWCell){
    //Activation and state machine
    if(pathIndex == 0)
	operation_mode = OM_CHANGE_MODE;

    switch(operation_mode)
    {
    case OM_LINE_FOLLOWING:
        followLine();
        /**///The tested line follower
        if(robotTurn<=0.01 && robotTurn >= -0.01)
            robotSpeed = 0.4;
        else if(robotTurn >=0.3 || robotTurn <= -0.3)
            robotSpeed = 0.2;
        else if(robotTurn>0)
            robotSpeed = 0.4-0.3*robotTurn;
        else if(robotTurn<0)
            robotSpeed = 0.4+0.3*robotTurn;
        /*/
        robotSpeed = 0.2;
        /**/
        //std::cout << robotSpeed << " " << robotTurn << std::endl;
    break;
    case OM_GOING_HOME:
        followLine();
        /**///The tested line follower
        if(robotTurn<=0.01 && robotTurn >= -0.01)
            robotSpeed = 0.4;
        else if(robotTurn >=0.3 || robotTurn <= -0.3)
            robotSpeed = 0.2;
        else if(robotTurn>0)
            robotSpeed = 0.4-0.3*robotTurn;
        else if(robotTurn<0)
            robotSpeed = 0.4+0.3*robotTurn;
        /*/
        robotSpeed = 0.2;
        /**/
    break;
    case OM_START:
        robotSpeed = 0;
        followLine();
    break;
    case OM_ODOM_FWD:
        //Here we trust odometry
        linearOdom.reference = intersectionFordwardMovement;
        if(!intersectionFordward()){
            operation_mode = OM_CHANGE_MODE;
        }
    break;
    case OM_ODOM_BWD:
        if(!intersectionFordward()){
            operation_mode = OM_CHANGE_MODE;
        }
    break;
    case OM_TURN_LEFT:
        if(!intersectionTurn())
            operation_mode = OM_CHANGE_MODE;
    break;
    case OM_TURN_RIGHT:
        if(!intersectionTurn())
            operation_mode = OM_CHANGE_MODE;
    break;
    case OM_TURN_ALL_LEFT:
         if(!intersectionTurn())
             operation_mode = OM_CORRECT;
    break;
    case OM_TURN_ALL_RIGHT:
        if(!intersectionTurn())
            operation_mode = OM_CORRECT;
    break;
    case OM_CORRECT:
        robotSpeed = 0;
        followLine();
        if(robotTurn < 0.02 && robotTurn > -0.02)
            operation_mode = OM_CHANGE_MODE;
    break;
    case OM_STOP:
        robotTurn = 0;
        robotSpeed = 0;
        robotFinished = true;
        active = false;
        break;
    case OM_CHANGE_MODE:        
        operation_mode = stringToStateMachine(pathToWCell, pathIndex);
        pathIndex++;
        initialiseOdometry();
        break;
    default:
        robotTurn = 0;
        robotSpeed = 0;
        break;
    }

}

//////////////////////////////////////////////////////////
/// Line follower function
///
void lineDetector::followLine(){

    if(!original.empty()){
        //Transformation and binarization of the original image
        cv::cvtColor( original, img_hsv, CV_RGB2HSV );
        binarization(img_hsv, &img_bin);

        //Image processing, find line in different parts of the image. Definition of neuron's inputs
        if(!getLinePositions(img_bin)){
            if(operation_mode == OM_LINE_FOLLOWING){
                operation_mode = OM_START;
                initialiseOdometry();

            }

            if(operation_mode == OM_GOING_HOME)
                operation_mode = OM_STOP;

            if(operation_mode == OM_ODOM_FWD){
                robotTurn = 0;
                return;
            }
        }
        else{
            if(operation_mode == OM_START)
                operation_mode = OM_LINE_FOLLOWING;
        }

        if(operation_mode == OM_START){
            //robotTurn = 0.4;
            if(!intersectionTurn()){
                operation_mode = OM_STOP;
                errorMessage = "LF: line Lost";
                errorFlag = true;
            }

            return;
        }


        //Look for intersections
        if(findIntersections()){
            if(operation_mode == OM_LINE_FOLLOWING || operation_mode == OM_GOING_HOME)
            {
                operation_mode = OM_ODOM_FWD;
                initialiseOdometry();
            }
        }



        //Neural training;
        if(ON_TRAINING){
            neuralTraining();
        }

        //Update weight size for averaging output
        totalWl = 0;
        totalWr = 0;
        for(int i = 0; i<nOfRectangles; i++ ){
            totalWl += w[2*i];
            totalWr += w[2*i+1];
        }

        //Set turning parameter
        double NNoutput = 0;
        int tolook = nOfRectangles;
        if(OM_CORRECT || OM_ODOM_FWD){
            tolook = 1;
        }

        totalWl = 1;
        for(int i = 0; i < tolook; i++){
            NNoutput -= w[2*i]*input[2*i]/totalWl;
            NNoutput += w[2*i+1]*input[2*i+1]/totalWr;
        }

        if(operation_mode == OM_LINE_FOLLOWING || operation_mode == OM_GOING_HOME || operation_mode == OM_ODOM_FWD){
            //Double PID
            double positionError = lfPosControl.calculateOutput(NNoutput);
            lfVelControl.reference = positionError;
            robotTurn = lfVelControl.calculateOutput(angularVel);
        }else{
            robotTurn = 0.7*lfPosControl.calculateOutput(NNoutput);
        }

        if(robotTurn > 1)
            robotTurn = 1;
        if(robotTurn < -1)
            robotTurn = -1;


    }else{
        robotTurn = 0;
        robotSpeed = 0;
    }

    return;


}


void lineDetector::neuralTraining(){
    //Update weights
    for(int i = 2; i < w.size(); i++){
            lastw[i] = w[i];
            w[i] += learning_rate*prev_input[i]*(input[i%2]-prev_input[i%2]);
    }

    //save prev. input
    for(int i = 0; i<w.size(); i++)
        prev_input[i] = input[i];

    //Save weights
    saveWeights(w, lastw, input, weight_lineNumber, 0);
    weight_lineNumber++;

}

////////////////////////////////////////////////////////////////////////////
/// Function to detect the position of the line in each level
/// Returns false if the line is lost in the closest part of the image
bool lineDetector::getLinePositions(cv::Mat input_image){

    for( int i = 0; i < nOfRectangles; i++){
        //Cut section of the image

        cv::Mat subImg = input_image(cv::Rect(0,input_image.rows-(1+i)*rectangleHeight,input_image.cols,rectangleHeight));
        //ERROR
        //Finding centroid of the seen line.
        lineCenters[i] = getCentroid(subImg);

        if((lineCenters[i].centroid.x != -1 && lineCenters[i].centroid.y != -1) && !std::isnan(lineCenters[i].centroid.x) && !std::isnan(lineCenters[i].centroid.y)){
            //Define output to motor controller
            //%desviation
            double error = (lineCenters[i].centroid.x-(input_image.cols/2))/(input_image.cols/2);
            if(DISPLAY==DISPLAY_ERROR)
                std::cout << "error: " << error << std::endl;

            //Set inputs to neurons
            if(error>0){
                input[2*i] = 0;
                input[2*i+1] = error;
            }
            else{
                input[2*i] = -error;
                input[2*i+1] = 0;
            }


        }else{
            if(prev_input[2*i]>0.8)
                input[2*i] = 1;
            else
                input[2*i] = 0;
            if(prev_input[2*i+1]>0.8)
                input[2*i+1] = 1;
            else
                input[2*i+1] = 0;
        }
    }
    //Return false if line is lost (in closest layer)
    if((lineCenters[0].centroid.x == -1 && lineCenters[0].centroid.y == -1) || std::isnan(lineCenters[0].centroid.x) || std::isnan(lineCenters[0].centroid.y))
        return false;

    return true;
}



///////////////////////////////////////////////////////////////
/// Intersection detection function, find crossess in the image
/// Returns true if it finds an intersection
bool lineDetector::findIntersections(){
    //look at the area of the lines in the layers of the image.
    //If the area is bigger than a threshold in the closest layer, return true
    if(DISPLAY==DISPLAY_INTERSECTIONS)
        std::cout << crossThreshold << "\t" << lineCenters[1].image_moments.m00 << "\t " << lineCenters[0].image_moments.m00 << std::endl;

    /*/
    switch(active_intersection){
    case 0:
        if(lineCenters[1].image_moments.m00 >crossThreshold)
            active_intersection = 1;
        else
            active_intersection = 0;
        break;
    case 1:
        if(lineCenters[1].image_moments.m00 <crossThreshold && lineCenters[0].image_moments.m00 > crossThreshold)
            active_intersection = 2;
        else
            if(lineCenters[1].image_moments.m00 <crossThreshold && lineCenters[0].image_moments.m00 < crossThreshold)
                active_intersection = 0;
            else
                active_intersection = 1;
        break;
    case 2:
        local_ref_x = x;
        local_ref_y = y;
        local_ref_theta = theta;
        active_intersection = 0;
        return true;
        break;
    default:
        break;
    }


    /*/
    if(lineCenters[0].image_moments.m00 > crossThreshold)
        return true;
    /**/

    return false;
}


///////////////////////////////////////////////////////////////
/// Go a bit fordward in intersections using odometry
///
bool lineDetector::intersectionFordward(){

    double lastMovedDistance = (x-lastx)/cos(theta);

    if((totalMovedDistance < linearOdom.reference*0.95 || totalMovedDistance > linearOdom.reference*1.05) && (robotSpeed > 0.01 || robotSpeed < -0.01)){
        //display odometry readings
        if(DISPLAY == DISPLAY_ODOMETRY)
            std::cout << "FWD -> x:" << x << "\t y:" << y << "\t theta:" << theta << "\t error:" << linearOdom.error << "\t lmd:" << lastMovedDistance <<  "\t tmd:" << totalMovedDistance << "\t ifm:" << linearOdom.reference<< std::endl;

        updateThetas();
        totalMovedDistance += lastMovedDistance;

        followLine();
        robotSpeed = linearOdom.calculateOutput(totalMovedDistance);

        if(robotSpeed > 0.3)
            robotSpeed = 0.3;
        if(robotSpeed < -0.2)
            robotSpeed = -0.2;

        if(robotSpeed < 0.04 && robotSpeed > -0.04){
            robotTurn = 0;
        }

        if(operation_mode==OM_STOP){
            operation_mode==OM_ODOM_FWD;
            ERROR
        }

        lastx = x;

    }else{
        return false;
    }
    return true;

}



////////////////////////////////////////////////////////////////
/// Turn on intersections
///
bool lineDetector::intersectionTurn(){

    robotSpeed = 0;
    updateThetas();
    double error = angleOdom.reference-theta;

    if(DISPLAY == DISPLAY_ODOMETRY){
       std::cout << "TURN -> x:" << x << "\t y:" << y << "\t theta:" << theta << "\t error:" << error << "\t goal:" << angleOdom.reference<< std::endl;
    }

    if(!(error/angleOdom.reference > -0.05 && error/angleOdom.reference < 0.05)){
        robotTurn = -angleOdom.calculateOutput(theta);
        if(robotTurn > 1.8)
            robotTurn = 1.8;
        if(robotTurn < -1.8)
            robotTurn = -1.8;
        lastheta = theta;
        if(operation_mode == OM_START)
            robotTurn = 0.4;

    }
    else
        return false;

    return true;
}

////////////////////////////////////////////////////////////////
/// Initialise odometry
///
void lineDetector::initialiseOdometry(){
    lastx = x;
    lasty = y;
    lastheta = theta;
    totalMovedDistance = 0;
    linearOdom.resetController();
    angleOdom.resetController();
    angleOdom.lastinput = lastheta;
    lfVelControl.resetController();
    lfPosControl.resetController();

    switch(operation_mode){
    case OM_ODOM_FWD:
        linearOdom.reference = intersectionFordwardMovement;
    break;
    case OM_ODOM_BWD:
        linearOdom.reference = intersectionBackwardMovement;
    break;
    case OM_TURN_LEFT:
        angleOdom.reference = theta-3.142/2;
    break;
    case OM_TURN_RIGHT:
        angleOdom.reference = theta+3.142/2;
    break;
    case OM_TURN_ALL_LEFT:
        angleOdom.reference = theta-3.1415;
    break;
    case OM_TURN_ALL_RIGHT:
        angleOdom.reference = theta+3.1415;
    break;
    case OM_START:
        angleOdom.reference = theta-3.14/4;
    break;
    default:
        angleOdom.reference = theta;
        linearOdom.reference = 0;
    break;
    }
}

void lineDetector::updateThetas(){
    //Manage problems in case we jump from -180 to 180
    if((lastheta - theta)>3.14)
        theta = 2*3.14+theta;
    else if((lastheta - theta) < -3.14)
        theta = -2*3.14 +theta;

    if(theta < -2*3.14){
        theta += 2*3.14;
        lastheta+= 2*3.14;
    }
    if(theta > 2*3.14){
        theta -=2*3.14;
        lastheta -=2*3.14;
    }
}



/////////////////////////////////////////////////////////////
/// binarization of image in hsv space with predefined parameters
///
void lineDetector::binarization(cv::Mat input, cv::Mat *output){
    //Binarization
    cv::inRange(input, cv::Scalar (cvtweaks.hsvLowThreshold[0], cvtweaks.hsvLowThreshold[1], cvtweaks.hsvLowThreshold[2]),
                            cv::Scalar (cvtweaks.hsvHighThreshold[0], cvtweaks.hsvHighThreshold[1], cvtweaks.hsvHighThreshold[2]),
                *output);
    return;
}


/////////////////////////////////////////////////////////////////////////////////
/// Function to obtain the position of the centroid of the biggest object in the image
///
line_center lineDetector::getCentroid(cv::Mat input){

    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    std::vector<cv::Moments> mu;

    line_center output;

    cv::findContours(input,contours,hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

    if(contours.size()>0){

        for( size_t i = 0; i < contours.size(); i++ ){
            mu.push_back( moments( contours[i], false ));
        }
    
        //The moments m10 and m01 contain the coordinates of the center of the line. m00 is the area of the object
        //Get biggest object
        int big = 0;
        for(int i = 1; i<contours.size(); i++){
            if(mu[i-1].m00<mu[i].m00)
                big = i;
        }

        //Get it's center
        cv::Point2d mc( static_cast<float>(mu[big].m10/mu[big].m00) , static_cast<float>(mu[big].m01/mu[big].m00) );
    
        // For visualization
        if(DISPLAY==DISPLAY_CENTROIDS){
            std::cout << mc.x << "\t" << mc.y << std::endl;
            cv::circle(original, mc, 4, cv::Scalar( 255, 255, 255), -1, 8, 0);
        }
        //*/

        output.centroid = mc;
        output.image_moments = mu[big];
    
    }else{
        if(DISPLAY==DISPLAY_CENTROIDS){
            std::cout << "no contours found " << std::endl;
        }
        cv::Point2d mc(-1, -1);
        output.centroid = mc;
    }

    return output;

}


////////////////////////////////////////////////////////////////////////
/// Check parameters on a string
///
int lineDetector::stringToStateMachine(std::string string, int indexToRead){
    int SM_index;
    char character;

    if(string.size()>indexToRead)
        character = string[indexToRead];
    else
        character = 's';

    switch(character){
    case 'x':
        SM_index = OM_START;
    break;
    case 'l':                   //Left
        SM_index = OM_TURN_LEFT;
    break;
    case 'r':                   //Right
        SM_index = OM_TURN_RIGHT;
    break;
    case 'e':                   //all lEft
        SM_index = OM_TURN_ALL_LEFT;
    break;
    case 'i':                   //all rIght
        SM_index = OM_TURN_ALL_RIGHT;
    break;
    case 'f':                   //Follow
        SM_index = OM_LINE_FOLLOWING;
    break;
    case 'b':                   //go Backwards
        SM_index = OM_ODOM_BWD;
        robotSpeed = 0.07;
    break;
    case 'h':
        SM_index = OM_GOING_HOME; //go home
    break;
    case 's':                   //Stop
        SM_index = OM_STOP;
        //std::cout << "stopped" << std::endl;
        errorFlag = false;
        errorMessage = "LF: all OK";
    break;
    default:
        SM_index = OM_STOP;
        //std::cout << "stopped" << std::endl;
        errorFlag = false;
        errorMessage = "LF: all OK";
    }


    return SM_index;

}















