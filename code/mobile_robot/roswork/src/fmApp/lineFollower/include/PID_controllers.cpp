#include"PID_controllers.hpp"

//Initialization
PID_controller::PID_controller(){
    P = 1;
    I = 0;
    D = 0;
    maxvecsize = 100;
    error = 0;
    lastinput = 0;
    reference = 0;
}
PID_controller::~PID_controller(){
}

//Calculate output of the controller, given new input
double PID_controller::calculateOutput(double newInput){

    double output;
    error = reference-newInput;

    double differentialError = newInput-lastinput;
    lastinput = newInput;

    errorHistory = vectorMoveLeft(errorHistory, error);
    double integralError = vectorSum(errorHistory);

    output = error*P + differentialError*D + integralError*I;

    return output;

}

//Reset controller history
void PID_controller::resetController(){
    error = 0;
    lastinput = 0;
    vectorClear(errorHistory);
}

//Update vector pushing a new value and pushing the rest to the left, keeping a constant maximum size
std::vector<double> PID_controller::vectorMoveLeft(std::vector<double> thevector, double thenewvalue){

    if(thevector.size()>maxvecsize){
        std::vector<double> tmp(thevector.begin() + 1, thevector.end());
        thevector = tmp;
    }

    thevector.push_back(thenewvalue);
    return thevector;
}

//Sum of elements of a vector
double PID_controller::vectorSum(std::vector<double> thevector){
    double sum = 0;
    for(int i = 0; i < thevector.size(); i++)
        sum+=thevector[i];

    return sum;
}

// Set to 0 al values in a vector
void PID_controller::vectorClear(std::vector<double> &thevector)
{
    for(int i = 0; i < thevector.size(); i++)
        thevector[i] = 0;
    return;
}
