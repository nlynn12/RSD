#ifndef _PID_CONTROLLERS
#define _PID_CONTROLLERS

#include <iostream>
#include <vector>

// Class defining a PID controller, includes the P, I and D parameters of a PID and some utilities to manage the PID

class PID_controller
{
public:
    PID_controller();
    ~PID_controller();
    double P, I, D;
    double error;
    double lastinput;
    double reference;
    double maxvecsize;
    std::vector<double> errorHistory;

    void resetController();
    double calculateOutput(double newInput);

private:
    std::vector<double> vectorMoveLeft(std::vector<double> thevector, double thenewvalue);
    double vectorSum(std::vector<double> thevector);
    void vectorClear(std::vector<double> &thevector);

};





#endif
