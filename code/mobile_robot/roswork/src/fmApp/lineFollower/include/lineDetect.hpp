
#ifndef _LINE_DETECT
#define _LINE_DETECT

#include <iostream>
#include <opencv2/opencv.hpp>
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include "dataCollection.hpp"
#include "PID_controllers.hpp"

#define OM_STOP -1
#define OM_LINE_FOLLOWING 0
#define OM_ODOM_FWD 1
#define OM_ODOM_BWD 2
#define OM_TURN_LEFT 3
#define OM_TURN_RIGHT 4
#define OM_TURN_ALL_LEFT 5
#define OM_TURN_ALL_RIGHT 6
#define OM_CHANGE_MODE 7
#define OM_GOING_HOME 8
#define OM_CORRECT 9
#define OM_START 10

#define DISPLAY_ERROR 1
#define DISPLAY_CENTROIDS 2
#define DISPLAY_INTERSECTIONS 3
#define DISPLAY_ODOMETRY 4

//Struct with center and image moments of each part of the line
struct line_center{
    cv::Point2d centroid;
    cv::Moments image_moments;

    bool operator < (const line_center& str) const
    {
        return (image_moments.m00 < str.image_moments.m00);
    }

};

//Opencv parameters for image processing.
struct cv_tweaks
{
    int hsvLowThreshold[3];
    int hsvHighThreshold[3];
    int dilation_elem;
    int dilation_size;
    int erosion_elem;
    int erosion_size;
    int harris_thresh;
};


//Class including the functions and variables used to control the Frobomind into the line-following area
class lineDetector{
public:
    lineDetector(int rows, int cols);
    lineDetector(int rows, int cols, std::string window);
    ~lineDetector();

    void mission(std::string pathToWCell);

    //PIDs
    PID_controller linearOdom, angleOdom, lfPosControl, lfVelControl;
    double intersectionBackwardMovement;
    double intersectionFordwardMovement;

    cv_tweaks cvtweaks;
    double crossThreshold;

    std::string figure_name;
    cv::Mat original, img_bin, img_gray;

    double x, y, theta, angularVel;

    double robotTurn;
    double robotSpeed;
    int pathIndex;

    std::string errorMessage;
    bool errorFlag;
    bool active;
    bool robotFinished;
    bool GoingHome;

    bool ON_TRAINING;
    int DISPLAY;

private:

    //functions
    void initialization(int rows, int cols);
    void followLine();
    void neuralTraining();
    void binarization(cv::Mat input, cv::Mat *output);
    void MorphologycalOperations(cv::Mat input, cv::Mat *output);
    line_center getCentroid(cv::Mat input);
    bool getLinePositions(cv::Mat input_image);
    bool findIntersections();
    double findLostLine();
    void harrisCornerDetector();
    bool intersectionFordward();
    bool intersectionTurn();
    void initialiseOdometry();
    void updateThetas();
    void fitPoints(const std::vector<line_center> points);
    int stringToStateMachine(std::string string, int indexToRead);

    //NN variables
    double input[8];
    double prev_input[8];
    std::vector<double> w;
    std::vector<double> lastw;
    int weight_lineNumber;
    double learning_rate;
    double totalWl, totalWr;


    //Image areas
    int Recu[4];
    int Recv[4];
    int Recwidth[4];
    int Recheight[4];
    int nOfRectangles;
    int rectangleHeight;

    //Image features
    std::vector<line_center> lineCenters;
    double line_theta, line_offset;

    //Odometry navigation
    double local_ref_x, local_ref_y, local_ref_theta;
    double lastx, lasty, lastheta;
    double totalMovedDistance;
    double goaltheta;

    //state machine
    int operation_mode;
    int active_intersection;

    cv::Mat img_hsv;


};



#endif
