#ifndef WALL_FOLLOWING_HPP
#define WALL_FOLLOWING_HPP

#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/Imu.h>

#define RAD2DEG 180/M_PI
#define DEG2RAD M_PI/180

class wall_following
{
public:
    wall_following(ros::NodeHandle &n_);

    void lidar_cb(const sensor_msgs::LaserScan::ConstPtr &laser_scan);
    void imu_cb(const sensor_msgs::Imu::ConstPtr &data);

    double get_result();
    double get_distance_front();
    void set_wall_distance(double distance);
    double get_current_yaw();

    void set_follow_left();
    void set_follow_right();
//private:    
    unsigned int lidar_wall_index = 45;

    ros::NodeHandle &n;

    ros::Subscriber lidar_sub;
    ros::Subscriber imu_sub;

    double distance_min = 0;
    double distance_front = 4;
    double current_yaw = 0;

    struct PID
    {
        double K_p = 1;
        double K_i = 0;
        double K_d = 0;

        double target       = 0;
        double error        = 0;
        double integral     = 0;
        double prev_error   = 0;
        double derivative   = 0;
        double result       = 0;
    };

    struct PID angle_PID;
    struct PID wall_PID;

    void process_PID(struct PID &PID, double sensor_value);
};

#endif /* WALL_FOLLOWING_HPP */
