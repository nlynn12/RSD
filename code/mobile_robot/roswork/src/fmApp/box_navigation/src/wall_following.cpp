#include "wall_following.hpp"

#include <geometry_msgs/Quaternion.h>

wall_following::wall_following(ros::NodeHandle &n_) : n(n_)
{
    lidar_sub   = n.subscribe("/laser_scan", 1, &wall_following::lidar_cb, this);
    imu_sub     = n.subscribe("/fmInformation/imu", 1, &wall_following::imu_cb, this);

    wall_PID.K_p = 1;
    wall_PID.K_d = 10;
    angle_PID.K_p = 10;
    angle_PID.K_d = 0;
}

void wall_following::imu_cb(const sensor_msgs::Imu::ConstPtr &data)
{
    double ysqr = data->orientation.y * data->orientation.y;
    double t0 = -2.0 * (ysqr + data->orientation.z * data->orientation.z) + 1.0;
    double t1 = +2.0 * (data->orientation.x * data->orientation.y - data->orientation.w * data->orientation.z);
    current_yaw = std::atan2(t1, t0);

    process_PID(angle_PID, current_yaw);


    if(lidar_wall_index != 45)
        angle_PID.result *= -1;

    //ROS_INFO_STREAM("Yaw: " << current_yaw*RAD2DEG << "\nPID Result: " << angle_PID.result << "\n" );
}

void wall_following::lidar_cb(const sensor_msgs::LaserScan::ConstPtr &laser_scan)
{
    int size = laser_scan->ranges.size();
    distance_min = laser_scan->ranges[lidar_wall_index];
    distance_front = laser_scan->ranges[size/2];

    process_PID(wall_PID, distance_min);

    angle_PID.target = current_yaw + wall_PID.result;

    //ROS_INFO_STREAM("Distance Min: " << distance_min << "\nPID Result: " << wall_PID.result << "\nAngle Target: "  << angle_PID.target << "\n");
}

void  wall_following::process_PID(struct PID &PID, double sensor_value)
{
    PID.error = sensor_value - PID.target;
    PID.integral += PID.error;
    PID.derivative = PID.error - PID.prev_error;

    PID.prev_error = PID.error;

    PID.result = PID.K_p*PID.error + PID.K_i*PID.integral + PID.K_d*PID.derivative;
}

double wall_following::get_result()
{
    return angle_PID.result;
}

void wall_following::set_wall_distance(double distance)
{
    wall_PID.target = distance;
}

double wall_following::get_current_yaw()
{
    return current_yaw;
}

double wall_following::get_distance_front()
{
    return distance_front;
}

void wall_following::set_follow_left()
{
    lidar_wall_index = 45 + 180;
}

void wall_following::set_follow_right()
{
    lidar_wall_index = 45;
}
