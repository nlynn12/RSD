#include <ros/ros.h>
#include <geometry_msgs/TwistStamped.h>

#include <top_module/zone_order.h>
#include <top_module/zone_state.h>
#include "wall_following.hpp"

#define MAX_SPEED 0.3
#define MAX_ANG_SPEED 0.6

enum states
{
    ST_IDLE,
    ST_GO_INSIDE_BOX,
    ST_GO_TO_BACK_WALL,
    ST_TURN_90,
    ST_GO_TO_CHARGER,
    ST_GO_TO_CHARGER_DONE,
    ST_CHARGING,
    ST_CHECK_CHARGING,
    ST_CHECKING_CHARGING,
    ST_BACK,
    ST_TURN_180,
    ST_TURN_90_2,
    ST_GO_OUT,
    ST_GO_OUT_2,
};

enum states state = ST_CHARGING;

void publish_twist(ros::Publisher &twist_publisher, double angular, double speed)
{
    //preparing message
    geometry_msgs::TwistStamped twist_message;

    //Don't go crazy
    if(angular > MAX_ANG_SPEED)
        angular = MAX_ANG_SPEED;
    else if(angular < -MAX_ANG_SPEED)
        angular = -MAX_ANG_SPEED;

    twist_message.twist.angular.z = angular;
    twist_message.twist.linear.x = speed;

    twist_message.header.stamp = ros::Time::now();

    //publishing message
    twist_publisher.publish(twist_message);
}

double get_speed(double speed)
{
    return speed;
}

bool top_module_cb(top_module::zone_order::Request &req, top_module::zone_order::Response &res)
{
    switch (state) {
    case ST_IDLE:
        if(req.order == "GO_CHARGE")
        {
            state = ST_GO_INSIDE_BOX;
            res.confirmation = true;
            res.message = "OK";
            ROS_INFO("ST_GO_INSIDE_BOX");
        }
        else
        {
            res.confirmation = false;
            res.message = "Wrong Command try GO_CHARGE";
        }
        break;
    case ST_CHARGING:
        if(req.order == "STOP_CHARGING")
        {
            state = ST_BACK;
            res.confirmation = true;
            res.message = "OK";
            ROS_INFO("ST_BACK");
        }
        else if(req.order == "CHECK_CHARGING")
        {
            state = ST_CHECK_CHARGING;
            res.confirmation = true;
            res.message = "OK";
            ROS_INFO("ST_CHECK_CHARGING");
        }
        else
        {
            res.confirmation = false;
            res.message = "Wrong Command try STOP_CHARGING or CHECK_CHARGING";
        }
        break;
    case ST_CHECKING_CHARGING:
         if(req.order == "GO_CHARGE_AGAIN")
         {
             state = ST_GO_TO_CHARGER;
             res.confirmation = true;
             res.message = "OK";
             ROS_INFO("ST_GO_TO_CHARGER");
         }
         else
         {
             res.confirmation = false;
             res.message = "Wrong Command try GO_CHARGE_AGAIN";
         }
        break;
    default:
        res.confirmation = false;
        res.message = "Node is in a wrong state! Wait until it finishies!";
        break;
    }


    return true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "box_navigation");
    ros::NodeHandle n("~");

    ros::Rate loop_rate(100);

    ros::Publisher twist_publisher = n.advertise<geometry_msgs::TwistStamped>("/fmCommand/cmd_vel", 1);
    ros::ServiceServer top_module_srv = n.advertiseService("/zone_activation/box_navigation/", top_module_cb);
    ros::ServiceClient top_module_src_client = n.serviceClient<top_module::zone_state>("/zone_activation/TopModule/");
    wall_following wf(n);

    wf.set_wall_distance(0.30);
    wf.wall_PID.K_d = 5;

    top_module::zone_state zone_state_msg;

    double target_yaw;

    while (ros::ok())
    {
        switch (state) {
        case ST_IDLE:
            //DO NOTHING WAITING FOR A SERVICE CALL
            break;
        case ST_GO_INSIDE_BOX:
            if(wf.get_distance_front() > 1.6)
            {
                publish_twist(twist_publisher, 0, get_speed(MAX_SPEED));
            }
            else
            {
                publish_twist(twist_publisher, 0, get_speed(MAX_SPEED));
                state = ST_GO_TO_BACK_WALL;
                wf.set_follow_right();
                wf.set_wall_distance(0.6);
                ROS_INFO("ST_GO_TO_BACK_WALL");
            }
            break;
        case ST_GO_TO_BACK_WALL:
            if(wf.get_distance_front() > 0.25)
            {
                publish_twist(twist_publisher, wf.get_result(), get_speed(MAX_SPEED));
            }
            else
            {
                publish_twist(twist_publisher, 0, 0);
                state = ST_TURN_90;
                ROS_INFO("ST_TURN_90");
            }
            break;
        case ST_TURN_90:
            if(wf.get_distance_front() < 1.2)
            {
                publish_twist(twist_publisher, 1.0, 0);
            }
            else
            {
                publish_twist(twist_publisher, 0, 0);
                state = ST_GO_TO_CHARGER;
                wf.set_wall_distance(0.30);
                wf.wall_PID.K_d = 5;
                ROS_INFO("ST_GO_TO_CHARGER");
            }
            break;
        case ST_GO_TO_CHARGER:
            if(wf.get_distance_front() > 0.12)
            {
                publish_twist(twist_publisher, wf.get_result(), get_speed(0.1));
            }
            else
            {
                publish_twist(twist_publisher, 0, 0);
                state = ST_CHARGING;

                zone_state_msg.request.status = true;
                zone_state_msg.request.message = "CHARGING";
                top_module_src_client.call(zone_state_msg);

                ROS_INFO("ST_CHARGING");
            }
            break;
        case ST_CHARGING:
            //DO NOTHING WAITING FOR A SERVICE
            break;
        case ST_BACK:
            if(wf.get_distance_front() < 0.20)
            {
                publish_twist(twist_publisher, wf.get_result(), -1*get_speed(0.1));
            }
            else
            {
                publish_twist(twist_publisher, 0, 0);
                state = ST_TURN_180;
                target_yaw = wf.get_current_yaw() - M_PI;
                ROS_INFO("ST_TURN_180");
            }
            break;
        case ST_CHECK_CHARGING:
            if(wf.get_distance_front() < 0.20)
            {
                publish_twist(twist_publisher, wf.get_result(), -1*get_speed(0.1));
            }
            else
            {
                publish_twist(twist_publisher, 0, 0);
                state = ST_CHECKING_CHARGING;
            }
            break;
        case ST_CHECKING_CHARGING:
             //DO NOTHING WAITING FOR A SERVICE
            break;
        case ST_TURN_180:
            if(wf.get_current_yaw() > target_yaw)
            {
                publish_twist(twist_publisher, 1.0, 0);
            }
            else
            {
                publish_twist(twist_publisher, 0, 0);
                state = ST_GO_OUT;
                wf.set_follow_left();
                wf.set_wall_distance(0.3);
                wf.wall_PID.K_d = 10;
                ROS_INFO("ST_GO_OUT");
            }
            break;
        case ST_GO_OUT:
            if(wf.get_distance_front() > 0.5)
            {
                publish_twist(twist_publisher, wf.get_result(), get_speed(MAX_SPEED));
            }
            else
            {
                publish_twist(twist_publisher, 0, 0);
                state = ST_TURN_90_2;
                ROS_INFO("ST_TURN_90_2");
            }
            break;
        case ST_TURN_90_2:
            if(wf.get_distance_front() < 1.2)
            {
                publish_twist(twist_publisher, -1.0, 0);
            }
            else
            {
                publish_twist(twist_publisher, 0, 0);
                state = ST_GO_OUT_2;
                wf.set_wall_distance(0.60);
                ROS_INFO("ST_GO_OUT_2");
            }
            break;
        case ST_GO_OUT_2:
            if (wf.distance_min < 0.80)
            {
                publish_twist(twist_publisher, wf.get_result(), get_speed(MAX_SPEED));
            }
            else
            {
                publish_twist(twist_publisher, 0, 0);
                state = ST_IDLE;

                zone_state_msg.request.status = true;
                zone_state_msg.request.message = "OUT_OF_BOX";
                top_module_src_client.call(zone_state_msg);

                ROS_INFO("ST_IDLE");
            }
            break;
        default:
            break;
        }


        loop_rate.sleep();
        ros::spinOnce();
    }

    return 0;
}
