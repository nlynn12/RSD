#!/usr/bin/env python
# license removed for brevity
import rospy
from battery_check.srv import battery_state
from msgs.msg import nmea
from top_module import zone_state

robot_state = -1;
voltage = 0.0

def state_callback(data):
    global robot_state
    robot_state = data.data
    
def nmea_callback(data):
    global voltage
    voltage = int(data.data[3])*0.03747

def service_callback(data):
    if(robot_state > 0):
        if(voltage > 12.7):
            return 1
        if (voltage > 12.2):
            return 2
        return 3

        
def loop():
    rospy.init_node('battery_manager', anonymous=True)
    s = rospy.Service('getBatteryState', battery_state , service_callback)
    subNmea = rospy.Subscriber('/fmSignal/nmea_from_frobit', nmea, nmea_callback)

    while not rospy.is_shutdown():    
        rospy.spin()

if __name__ == '__main__':
    try:
        loop()
    except rospy.ROSInterruptException:
        pass