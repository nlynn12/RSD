cmake_minimum_required(VERSION 2.8.3)
project(top_module)

set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -std=gnu++11")
set(CMAKE_BUILD_TYPE Release)
find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  message_generation
)
add_service_files(
  FILES
  zone_state.srv
  zone_order.srv
  moveService.srv
)

generate_messages(
  DEPENDENCIES
  std_msgs
)
catkin_package(
  CATKIN_DEPENDS roscpp rospy message_runtime std_msgs
)

include_directories(
    ${catkin_INCLUDE_DIRS}
#    ${Boost_INCLUDE_DIR}
#    ${EIGEN3_INCLUDE_DIR}
)

## Declare a C++ executable
 set(HEADERS)
 set(SOURCES src/topModule.cpp)
 add_executable(topModule ${SOURCES} ${HEADERS})

add_dependencies(topModule top_module_gencpp)
## Specify libraries to link a library or executable target against
 target_link_libraries(topModule
   ${catkin_LIBRARIES}

 )

add_dependencies(topModule ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
