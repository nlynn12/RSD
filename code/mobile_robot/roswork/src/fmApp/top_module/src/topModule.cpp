#include<ros/ros.h>
#include<string>
#include<top_module/moveService.h>
#include<top_module/zone_order.h>
#include<top_module/zone_state.h>
#include<std_msgs/Bool.h>
#include<std_msgs/String.h>
#include<std_msgs/Int32.h>
#include<sstream>


#define box 1
#define rcRoom 2
#define bigRoom 3
#define droneCage 4

std::string int_to_string(const int data){
    std::stringstream ss;
    ss << data;
    return ss.str();
}

// TODO Add service interface to waypoint navigation


enum state{no_task,new_task_del,wait_for_free_feeder,get_bricks,wait_for_accept_drone,move_out_drone_cage,move_to_dispenser_confirmation,move_to_cell_r,move_to_cell_u,empty_bricks,move_home,new_task_recv,wait_for_robotcell_accept,error_state};

std::string command_in = "";
int bat_state = 3;
bool robot_available = true;

bool linefollowing_finished = false;
bool boxnavigation_finished = false;
bool waypointnav_finished = false;
bool unloading_succesfull = false;


int current_workcell = 0; // Workcell that we need to go to.
int current_zone = 1;  // Used to define what navigation type needed
std::string commandTopicName = "mes_mr_client";

// Get command(next_state) from Mes interface
void getCommand(const std_msgs::String::ConstPtr& msg){
    command_in = msg->data;
    ROS_INFO("I got: %s",command_in.c_str());
}

void goCharge();
void linefollowOut();


void leaveCharge();
void unload();
void moveToBigRoom();
void call_linefollow(int ws, std::string task);
void moveWayPoint(std::string where);
void moveToDispenser();
void move_to_current_cell(int current_ws,std::string task);
bool zoneDone(top_module::zone_state::Request& request, top_module::zone_state::Response& response);
bool unloadDone(top_module::zone_state::Request& request, top_module::zone_state::Response& response);
void moveInsideDroneCage();
int chargeComplete();
void leaveDroneCage();
void wrongZone(std::string);

ros::ServiceClient lineFollowService;
ros::ServiceClient boxNavService;
ros::ServiceClient wptNavService;
ros::ServiceClient moveTipperClient;
ros::Publisher mesPublisher;

int main(int argc, char **argv){
    ros::init(argc,argv,"topModule_test");
    ros::NodeHandle n;
    
    //Command interface to movement nodes
    lineFollowService = n.serviceClient<top_module::zone_order>("/zone_activation/line_following");
    boxNavService = n.serviceClient<top_module::zone_order>("/zone_activation/box_navigation");
    wptNavService = n.serviceClient<top_module::zone_order>("/zone_activation/wpt_navigation");
    moveTipperClient = n.serviceClient<top_module::moveService>("moveService");
    
    //Service to which the movement nodes report status to
    ros::ServiceServer transportStatus = n.advertiseService("/zone_activation/TopModule/",zoneDone);
    ros::ServiceServer unloadStatus = n.advertiseService("/unload_activation/TopModule/",unloadDone);
    
    // Mes client interface
    ros::Publisher mesPublisher = n.advertise<std_msgs::String>("/topModule/getCurrentState",1);
    ros::Publisher zonePublisher = n.advertise<std_msgs::Int32>("/topModule/Zone",1);
    ros::Publisher statePublisher = n.advertise<std_msgs::Int32>("/topModule/State",1);
    
    // Mes client interface
    ros::Subscriber command_sub = n.subscribe(commandTopicName,1,getCommand);
    
    // Tipper control interface
    state s = no_task;
    
    while(ros::ok()){
        
        
        switch(s){
            case no_task:
            {
                // Wait for task
                std_msgs::String msg;
                if(chargeComplete() == 3){ // only go on if there is enough charge for 1 run.
                    msg.data = "ACCEPT_TASKS";
                    if(command_in == "New_task_del"){
                        s = new_task_del;
                    }else if(command_in == "New_task_recv"){
                        s = new_task_recv;
                    }
                }else{
                    goCharge();
                    msg.data = "REJECT_TASKS";
                }
                ros::Duration(1).sleep(); // No need to waste power
                mesPublisher.publish(msg);
                break;
            }
            case new_task_recv:
            {
            if(command_in == "Move_to_cell_1_recv"){
                current_workcell = 1;
                s = move_to_cell_r;
            }else if(command_in == "Move_to_cell_2_recv"){
                current_workcell = 2;
                s = move_to_cell_r;
            }
                break;
            }
            case new_task_del:
                // Accept task if we can
                if(command_in == "Wait_for_free_feeder"){
                    s = wait_for_free_feeder;
                }else if( command_in == "Get_bricks"){
                    s = get_bricks;
                }
                
                break;
            case wait_for_free_feeder:
                if(command_in == "Get_bricks"){
                    s = get_bricks;
                }
                break;
                
            case get_bricks:
            {
                moveToDispenser();
                
                // Tell mes client we are at the feeder
                std_msgs::String msg;
                msg.data = "AT_FEEDER";
                mesPublisher.publish(msg);
                
                s = wait_for_accept_drone;
                break;
            }
            case wait_for_accept_drone:
                if(command_in == "Move_out_of_drone_cage")
                    s = move_out_drone_cage;
                break;
            case move_out_drone_cage: 
            {   
                leaveDroneCage();
                current_zone = bigRoom;
                std_msgs::String msg;
                msg.data = "OUT_OF_DRONE_CAGE";
                mesPublisher.publish(msg);
                ROS_INFO("Leave DroneCage Complete");
                ROS_INFO("Current_zone = %d",current_zone);
                s = move_to_dispenser_confirmation;
            }
            case move_to_dispenser_confirmation:
                if(command_in == "Move_to_cell_1_del"){
                    current_workcell = 1;
                    s = move_to_cell_u;
                }else if(command_in == "Move_to_cell_2_del"){
                    current_workcell = 2;
                    s = move_to_cell_u;
                }
            
            break;
            case move_to_cell_r:
            {
                move_to_current_cell(current_workcell,"Load");
                   ros::Duration(1).sleep();
                std_msgs::String msg;
                msg.data = "AT_ROBOT_CELL";
                mesPublisher.publish(msg);

                s = wait_for_robotcell_accept;
                
                break;
            }
            case move_to_cell_u:
            {
                ROS_INFO("MoveToCell");
                move_to_current_cell(current_workcell,"Unload");
                ROS_INFO("MoveToCell");

                std_msgs::String msg;
                msg.data = "AT_ROBOT_CELL";
                mesPublisher.publish(msg);
                
                s = empty_bricks;
                
                break;
            }
            case empty_bricks:{
            std_msgs::String msgs;
            msgs.data = "BRICKS_EMPTY";
            mesPublisher.publish(msgs);
                unload();
                s = move_home;
                break;
            }
            case wait_for_robotcell_accept:
                if(command_in == "Move_home"){
                    s = move_home;
                }
                break;
            case move_home:
        {
                goCharge();
                ros::Duration(1).sleep();
                std_msgs::String msgs;
                msgs.data = "AT_HOME";
                mesPublisher.publish(msgs);
                s = no_task;
                break;
        }
            case error_state:
                ROS_ERROR("State error.\n Something went wrong in topModule");
                break;
            default:
                ROS_ERROR("Mobile robot is confused");
                break;
        }
        
        //Error handling. This is the command MES uses to reset Mobile Robot
        if(command_in == "Move_home"){
            s = move_home;
        }
        command_in = "";
        
        if(current_zone == -1){
            s = error_state;
        }
        std_msgs::Int32 state_data;
        state_data.data = s;
        statePublisher.publish(state_data);
        std_msgs::Int32 zone_data;
        zone_data.data = current_zone;
        zonePublisher.publish(zone_data);
        ros::spinOnce();
    }
    return 0;
}


void linefollowOut(){
    top_module::zone_order srv;
    std::string temp = "GoHome";
    srv.request.order = temp;
    bool orderAccepted = lineFollowService.call(srv);
    if(orderAccepted){
        while(!linefollowing_finished){
            ros::Duration(0.3).sleep();
            ros::spinOnce();
        }
        linefollowing_finished = false;
    }else{
        ROS_ERROR("Order rejected by linefollowing node");
    }
}

void moveToBigRoom(){
    if(current_zone == rcRoom){
        linefollowOut();
    }else if(current_zone == box){
        leaveCharge();
    }else if(current_zone == droneCage){
        leaveDroneCage();
    }
    
    current_zone = bigRoom;
}

void leaveDroneCage(){
    top_module::zone_order srv;
    std::string temp = "GoHome";
    srv.request.order = temp;
    bool orderAccepted = lineFollowService.call(srv);
    if(orderAccepted){
        while(!linefollowing_finished){
            ros::Duration(0.3).sleep();
            ros::spinOnce();
            ROS_INFO("WAIT");
        }
        linefollowing_finished = false;
    }else{
        ROS_ERROR("Order rejected by linefollowing node");
    }
}

void moveInsideDroneCage(){
    top_module::zone_order srv;
    std::string temp = "DroneDoor";
    srv.request.order = temp;
    bool orderAccepted = lineFollowService.call(srv);
    if(orderAccepted){
        while(!linefollowing_finished){
            ros::Duration(0.3).sleep();
            ros::spinOnce();
        }
        linefollowing_finished = false;
    }else{
        ROS_ERROR("Order rejected by linefollowing node");
    }
    
}

void call_linefollow(int ws, std::string task){
    top_module::zone_order srv;
    std::string temp = "C";
    temp.append(int_to_string(ws));
    temp.append(task);
    srv.request.order = temp;
    bool orderAccepted = lineFollowService.call(srv);
    if(orderAccepted){
        while(!linefollowing_finished && ros::ok()){
            ros::Duration(0.3).sleep();
            ros::spinOnce();
        }
        linefollowing_finished = false;
    }else{
        ROS_ERROR("Order rejected by linefollowing node");
    }
}

void moveWayPoint(std::string where){
    ros::Duration(1).sleep();       
    ROS_INFO("moveWayPoint");
    top_module::zone_order msg;
    msg.request.order = where;
    if(wptNavService.call(msg)){
        ROS_INFO("WaypointNavigation: %s",msg.response.message.c_str());
    }else{
        ROS_ERROR("WaypointNavigation failed");
    }
    while(!waypointnav_finished && ros::ok()){
        ros::Duration(0.3).sleep();
        ros::spinOnce();
    }
    waypointnav_finished = false;
}

void moveToDispenser(){
    
    if(current_zone == rcRoom or current_zone == box){
        moveToBigRoom();
    }else{
        wrongZone("moveToDispenser1");
    }
    
    ROS_INFO("moveToBigRoom");
    
    if(current_zone == bigRoom){
        moveWayPoint("droneCage");
    //NOTE Live function
        current_zone = droneCage;
    }else{
        wrongZone("moveToDispenser2");
    }
    
    //NOTE Live function
    /**/
    if(current_zone == droneCage){
        moveInsideDroneCage();
    }else{
        wrongZone("moveToDispenser3");
    }
    /**/
    
    ROS_INFO("Current_zone: %d",current_zone);
}

void move_to_current_cell(int current_ws,std::string task){
    ROS_INFO("Current_zone: %d",current_zone);
    
    if(current_zone == box){
        leaveCharge();
        current_zone = bigRoom;
    }
    
    if(current_zone == bigRoom){
        moveWayPoint("lineFollowing");
        current_zone = rcRoom;
    }else{
        wrongZone("move_to_current_cell1");
    }
    ROS_INFO("Im at zone %d",current_zone);
    
    if(current_zone == rcRoom){
        call_linefollow(current_ws,task);
    }else{
        wrongZone("move_to_current_cell2");
    }
}

void wrongZone(std::string task){
    ROS_ERROR("IM IN THE WRONG ZONE, source: %s",task.c_str());
}

bool zoneDone(top_module::zone_state::Request& request, top_module::zone_state::Response& response){
    
    ROS_INFO("zoneDone %d", current_zone);
    
    if(current_zone == rcRoom or current_zone == droneCage){
        ROS_INFO_STREAM("question from line follower" << request.status);
        if(request.status == true)
            linefollowing_finished = true;
    }else if (current_zone == box){
        boxnavigation_finished = true;
    }else if(current_zone == bigRoom){
        waypointnav_finished = true;
    }
    
    response.received = true;
    
    return true;
}

void leaveCharge(){
    top_module::zone_order msg;
    msg.request.order = "STOP_CHARGING";
    if(boxNavService.call(msg)){
        ROS_INFO("BoxNavigation: %s",msg.response.message.c_str());
    }else{
        ROS_ERROR("BoxNavigation failed");
    }
    
    ROS_INFO("leaveCharge1");
    
    while(!boxnavigation_finished && ros::ok()){
        ros::Duration(0.3).sleep();
        ros::spinOnce();
    }
    boxnavigation_finished = false;
    ROS_INFO("leaveCharge2");
    
}

void goCharge(){
    if(current_zone == droneCage){
        //TODO Call reverse out of droneCage (linefollower)
    }
    if(current_zone == rcRoom){
        linefollowOut();
        current_zone = bigRoom;
    }
    if(current_zone == bigRoom){
        moveWayPoint("charge");
        current_zone = box;
    }
    if(current_zone == box){
        top_module::zone_order msg;
        msg.request.order = "GO_CHARGE";
        if(boxNavService.call(msg)){
            ROS_INFO("BoxNavigation: %s",msg.response.message.c_str());
        }else{
            ROS_ERROR("BoxNavigation failed");
        }
        while(!boxnavigation_finished && ros::ok()){
            ros::Duration(0.3).sleep();
            ros::spinOnce();
        }
        current_zone = box;
        boxnavigation_finished = false;

    }
}

int chargeComplete(){
    if(bat_state == 1)
        return 1;
    if(bat_state == 2)
        return 2;
    if(bat_state == 3)
        return 3;
    return -1;
}

void unload(){
    top_module::moveService msg;
    msg.request.data = 0;
    bool res =  moveTipperClient.call(msg);
    ROS_INFO("Got this response: %s",msg.response.info.c_str());
    
    while(!unloading_succesfull && ros::ok()){
        ros::Duration(0.3).sleep();
        ros::spinOnce();
    }
    if(unloading_succesfull){
        ROS_INFO("Unload finished");

    }else{
        //TODO Tell hmi something went wrong and add error codes
        ROS_ERROR("Unloading error: %s",msg.response.info.c_str());
    }
    unloading_succesfull = false;
}

bool unloadDone(top_module::zone_state::Request& request, top_module::zone_state::Response& response){
    unloading_succesfull = true;
    return true;
}
