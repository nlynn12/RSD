/*
 * Copyright 2016 Nicolai Lynnerup <nlynn12@student.sdu.dk>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <iostream>
#include <vector>
#include <string>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <hmi/getAllTopics.h>

bool find_topics(hmi::getAllTopics::Request &req,
		 hmi::getAllTopics::Response &res){
    ros::master::V_TopicInfo master_topics;
    ros::master::getTopics(master_topics);
    std::vector<std::string> topic_names_vec;
    std::vector<std::string> topic_types_vec;
  
    for (ros::master::V_TopicInfo::iterator it = master_topics.begin() ; it != master_topics.end(); it++) {
	const ros::master::TopicInfo& topic = *it;
	//std::cout << info.name << " " << info.datatype << std::endl;
	topic_names_vec.push_back(topic.name);
	topic_types_vec.push_back(topic.datatype);
    }
    
    res.topic_names = topic_names_vec;
    res.topic_types = topic_types_vec;
    
    return true;
}


/*ros::master::V_TopicInfo master_topics;
    ros::master::getTopics(master_topics);
    std_msgs::String msg;
    for (ros::master::V_TopicInfo::iterator it = master_topics.begin() ; it != master_topics.end(); it++) {
	const ros::master::TopicInfo& info = *it;
	//std::cout << "topic_" << it - master_topics.begin() << ": " << info.name << std::endl;
	std::cout << info.name << " " << info.datatype << std::endl;
	if(it- master_topics.begin() > 0){
	  msg.data += ",";
	}
	msg.data += info.name;
    }
    res.topics = msg.data;*/

int main(int argc, char **argv){
    ros::init(argc,argv,"hmi");
    ros::NodeHandle n;
    
    ros::ServiceServer service = n.advertiseService("getAllTopics", find_topics);
    ros::spin();

    return 0;
}
