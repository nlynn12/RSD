(function () {
    "use strict";

    angular.module('HMI').factory('Dialog', ['ngDialog','$rootScope','$sce',
        function (ngDialog,$rootScope,$sce) {
            var service = {};
            
            service.confirm = function(header, msg, controls){
                var scope = $rootScope.$new(true);
                scope.header = header;
                scope.msg = $sce.trustAsHtml(msg);
                scope.controls = controls;
                return ngDialog.openConfirm({
                    closeByEscape: false,
                    closeByDocument: false,
                    showClose: false,
                    className: 'ngdialog-theme-default custom-width-500',
                    template: 'components/dialogs/alert.dialog.html',
                    scope: scope
                });
            };
            
            service.alert = function(header, msg, controls){
                var scope = $rootScope.$new(true);
                scope.header = header;
                scope.msg = $sce.trustAsHtml(msg);
                scope.controls = controls;
                return ngDialog.open({
                    closeByEscape: false,
                    closeByDocument: false,
                    showClose: false,
                    className: 'ngdialog-theme-default custom-width-500',
                    template: 'components/dialogs/alert.dialog.html',
                    scope: scope
                });
            };
            
            service.settings = function(header, targetCount, sampleRate, controls){
                var scope = $rootScope.$new(true);
                scope.header = header;
                scope.targetCount = targetCount;
                scope.sampleRate = sampleRate;
                scope.controls = controls;
                return ngDialog.openConfirm({
                    closeByEscape: true,
                    closeByDocument: false,
                    showClose: true,
                    className: 'ngdialog-theme-default custom-width-500',
                    template: 'components/dialogs/settings.dialog.html',
                    controller: 'SettingsCtrl',
                    scope: scope
                });
            };
            
            service.validator = function(header, srv, tpc, pSrv, pTpc){
                var scope = $rootScope.$new(true);
                scope.header = header;
                scope.srv = srv;
                scope.tpc = tpc;
                scope.pSrv = pSrv;
                scope.pTpc = pTpc;
                scope.validated = false;
                scope.display = false;
                return ngDialog.openConfirm({
                    closeByEscape: false,
                    closeByDocument: false,
                    showClose: false,
                    className: 'ngdialog-theme-default custom-width-700',
                    template: 'components/dialogs/rosvalidator.dialog.html',
                    controller: 'ROSValidatorCtrl',
                    scope: scope
                });
            };
            
            return service;
        }
    ]);
})();