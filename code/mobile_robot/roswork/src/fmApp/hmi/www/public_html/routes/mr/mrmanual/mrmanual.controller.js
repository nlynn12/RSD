(function () {
    "use strict";

    function MRManualCtrl($scope, $log, $route, $window, ROS) {
        console.log(document.getElementById('ManualControl'));

        var joystick = new VirtualJoystick({
            container: document.getElementById('ManualControl'),
            //stationaryBase  : true,
            //baseX : document.getElementById('ManualControl').offsetTop/2,
            //baseY : document.getElementById('ManualControl').offsetLeft/2,
            mouseSupport: true,
        });


        /*joystick.addEventListener('mouseDown',function(){
         
         })	*/
        /*joystick.addEventListener('mouseUp', function(){
         
         console.log('up')
         });*/

        /*joystick.addEventListener('touchStart', function(){
         console.log('down')
         var twist = new ROSLIB.Message({
         linear: {x: 0.1,y: 0.2,z: 0.3},
         angular: {x: -0.1,y: -0.2,z: -0.3}
         })
         ROS.publish('/topicname','geometry_msgs/Twist',twist)
         })
         
         joystick.addEventListener('touchEnd', function(){
         console.log('up')
         });*/
        var linearSpeed = 0;
        var angularSpeed = 0;
        var oldDeltaY = 0;
        var oldDeltaX = 0;
        var intervalId = setInterval(function () {
            var maxAngularSpeed = 1;
            var maxLinearSpeed = 0.7;

            if (joystick.deltaX() === 0 && joystick.deltaY() === 0) {
                linearSpeed = 0;
                angularSpeed = 0;
            }
            var date = new Date();
            //console.log('move')
            if (joystick.deltaY() < oldDeltaY - 5) {
                linearSpeed = linearSpeed + 0.1;
                if (Math.abs(linearSpeed) > maxLinearSpeed) {
                    linearSpeed = maxLinearSpeed;
                }
            }
            if (joystick.deltaY() > oldDeltaY + 5) {
                linearSpeed = linearSpeed - 0.1;
                if (Math.abs(linearSpeed) > maxLinearSpeed) {
                    linearSpeed = -maxLinearSpeed;
                }
            }
            if (joystick.deltaX() < oldDeltaX) {
                angularSpeed = angularSpeed + 0.2;
                if (Math.abs(angularSpeed) > maxAngularSpeed) {
                    angularSpeed = maxAngularSpeed;
                }
            }
            if (joystick.deltaX() > oldDeltaX) {
                angularSpeed = angularSpeed - 0.2;
                if (Math.abs(angularSpeed) > maxAngularSpeed) {
                    angularSpeed = -maxAngularSpeed;
                }
            }
            var header = new ROSLIB.Message({
                seq: 1,
                stamp: date.getSeconds(),
                frame_id: ''
            });
            var twist = new ROSLIB.Message({
                linear: {x: linearSpeed, y: 0, z: 0},
                angular: {x: 0, y: 0, z: angularSpeed}
            });
            var twistStamp = new ROSLIB.Message({
                header: header,
                twist: twist
            });
            ROS.publish('/fmCommand/cmd_vel', 'geometry_msgs/TwistStamped', twistStamp);
            oldDeltaY = joystick.deltaY();
            oldDeltaX = joystick.deltaX();
        }, 1 / 30 * 1000);

        $scope.$on('$routeChangeStart', function () { // Option for args: next, current
            clearInterval(intervalId);
        });
    }


    angular.module("HMI").controller("MRManualCtrl", MRManualCtrl);
})();