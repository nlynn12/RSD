(function () {
    "use strict";

    function HomeCtrl($scope, $interval, $location, $log, $localStorage, $window, ROS, Dialog) {
        var services       = [],
            topics         = [],
            paramServices  = [],
            paramTopics    = [];
        $scope.gotSrv      = false;
        $scope.gotTpc      = false;
        $scope.gotParamSrv = false;
        $scope.gotParamTpc = false;
        
        ROS.getServices(function (srv) {
            services = srv;
            $scope.gotSrv = true;
        });

        ROS.getTopics(function (tpc) {
            topics = tpc;
            $scope.gotTpc = true;
        });
        
        ROS.getParamTopics(function(tpc){
            paramTopics = tpc;
            $scope.gotParamTpc = true;
        });
        
        ROS.getParamServices(function(srv){
            paramServices = srv;
            $scope.gotParamSrv = true;
        });

        $scope.$watchGroup(['gotSrv', 'gotTpc','gotParamSrv','gotParamTpc'], function (newValues) {
            if (newValues[0] === true && newValues[1] === true && newValues[2] === true && newValues[3] === true) {
                Dialog.validator("Validating ROS Services and Topics", services, topics, paramServices, paramTopics).then(
                        function () {
                            // confirm();
                        }, function () {
                    // closeThisDialog();
                    $location.path('/');
                    //$window.location.href = 'https://www.google.com';
                });
            }
        });
        
        
        $scope.$storage = $localStorage.$default({
            overAllOEE: []
        });
        
        // OEE = Availability * Performance * Quality
        //    Availability = Run Time / Total Time
        //    Performance = Total Count / Target Count
        //    Quality = Good Count / Total Count
        
        $scope.OEE = {
            current: {Availability: 0.00,
                      Performance: 0.00,
                      Quality: 100.00,
                      OEE: 0.00},
            previous: {Availability: 0.00,
                       Performance: 0.00,
                       Quality: 100.00,
                       OEE: 0.00},
            Period: {title: 'Live', value: 0, available:true},
            AvailablePeriods: [{title: 'Live', value: 0, available:true},
                               {title: 'Last Hour', value: 3600, available:false},
                               {title: 'Last 2 Hours', value: 7200, available:false},
                               {title: 'Last 4 Hours', value: 14400, available:false},
                               {title: 'Last 8 Hours', value: 28800, available:false},
                               {title: 'Last 12 Hours', value: 43200, available:false},
                               {title: 'Last 24 Hours', value: 86400, available:false}],
            timers: [{title: "Total Time", display: "00:00:00",totalSeconds: 0},
                     {title: "Run Time", display: "00:00:00",totalSeconds: 0},
                     {title: "Down Time", display: "00:00:00",totalSeconds: 0}],
            targetCount: 50,
            totalCount: 46
        };
        
        $scope.MES = {
            order: {current: [{type: 'Red brick', amount: 2},
                              {type: 'Yellow brick', amount: 1},
                              {type: 'Blue brick', amount: 4}],
                    previous: [{type: 'Red brick', amount: 5},
                               {type: 'Yellow brick', amount: 2},
                               {type: 'Blue brick', amount: 3}]}
        };
        
        var sampleRate = 30000;//900000; // 15minutes by default
        $scope.advancedSettings = function(){
            Dialog.settings("OEE Advanced Settings",$scope.OEE.targetCount,sampleRate,true).then(function(tc){
                if(tc[0] === undefined || tc[0] === "undefined" || tc[0] === null){
                    //$scope.OEE.targetCount = 10;
                }else{
                    $scope.OEE.targetCount = tc[0];
                }
                if(tc[0] === undefined || tc[0] === "undefined" || tc[0] === null){
                    sampleRate = 30000;
                }else{
                    sampleRate = tc[1];
                }
            }, function(arg){
                if(arg === 'DeleteOEE'){
                    delete $scope.$storage.overAllOEE;
                    $scope.$storage.overAllOEE = [];
                    snapshotID = 0;
                    $scope.OEE.timers.forEach(function(timer){
                        timer.totalSeconds = 0;
                        timer.display = "00:00:00";
                    });
                }
            });
        };
        
        $scope.periodChange = function(){
            $log.info($scope.OEE.Period.value);
            // Get data based on value/sample rate
        };
        
        var snapshotID = 0;
        var OEETimer = $interval(function(){
            $scope.$storage.overAllOEE.push({availability: 0,
                         performance: 0,
                         quality: 100,
                         oee: 0,
                         timestamp:[0,0,0]});
            // Availability = Run Time / Total Time
            $scope.$storage.overAllOEE[snapshotID].availability = ($scope.OEE.timers[1].totalSeconds / $scope.OEE.timers[0].totalSeconds)*100;
            // Performance = Total Count / Target Count
            if($scope.OEE.targetCount === 0){
                $scope.$storage.overAllOEE[snapshotID].performance = 0;
            } else if($scope.OEE.totalCount >= $scope.OEE.targetCount){
                $scope.$storage.overAllOEE[snapshotID].performance = 100;
            } else{
                $scope.$storage.overAllOEE[snapshotID].performance = ( $scope.OEE.totalCount / $scope.OEE.targetCount )*100;
            }
            
            $scope.$storage.overAllOEE[snapshotID].quality = 100;
            
            // ONLY FOR TESTING START
            $scope.OEE.previous.Availability = $scope.OEE.current.Availability;
            $scope.OEE.previous.Performance = $scope.OEE.current.Performance;
            $scope.OEE.previous.Quality = $scope.OEE.current.Quality;
            $scope.OEE.previous.OEE = $scope.OEE.current.OEE;
            
            $scope.OEE.current.Availability = parseFloat($scope.$storage.overAllOEE[snapshotID].availability).toFixed(2);
            $scope.OEE.current.Performance = parseFloat($scope.$storage.overAllOEE[snapshotID].performance).toFixed(2);
            $scope.OEE.current.Quality = parseFloat($scope.$storage.overAllOEE[snapshotID].quality).toFixed(2);
            $scope.OEE.current.OEE = parseFloat((($scope.OEE.current.Availability/100) * ($scope.OEE.current.Performance/100) * ($scope.OEE.current.Quality/100))*100).toFixed(2);
            // ONLY FOR TESTING END
            $scope.OEE.timers.forEach(function(timer,key){
                $scope.$storage.overAllOEE[snapshotID].timestamp[key] = timer.totalSeconds;
            });
            snapshotID++;
        },sampleRate); // Take a snapshot
        
        var timers = [];
        timers[0] = $interval(function(){
            $scope.OEE.timers[0].totalSeconds++;
            $scope.OEE.timers[0].display = secondsToHms($scope.OEE.timers[0].totalSeconds);
            
            if($scope.systemIsRunning){
                $scope.OEE.timers[1].totalSeconds++;
                $scope.OEE.timers[1].display = secondsToHms($scope.OEE.timers[1].totalSeconds);
            }else{
                $scope.OEE.timers[2].totalSeconds++;
                $scope.OEE.timers[2].display = secondsToHms($scope.OEE.timers[2].totalSeconds);
            }
            
            if($scope.OEE.timers[0].totalSeconds === 3600 && sampleRate < 3600000){
                $scope.OEE.AvailablePeriods[1].available = true;
            }else if($scope.OEE.timers[0].totalSeconds === 7200 && sampleRate < 7200000){
                $scope.OEE.AvailablePeriods[2].available = true;
            }else if($scope.OEE.timers[0].totalSeconds === 14400  && sampleRate < 14400000){
                $scope.OEE.AvailablePeriods[3].available = true;
            }else if($scope.OEE.timers[0].totalSeconds === 28800  && sampleRate < 28800000){
                $scope.OEE.AvailablePeriods[4].available = true;
            }else if($scope.OEE.timers[0].totalSeconds === 43200  && sampleRate < 43200000){
                $scope.OEE.AvailablePeriods[5].available = true;
            }else if($scope.OEE.timers[0].totalSeconds === 86400  && sampleRate < 86400000){
                $scope.OEE.AvailablePeriods[6].available = true;
            }
        },1000);
        
        $scope.systemIsRunning = true;
        
        $scope.systemIsDown = function(){
            $scope.systemIsRunning = false;
        };
        
        $scope.systemRunning = function(){
            $scope.systemIsRunning = true;
        };
        
        function secondsToHms(d) {
            d = Number(d);
            var h = Math.floor(d / 3600);
            var m = Math.floor(d % 3600 / 60);
            var s = Math.floor(d % 3600 % 60);
            return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
        }
    }

    angular.module("HMI").controller("HomeCtrl", HomeCtrl);
})();