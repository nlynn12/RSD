(function () {
    "use strict";

    angular.module("HMI").factory('Device', [ function () {
            // Only object outside this factory
            var service = {};
            
            service.isAndroid = function(){
                var ua = navigator.userAgent.toLowerCase();
                return ua.indexOf("android") > -1;
            };

            // Return object to anything injecting this factory
            return service;
        }]);
})();