(function () {
    "use strict";

    function SettingsCtrl($scope, $log) {
        $log.info($scope.$parent.$parent.targetCount);
        $log.info($scope.sampleRate);
    }

    angular.module("HMI").controller("SettingsCtrl", SettingsCtrl);
})();