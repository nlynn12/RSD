#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>


using namespace cv;
using namespace std;



int main( int argc, char** argv )
{

   	ros::init(argc, argv, "line_follower");

    ros::NodeHandle n;

    image_transport::ImageTransport it(n);
    image_transport::Publisher pub = it.advertise("camera/image_raw", 1);

    VideoCapture cap;
    cap.open(0);
    //cap.set(CAP_PROP_BUFFERSIZE, 1);
    cap.set(CAP_PROP_FRAME_WIDTH, 320);
    cap.set(CAP_PROP_FRAME_HEIGHT, 240);
    Mat frame;
    sensor_msgs::ImagePtr msg;

    if(!cap.isOpened()) return -1;

    cap >> frame;

/*/    VideoWriter outputVideo;                                        // Open the output
    int ex = static_cast<int>(cap.get(CV_CAP_PROP_FOURCC));     // Get Codec Type- Int form
    Size S = Size((int) cap.get(CV_CAP_PROP_FRAME_WIDTH),    // Acquire input size
                   (int) cap.get(CV_CAP_PROP_FRAME_HEIGHT));
    outputVideo.open("oneRun.avi", ex, cap.get(CV_CAP_PROP_FPS), S, true);

    bool donotsave = false;
    if (!outputVideo.isOpened())
    {
        cout  << "Could not open the output video for write" << endl;
        donotsave = true;
    }
*/

    cout << "Camera node active" << endl;

    ros::Rate loop_rate(40);
    while (n.ok()) {
      cap >> frame;
      // Check if grabbed frame is actually full with some content
      if(!frame.empty()) {
        msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame).toImageMsg();
        pub.publish(msg);
/*
        if(!donotsave)
            outputVideo << frame;
*/
        cv::waitKey(1);
      }

      ros::spinOnce();
      loop_rate.sleep();
    }



    cout << "closing camera node" << endl;

}
