#include "ros/ros.h"
#include "std_msgs/String.h"
#include "msgs/RoutePt.h"
#include "msgs/waypoint_navigation_status.h"
#include "msgs/BoolStamped.h"
#include "msgs/FloatArrayStamped.h"
#include "msgs/RemoteControl.h"
#include <vector>
#include <math.h>
#include <string>
#include <sys/socket.h>
#include<iostream>
#include<arpa/inet.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include <netinet/in.h>
#include<stdio.h>
#include<stdlib.h>
#include <cstdlib>
#include <sstream>
#include<string.h> //memset

#include <top_module/zone_state.h>
#include <top_module/zone_order.h>

using namespace std;

#define WAIT 0
#define STOP 2
#define NEW_POSITION 3
#define MISSION_DONE 4
#define POINT_ERROR 5

#define MISSION_CHARGE 0
#define MISSION_LINE_FOLLOWING 1

int state = 0;
std::vector<msgs::RoutePt> pointsToCharge(5);
std::vector<msgs::RoutePt> pointsToLineFollowing(6);
std::vector<msgs::RoutePt> pointsToDroneCage(5);
msgs::RoutePt nextPoint;
int pointPointer = 1;
bool enabled = false;
bool postOnce = true;
int mode = 999;
string mission;
string comingFrom = "charge";

std::vector<float> parseMarkerPose(string input){
    std::vector<float> vect;
    std::stringstream ss(input);
    std::string token;
    for(int i = 0;i<5;i++) {
        std::getline(ss, token, ',');
        vect.push_back(std::stof(token));
    }
    return vect;
}

msgs::RoutePt getNextPoint(msgs::RoutePt routePoint ){
    msgs::RoutePt point;
    point.northing = routePoint.northing;
    point.easting = routePoint.easting;
    point.linear_vel = 0.35;
    point.nav_mode=1;
    return point;
}


void wptCallback(const msgs::waypoint_navigation_status wptStatus){
    mode = wptStatus.mode;
    if(mission.compare("charge") == 0){
        if(comingFrom.compare("lineFollowing")== 0){
            if(mode == -1){
                if(enabled){
                    if(postOnce){
                        if(pointPointer<pointsToCharge.size()){ //mode -1 Indicates that the robot is stopped and there is no more points in the list
                            nextPoint = getNextPoint(pointsToCharge[pointPointer]);
                            pointPointer++;
                            state = NEW_POSITION;
                        }else{
                            state = MISSION_DONE;
                            comingFrom = "charge";
                        }
                    }
                }else{
                    state = WAIT;
                }
            }
        }
        if(comingFrom.compare("droneCage")== 0){
            if(mode == -1){
                if(enabled){
                   if(postOnce){
                        if(pointPointer<4){
                            switch(pointPointer){
                            case 1:
                                nextPoint = pointsToDroneCage[3];
                                break;
                            case 2:
                                nextPoint = pointsToLineFollowing[2];
                                break;
                            case 3:
                                nextPoint = pointsToLineFollowing[1];
                                break;
                            }
                            pointPointer++;
                            state = NEW_POSITION;
                        }else{
                            state = MISSION_DONE;
                            comingFrom = "charge";
                        }
                    }
                }else{
                    state = WAIT;
                }
            }
        }
    }

    if(mission.compare("lineFollowing") == 0){
        if(comingFrom.compare("charge")== 0){
            if(mode == -1){
                if(enabled){
                   if(postOnce){
                        if(pointPointer<pointsToLineFollowing.size()){ //mode -1 Indicates that the robot is stopped and there is no more points in the list
                            nextPoint = getNextPoint(pointsToLineFollowing[pointPointer]);
                            pointPointer++;
                            state = NEW_POSITION;
                        }else{
                            state = MISSION_DONE;
                            comingFrom="lineFollowing";
                        }
                    }
                }else{
                    state = WAIT;
                }
            }
        }
        if(comingFrom.compare("droneCage")== 0){
            if(mode == -1){
                if(enabled){
                    if(postOnce){
                        if(pointPointer<pointsToLineFollowing.size()){
                            if(pointPointer==1){
                                nextPoint = pointsToDroneCage[3];
                            }else{
                                nextPoint = getNextPoint(pointsToLineFollowing[pointPointer]);
                            }
                            pointPointer++;
                            state = NEW_POSITION;
                        }else{
                            state = MISSION_DONE;
                            comingFrom = "lineFollowing";
                        }
                    }
                }else{
                    state = WAIT;
                }
            }
        }
    }
    if(mission.compare("droneCage") == 0){
        if(comingFrom.compare("charge")== 0){
            if(mode == -1){
                if(enabled){
                    if(postOnce){
                        if(pointPointer<pointsToDroneCage.size()){ //mode -1 Indicates that the robot is stopped and there is no more points in the list
                            nextPoint = getNextPoint(pointsToDroneCage[pointPointer]);
                            pointPointer++;
                            state = NEW_POSITION;
                        }else{
                            state = MISSION_DONE;
                            comingFrom = "droneCage";
                        }
                   }
                }else{
                    state = WAIT;
                }
            }
        }
        if(comingFrom.compare("lineFollowing")== 0){
            if(mode == -1){
                if(enabled){
                    if(postOnce){
                        if(pointPointer<pointsToCharge.size()+1){ //mode -1 Indicates that the robot is stopped and there is no more points in the list
                            if(pointPointer<pointsToCharge.size()-1){
                                nextPoint = getNextPoint(pointsToCharge[pointPointer]);
                            }else{
                                switch(pointPointer){
                                case 4:
                                    nextPoint = pointsToDroneCage[3];
                                    break;
                                case 5:
                                    nextPoint = pointsToDroneCage[4];
                                    break;
                                }
                            }
                            pointPointer++;
                            state = NEW_POSITION;
                        }else{
                            state = MISSION_DONE;
                            comingFrom = "droneCage";
                        }
                    }
                }else{
                    state = WAIT;
                }
            }
        }
    }

    if(mission.compare("stop") == 0){
        pointPointer--;
        state = STOP;
    }
    if(mode!=-1){
        postOnce=true;
    }

    /*ROS_INFO("WptCallback mission: %s",mission.c_str());
    ROS_INFO("WptCallback Coming from: %s",comingFrom.c_str());
    ROS_INFO("Mode: %d", mode);
    ROS_INFO("enabled: %d", enabled);
    ROS_INFO("postOnce: %d", postOnce);
    ROS_INFO("pointPointer: %d", pointPointer);*/
}

void enablerCallback(const msgs::BoolStamped boolMsg){
    enabled = boolMsg.data;
}


bool missionCallback(top_module::zone_order::Request& missionIncoming,top_module::zone_order::Response& res){
    mission = missionIncoming.order;
    if(mission.compare("stop")!=0){
        pointPointer = 1;
    }
    res.confirmation = true;
    res.message = "Mission Received";
    pointPointer = 1;
    postOnce = true;
    mode = -1;
    return true;
}



int main(int argc, char **argv)
{
    pointsToCharge[1].easting = 3.6;
    pointsToCharge[1].northing = 1.6;
    pointsToCharge[2].easting = 1.6;
    pointsToCharge[2].northing = 1.6;
    pointsToCharge[3].easting = 0;
    pointsToCharge[3].northing = 1.6;
    pointsToCharge[4].easting = 0;
    pointsToCharge[4].northing = 0;

    pointsToLineFollowing[1].easting = -0.2;
    pointsToLineFollowing[1].northing = 0.2;
    pointsToLineFollowing[2].easting = -0.2;
    pointsToLineFollowing[2].northing = 0.8;
    pointsToLineFollowing[3].easting = 1.6;
    pointsToLineFollowing[3].northing = 0.8;
    pointsToLineFollowing[4].easting = 3.9;
    pointsToLineFollowing[4].northing = 0.8;
    pointsToLineFollowing[5].easting = 3.9;
    pointsToLineFollowing[5].northing = 0.4;

    pointsToDroneCage[1].easting = -0.2;
    pointsToDroneCage[1].northing = 0.2;
    pointsToDroneCage[2].easting = -1; //Waiting point
    pointsToDroneCage[2].northing = 0.2;
    pointsToDroneCage[3].easting = -1.5;
    pointsToDroneCage[3].northing = 0.8;
    pointsToDroneCage[4].easting = -1.5;
    pointsToDroneCage[4].northing = 1.2;

    ros::init(argc, argv, "gps_to_map_node");

    ros::NodeHandle n;
    ros::ServiceServer serviceMission = n.advertiseService("/zone_activation/wpt_navigation", missionCallback);
    ros::Subscriber wptStatusSub = n.subscribe("/fmInformation/wptnav_status", 1000, wptCallback);
    ros::Subscriber enablerSub = n.subscribe("/fmSafe/actuation_enable", 1000, enablerCallback);
    ros::ServiceClient clientMission = n.serviceClient<top_module::zone_state>("zone_activation/TopModule");
    ros::Publisher pubKeyboardOverride = n.advertise<msgs::RemoteControl>("/fmHMI/remote_control", 1000);
    ros::Publisher pubOdom = n.advertise<msgs::FloatArrayStamped>("/fmInformation/odom_reset", 1);
    ros::Publisher pub = n.advertise<msgs::RoutePt>("/fmPlan/route_point", 1000);

    top_module::zone_state clientMessage;

    //Overriding Keyboard
    msgs::RemoteControl keyboardMsg;
    keyboardMsg.header.stamp = ros::Time::now();
    keyboardMsg.switches = 0;
    keyboardMsg.switches |= 0x01;
    keyboardMsg.analog_joy_a_up_down = 0;
    keyboardMsg.analog_joy_a_left_right = 0;
    keyboardMsg.remote_connected = true;
    keyboardMsg.deadman_state = true;
    keyboardMsg.emergency_stop = false;
    keyboardMsg.rc_battery_low_warning = false;
    keyboardMsg.rc_battery_low_alert = false;

    //UDP Client Bring-up
    int retry = 0;
    int retryAux = 0;

    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 100000;
    int sockfd;
    sockfd = socket(AF_INET,SOCK_DGRAM,0);
    if(sockfd<0){
        printf("\nSocket Creation FAILED!");
        return 0;
    }
    struct sockaddr_in serv,client;

    serv.sin_family = AF_INET;
    serv.sin_port = htons(5005);
    serv.sin_addr.s_addr = inet_addr("10.126.128.38");
    int conNumber = connect(sockfd,(const struct sockaddr *)&serv,sizeof(serv));
    if (conNumber!=0){
        printf("\nSocket Connection FAILED!\n");
        return 0;
    }
    char udpResponse[1024];
    const char* msg = "markerpose_7";
    socklen_t l = sizeof(client);
    socklen_t m = sizeof(serv);
    setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval));

    msgs::FloatArrayStamped odomPos;
    odomPos.data.push_back(1.30);
    odomPos.data.push_back(-1.60);
    odomPos.data.push_back(M_PI);
    pubOdom.publish(odomPos);
    std::vector<float> vectPos;

    ros::Rate loop_rate(100);

    //State Machine to check on correct point reaching
    while(ros::ok()){
        switch(state){
        case NEW_POSITION:
            keyboardMsg.switches |= 0x01;
            pubKeyboardOverride.publish(keyboardMsg);
            memset(udpResponse,'\0', 1024);
            sendto(sockfd,msg,sizeof(msg)+8,0,(struct sockaddr *)&serv,m);
            while(recvfrom(sockfd,udpResponse,1024,0,(struct sockaddr *)&client,&l)<=0 && retry <20){
                retry++;
            };
            if(retry<20){
                vectPos.clear();
                vectPos =  parseMarkerPose(string(udpResponse,1024));
                while(vectPos[4]<0.1){
                    memset(udpResponse,'\0', 1024);
                    sendto(sockfd,msg,sizeof(msg)+8,0,(struct sockaddr *)&serv,m);
                    while(recvfrom(sockfd,udpResponse,1024,0,(struct sockaddr *)&client,&l)<=0 && retryAux <10){
                        retry++;
                    };
                    if(retryAux<10){
                        vectPos.clear();
                        vectPos =  parseMarkerPose(string(udpResponse,1024));
                    }
                    retryAux=0;
                }
                //if(vectPos[4]>0.2){ //Check for quality of the marker
                    odomPos.data.clear();
                    odomPos.data.push_back(vectPos[1]);
                    odomPos.data.push_back(vectPos[2]);
                    odomPos.data.push_back(vectPos[3]+M_PI); //+pi is because the marker is positioned "backwards"
                    pubOdom.publish(odomPos);
                //}
            }
            retry =0;
            nextPoint.cmd=2;
            nextPoint.nav_mode=1;
            pub.publish(nextPoint);
            ros::Duration(0.05).sleep();
            pub.publish(nextPoint);

            postOnce=false;
            state = WAIT;
            break;
        case MISSION_DONE:
            keyboardMsg.switches = 0;
            pubKeyboardOverride.publish(keyboardMsg);
            clientMessage.request.status = true;
            clientMission.call(clientMessage);
            postOnce=true;
            state = WAIT;
            mission = "noMission";
            break;
        case STOP:
            break;
        case WAIT:

            break;
        }
        loop_rate.sleep();
        ros::spinOnce();
    }

    return 0;
}


