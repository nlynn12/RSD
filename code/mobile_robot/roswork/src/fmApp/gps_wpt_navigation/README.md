# Description of the package

This package is in charge of the navigation between the line following zone and the charging zone. It does so by using waypoint navigation and correcting
with the marker locator camera(gps)

# Instructions of use (as of now 11/11/16)
1.- Launch robot controller or demo
2.- Launch gps_wps_navigation_node
3.- Enable actuation
4.- Publish a string in topic /mission with containing "charge" or "lineFollowing"
5.- Profit.


