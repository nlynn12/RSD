#include<serial/serial.h>
#include<ros/ros.h>
#include<top_module/moveService.h>
#include<top_module/zone_state.h>
#include<string>
#include<sstream>
#include<std_msgs/String.h>
#include<iostream>

enum commands {noCommand = 0, go, getState, stopTipper, changeSpeed, ackError};
enum arduino_states {ok=0, moving, moving_back, inError, masterCommandError, commandNotFound, speedError};

arduino_states arduino_state = ok;

bool move_now = false;
bool move_done = false;
bool moveDone();


serial::Serial serial_handle;

void writeCommand(int command){
    std::stringstream ss;
    ss << command << ";";
    std::string str = ss.str();
    serial_handle.write(str);
}

void readState(){
    serial_handle.flush();
    std::string result;
    writeCommand(getState);
    result = serial_handle.readline(10);
    int temp = 0;
    temp = (int)(result[0] - '0');
    arduino_state = static_cast<arduino_states>(temp);
}

void writeSpeed(int speed){
    std::stringstream ss;
    ss << speed << " " << speed << ";";
    std::string str = ss.str();
    serial_handle.write(str);
}

bool ackErrorCB(top_module::moveService::Request &req,top_module::moveService::Response &res ){
    readState();
    if(arduino_state > moving){ // The arduino is actually in error
        writeCommand(ackError);
        res.info = "";
        return true;
    }
    else{
        res.info = "Not in error";
        return false;
    }  
    
}

bool stopTipperCB(top_module::moveService::Request &req,top_module::moveService::Response &res){
    readState();
    if(arduino_state == ok){
        res.info = "Not moving";
        return true;
    }else if(arduino_state == 1){
        writeCommand(stopTipper);
        ros::Duration(3).sleep();
        readState();
        if(arduino_state == ok){
            res.info = "Stopped";
            return true;
        }else{
            res.info = "Could not stop";
            return false; 
        }
    }
    res.info = "Undefined behaviour";
    return false;
}

bool setSpeedCB(top_module::moveService::Request &req,top_module::moveService::Response &res ){
    readState();
    if(arduino_state == ok){
        writeSpeed(req.data);
        ros::Duration(1).sleep();
        readState();
        if(arduino_state == ok){
            res.info = "Set speed succesful";
            return true;
        }else{
            res.info = "Set speed failed. In error after set";
            return false;
        }
    }else{
        res.info = "Set speed failed. In error before set";
        return false;
    }
    
}

bool moveNowCB(top_module::moveService::Request &req,top_module::moveService::Response &res ){
    readState();
    if(arduino_state == ok){
        writeCommand(go);
        ros::Duration(1).sleep();
        move_now = true;
        return true;
    }else{
     return false;   
    }
}


int main(int argc, char** argv){
    
    ros::init(argc,argv,"piToArduinoInterface");
    
    ros::NodeHandle nh;
    
    ros::ServiceServer moveServer = nh.advertiseService("moveService",moveNowCB);
    ros::ServiceServer stopServer = nh.advertiseService("stopService",stopTipperCB);
    ros::ServiceServer ackServer = nh.advertiseService("arduinoAckService",ackErrorCB);
    ros::ServiceServer speedServer = nh.advertiseService("setSpeedService",setSpeedCB);
    ros::ServiceClient topModule = nh.serviceClient<top_module::zone_state>("/unload_activation/TopModule/");
    ros::Publisher state_pub = nh.advertise<std_msgs::String>("arduinoStateTopic",10);
    
    // CODE FROM FLIR PTU NODE https://github.com/ros-drivers/flir_ptu/blob/master/flir_ptu_driver/src/node.cpp
   // <STOLEN CODE>
        std::string port;
        int32_t baud;
     try
    {  
       ros::param::param<std::string>("~port", port, "/dev/ttyACM0");
        ros::param::param<int32_t>("~baud", baud, 9600);
        serial_handle.setPort(port);
        serial_handle.setBaudrate(baud);
        serial::Timeout to = serial::Timeout(200, 200, 0, 200, 0);
        serial_handle.setTimeout(to);
        serial_handle.open();
    }
    catch (serial::IOException& e)
    {
        ROS_ERROR_STREAM("Unable to open port " << port);
        return 1;
    }
    // </STOLEN CODE>
    
    ros::Rate loopRate(2);
    
    while(ros::ok()){
        std_msgs::String msg;
        
        if(serial_handle.isOpen()){
            readState();

            std::stringstream ss;
            ss << arduino_state;
            msg.data = ss.str();
            if(move_now){
               move_done = moveDone();
                std::cout << "Waiting" << std::endl;
            }
            if(move_done){
                std::cout << "DONE" << std::endl;
                top_module::zone_state msgs;
                topModule.call(msgs);
                move_now = false;
                move_done = false;
            }
        }else{
            msg.data = "No connection";  
        }
        
        state_pub.publish(msg);
        
        ros::spinOnce();
        loopRate.sleep();
    }
    
    return 0;
    
}

bool moveDone(){
    readState();
    switch(arduino_state){
        case moving_back:
            return true;
        case ok:
            return false;
        case inError:
            return false;
        case masterCommandError:
             return false;
        case commandNotFound:
           return false;
        default:
            return false;
    }
    return false;
}
