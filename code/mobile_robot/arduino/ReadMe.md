Use:
    
    Install Arduino IDE
    Move content of libs folder into Arduino library folder (most likely /home/XX/Arduino/libraries)
    Flash from IDE
    
I/O Interface:
    
    Setting drive_motor, pin 3, high will start the unloading IFF the controller is ready.
    The drive is ready when it is in standstill in at the first sensor
    
    When the controller gets the command to drive, motor_ready gets put low. The RPi can then poll this signal to see when the unloading is done.
    
    If the unloading takes to long, defined by the #define in line 5, the system will go into an error mode and stop all movements. The motor_in_error pin, pin 5, will go high until the RPi acknowledge the error, after it has been solved.
    After the ack_error, the "bucket" will return to start position

