#include <CmdMessenger.h>
#include"Timer.h"

char field_separator   = ',';
char command_separator =  ';';
char escape_separator  = '/';

// Instantiate Messenger object with the default separator (the space character)

CmdMessenger cmdMessenger = CmdMessenger(Serial, field_separator, command_separator);
#define sec 1000

#define forward HIGH // Should be changed to low if the wireing demands
#define backwards not forward

//-------------------------Time intervals -----------------------------
#define move_time             500 //Time the motor has to run before it goes backwards again
#define ramp_step             40
#define ramp_down_time        2500

enum state {READY = 0, START_MOVE, MOVE_FORWARD, START_BACKWARDS, MOVE_BACKWARD}; // Internal state definition
enum commands {noCommand = 0, go, getState, stopTipper, changeSpeed, reset};
enum arduino_states {ok = 0, moving, moving_back, masterCommandError, commandNotFound}; // State and "Errorcodes"
// Definitions for I/O handling

int begin_switch      = 2;

int step_pulse        = 9;s
int motor_enable      = 10;
int motor_direction   = 11;

int led_pin           = 13;
bool led_status = true;
bool stop_true = false;
// End definitions

state s = READY;

commands command = noCommand;
arduino_states arduino_state = ok;

int ramp_index = 0;
int step_count = 0;
bool ramp_up = true;

//Timer for timeouts and ids for the different events
Timer t;
int timer_id;
int ramp_timer_id;
int ramp_down_id;
int switchState = 1;

void setup() {
  pinMode(begin_switch, INPUT);
  pinMode(led_pin, OUTPUT);

  pinMode(motor_enable, INPUT);
  pinMode(motor_direction, OUTPUT);
  pinMode(step_pulse, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(begin_switch), switchCallback, CHANGE);

  // Initiate Serial Communication
  Serial.begin(9600);
  // Attach the callback function to the Messenger
  cmdMessenger.attach(go, goCallback);
  cmdMessenger.attach(getState, stateCallback);
  cmdMessenger.attach(stopTipper, stopCallback);
  cmdMessenger.attach(ackError, ackErrorCallback);
  cmdMessenger.attach(unknownCommandCallback);
  setPwmFrequency(step_pulse, 8);
  analogWrite(step_pulse, 0);
  digitalWrite(motor_enable, LOW);

}

void loop() {
  switch (s) {
    case READY:
      analogWrite(step_pulse, 0);
      digitalWrite(motor_enable, LOW);
      if (switchState == HIGH) {
        arduino_state = ok;
        if (command == go) {
          command = noCommand;
          s = START_MOVE;
        }
      }
      break;
    case START_MOVE:
      //Set the motor pins
      arduino_state = moving;
      digitalWrite(motor_enable, HIGH);
      digitalWrite(motor_direction, forward);

      //Reset ramping variables
      ramp_index = 0;
      ramp_up = true;

      unload();
      s = MOVE_FORWARD;
      break;

    case MOVE_FORWARD:
      //Serial.println(step_count);
      if (step_count == 12) {
        s = START_BACKWARDS;
        digitalWrite(motor_direction, backwards);
      }
      analogWrite(step_pulse, ramp_index);
      if (step_count % 2 == 0) {
        delay(3000);
      }
      break;

    case START_BACKWARDS:
      arduino_state = moving_back;
      step_count = 0;
      ramp_index = 0;
      stop_true = false;
      ramp_timer_id = t.every(ramp_step, ramp_function);
      t.stop(timer_id);
      s = MOVE_BACKWARD;
      break;

    case MOVE_BACKWARD:
      analogWrite(step_pulse, ramp_index);
      if (switchState == HIGH) { // Has it reached the light gate
        cmdMessenger.sendCmd(11);
        t.stop(ramp_timer_id);
        ramp_index = 0;
        analogWrite(step_pulse, 0);
        digitalWrite(motor_enable, LOW); // Disable motor controller
        s = READY;
        led_off();
      }
      break;
      case STOP:
          t.stop(ramp_timer_id);
          t.stop(timer_id);
  }
  if (command == reset) {
    // RPi acknowledging the error, having fixed it
    // Reset the system to original position
    command = noCommand;
    arduino_state = ok;
    digitalWrite(motor_direction, backwards);
    s = START_BACKWARD;
  }
  if(command == stopTipper){
     s = STOP;
  }
  digitalWrite(led_pin, digitalRead(begin_switch));
  t.update();
  cmdMessenger.feedinSerialData();
}


void move_timeout() {
  step_count = step_count + 1;
  if (stop_true) {
    stop_true = false;
    ramp_index = 0;
  } else {
    stop_true = true;
  }
  digitalWrite(motor_enable, stop_true);
};

void ramp_function() {
  if (ramp_index <= 100) {
    if (ramp_up == true) {
      ramp_index += 5;
      if (ramp_index > 100) {
        ramp_index = 100;
      }
    } else {
      ramp_index -= 5;
      if (ramp_index <= 40 and s == MOVE_BACKWARD) {
        ramp_index = 40;
      } else if (ramp_index <= 0) {
        ramp_index = 0;
      }
    }
  }
}

void ramp_down() {
  ramp_up = false;
}

void led_on() {
  digitalWrite(led_pin, HIGH);
}

void led_off() {
  digitalWrite(led_pin, LOW);
}

bool setPwmFrequency(int pin, int divisor) {
  byte mode;
  bool ret_val = false;
  if (pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch (divisor) {
      case 1: mode = 0x01; ret_val = true; break;
      case 8: mode = 0x02; ret_val = true; break;
      case 64: mode = 0x03; ret_val = true; break;
      case 256: mode = 0x04; ret_val = true; break;
      case 1024: mode = 0x05; ret_val = true; break;
      default: ret_val = false; return;
    }
    if (pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if (pin == 3 || pin == 11) {
    switch (divisor) {
      case 1: mode = 0x01; ret_val = true; break;
      case 8: mode = 0x02; ret_val = true; break;
      case 32: mode = 0x03; ret_val = true; break;
      case 64: mode = 0x04; ret_val = true; break;
      case 128: mode = 0x05; ret_val = true; break;
      case 256: mode = 0x06; ret_val = true; break;
      case 1024: mode = 0x7; ret_val = true; break;
      default: ret_val = false; return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}

void stateCallback() {
  cmdMessenger.sendCmd(arduino_state);
  command = noCommand;
}

void unload() {
  ramp_timer_id = t.every(ramp_step, ramp_function);
  ramp_index = 0;
  timer_id = t.every(move_time, move_timeout); // Timeout event for motor movement
}

void switchCallback() {
  switchState = digitalRead(begin_switch);
}

void unknownCommandCallback() {
  cmdMessenger.sendCmd("No such command");
}

void goCallback() {
  command = go;
}

void stopCallback() {
  command = stopTipper;
}

void resetCallback() {
  command = reset;
}

