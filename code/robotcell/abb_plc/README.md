ABB PLC 
====

#### Important
To open and load the program to the PLC you will need a windows machine and the AC500 software.

#### General
This folder contains the code project for the PLC. The PLC is from ABB, model PM554-T-ETH A4. The PLC is set up to control the ACS 150-01E-04A7-2 (frequency converter).
The PLC is set up to read a register on a PC via the protocol Open MODBUS that runs over TCP/IP.


System Overview
----
We are using open Modbus which is a TCP/IP version of the MODBUS protocol:
```
client->Master, Server->slave
```

System Diagram
```
ACS150                                   PLC                       External
----------------------------------------------------------------------------
 Freq Converter  <- Logic Level -> 'Modbus Master' <- TCP/IP -> MODBUS slave

```

 
Instructions
-----------
Running the Project on PLC:
```
1. Doubleclick on the project file.
2. When AC500 is open, doubleclick the file called AC500 in the left pane.
3. Press F8 to "Login".
4. Press F5 to "Run".
```
Make sure that you are connected via LAN and that you have an IP within the same range of the PLC:
```
Example IP: 192.168.100.99
```
PLC IP:
```
192.168.100.110
```
Create bootable PLC with SD-card:

[Boot via SD](http://www402.abbext.com/PROGRAM-UPLOAD-FROM-AC500-q29676.aspx)


Test Conveyer Belt
----------
1. Make sure that the program is running on the PLC
2. Download MODSIM32 from dark places of the internet
3. In MODSIM32 create a connection using TCP/IP and Port 502
4. Go to File -> New
5. Change the value of regiser 40101 to '1'


Readings
-----------
[Frequency Converter](http://www.abb.com/product/seitp322/b7ed8eb851044fbf44257a62004cd28c.aspx?tabKey=2&gid=TEMP.FIDRI68581966)

[Frequency Converter User Manuel](https://library.e.abb.com/public/3ce4c88ea30720d0c1257850004a181e/DA_ACS150_UM_C_screen_res.pdf)

[Python Exanmples to use MODBUS](http://pymodbus.readthedocs.io/en/latest/examples/asynchronous-server.html)

[AC500 Application Examples Modbus](http://www.abb.com/abblibrary/downloadcenter/?CategoryID=9AAC177443&View=Result&DocumentKind=Technical+Description&QueryText=Application+Examples+Modbus+TCP&SortBy=Score)

[Open MODBUS example using TCP/IP in AC500](http://search-ext.abb.com/library/Download.aspx?DocumentID=2CDC125166M0201&LanguageCode=en&DocumentPartId=&Action=Launch)

Videos
----------
#### Write your first program
<a href="http://www.youtube.com/watch?feature=player_embedded&v=sIUFx0tG0Hk
" target="_blank"><img src="http://img.youtube.com/vi/sIUFx0tG0Hk/0.jpg" 
alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>

#### How to Run your first program on the PLC
<a href="http://www.youtube.com/watch?feature=player_embedded&v=L0HhKEg7vEI
" target="_blank"><img src="http://img.youtube.com/vi/L0HhKEg7vEI/0.jpg" 
alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>

