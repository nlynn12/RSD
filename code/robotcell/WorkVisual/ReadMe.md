Instructions
-----------
1. To edit Work Visual connections to match the internal 5/2 valve install  Work Visual from the robot D:\WorkVisual\Setup_WorkVisual 
2. Currently used Work Visual files are available on git. Else the configuration can be downloaded from the robot and be modified directly from the robot.
3. To acces the current project the your ip has to be 172.31.1.xxx just not 172.31.1.147 as that is the robots ip. Subnet is 255.255.0.0. ping 172.31.1.147 to ensure you have connection to the robot.
4. Find robot program under browse
5. open the KRC IO mapping
6. Map Digout 7 to to fieldbus EM8905-1001 I/O Module channel 7. Map Digout 10 to to fieldbus EM8905-1001 I/O Module channel 10.
7. Deploy the program to the robot. Expertmode required.
8. The connections can be tested manually by controlling the I/O on the smartHMI or running the RSI program designed for this project 

Info
-----------
The current configuration is available under git
Datablade
-----------
1. Datasheet for Work Visual 3.3 er placeret på Git RSD/hardware/datasheet
