Instructions
-----------
1. To edit RSI Ladder diagram install  RSIvisual from black board
2. Currently used RSI files are available on git
3. RSIJointControl filen med typen RSI file kan åbnes i RSI visual og redigeres, når den gemmes genererer den de andre filer undtagen RSIJointControlConfig og RSIJointControl.src.
RSIJointControlConfig indeholder definationer for xml protokollen og ip for robot samt port. Skal redigeres hvis ladder diagram redigeres.
RSIJointControl.src indeholder selve robotprogrammet der køres og bør ikke ændres.
4. Disse filer undtagen src placeres i C:\KRC\ROBOTER\Config\User\Common\SensorInterface på robotten via usb. Expert user er required.
5. RSIJointControl.src placeres i KRC:\R1\Program
6. Kør RSIJointControl.src med en RSI server kørende på en PC.

Info
-----------
1. Nuværende configuration kører IPO_FAST hvilket betyder at der skal svares på UDP beskeden indenfor 4ms. Dette er nødvendigt for ikke at støde ind i sensor fejl (reason unknown)
2. Robotten sender UDP beskeder til 192.168.100.50:49000, computeren der skal kommunikerer skal derfor have denne IP.
3. Fejlbeskeden hvis der ikke svares på UDP er: Signal flow (running): Object ETHERNET1 returns error RSITimeout
4. RSI protokollen er defineres således at den virker i sammenarbejde med IO mapping i Work Visual. Portene er således at Digout 7 & Digout 10 styrer gripperen. De kan derfor læses direkte i xml filen der sendes via UDP beskeden. For at sætte gripperen anvendes kun en bool der senedes via xml til gripperen. RSI Visual inverterer så selv digout 7 og digout 10 således at det passer.
5. Filerne er lagt på robotten og Work Visual er konfigureret.

Datablade
-----------
1. Datablad for RSIvisual 3.3 er placeret på Git RSD/hardware/datasheet
