#include "kukaRSIParser.h"

#define DEBUG false

std::string _sendString = "<Sen Type=\"ROS\"><EStr></EStr></Sen>";

KukaRSIParser::KukaRSIParser()
{
    // Init variables
    _IPOC = -1;
    _digOut.resize(3);
}

/*KukaRSIParser::KukaRSIParser(std::string package)
{
    // Init variables
    _IPOC = -1;

    // Load
    loadFile(package);
}*/

KukaRSIParser::~KukaRSIParser()
{

}

bool KukaRSIParser::parseXML(std::string xmlString, bool loadFromFile)
{
    // Open document
    if(loadFromFile)
    {
        if(_doc.LoadFile(xmlString.c_str()) != 0)
        {
            std::cerr << "Error opening file '" << xmlString << "'!" << std::endl;
            return false;
        }
    }
    else
    {
        if(_doc.Parse(xmlString.c_str()) != 0)
        {
            std::cerr << "Error parsing string!" << std::endl;
            return false;
        }
    }

    // Check if "Rob" file and robot = KUKA
    if(std::string(_doc.RootElement()->Value()) == "Rob" && std::string(_doc.FirstChildElement("Rob")->FirstAttribute()->Value()) == "KUKA")
    {
        // Get RIst
        XMLElement *AIPosElement = _doc.RootElement()->FirstChildElement("AIPos");

        if(AIPosElement != 0)
        {
            _AIPos.q[0] = atof(AIPosElement->Attribute("A1"));
            _AIPos.q[1] = atof(AIPosElement->Attribute("A2"));
            _AIPos.q[2] = atof(AIPosElement->Attribute("A3"));
            _AIPos.q[3] = atof(AIPosElement->Attribute("A4"));
            _AIPos.q[4] = atof(AIPosElement->Attribute("A5"));
            _AIPos.q[5] = atof(AIPosElement->Attribute("A6"));
        }
        else
        {
            std::cerr << "Error finding AIPos element!" << std::endl;
            return false;
        }

        // Get Digout (External emergency)
        XMLElement *DigElement = _doc.RootElement()->FirstChildElement("Digout");

        if(DigElement != 0)
        {
            const char* attribute_o1 = DigElement->Attribute("o1");
	    const char* attribute_o7 = DigElement->Attribute("o7"); 
            const char* attribute_o10 = DigElement->Attribute("o10"); 
	    if(attribute_o1 != 0){
                _digOut[0] = std::atoi(attribute_o1);
	    } 
#if DEBUG == true
	    else {
                std::cout << "Did not recieve attribute o1 of Digout." << std::endl;
	    }
#endif
	    if(attribute_o7 != 0){
                _digOut[1] = std::atoi(attribute_o7);
	    } 
#if DEBUG == true
	    else {
                std::cout << "Did not recieve attribute o7 of Digout." << std::endl;
	    }
#endif
	    if(attribute_o10 != 0){
                _digOut[2] = std::atoi(attribute_o10);
	    } 
#if DEBUG == true
	    else {
                std::cout << "Did not recieve attribute o10 of Digout." << std::endl;
	    }
#endif
        }
        else
        {
#if DEBUG == true
            std::cout << "Error finding Digout element!" << std::endl;
#endif
        }

        // Get delay
        XMLElement *DelayElement = _doc.RootElement()->FirstChildElement("Delay");

        if(DelayElement != 0)
        {
            const char* attribute_d = DelayElement->Attribute("D");
	    if(attribute_d != 0){
		    _delay = atoi(attribute_d);
	    } 
#if DEBUG == true
	    else {
                std::cout << "Did not recieve attribute D of Delay." << std::endl;
	    }
#endif
            
        }
#if DEBUG == true
        else {
            std::cout << "Error finding delay element!" << std::endl;
	}
#endif	

        // Get IPOC
        XMLElement *IPOCElement = _doc.RootElement()->FirstChildElement("IPOC");

        if(IPOCElement != 0){
            const char* attribute_ipoc = IPOCElement->FirstChild()->Value(); 
            if(attribute_ipoc != 0){
                _IPOC = atoi(attribute_ipoc);
            } 
#if DEBUG == true
            else {
                std::cout << "Did not recieve IPOC." << std::endl;
	    }
#endif
	} else
        {
            std::cerr << "Error finding IPOC element!" << std::endl;
            return false;
        }
	
    }
    else
    {
        std::cerr << "XML Root element != \"Rob\" or Robot type != \"KUKA\"" << std::endl;
        return false;
    }

    return true;
}

std::string KukaRSIParser::constructFile(RSI_AIPos AIPos)
{
    XMLDocument newDoc;

    if(newDoc.Parse(_sendString.c_str()) != 0)
    {
        std::cerr << "Error parsing sendString!" << std::endl;
        return "";
    }

    // Add AIPos element
    newDoc.RootElement()->InsertEndChild(newDoc.NewElement("AKorr"));

    for(int i=0; i<6; i++)
    {
        std::string attribute = "A" + SSTR(i+1);
        std::stringstream str;
        str << std::fixed << std::setprecision(DEF_PRECISION) << AIPos.q[i];
        std::string value = str.str();
        newDoc.RootElement()->FirstChildElement("AKorr")->SetAttribute(attribute.c_str(), value.c_str());
    }
    
    // Add Gripper element
    newDoc.RootElement()->InsertEndChild(newDoc.NewElement("Gripper"));
    std::string val_grasp = "0";
    if(AIPos.grasp){
        val_grasp = "1";
    }
    newDoc.RootElement()->FirstChildElement("Gripper")->SetAttribute("G", val_grasp.c_str());
    

    // Add IPOC
    if(_IPOC == -1)
    {
        std::cerr << "IPOC Number unknown!" << std::endl;
        return "";
    }

    newDoc.RootElement()->InsertEndChild(newDoc.NewElement("IPOC"));
    newDoc.RootElement()->FirstChildElement("IPOC")->InsertEndChild(newDoc.NewText(boost::lexical_cast<std::string>(_IPOC).c_str()));

    // Get string
    XMLPrinter printer;
    newDoc.Accept(&printer);

    // Return XML formatted string
    return std::string(printer.CStr());
}

