/*
 * Copyright 2016 Lukas Schwartz <lschw12@student.sdu.dk>  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

// Includes
#include <iostream>
#include <queue>
#include <boost/thread.hpp>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <ctime>
#include <ratio>
#include <mutex>
#include <chrono>
#include <fstream>
#include "kukaRSIParser.h"
#include "UDP.h"
#include "PracticalSocket.h"
#include <rw/math.hpp>
#include <rw/math/Q.hpp>
#include <rw/trajectory/RampInterpolator.hpp>

#define DEBUG                   false
#define DEBUG_SETQ		false
#define MONITOR                 false
#define TCP_IP_SERVER           "192.168.100.50"
//#define TCP_IP_SERVER         "192.168.100.50"
#define UDP_IP_SERVER           "192.168.100.50"
#define UDP_IP_CLIENT           "192.168.100.100"
#define UDP_PORT_SERVER         49000
#define UDP_PORT_CLIENT         53453
#define TCP_PORT_SERVER         49002

#define RSI_TIME                0.004   // s / after speed up to make it work
#define DEGREETORAD             (M_PI/180.0)
#define RADTODEGREE             (180.0/M_PI)
#define JOINT_SPEED_MAX         300
#define JOINT_SPEED_NORMAL      20
#define JOINT_SPEED_BRAKE       10
#define JOINT_ACC_MAX           300
#define JOINT_ACC_NORMAL        20
#define RSI_TIME_CHECK (RSI_TIME*1000)*2
#define RSI_DEAD_TIME           0.015   // s, used to decide when the connection to the robot is lost
const rw::math::Q QMAX(6, 170, 45, 156, 185, 120, 350);
const rw::math::Q QMIN(6, -170, -190, -120, -185, -120, -350);
const rw::math::Q speedBrake(6, JOINT_SPEED_BRAKE, JOINT_SPEED_BRAKE, JOINT_SPEED_BRAKE, JOINT_SPEED_BRAKE, JOINT_SPEED_BRAKE, JOINT_SPEED_BRAKE);
rw::math::Q accNormal(6, JOINT_ACC_NORMAL, JOINT_ACC_NORMAL, JOINT_ACC_NORMAL, JOINT_ACC_NORMAL, JOINT_ACC_NORMAL, JOINT_ACC_NORMAL);

/// NOTE: ALL messages that are sent to the ROS system via TCP must start with '(' and end with ')', these two characters may NOT be used otherwise.


// Max joint speed
//360 °/s
//300 °/s
//360 °/s
//381 °/s
//388 °/s
//615 °/s

template <typename T>
class SynchronisedQueue
{
    private:
        std::queue<T> m_queue;              // Use STL queue to store data
        boost::mutex m_mutex;               // The mutex to synchronise on
        boost::condition_variable m_cond;   // The condition to wait for

    public:
        // Add data to the queue and notify others
        void enqueue(const T& data)
        {
            // Acquire lock on the queue
            boost::unique_lock<boost::mutex> lock(m_mutex);

            // Add the data to the queue
            m_queue.push(data);

            // Notify others that data is ready
            m_cond.notify_one();
        }

        // Get data from the queue. Wait for data if not available
        T dequeue()
        {
            // Acquire lock on the queue
            boost::unique_lock<boost::mutex> lock(m_mutex);

            // When there is no data, wait till someone fills it.
            // Lock is automatically released in the wait and obtained
            // again after the wait
            while(m_queue.size()==0)
                m_cond.wait(lock);

            // Retrieve the data from the queue
            T result = m_queue.front();
            m_queue.pop();

            return result;
        }

        int size()
        {
            // Acquire lock on the queue
            boost::unique_lock<boost::mutex> lock(m_mutex);
            return m_queue.size();
        }

        T front()
        {
            // Acquire lock on the queue
            boost::unique_lock<boost::mutex> lock(m_mutex);
            return m_queue.front();
        }

        T back()
        {
            // Acquire lock on the queue
            boost::unique_lock<boost::mutex> lock(m_mutex);
            return m_queue.back();
        }

        void clear(bool smoothBrake)
        {
            // Acquire lock on the queue
            boost::unique_lock<boost::mutex> lock(m_mutex);

            std::queue<rw::math::Q> empty;

            if(m_queue.empty() == false && smoothBrake)
            {
                rw::math::Q qStart = m_queue.front();
                m_queue.pop();

                if(m_queue.empty() == false)
                {
                    rw::math::Q qNext = m_queue.front();
                    m_queue.pop();
                    rw::math::Q qDelta = qNext-qStart;

                    for(unsigned i=0; i<qDelta.size(); i++)
                    {
                        if(qDelta[i] > 0.05)
                            qNext[i] += 1.0;
                        else if(qDelta[i] < -0.05)
                            qNext[i] -= 1.0;
                    }

                    rw::trajectory::RampInterpolator<rw::math::Q> rampInterpolator(qStart, qNext, speedBrake, accNormal);
                    for(double j = 0.0; j <= rampInterpolator.duration(); j += RSI_TIME)
                       empty.push(rampInterpolator.x(j));
                }
            }

            std::swap(m_queue, empty);
        }
};

// Global variables
bool shutdownIndicator = false;
SynchronisedQueue<rw::math::Q> _qQueue;
rw::math::Q _qCurrent(6,0,0,0,0,0,0), _qOffset(6, 0, -90, 90, 0, 90, 0); // nice offset, someow it works...
boost::mutex _qMutex;
bool _safety = true, _digOut7 = false, _digOut10 = false, _graspStatus = true;
boost::mutex _safetyMutex; 
std::chrono::high_resolution_clock::time_point _last_recv_time; // used to see when last package from Kuka was recieved
bool _connectedToRobot = false;
std::list< TCPSocket * > tcp_sockets;
std::mutex socket_protection;

// Prototypes
bool setConfigurationCallback(rw::math::Q);

// Functions

void udp_communication()
{
#if MONITOR == true
    rw::math::Q lastRealQ(6, 0, 0, 0, 0, 0, 0);
    bool suspiciousQ = false;
    int errorCount = 0;
    std::chrono::high_resolution_clock::time_point starttime, endtime;
    double lastTime = 0;
    std::string lastPackage = "", lastReply;
    std::fstream filelog("monitorDataExchange.log", std::fstream::out | std::fstream::app);
    time_t timenow;
    std::time(&timenow);
    filelog << "\n--------------------------\nStarted Monitor: " << std::asctime(std::localtime(&timenow)) << "\n" << std::flush;
    int client_connection_count = 1, server_connection_count = 1;
#endif
    // Variables
    udp_client_server::udp_client *client = NULL;
    udp_client_server::udp_server *server = NULL;
    while(!shutdownIndicator && server == NULL){
	try{
    		server = new udp_client_server::udp_server(UDP_IP_SERVER, UDP_PORT_SERVER);
    	} catch (std::runtime_error &err) {
	    std::cout << "Could not create udp server, retrying.\nError was: " << err.what() << std::endl;
	    server = NULL;
	    usleep(500000); // µs, wait half a second
#if MONITOR == true
            server_connection_count++;
#endif
    	}
    }
#if MONITOR == true
            filelog << "\n--------------------------\nConnected to UDP server after: " << server_connection_count << " tries.\n";
#endif

    while(!shutdownIndicator && client == NULL){
	try{
    		client = new udp_client_server::udp_client(UDP_IP_CLIENT, UDP_PORT_CLIENT);
    	} catch (std::runtime_error &err) {
	    std::cout << "Could not create udp client, retrying.\nError was: " << err.what() << std::endl;
	    client = NULL;
	    usleep(500000); // µs, wait half a second
#if MONITOR == true
            client_connection_count++;
#endif
        }
    }
#if MONITOR == true
            filelog << "\n--------------------------\nConnected to UDP client after: " << client_connection_count << " tries.\n";
#endif

    
    KukaRSIParser rsiParser;
    char *msg = new char[1000];
    std::string strPackage, strReply;
    rw::math::Q qCurrentReal(6);
    int oldIPOC = 0;
    rw::math::Q qNew(6);
    rw::math::Q qCurrent(6);
    int delay = 0, IPOC = 0;
    bool RSITimeExceeded = false;
    // Loop
    while(!shutdownIndicator)
    {
        try
        {
            // Receive packet from client
            int msgSize = server->recv(msg, 1000);
#if MONITOR == true
            starttime = std::chrono::high_resolution_clock::now();
#endif
            if(msgSize != -1)
            {
                // Convert to string
                strPackage = (std::string(msg)).substr(0,msgSize-1);

                
                // Parse packet
                rsiParser.parseXML(strPackage);

                // Get IPOC
                IPOC = rsiParser.getIPOC();

		// Get current joint configuration
                KukaRSIParser::RSI_AIPos AIPos = rsiParser.getQ();
                
		// Get safety and pneumatics
                _safetyMutex.lock();
                // Set timestamp
                _last_recv_time = std::chrono::high_resolution_clock::now();
		// get other stuff
                _safety = rsiParser.getDigout(0);
                _digOut7 = rsiParser.getDigout(1);
                _digOut10 = rsiParser.getDigout(2);
                AIPos.grasp = _graspStatus;
                _safetyMutex.unlock();

		for(int i=0; i<6; i++)
                    qCurrentReal[i] = AIPos.q[i];
		
#if MONITOR == true
		suspiciousQ = false;
		if(qCurrentReal == rw::math::Q(6, 0, 0, 0, 0, 0, 0) && qCurrentReal != lastRealQ){
			suspiciousQ = true;
		}
#endif
                // Get current in offset coordinates
                qCurrent = qCurrentReal - _qOffset;
                _qMutex.lock();
                _qCurrent = qCurrent;
                _qMutex.unlock();

                // Check IPOC
		RSITimeExceeded = (IPOC-oldIPOC) > RSI_TIME_CHECK;
                if(RSITimeExceeded) {
#if DEBUG == true
                    std::cout << "Connection to robot (re)established!" << std::endl;
#endif
                    _qQueue.clear(false);
                }
                oldIPOC = IPOC;

                // Inform
#if DEBUG == true
                    std::cout << "Received: \n" << strPackage << std::endl << std::endl;
#endif

                // Check delay
                delay = rsiParser.getDelay();
                if(delay > 0) {
                    std::cerr << "Delay: " << delay << std::endl;
                }
                /****************************************************************************/
                qNew = qCurrent;
                if(_qQueue.size())
                    qNew = _qQueue.dequeue();

                // Update new pos
                for(int i=0; i<6; i++)
                    AIPos.q[i] = qNew[i];
                
                /****************************************************************************/

                // Construct return frame
                strReply = rsiParser.constructFile(AIPos);

#if DEBUG == true
                std::cout << "Sent: \n" << strReply << std::endl << std::endl;
#endif
                // Send frame
                client->send(strReply.c_str(), strReply.size());

            }
            else
            {
                std::cerr << "Received empty frame!" << std::endl;
            }

#if MONITOR == true
            endtime = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(endtime - starttime);
            if((delay > 0 || RSITimeExceeded || time_span.count() > RSI_TIME || suspiciousQ) && msgSize != -1){ 
                std::time(&timenow);
                filelog << "\n--------------------------\n" << std::asctime(std::localtime(&timenow))
                        << ".\nError: " << errorCount++
                        << ".\nDelay:" << delay
                        << ".\nRSI Time Exceeded: " << RSITimeExceeded
                        << ".\nIPOC number: " << IPOC
                        << ".\nSuspicious Q: " << suspiciousQ
                        << ".\nCurrent reply time: " << time_span.count()
                        << ".\nLast reply time: " << lastTime
                        << ".\n\nCurrent message was:\n"  << strPackage  << "\nThis was replied with:\n" << strReply 
                        << " \n\nPrevious message was:\n" << lastPackage << "\nThis was replied with:\n" << lastReply 
                        << "\n" << std::flush;
            } 
            if(errorCount % 100 == 0 && errorCount != 0) {
                std::cout << "Error count reached " << errorCount << std::endl;		    
	    }
            lastTime = time_span.count();
            lastPackage = strPackage;
            lastReply = strReply;
#endif
            // Signal interrupt point
            boost::this_thread::interruption_point();
        }
        catch(const boost::thread_interrupted&)
        {
            std::cout << "- UDP thread interrupted. Exiting thread." << std::endl;
            break;
        }
    }
#if MONITOR == true
    std::time(&timenow);
    filelog << "\n--------------------------\nStopped Monitor: " << std::asctime(std::localtime(&timenow)) << "\n--------------------------\n" << std::flush;
    filelog.close();
#endif
    shutdownIndicator = true;
    std::cout << "UDP thread shut down." << std::endl;
}


bool setConfigurationCallback(rw::math::Q q, rw::math::Q speed)
{
    rw::math::Q qEnd(6);
    _qMutex.lock();
    rw::math::Q qStart = _qCurrent;
    _qMutex.unlock();

    if(_qQueue.size())
        qStart = _qQueue.back();

    // Check joint values
    for(unsigned int i=0; i<6; i++)
    {
    // Get joint valuessock
        qEnd(i) = q[i] * RADTODEGREE;
        if(qEnd(i) > QMAX(i) || qEnd(i) < QMIN(i))
        {
            std::cerr << "Joint limit overrule!" << std::endl;
            return false;
        }
    }
    qEnd -= _qOffset; // fix his stupidity

    // Check speed values
    rw::math::Q qSpeed(6);
    for(unsigned int i=0; i<qSpeed.size(); i++)
    {
        if(speed[i] < 1.0 || speed[i] > JOINT_SPEED_MAX)
            qSpeed(i) = JOINT_SPEED_NORMAL;
        else
            qSpeed(i) = speed[i];
    }

    // Check acceleration values
    // TODO

    // Create interpolated trajectory
    rw::trajectory::RampInterpolator<rw::math::Q> rampInterpolator(qStart, qEnd, qSpeed, accNormal);
    for(double j = 0.0; j <= rampInterpolator.duration(); j += RSI_TIME)
        _qQueue.enqueue(rampInterpolator.x(j));

    return true;
}

// TCP client handling function
void HandleTCPClient(TCPSocket *sock)
{
    if(sock == NULL){
        throw ("HandleTCPClient recieved NULL socket.");
    }
    // Inform
    std::cout << "Got connection from: " << sock->getForeignAddress() << "," << sock->getForeignPort() << std::endl;
    
    socket_protection.lock();
    tcp_sockets.push_back(sock);
    std::list< TCPSocket * >::iterator entry = tcp_sockets.end(); // save the entry for deletion
    socket_protection.unlock();
    
    
    while(!shutdownIndicator)
    {
        std::string msg = sock->recv(150);

        if(msg.empty())
        {
            break;
        }
        else
        {
            //if(DEBUG)
                //std::cout << msg << std::endl;

            if(msg.find("GetSafety") != std::string::npos) 
            {
                _safetyMutex.lock();
		socket_protection.lock();
                if(_safety) {
                    sock->send("(GetSafety{T})");
                } else {
                    sock->send("(GetSafety{F})");
                }
                socket_protection.unlock();
                _safetyMutex.unlock();
            }
            else if(msg.find("GetQueueSize") != std::string::npos)
            {
                std::string str = "(GetQueueSize{" + SSTR(_qQueue.size()) + "})";
		socket_protection.lock();
                sock->send(str);
		socket_protection.unlock();
            }
            else if(msg.find("StopRobot") != std::string::npos) 
            {
                _qQueue.clear(true);
            }
            else if(msg.find("GetConfig") != std::string::npos)
            {
                _qMutex.lock();
                rw::math::Q qTemp = _qCurrent;
                _qMutex.unlock();
		qTemp += _qOffset;
		qTemp *= DEGREETORAD;

#if DEBUG == true || DEBUG_SETQ == true
		std::cout << "Current config req: " << qTemp << std::endl;
#endif
                std::ostringstream qStream;
                qStream << qTemp;
                std::string qStr = "(GetConfig:" + qStream.str() + ")";
		socket_protection.lock();
                sock->send(qStr);
		socket_protection.unlock();
            }
            else if(msg.find("IsMoving") != std::string::npos)
            {
		socket_protection.lock();
                if(_qQueue.size())
                    sock->send("(IsMoving{T})");
                else
                    sock->send("(IsMoving{F})");
		socket_protection.unlock();
            }
            else if(msg.find("SetConfiguration:") != std::string::npos) 
            {
#if DEBUG == true || DEBUG_SETQ == true
		    std::cout << "Original msg: " << msg << std::endl;
#endif
                // Get Q
                std::string msg2 = msg;

                // Isolate data
                rw::math::Q q(6);
                int i = 0;
                while(msg2[i++] != '{');
                msg2 = msg2.substr(i, msg2.size()-i);
                i = 0;
                while(msg[i++] != '}');
                msg2 = msg2.substr(0, i-1);

                // Get data
                std::stringstream ss(msg2);
                double d = 0.0;
                i = 0;
                while(ss >> d)
                {
                    q[i++] = d;
                    if(ss.peek() == ',' || ss.peek() == ' ')
                        ss.ignore();
                }

                // Get speed
                rw::math::Q speed(6);

                // Isolate data
                i = 0;
                while(msg[i++] != 'V');
                msg = msg.substr(i+1, msg.size()-i-1);
                i = 0;
                while(msg[i++] != '}');
                msg = msg.substr(0, i-1);

                // Get data
                std::stringstream ss2(msg);
                d = 0.0;
                i = 0;
                while(ss2 >> d)
                {
                    speed[i++] = d;
                    if(ss2.peek() == ',' || ss2.peek() == ' ')
                        ss2.ignore();
                }

                setConfigurationCallback(q, speed);

#if DEBUG == true || DEBUG_SETQ == true
                    std::cout << q << "\n" << speed << std::endl;
#endif
            }
            else if(msg.find("ActuateValve") != std::string::npos) {
                
                bool flowforward = true;
                
                if(msg[13] == 'T'){
                    flowforward = true;
                } else if(msg[13] == 'F'){
                    flowforward = false;
                } else {
                    std::cout << "Actuate valve got:" << msg[13] << std::endl;
                }
#if DEBUG == true || DEBUG_SETQ == true
                    std::cout << "Msg: " << msg << "\nActuated valve to "; 
                    if(flowforward){
                        std::cout << "open." << std::endl;
                    } else {
                        std::cout << "grasp." << std::endl;
                    }
#endif
                
                // do stuff to send command to Kuka here...
                _safetyMutex.lock();
		_graspStatus = flowforward;
		_safetyMutex.unlock();
                
            }
            else if(msg.find("GetValve") != std::string::npos){ 
		
		std::string rply = "(GetValve{";
                _safetyMutex.lock();
		if(_digOut7){
			rply += "1,";
		} else {
			rply += "0,";
		}

		if(_digOut10){
			rply += "1})";
		} else {
			rply += "0})";
		}
		_safetyMutex.unlock();
		
		socket_protection.lock();
		sock->send(rply);
		socket_protection.unlock();
		
            }
            else if(msg.find("IsConnected") != std::string::npos){ 
		std::string rply;
		if(_connectedToRobot){
			rply = "(IsConnected{T})";
		} else {
			rply = "(IsConnected{F})";
		}
		socket_protection.lock();
		sock->send(rply);
		socket_protection.unlock();
	    } 
	    else {
#if DEBUG == true || DEBUG_SETQ == true
		    std::cout << "Error: Got message: " << msg << std::endl;
#endif
		socket_protection.lock();
                sock->send("(error)");
		socket_protection.unlock();
	    }
        }
    }

    socket_protection.lock();
    tcp_sockets.erase(entry);
    socket_protection.unlock();
    delete sock;
    std::cout << "TCP handler shutdown." << std::endl;
    
}


void TCPServer(){
	    // Create server and handle connections
    while(true){
        try
        {
            // Inform
            TCPServerSocket server(TCP_PORT_SERVER);
            std::cout << "TCP Server is running.." << std::endl;
        
            // Run forever
            while(!shutdownIndicator)
            {
                // Wait for a client to connect
                HandleTCPClient(server.accept());
            }
        
            server.closeConnection();
        }
        catch (SocketException &e)
        {
            std::cerr << "TCP Server error: " << e.what() << std::endl;
	    usleep(500000); // µs, wait half a second
        }
    }
    shutdownIndicator = true;
    std::cout << "TCP Server shut down." << std::endl;
}

// makes sure that the connection is up and running with the robot and sends the robot configuration regularly
void monitorRobotConnection(){
    rw::math::Q lastQ(6);
    while(!shutdownIndicator){
        /// Monitor the Robot Connection
        std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
        _safetyMutex.lock();
        // compare time and unlock
        std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(now - _last_recv_time);
        _safetyMutex.unlock();
        // find out if there has been no "talking" for a while
        bool currentStatus = true;
        if(time_span.count()  > RSI_DEAD_TIME){
            currentStatus = false;
        }
        if(currentStatus != _connectedToRobot){
            _connectedToRobot = currentStatus;
            std::string message = "(PushIC{";
	    // time info
 	    time_t timenow;
	    std::time(&timenow);
            if(_connectedToRobot){
                message += "T})";
                std::cout << "(Re)established connection to robot (" << std::asctime(std::localtime(&timenow)) << ")." << std::endl;    
            } else {
                message += "F})";
                std::cout << "Disconnected from robot (" << std::asctime(std::localtime(&timenow)) << ")." << std::endl;    		    
            }
            socket_protection.lock();
            for(std::list< TCPSocket* >::iterator it = tcp_sockets.begin(); it != tcp_sockets.end(); it++){
                if((*it) != NULL){
                    (*it)->send(message);
		}
            }
            socket_protection.unlock();
        }
        /// Send the config
        _qMutex.lock();
        rw::math::Q qTemp = _qCurrent;
        _qMutex.unlock();
        if(qTemp != lastQ || DEBUG){
            lastQ = qTemp;
            qTemp += _qOffset;
            qTemp *= DEGREETORAD;
            std::ostringstream qStream;
            qStream << qTemp;
            std::string qStr = "(PushRC:" + qStream.str() + ")";
            socket_protection.lock();
            for(std::list< TCPSocket* >::iterator it = tcp_sockets.begin(); it != tcp_sockets.end(); it++){
                if((*it) != NULL){
                    (*it)->send(qStr);
                }      
            }
            socket_protection.unlock();
        }
        // wait some time
        usleep(10000); // µs, ~100Hz

    }
    std::cout << "Monitor thread shut down." << std::endl;
}


int main(int argc, char** argv)
{
    shutdownIndicator = false;
    
    // set acc if provided
    if(argc > 1){
        double accUsed = std::atof(argv[1]);
        if(accUsed > JOINT_ACC_MAX){
            accUsed = JOINT_ACC_MAX;
        }
        std::cout << "Manually specified new acceleration to: " << accUsed << std::endl;
        for(size_t i = 0; i < accNormal.size(); i++){
            accNormal[i] = accUsed;
        }
    }

    // Create and run server and client threads
    boost::thread _udpThread = boost::thread(udp_communication);
    std::cout << "UDP Server+Client started.." << std::endl;
    
    boost::thread _tcpServerThread = boost::thread(TCPServer);
    
    boost::thread monitor = boost::thread(monitorRobotConnection);

    std::string input;
    while(!shutdownIndicator){
	    std::getline(std::cin, input);
	    if(input == "q"){
		    shutdownIndicator = true;
	    } 
	    
    }


    // Interrupt threads
    monitor.join();
    _udpThread.interrupt();
    _tcpServerThread.interrupt(); 

    std::cout << "All threads shut down." << std::endl;
    
    // Return
    return 0;
}
