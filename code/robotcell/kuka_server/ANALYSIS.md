# Code Analysis


This analysis was made by Lukas and I will not guarantee that this is complete nor correct.  
*USE AT OWN RISK.*


## Note
Before using the Q's from the ROS / server Interface, ensure that they are the right unit.
I, Lukas, can see that they take in Q's (from TCP) in Rad and convert them to Deg before sending to the robot.
It is however not certain that they conver the Q's from the robot to Rad before sending them to the ROS interface through TCP (maybe they do the conversion in the ROS node?).


Interface Structure
------------

In order to set or get information from the server a tcp package is sent to the server.
The server recieves and analysis what to do with it in function [HandleTCPClient(TCPSocket *sock)](../code/robotcell/kuka_server/src/kukaRSI.cpp#L344).
It here reads the message string such as 'SetConf{r1,r2,r3,r4,r5,r6}V{v1,v2,v3,v4,v5,v6}' from which it gets what to do and the data.

In case of a set commando the data is passed into queues from which the [server()](../code/robotcell/kuka_server/src/kukaRSI.cpp#L188) pulls, converts it to the wanted format and puts it in a buffer to be send.
The [client()](../code/robotcell/kuka_server/src/kukaRSI.cpp#L157) thread is the one that the pulls from the final '_sendQueue' and sends it to the Kuka.

Get commands are replied given information stored in the server program which was earlier supplied by the robot.


## Command Structure to Kuka

For more see the [RSI datasheet](../hardware/datasheet/KST_RSI_33_en.pdf), Section 7.4 Configuring an XML file for the Ethernet connection, page 43. 

#### Datastructure Overview
XML data structure to send over Ethernet
```XML
<ROOT>
  <CONFIG>
    *Configuration of the connection parameters between the sensor system and the interface*
  </CONFIG>
  <SEND>
    <ELEMENTS>
       *Configuration of the transmission structure*
    </ELEMENTS>
  </SEND>
  <RECEIVE>
    <ELEMENTS>
      *Configuration of the reception structure*
    </ELEMENTS>
  </RECEIVE>
</ROOT>
```

#### XML structure for data transmission

Signal Inputs: 
 - TAG | element name
 - TYPE | data type = BOOL / DOUBLE / LONG
 - INDX | Number of the ethernet object input = 1..64, must be numbered consecutively


## Controlling the Valves

See section 6 for more details.

| Valve | Digital Output |
|---|---|
| 1 | DO7 / DO10 |
| 2 | DO8 / DO11 |
| 3 | DO9 / DO12 |


** The valves are not short circuit proof! **

The inputs and outputs are not preconfigured and must be configured in WorkVisual, more information is in the WorkVisual documentation.


