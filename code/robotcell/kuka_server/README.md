Kuka Server
====

#### Important
If you are about to use the interface, see the [analysis / notes of the code](../code/robotcell/kuka_server/ANALYSIS.md).

#### General
This folder contains the code first supplied by Jens Cortsen to communicate directly with the Kuka.

The server will run by itself and communicate with the robot.

Due to ROS overhead and uncertainity in the scheduling, this is not a ROS node.
This is because the Kuka requires its UDP packages to be responded to strictly within its timing.
Otherwise it will shut down.
A [ROS node](../code/src/kuka_ros) was also supplied to communicate with the server.


System Overview
----

System Diagram
```
Robot                PC                 External
-------------------------------------------------
 RSI  <- UDP -> 'Kuka Server' <- TCP -> ROS Node

```

 
Build & Run Instructions
-----------
Building the project:
```sh
cd .../robotcell/kuka_server/
mkdir build
cd build/
cmake ..
make
```
Before running the server you can ensure that you have connection to the robot by pinging it.
```sh
ping 192.168.100.100
```


Running the project:
```sh
cd .../robotcell/kuka_server/bin/
nice -n -20 ./kuka_rsi
```
(nice makes the program run in high priority :slight_smile:, but can also be run without nice.)


