Safety PLC 
====

#### Important
To open and load the program to the PLC you will need a windows machine and the Flexi Soft Designer from SICK.

#### General
This folder contains the code project for the safety PLC. The PLC is from SICK and called `CPU1` and is setup with the M4000 light curtain also from sick.
The CPU is set up with a E-Stop button and a Startbutton. Furthermore it has a 3-state tower light.



System Overview
----
- SICK CPU
- SICK IO extension
- SICK M4000 Light Curtain
- start/reset button.
- E-stop 
- Tower Light

Functionality
----
The output of the system is one On/Off signal (24V). 
This signal is shared by the main plc and the Robot.

The robot is connected to the safetyplc such that is breaks when the security plc goes off.

The safety plc is currently powered through the power outlet provided by the frequency converer (also used to power the ABB plc).
The Kuka External Break is currently connected to the DI7 port on the ABB plc.

 
Programming
-----------
Download the Flexi soft designer. Open the project located on git. Connect the orange cable.

Note: Transfer means program to plc. Upload means retrieve program from plc. 

Note: Default password for the plc is *SICKSAFE*.

Instructions
-----------

- When disrupting the lightcurtain the output will switch to off.
  * The red light will indicate an error
  * To reset the lightcurtain press once on the start button. (Green Flashing  starts)
  * To restart application press start button again.
- When pressing th E-stop the output will switch to off.
  * The red light will indicate an error
  * To release the E-stop pull the red switch back. (Green Flashing starts) (Not twisting)
  * To restart application pres start button.


Mapings on the IO
------------
The mapings of the connection bus is as follows, from left (1) to right (10).

| IO port | Connection | 
|----|----| 
| 1 | GND | 
| 2 | VCC | 
| 3 | E-Stop | 
| 4 | Light Curtain & Start | 
| 5 | Light Curtain | 
| 6 | Light Curtain | 
| 7 | Kuka External Break | 
| 8 | Tower Light | 
| 9 | Tower Light | 
| 10 | Tower Light | 


Readings / Downloads
-----------
[Flexi Soft Designer](https://www.sick.com/dk/da/flexi-soft-designer/p/p81369)

[SICK M4000](https://www.sick.com/medias/M4000-Standard-Operating-Instructions.pdf?context=bWFzdGVyfHJvb3R8MzA3OTQ0MHxhcHBsaWNhdGlvbi9wZGZ8aDU2L2g1My84OTE1MTI0MTI1NzI2LnBkZnw3YTVkYzRhNTI2MWY3MGE0Mzg4ZDE4ZTUyZDFiOGIzMDY4ODAwYjYwYjIzMjZkYWFmNGQyYzU4M2U4ODFlYTVi)


