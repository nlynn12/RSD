#!/usr/bin/python
#Drone MES Client

import socket
import random
import time

IDLE = 0
REQUEST = 1
SUSPEND = 2

#2 amd 3 only
robotNumber = 3




#robot command to get from server
commandListServer_robot = ["No_task", "Wait_for_new_task", "Wait_for_task_accept", "Wait_for_bricks", "Sort_bricks", "Move_bricks_to_deliver", "Wait_for_mobile", "Deliver_to_mobile"]

#robot commands to send to server
commandListClient_robot = ["Idle", "Suspended", "Request_task", "Waiting_for_new_task", "Accept", "Reject", "Waiting_for_task_accept_mobile", "Waiting_for_task_accept_mobile_1", "Waiting_for_task_accept_mobile_2",  "Waiting_for_bricks", "Sorting_bricks", "Moving_to_delivering_position", "Waiting_for_mobile", "Moving_to_mobile", "Complete"]  


#Log info
def command(command ,id, comment):  
    str = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
    str += "<log_entry>"
    str += "<event>%s</event>" % commandListClient_robot[command]
    str += "<time>%s</time>" % time.ctime() 
    str += "<cell_id>%s</cell_id>" % id
    str += "<comment>%s</comment>" % comment
    str += "</log_entry>"
    return str

#TCP/IP client
def sendStatus(string):
    HOST = socket.gethostbyname('mwlinux.projectnet.wlan.sdu.dk')
    PORT = 21212
    # SOCK_STREAM == a TCP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #sock.setblocking(0)  # optional non-blocking
    sock.connect((HOST, PORT))

    print "sending data => %s" % (string)
    sock.send(string)
    reply = sock.recv(16384)  # limit reply to 16K
    print "reply => \n [%s]" % (reply)
    sock.close()
    return reply


def processReply(string):
    start = string.find("<event>")+7
    end = string.find("</event>")
    return string[start:end]

def findState(string):
    state = commandListServer_robot.index(string)
    return state
    
def main():
    testcounter = 0
    nxt_state = REQUEST

    #Startup and go to idle
    str = command(commandListClient_robot.index("Idle"),robotNumber,"Idle")   
    reply = sendStatus(str)
    print processReply(reply)
    state = findState(processReply(reply))
    
    while(1):
        if(state == commandListServer_robot.index("No_task")):
            #Idle, Request new task or suspend
            if(nxt_state == IDLE):
                str = command(commandListClient_robot.index("Idle"),robotNumber,"Idle")
            elif(nxt_state == REQUEST):
                str = command(commandListClient_robot.index("Request_task"),robotNumber,"Request new task")
            elif(nxt_state == SUSPEND):
                str = command(commandListClient_robot.index("Suspended"),robotNumber,"Suspend robot")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))

        elif(state == commandListServer_robot.index("Wait_for_new_task")):
            #Waiting for mobile to take task
            str = command(commandListClient_robot.index("Waiting_for_new_task"),robotNumber,"Waiting for Task")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))
            
        elif(state == commandListServer_robot.index("Wait_for_task_accept")):
            #Waiting for mobile to accept task
            str = command(commandListClient_robot.index("Waiting_for_task_accept_mobile"),robotNumber,"Waiting for mobile to accept task")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))

            
        elif(state == commandListServer_robot.index("Wait_for_bricks")):
            #Wait for bricks to arrive
            str = command(commandListClient_robot.index("Waiting_for_bricks"),robotNumber,"Preparing for bricks")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))

        elif(state == commandListServer_robot.index("Sort_bricks")):
            #Sort the bricks
            str = command(commandListClient_robot.index("Sorting_bricks"),robotNumber,"Sorting the bricks")
            reply = sendStatus(str)
            testcounter = testcounter + 1
            print processReply(reply)
            state = findState(processReply(reply))

            # Simulating until sorting is complete
            if( testcounter == 8):
                testcounter = 0
                str = command(commandListClient_robot.index("Moving_to_delivering_position"),robotNumber,"Moving bricks to delivery position")
                reply = sendStatus(str)
                print processReply(reply)
                state = findState(processReply(reply))

        elif(state == commandListServer_robot.index("Move_bricks_to_deliver")):
            # Move bricks to delivery postion
            str = command(commandListClient_robot.index("Moving_to_delivering_position"),robotNumber,"Moving bricks to delivery position")
            reply = sendStatus(str)
            testcounter = testcounter + 1
            print processReply(reply)
            state = findState(processReply(reply))

            # Simulating until moving is complete
            if( testcounter == 3):
                testcounter = 0
                str = command(commandListClient_robot.index("Waiting_for_mobile"),robotNumber,"Wait until a mobile arrives")
                reply = sendStatus(str)
                print processReply(reply)
                state = findState(processReply(reply))

        elif(state == commandListServer_robot.index("Wait_for_mobile")):
            # Wait for mobile to arrive
            str = command(commandListClient_robot.index("Waiting_for_mobile"),robotNumber,"Wait until a mobile arrives")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))

        elif(state == commandListServer_robot.index("Deliver_to_mobile")):
            # Move bricks to mobile
            str = command(commandListClient_robot.index("Moving_to_mobile"),robotNumber,"Moving bricks to mobile")
            reply = sendStatus(str)
            testcounter = testcounter + 1
            print processReply(reply)
            state = findState(processReply(reply))

            # Simulating until moving is complete
            if( testcounter == 3):
                testcounter = 0
                str = command(commandListClient_robot.index("Complete"),robotNumber,"Task complete")
                #nxt_state = SUSPEND
                reply = sendStatus(str)
                print processReply(reply)
                state = findState(processReply(reply))
            

        print "state %d " % state
        time.sleep(3)

if __name__ == "__main__":
    main()
