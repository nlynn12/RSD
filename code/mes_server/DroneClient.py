#!/usr/bin/python
#Drone MES Client

import socket
import random
import time

IDLE = 0
REQUEST = 1
SUSPEND = 2

#4 amd 5 only
droneNumber = 4

#drone commands to get from server
commandListServer_drone = ["No_task", "New_task", "Deliver_bricks"]

#drone commands to send to server
commandListClient_drone = ["Idle", "Suspended","Request_task", "Accept", "Reject",  "Delivering_bricks", "Complete"]

#Log info
def command(command ,id, comment):  
    str = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
    str += "<log_entry>"
    str += "<event>%s</event>" % commandListClient_drone[command]
    str += "<time>%s</time>" % time.ctime() 
    str += "<cell_id>%s</cell_id>" % id
    str += "<comment>%s</comment>" % comment
    str += "</log_entry>"
    return str

#TCP/IP client
def sendStatus(string):
    #HOST = socket.gethostbyname('localdk')
    #HOST = "10.126.128.12"
    HOST = socket.gethostbyname('robolab01.projectnet.wlan.sdu.dk')
    PORT = 21212
    # SOCK_STREAM == a TCP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #sock.setblocking(0)  # optional non-blocking
    sock.connect((HOST, PORT))

    print "sending data => %s" % (string)
    sock.send(string)
    reply = sock.recv(16384)  # limit reply to 16K
    print "reply => \n [%s]" % (reply)
    sock.close()
    return reply


def processReply(string):
    start = string.find("<event>")+7
    end = string.find("</event>")
    return string[start:end]

def findState(string):
    state = commandListServer_drone.index(string)
    return state
    
def main():
    # State is controlled by server
    testcounter = 0
    nxt_state = REQUEST
    accept_new_tasks = True

    #Startup and go to idle
    str = command(commandListClient_drone.index("Idle"),droneNumber,"Waiting for Task")
    reply = sendStatus(str)
    print processReply(reply)
    state = findState(processReply(reply))

    
    while(1):
        if(state == commandListServer_drone.index("No_task")):
            #idle, Request new task or suspend
            if(nxt_state == IDLE):
                str = command(commandListClient_drone.index("Idle"),droneNumber,"Idle")
            elif(nxt_state == REQUEST):
                str = command(commandListClient_drone.index("Request_task"),droneNumber,"Request a new task")
            elif(nxt_state == SUSPEND):
                str = command(commandListClient_drone.index("Suspended"),droneNumber,"Suspend mobile")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply))

        elif(state == commandListServer_drone.index("New_task")):
            #Accept or reject new tak
            if(accept_new_tasks == True):
                str = command(commandListClient_drone.index("Accept"),droneNumber,"Accept new task")
            elif(accept_new_tasks == False):
                str = command(commandListClient_drone.index("Reject"),droneNumber,"Reject new task")
            reply = sendStatus(str)
            print processReply(reply)
            state = findState(processReply(reply)) 
        
            
        elif(state == commandListServer_drone.index("Deliver_bricks")):
            #Execute
            str = command(commandListClient_drone.index("Delivering_bricks"),droneNumber,"Hypersonic, executing task")
            reply = sendStatus(str)
            testcounter = testcounter + 1
            print processReply(reply)

            # Simulating until order is complete
            if( testcounter == 1):
                #complete and return to idle
                testcounter = 0
                str = command(commandListClient_drone.index("Complete"),droneNumber,"Task Complete")
                reply = sendStatus(str)
                print processReply(reply)
                state = findState(processReply(reply))   
            

        print "state %d " % state
        time.sleep(3)

if __name__ == "__main__":
    main()
