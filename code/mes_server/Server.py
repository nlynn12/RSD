#!/usr/bin/python


# NOTE!
# TO_DO, change which drone that serves the mobile robot. Right now it is just the drone which request a task first, after mobile arrival.

import SocketServer
import socket
import subprocess
import sys
from threading import Thread, Lock
import time
import random
from random import randint

#HOST = socket.gethostbyname('localhost')#('robolab01.projectnet.wlan.sdu.dk')
HOST = '10.126.128.30'
PORT = 21212

#mobile commands to get from server
commandListServer_mobile = ["No_task",                           # Tell MR there is no task (in Idle and Suspended mode).
			    "New_task_del",                      # Tell MR there is a task to deliver LEGOs to a Robot Cell.
			    "New_task_recv",                     # Tell MR there is a task to receive LEGOs from a Robot Cell.
			    "Wait_for_free_feeder",              # Tell MR to wait until the 
			    "Get_bricks",                        # Tell MR to move to dispenser and receive bricks.
			    "Wait_for_accept_drone",             # Tell MR to wait for the drone's acknowledgment that bricks have been placed on MR.
			    "Move_to_cell_1_del",                # Tell MR to move to cell 1 and await instruction.
			    "Move_to_cell_2_del",                # Tell MR to move to cell 2 and await instruction.
			    "Empty_bricks",                      # Tell MR to deliver payload.
			    "Move_home",                         # Tell MR to move home.
			    "Move_to_cell_1_recv",               # Tell MR to move to cell 1, specifically to where bricks can be received.
			    "Move_to_cell_2_recv",               # Tell MR to move to cell 2, specifically to where bricks can be received.
			    "Wait_for_accept_robot"]             # Tell MR to stay in robot cell until robot has placed bricks on MR.
                                                                 
#mobile commands to send to server                               
commandListClient_mobile = ["Idle",                              #
			    "Suspended",                         #
			    "Request_task",                      #
			    "Accept_del",                        #
			    "Reject_del",                        #
			    "Accept_recv",                       #
			    "Reject_recv",                       #
			    "Waiting_for_free_feeder",           #
			    "Moving_to_feeder",                  #
			    "Waiting_for_accept_drone",          #
			    "Waiting_for_accept_drone_1",        #
			    "Waiting_for_accept_drone_2",        #
			    "Moving_to_cell_1_del",              #
			    "Moving_to_cell_2_del",              #
			    "Emptying_bricks",                   #
			    "Bricks_empty",                      #
			    "Moving_home",                       #
			    "Complete",                          #
			    "Moving_to_cell_1_recv",             #
			    "Moving_to_cell_2_recv",             #
			    "Waiting_for_accept_robot"]          #
                                                                 
#mobile order types                                              
mobileOrderTypes = ["Non",                                       #
		    "Deliver_cell_1",                            #
		    "Deliver_cell_2",                            #
		    "Receive_cell_1",                            #
		    "Receive_cell_2"]                            #
                                                                 
#drone commands to get from server                               
commandListServer_drone = ["No_task",                            #
			   "New_task",                           #
			   "Deliver_bricks"]                     #
                                                                 
#drone commands to send to server                                
commandListClient_drone = ["Idle",                               #
			   "Suspended",                          #
			   "Request_task",                       #
			   "Accept",                             #
			   "Reject",                             #
			   "Delivering_bricks",                  #
			   "Complete"]                           #
                                                                 
#robot command to get from server                                
commandListServer_robot = ["No_task",                            #
			   "Wait_for_new_task",                  #
			   "Wait_for_task_accept",               #
			   "Wait_for_bricks",                    #
			   "Sort_bricks",                        #
			   "Move_bricks_to_deliver",             #
			   "Wait_for_mobile",                    #
			   "Deliver_to_mobile"]                  #

#robot commands to send to server
commandListClient_robot = ["Idle",                               #
			   "Suspended",                          #
			   "Request_task",                       #
			   "Waiting_for_new_task",               #
			   "Waiting_for_task_accept_mobile",     #
			   "Waiting_for_task_accept_mobile_1",   #
			   "Waiting_for_task_accept_mobile_2",   #
			   "Waiting_for_bricks",                 #
			   "Sorting_bricks",                     #
			   "Moving_to_delivering_position",      #
			   "Waiting_for_mobile",                 #
			   "Moving_to_mobile",                   #
			   "Complete"]                           #

random.seed()


#all clients is Suspended intil they sent a idle command
MOBILE_1 = 0
MOBILE_2 = 1
ROBOT_1 = 2
ROBOT_2 = 3
DRONE_1 = 4
DRONE_2 = 5
#0-1 = mobile, 2-3 = robot, 4-5 drone
States = [1,1,1,1,1,1]

# 0 = mobile robot 1, 1 = mobile robot 2
mobileStates = [0,0]

# Is the feeding platform free
feeder_free = True

state_mutex = Lock()

############################################################################


# Order is a count of LEGO bricks.
# to generate a new order of 1 red, 2 yellow and 3 blue:
# myorder = Order(1,2,3)
# or
# myorder = Order()
# myorder.red = 1
# myorder.yellow = 2
# myorder.blue = 3
class Order:
    red = 0
    yellow = 0
    blue = 0
    def __init__(self, r, y, b):
      self.red=r
      self.yellow=y
      self.blue=b

# TODO: Generate actually meaningful order.
#       ... If you could call this meaningful...
def generateOrder():
    r = 0
    y = 0
    b = 0
    for x in range(0, 5):
	i = randint(0,2)	
	if(i == 0):
		r += 1
	if(i == 1):
	   	y += 1
    	if(i == 2):
	   	b += 1
	
    test_order = Order(r,y,b)
    return test_order

def reverse_string(my_str):
    if my_str:
        return my_str[::-1]

# command is a request for a client to change state.
# Usage:
# cmd = command(command=CMD_IDX,
# 	       comment="My comment",
# 	       commandList=CMD_LIST_SEE_TOP_OF_FILE,
# 	       order=None)
def command(command,comment, commandList, order=None):
    if order is None:
        order = Order(-1,-1,-1)
    str = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
    str += "<log_entry>"
    str += "<event>%s</event>" % commandList[command]
    str += "<time>%s</time>" % time.ctime()
    str += "<order>"
    str += "<brick type=\"red\">%s</brick>"  %order.red
    str += "<brick type=\"yellow\">%s</brick>" %order.yellow
    str += "<brick type=\"blue\">%s</brick>" %order.blue
    str += "</order>"
    str += "<comment>%s</comment>" % comment
    str += "</log_entry>"
    return str

def findState_mobile(string):
    start = string.find("<event>")+7
    end = string.find("</event>")
    state = commandListClient_mobile.index(string[start:end])
    print state
    return state

def findState_drone(string):
    start = string.find("<event>")+7
    end = string.find("</event>")
    state = commandListClient_drone.index(string[start:end])
    print state
    return state

def findState_robot(string):
    start = string.find("<event>")+7
    end = string.find("</event>")
    state = commandListClient_robot.index(string[start:end])
    print state
    return state




def findID(string):
    start = string.find("<cell_id>")+9
    end = string.find("</cell_id>")
    return int(string[start:end])

def isRobotAvailable():
    robotlist = []
    if(States[ROBOT_1] == commandListClient_robot.index("Waiting_for_new_task")):
        robotlist.append(ROBOT_1)
    if(States[ROBOT_2] == commandListClient_robot.index("Waiting_for_new_task")):
        robotlist.append(ROBOT_2)
    if(len(robotlist) == 1):
        return robotlist[0]
    elif(len(robotlist) == 2):
        n = random.randrange(0, 1, 1)
        return robotlist[n]
    else:
        return -1

def isRobotReq(id):
    robotlist = []
    if(id == MOBILE_1):
        if(States[ROBOT_1] == commandListClient_robot.index("Waiting_for_mobile") and mobileStates[MOBILE_2] != mobileOrderTypes.index("Receive_cell_1")):
            robotlist.append(ROBOT_1)
        if(States[ROBOT_2] == commandListClient_robot.index("Waiting_for_mobile") and mobileStates[MOBILE_2] != mobileOrderTypes.index("Receive_cell_2")):
            robotlist.append(ROBOT_2)

    elif(id == MOBILE_2):
        if(States[ROBOT_1] == commandListClient_robot.index("Waiting_for_mobile") and mobileStates[MOBILE_1] != mobileOrderTypes.index("Receive_cell_1")):
            robotlist.append(ROBOT_1)
        if(States[ROBOT_2] == commandListClient_robot.index("Waiting_for_mobile") and mobileStates[MOBILE_1] != mobileOrderTypes.index("Receive_cell_2")):
            robotlist.append(ROBOT_2)
    
    if(len(robotlist) == 1):
        return robotlist[0]
    elif(len(robotlist) == 2):
        n = random.randrange(0, 1, 1)
        return robotlist[n]
    else:
        return -1



def isMobileAvailable_drone():
    robotlist = []

    if(States[MOBILE_1] == commandListClient_mobile.index("Waiting_for_accept_drone")):
        robotlist.append(MOBILE_1)
    if(States[MOBILE_2] == commandListClient_mobile.index("Waiting_for_accept_drone")):
        robotlist.append(MOBILE_2)
    if(len(robotlist) == 1):
        return robotlist[0]
    elif(len(robotlist) == 2):
        n = random.randrange(0, 1, 1)
        return robotlist[n]
    else:
        return -1



def reserveRobot():
    reservelist = []
    if(States[ROBOT_1] == commandListClient_robot.index("Waiting_for_new_task")):
        reservelist.append(ROBOT_1)
    if(States[ROBOT_2] == commandListClient_robot.index("Waiting_for_new_task")):
        reservelist.append(ROBOT_2)
    if(len(reservelist) == 1):
        return reservelist[0]
    elif(len(reservelist) == 2):
        n = random.randint(0, 1)
        return reservelist[n]
    else:
        return -1

    

def processMobile(id,data):
    global feeder_free
    index = findState_mobile(data)
    print "mobile index %(number)d %(string)s " % { "number": index, "string": commandListClient_mobile[index]}
    stat_robot = isRobotAvailable()
    stat_robot_2 = isRobotReq(id)
    #print "mobile stat_robot %d" % stat_robot

    # Get order from server
    if(States[id] < 2):
        States[id] = index
        if(States[id] == commandListClient_mobile.index("Suspended")):
            return command(commandListServer_mobile.index("No_task"), "Just wait", commandListServer_mobile)
        elif(States[id] == commandListClient_mobile.index("Idle")):
            return command(commandListServer_mobile.index("No_task"), "Just wait", commandListServer_mobile)
        elif(States[id] == commandListClient_mobile.index("Request_task")):
            
            # Check if any free robot cell and reserve cell - Deliver part
            if(stat_robot >= ROBOT_1):
                free_cell = reserveRobot()
                if(id == MOBILE_1):
                    States[free_cell] = commandListClient_robot.index("Waiting_for_task_accept_mobile_1")
                    return command(commandListServer_mobile.index("New_task_del"), "New task robot cell: " + str(free_cell-ROBOT_1+1), commandListServer_mobile)
                elif(id == MOBILE_2):
                    States[free_cell] = commandListClient_robot.index("Waiting_for_task_accept_mobile_2")
                    return command(commandListServer_mobile.index("New_task_del"), "New task robot cell: " + str(free_cell-ROBOT_1+1), commandListServer_mobile)

                
            # Check if any robot is requesting pickup    
            elif(stat_robot_2 >= ROBOT_1):
                if(stat_robot_2 == ROBOT_1):
                    mobileStates[id] = mobileOrderTypes.index("Receive_cell_1")
                    return command(commandListServer_mobile.index("New_task_recv"), "New task robot cell: " + str(stat_robot_2-ROBOT_1+1), commandListServer_mobile)
                elif(stat_robot_2 == ROBOT_2):
                    mobileStates[id] = mobileOrderTypes.index("Receive_cell_2")
                    return command(commandListServer_mobile.index("New_task_recv"), "New task robot cell: " + str(stat_robot_2-ROBOT_1+1), commandListServer_mobile)
            else:
                States[id] = commandListClient_mobile.index("Idle")
                return command(commandListServer_mobile.index("No_task"), "Just wait", commandListServer_mobile)    
            

    # Continue with order    
    else:
        if(States[id] == commandListClient_mobile.index("Request_task")):
            if(index == commandListClient_mobile.index("Accept_del")):
                if(States[ROBOT_1] == commandListClient_robot.index("Waiting_for_task_accept_mobile_1") and id == MOBILE_1):
                    mobileStates[id] = mobileOrderTypes.index("Deliver_cell_1")
                    States[ROBOT_1] = commandListClient_robot.index("Waiting_for_bricks")
                elif(States[ROBOT_2] == commandListClient_robot.index("Waiting_for_task_accept_mobile_1") and id == MOBILE_1):
                    mobileStates[id] = mobileOrderTypes.index("Deliver_cell_2")
                    States[ROBOT_2] = commandListClient_robot.index("Waiting_for_bricks")
                elif(States[ROBOT_1] == commandListClient_robot.index("Waiting_for_task_accept_mobile_2") and id == MOBILE_2):
                    mobileStates[id] = mobileOrderTypes.index("Deliver_cell_1")
                    States[ROBOT_1] = commandListClient_robot.index("Waiting_for_bricks")
                elif(States[ROBOT_2] == commandListClient_robot.index("Waiting_for_task_accept_mobile_2") and id == MOBILE_2):
                    mobileStates[id] = mobileOrderTypes.index("Deliver_cell_2")
                    States[ROBOT_2] = commandListClient_robot.index("Waiting_for_bricks")

                #check if feeder is free
                if(feeder_free == True):
                    feeder_free = False
                    States[id] = commandListClient_mobile.index("Moving_to_feeder")
                    return command(commandListServer_mobile.index("Get_bricks"), "Move to feeder", commandListServer_mobile)
                else:
                    States[id] = commandListClient_mobile.index("Waiting_for_free_feeder")
                    return command(commandListServer_mobile.index("Wait_for_free_feeder"), "Wait until feeder is free", commandListServer_mobile)
                
            elif(index == commandListClient_mobile.index("Reject_del")):
                if(States[ROBOT_1] == commandListClient_robot.index("Waiting_for_task_accept_mobile_1") and id == MOBILE_1):
                    States[ROBOT_1] = commandListClient_robot.index("Waiting_for_new_task")
                elif(States[ROBOT_2] == commandListClient_robot.index("Waiting_for_task_accept_mobile_1") and id == MOBILE_1):
                    States[ROBOT_2] = commandListClient_robot.index("Waiting_for_new_task")
                elif(States[ROBOT_1] == commandListClient_robot.index("Waiting_for_task_accept_mobile_2") and id == MOBILE_2):
                    States[ROBOT_1] = commandListClient_robot.index("Waiting_for_new_task")
                elif(States[ROBOT_2] == commandListClient_robot.index("Waiting_for_task_accept_mobile_2") and id == MOBILE_2):
                    States[ROBOT_2] = commandListClient_robot.index("Waiting_for_new_task")
                States[id] = commandListClient_mobile.index("Idle")
                return command(commandListServer_mobile.index("No_task"), "Just wait", commandListServer_mobile)


            elif(index == commandListClient_mobile.index("Accept_recv") and mobileStates[id] == mobileOrderTypes.index("Receive_cell_1")):
                States[id] = commandListClient_mobile.index("Moving_to_cell_1_recv")
                return command(commandListServer_mobile.index("Move_to_cell_1_recv"), "Receive bricks from cell 1", commandListServer_mobile)

            elif(index == commandListClient_mobile.index("Accept_recv") and mobileStates[id] == mobileOrderTypes.index("Receive_cell_2")):
                States[id] = commandListClient_mobile.index("Moving_to_cell_2_recv")
                return command(commandListServer_mobile.index("Move_to_cell_2_recv"), "Receive bricks from cell 2", commandListServer_mobile)

            elif(index == commandListClient_mobile.index("Reject_recv") and mobileStates[id] == mobileOrderTypes.index("Receive_cell_1")):
                States[id] = commandListClient_mobile.index("Idle")
                mobileStates[id] = mobileOrderTypes.index("Non")
                return command(commandListServer_mobile.index("No_task"), "Just wait", commandListServer_mobile)

            elif(index == commandListClient_mobile.index("Reject_recv") and mobileStates[id] == mobileOrderTypes.index("Receive_cell_2")):
                States[id] = commandListClient_mobile.index("Idle")
                mobileStates[id] = mobileOrderTypes.index("Non")
                return command(commandListServer_mobile.index("No_task"), "Just wait", commandListServer_mobile)



        
        # Check if feeder has become free
        elif(States[id] == commandListClient_mobile.index("Waiting_for_free_feeder")):
            if(feeder_free == True):
                feeder_free = False
                States[id] = commandListClient_mobile.index("Moving_to_feeder")
                return command(commandListServer_mobile.index("Get_bricks"), "Move to feeder", commandListServer_mobile)
            else:
                return command(commandListServer_mobile.index("Wait_for_free_feeder"), "Wait until feeder is free", commandListServer_mobile)   

        # Move to feeder
        elif(States[id] == commandListClient_mobile.index("Moving_to_feeder")):
            if(index == commandListClient_mobile.index("Waiting_for_accept_drone")):
                States[id] = index
                return command(commandListServer_mobile.index("Wait_for_accept_drone"), "Just wait", commandListServer_mobile)
            else:
                return command(commandListServer_mobile.index("Get_bricks"), "Move to feeder", commandListServer_mobile)

        # Wait for drone to update state    
        elif(States[id] == commandListClient_mobile.index("Waiting_for_accept_drone") or States[id] == commandListClient_mobile.index("Waiting_for_accept_drone_1") or States[id] == commandListClient_mobile.index("Waiting_for_accept_drone_2")):
            return command(commandListServer_mobile.index("Wait_for_accept_drone"), "Just wait", commandListServer_mobile)
            

        # When arriving -> wait_until_empty
        elif(States[id] == commandListClient_mobile.index("Moving_to_cell_1_del") or States[id] == commandListClient_mobile.index("Moving_to_cell_2_del")):
            if(index == commandListClient_mobile.index("Emptying_bricks")):
                 States[id] = commandListClient_mobile.index("Emptying_bricks")
                 return command(commandListServer_mobile.index("Empty_bricks"), "Empty bricks", commandListServer_mobile)
            elif(States[id] == commandListClient_mobile.index("Moving_to_cell_1_del")):
               return command(commandListServer_mobile.index("Move_to_cell_1_del"), "Speeding", commandListServer_mobile)
            elif(States[id] == commandListClient_mobile.index("Moving_to_cell_2_del")):
                return command(commandListServer_mobile.index("Move_to_cell_2_del"), "Speeding", commandListServer_mobile)

        # When arriving -> wait until full
        elif(States[id] == commandListClient_mobile.index("Moving_to_cell_1_recv") or States[id] == commandListClient_mobile.index("Moving_to_cell_2_recv")):
            if(index == commandListClient_mobile.index("Waiting_for_accept_robot")):
                 States[id] = commandListClient_mobile.index("Waiting_for_accept_robot")
                 updateRobot_mobile(id)
                 return command(commandListServer_mobile.index("Wait_for_accept_robot"), "Wait for robot to deliver bricks", commandListServer_mobile)
            elif(States[id] == commandListClient_mobile.index("Moving_to_cell_1_recv")):
               return command(commandListServer_mobile.index("Move_to_cell_1_recv"), "Speeding", commandListServer_mobile)
            elif(States[id] == commandListClient_mobile.index("Moving_to_cell_2_recv")):
                return command(commandListServer_mobile.index("Move_to_cell_2_recv"), "Speeding", commandListServer_mobile)

        # Wait until robot has delivered
        elif(States[id] == commandListClient_mobile.index("Waiting_for_accept_robot")):
            return command(commandListServer_mobile.index("Wait_for_accept_robot"), "Wait for robot to deliver bricks", commandListServer_mobile)


        # When empty, start robot and move home
        elif(States[id] == commandListClient_mobile.index("Emptying_bricks")):
            if(index == commandListClient_mobile.index("Emptying_bricks")):
                return command(commandListServer_mobile.index("Empty_bricks"), "Empty bricks", commandListServer_mobile)
            elif(index == commandListClient_mobile.index("Bricks_empty")):
                 if(mobileStates[id] == mobileOrderTypes.index("Deliver_cell_1")):
                     States[ROBOT_1] = commandListClient_robot.index("Sorting_bricks")
                 elif(mobileStates[id] == mobileOrderTypes.index("Deliver_cell_2")):
                     States[ROBOT_2] = commandListClient_robot.index("Sorting_bricks")
                 
                 States[id] = commandListClient_mobile.index("Moving_home")
                 return command(commandListServer_mobile.index("Move_home"), "Move to home position", commandListServer_mobile)

        # Moving Home
        elif(States[id] == commandListClient_mobile.index("Moving_home")):
            if(index == commandListClient_mobile.index("Complete")):
                States[id] = commandListClient_mobile.index("Idle")
                mobileStates[id] = mobileOrderTypes.index("Non")
                return command(commandListServer_mobile.index("No_task"), "Just wait", commandListServer_mobile)
            else:
                return command(commandListServer_mobile.index("Move_home"), "Move to home position", commandListServer_mobile)

                                
                
                
                

def updateMobile_drone_accept():
    global feeder_free
    if(States[MOBILE_1] == commandListClient_mobile.index("Waiting_for_accept_drone_1") or States[MOBILE_1] == commandListClient_mobile.index("Waiting_for_accept_drone_2")):
        if(mobileStates[MOBILE_1] == mobileOrderTypes.index("Deliver_cell_1")):   
            States[MOBILE_1] = commandListClient_mobile.index("Moving_to_cell_1_del");
        elif(mobileStates[MOBILE_1] == mobileOrderTypes.index("Deliver_cell_2")):   
            States[MOBILE_1] = commandListClient_mobile.index("Moving_to_cell_2_del");
               
    if(States[MOBILE_2] == commandListClient_mobile.index("Waiting_for_accept_drone_1") or States[MOBILE_2] == commandListClient_mobile.index("Waiting_for_accept_drone_2")):
        if(mobileStates[MOBILE_2] == mobileOrderTypes.index("Deliver_cell_1")):   
            States[MOBILE_2] = commandListClient_mobile.index("Moving_to_cell_1_del");
        elif(mobileStates[MOBILE_2] == mobileOrderTypes.index("Deliver_cell_2")):   
            States[MOBILE_2] = commandListClient_mobile.index("Moving_to_cell_2_del");
    feeder_free = True

def updateMobile_drone_reject():
    global feeder_free
    if(States[MOBILE_1] == commandListClient_mobile.index("Waiting_for_accept_drone_1") or States[MOBILE_1] == commandListClient_mobile.index("Waiting_for_accept_drone_2")):
        States[MOBILE_1] = commandListClient_mobile.index("Waiting_for_accept_drone")    
               
    if(States[MOBILE_2] == commandListClient_mobile.index("Waiting_for_accept_drone_1") or States[MOBILE_2] == commandListClient_mobile.index("Waiting_for_accept_drone_1")):
        States[MOBILE_2] = commandListClient_mobile.index("Waiting_for_accept_drone")         

def updateMobile_robot(id):
    if(States[MOBILE_1] == commandListClient_mobile.index("Waiting_for_accept_robot")):
        if(mobileStates[MOBILE_1] == mobileOrderTypes.index("Receive_cell_1") and id == ROBOT_1):
            States[MOBILE_1] = commandListClient_mobile.index("Moving_home")
        elif(mobileStates[MOBILE_1] == mobileOrderTypes.index("Receive_cell_2") and id == ROBOT_2):
            States[MOBILE_1] = commandListClient_mobile.index("Moving_home")
            
    elif(States[MOBILE_2] == commandListClient_mobile.index("Waiting_for_accept_robot")):
        if(mobileStates[MOBILE_2] == mobileOrderTypes.index("Receive_cell_1") and id == ROBOT_1):
            States[MOBILE_2] = commandListClient_mobile.index("Moving_home")
        elif(mobileStates[MOBILE_2] == mobileOrderTypes.index("Receive_cell_2") and id == ROBOT_2):
            States[MOBILE_2] = commandListClient_mobile.index("Moving_home")

def updateRobot_mobile(id):
    if(States[ROBOT_1] == commandListClient_robot.index("Waiting_for_mobile") and mobileStates[id] == mobileOrderTypes.index("Receive_cell_1")):
        States[ROBOT_1] = commandListClient_robot.index("Moving_to_mobile")

    elif(States[ROBOT_2] == commandListClient_robot.index("Waiting_for_mobile") and mobileStates[id] == mobileOrderTypes.index("Receive_cell_2")):
        States[ROBOT_2] = commandListClient_robot.index("Moving_to_mobile")

    

def processRobot(id,data):
    #print "processRobot"
    index = findState_robot(data)
    print "robot index %(number)d %(string)s " % { "number": index, "string": commandListClient_robot[index]}

    # Check if any orders
    if(States[id] < 2):      
        if(index == commandListClient_robot.index("Suspended")):
            States[id] = index
            return command(commandListServer_robot.index("No_task"), "Just wait", commandListServer_robot)
        elif(index == commandListClient_robot.index("Idle")):
            States[id] = index
            return command(commandListServer_robot.index("No_task"), "Just wait", commandListServer_robot)
        elif(index == commandListClient_robot.index("Request_task")):
            States[id] = commandListClient_robot.index("Waiting_for_new_task")
            return command(commandListServer_robot.index("Wait_for_new_task"), "Just wait", commandListServer_robot)
 

    # continue with order
    else:
        # Wait until a mobile is ready
        if(States[id] == commandListClient_robot.index("Waiting_for_new_task")):
            return command(commandListServer_robot.index("Wait_for_new_task"), "Just wait", commandListServer_robot)
        
        # Wait for mobile to accept task
        elif(States[id] == commandListClient_robot.index("Waiting_for_task_accept_mobile_1") or States[id] == commandListClient_robot.index("Waiting_for_task_accept_mobile_2")):
            return command(command     = commandListServer_robot.index("Wait_for_task_accept"),
                           comment     = "There is an order in this command.",
                           commandList = commandListServer_robot,
                           order       = generateOrder()
                           )
        
        # Just keep waiting, until arrival
        elif(States[id] == commandListClient_robot.index("Waiting_for_bricks")):
            return command(commandListServer_robot.index("Wait_for_bricks"), "Prepare for incoming bricks", commandListServer_robot)
        
        # Sort the bricks
        elif(States[id] == commandListClient_robot.index("Sorting_bricks")):
            if(index == commandListClient_robot.index("Waiting_for_bricks")):
                return command(commandListServer_robot.index("Sort_bricks"), "Sort the bricks", commandListServer_robot) 
            elif(index == commandListClient_robot.index("Sorting_bricks")):
                return command(commandListServer_robot.index("Sort_bricks"), "Sort the bricks", commandListServer_robot)
            elif(index == commandListClient_robot.index("Moving_to_delivering_position")):
                States[id] = commandListClient_robot.index("Moving_to_delivering_position")
                return command(commandListServer_robot.index("Move_bricks_to_deliver"), "Move the bricks", commandListServer_robot)
            
        # Move to delivering position
        elif(States[id] == commandListClient_robot.index("Moving_to_delivering_position")):
            if(index == commandListClient_robot.index("Moving_to_delivering_position")):
                return command(commandListServer_robot.index("Move_bricks_to_deliver"), "Move the bricks", commandListServer_robot)
            elif(index == commandListClient_robot.index("Waiting_for_mobile")):
                States[id] = commandListClient_robot.index("Waiting_for_mobile")
                return command(commandListServer_robot.index("Wait_for_mobile"), "Wait for mobile to arrive", commandListServer_robot)
            
        # Wait for mobile to arrive
        elif(States[id] == commandListClient_robot.index("Waiting_for_mobile")):
            return command(commandListServer_robot.index("Wait_for_mobile"), "Wait for mobile to arrive", commandListServer_robot)

        # Move bricks to mobile
        elif(States[id] == commandListClient_robot.index("Moving_to_mobile")):
            if(index == commandListClient_robot.index("Waiting_for_mobile")):
               return command(commandListServer_robot.index("Deliver_to_mobile"), "Deliver bricks to mobile", commandListServer_robot) 
            elif(index == commandListClient_robot.index("Moving_to_mobile")):
                return command(commandListServer_robot.index("Deliver_to_mobile"), "Deliver bricks to mobile", commandListServer_robot)
            elif(index == commandListClient_robot.index("Complete")):
                updateMobile_robot(id)
                States[id] = commandListClient_robot.index("Idle")
                return command( commandListServer_robot.index("No_task"), "Just wait", commandListServer_robot)
            

def processDrone(id,data): # done!
    #print "processDrone"
    index = findState_drone(data)
    print "drone index %(number)d %(string)s " % { "number": index, "string": commandListClient_drone[index]}
    stat_drone = isMobileAvailable_drone()

    # Get order from server
    if(States[id] < 2):
        States[id] = index
        if(States[id] == commandListClient_drone.index("Suspended")):
            return command(commandListServer_drone.index("No_task"), "Just wait", commandListServer_drone)
        elif(States[id] == commandListClient_drone.index("Idle")):
            return command(commandListServer_drone.index("No_task"), "Just wait", commandListServer_drone)
        elif(States[id] == commandListClient_drone.index("Request_task")):

            # Robot is waiting
            if(stat_drone >= 0):
                if(stat_drone == MOBILE_1 and id == DRONE_1):            
                    States[stat_drone] = commandListClient_mobile.index("Waiting_for_accept_drone_1")
                    return command(commandListServer_drone.index("New_task"), "New task mobile: " + str(stat_drone+1), commandListServer_drone)
                elif(stat_drone == MOBILE_2 and id == DRONE_1):            
                    States[stat_drone] = commandListClient_mobile.index("Waiting_for_accept_drone_1")
                    return command(commandListServer_drone.index("New_task"), "New task mobile: " + str(stat_drone+1), commandListServer_drone)
                elif(stat_drone == MOBILE_1 and id == DRONE_2 ):
                    States[stat_drone] = commandListClient_mobile.index("Waiting_for_accept_drone_2")
                    return command(commandListServer_drone.index("New_task"), "New task mobile: " + str(stat_drone+1), commandListServer_drone)
                elif(stat_drone == MOBILE_2 and id == DRONE_2 ):
                    States[stat_drone] = commandListClient_mobile.index("Waiting_for_accept_drone_2")
                    return command(commandListServer_drone.index("New_task"), "New task mobile: " + str(stat_drone+1), commandListServer_drone)
            else:
                States[id] = commandListClient_drone.index("Idle")
                return command(commandListServer_drone.index("No_task"), "Just wait", commandListServer_drone)

        

    # Continue with order
    else:
        if(States[id] == commandListClient_drone.index("Request_task")):
            if(index == commandListClient_drone.index("Accept")):
                States[id] = commandListClient_drone.index("Delivering_bricks")
                return command(commandListServer_drone.index("Deliver_bricks"), "Deliver bricks to mobile", commandListServer_drone)
            elif(index == commandListClient_drone.index("Reject")):
                States[id] = commandListClient_drone.index("Idle")
                updateMobile_drone_reject()
                return command(commandListServer_drone.index("No_task"), "Just wait", commandListServer_drone)
        
        elif(States[id] == commandListClient_drone.index("Delivering_bricks")):
            if(index == commandListClient_drone.index("Delivering_bricks")):
                return command(commandListServer_drone.index("Deliver_bricks"), "Deliver bricks to mobile", commandListServer_drone)
            elif(index == commandListClient_drone.index("Complete")):
                updateMobile_drone_accept()
                States[id] = commandListClient_drone.index("Idle")
                return command(commandListServer_drone.index("No_task"), "Just wait", commandListServer_drone)

    
    

        

############################################################################
'''  One instance per connection.
     Override handle(self) to customize action. '''

class TCPConnectionHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        state_mutex.acquire()
        print "SingleTCPHandler::handle()"

        # self.request is the client connection
        data = self.request.recv(1024)  # clip input at 1Kb
        print "handle(): Received data : data =>\n[%s]" % (data)
    
        id = findID(data)
        print "id %s " % id
     
        if(id == 0 or id == 1):
            reply = processMobile(id,data)
        elif(id == 2 or id == 3):
            reply = processRobot(id,data)
        elif(id == 4 or id == 5):
            reply = processDrone(id,data)
        else:
            reply = command(0,"Error in ID")

        print States
        print mobileStates
        print "handle(): Sending reply [%s]" % (reply)

        print "\n ------------------------------------------------------------------------------------------------------------------------------------------- \n"

        if reply is not None:
            self.request.send(reply)
        state_mutex.release()
        self.request.close()

############################################################################

class Server(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    # Ctrl-C will cleanly kill all spawned threads
    daemon_threads = True
    # much faster rebinding
    allow_reuse_address = True

    def __init__(self, server_address, RequestHandlerClass):
        SocketServer.TCPServer.__init__(\
        self,\
        server_address,\
        RequestHandlerClass)

############################################################################

if __name__ == "__main__":
    server = SocketServer.TCPServer((HOST, PORT), TCPConnectionHandler)
	

    #server = Server((HOST, PORT), TCPConnectionHandler)
    # terminate with Ctrl-C
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        print "Closing down MES Server"
        server.shutdown()
        server.server_close()
        sys.exit(0)

############################################################################
