# Standards of the ROS Workspace
* All catkin packages (ROS nodes) must be placed in the `src/` folder (nowhere else)
* Any test node is **not** allowed be pushed to git (to avoid contamination). Keep your test code local!
* All nodes **must** contain a `README.md` file that describes:
   * Short description
   * Input/outputs
   * How to use

# Creating and building catkin packages
The following is stolen from [ROS](http://wiki.ros.org/ROS/Tutorials/catkin/CreatingPackage#Creating_a_catkin_Package)

First change to the source space directory of the catkin workspace you created in the Creating a Workspace for catkin tutorial:

```bash
# You should have created this in the Creating a Workspace Tutorial
$ cd ~/catkin_ws/src
```

Now use the `catkin_create_pkg` script to create a new package called `beginner_tutorials` which depends on `std_msgs`, `roscpp`, and `rospy`:

```bash
$ catkin_create_pkg beginner_tutorials std_msgs rospy roscpp
```
This will create a `beginner_tutorials` folder which contains a package.xml and a CMakeLists.txt, which have been partially filled out with the information you gave `catkin_create_pkg`.

`catkin_create_pkg` requires that you give it a `package_name` and optionally a list of dependencies on which that package depends:

```bash
# This is an example, do not try to run this
# catkin_create_pkg <package_name> [depend1] [depend2] [depend3]
```

`catkin_create_pkg` also has more [advanced functionalities](http://wiki.ros.org/catkin/commands/catkin_create_pkg)


# Initial setup of ROS Workspace

1. Pull newest version from git
2. Change dir to catkin_ws: `cd code/`
3. Create build and devel folder: `catkin_make`
4. Verify that folders were created
5. Source setup.bash in the `.bashrc` file.

```shell
# ROS
source /opt/ros/indigo/setup.bash
source /home/nicolai/Documents/RSD/code/devel/setup.bash
```

Please note that you can only source one workspace at a time

## Using QtCreator with catkin

In order for QtCreator to understand the project, source the same things as above in the `.profile` file
