function plot_xy_eps(varargin)
    close all;
    % Input arguments
    % - path to csv
    % - title
    % - xlabel
    % - ylabel
    % - legends
    %
    % Usage:
    % plot_xy_eps('test/test.csv','Some Title','Time [s]','Samples','Legend1','Legend2')

    % Validate input arguments
    ninputs = length(varargin);
    for i = 1:ninputs
        if ~ischar(varargin{i})
            error('   Argument %i must be given as a string, not %s',i,class(varargin{i}));
        end
    end
    
    data = csvread(varargin{1});
    m = length(data(1,:));

    if(m >= 2)
        % Format data
        x = data(:,1);
        y = data(:,[2;m]);
        
        % Get legends
        legends = {ninputs-4};
        for i=1:ninputs-4
            legends{i} = varargin{i+4};
        end
        plot_title = varargin{2};
        
        % Check if legends has been provided for all cols
        if(length(legends) ~= length(y(1,:)))
            error('   %i legends provided for %i columns of data',length(legends),length(y(1,:)))
        end
        p = figure(1);
        plot(x,y);
        legend(legends)
        title(strcat('\fontsize{16}',plot_title))
        xlabel(varargin{3})
        ylabel(varargin{4})
        grid on;
        set(p,'Visible','off')
        
        %Create file name from title
        eps_title = strrep(plot_title,' ','_');
        eps_title = lower(eps_title);
        eps = strcat(eps_title,'.eps');
        
        if(exist(strcat('../docs/img/',eps),'file'))
            warning('File already exists')
            erase = input('> Do you want to overwrite it? [y/n]: ','s');
            if(erase == 'y' || erase == 'Y')
                disp('   Overwriting file...')
                print(strcat('../docs/img/',eps), '-depsc');
                disp('   Done.')
            else
                % generate new name
            end
            
        else
            print(strcat('../docs/img/',eps), '-depsc');
        end
    else
        error('   The data provided only contains %i column(s), 2 or more is expected',m);
    end
    disp('Script succeeded')
end