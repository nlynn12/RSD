# Data Processing
All data must be processed by means of MATLAB. In this folder you find plot functions which create graphs and write them as eps to the documentation folder.

When placing csv files in the data folder, please create a subfolder (if non exists) with a short and descriptive name, e.g.: `vision/`. No csv file is allowed to be placed in this folder, only its subfolders.


# MATLAB Plot Functions

If you create a new matlab plot function, please place it here and write a small user guide, like below.

#### Plot XY
Call function `plot_xy_eps()` from matlab terminal and give the following arguments in order
1. path to, and name of csv file
2. title of plot
3. label of x axis
4. label of y axis
5. legends

E.g. we have 2 columns in test.csv and therefore we give 2 legends: `plot_xy_eps('test/test.csv','Some Title','Time [s]','Samples','Legend1','Legend2')` and it generates the following output:

![](https://s18.postimg.io/9xajy25d5/tmp.png)

The data in the csv file must be formatted as; _x, y1, y2,..., yn_