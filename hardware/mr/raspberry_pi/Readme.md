User and password:
	- username: rsd-group1
	- password: admin

Used GPIO
	- 18 : Shut down
	- 27 : Reboot

Connection through ssh:
	
	- Currently the internet connection is made through the sdu-guest wifi network, meaning that the ip of the raspberry might change over time
	- The ethernet address is set to 192.168.100.32
	- To connect through ethernet:

		(Make sure the raspberry is connected and the ethernet cable is alright, leds for ethernet should be on and a connection should be found in the computer)
	
		ssh rsd-group1@192.168.100.32
		yes
		admin
