
Wireing:

    Motor controller to Arduino (name/function, pin nr, wire color)
     - Step, pin 9, white
     - Enable, pin 10, brown
     - motor_direction, pin 11, purple
    Motor controller to RPi
     - 5 V supply, any 5V pin, red
    
    Light switch to Arduino (name/function, pin nr, wire color)
     - 5 V, 5V, white
     - 3.3 V, 3.3V, red
     - ground, ground, black
     - ground, ground, green
     - output, pin 7, blue
     
    Arduino to RPi (name, pin nr arduino, pin nr RPi, wire color)
     - Motor enable, pin 3, ?, ?
     - Motor ready, pin 4, ?, red
     - Motor_in_error, pin 5, ? brown
     - Ack_error, pin 6, ? ?
    
    Motor controller to motor (function, color)
     - A phase +, black
     - A phase -, green
     - B phase +, red
     - B phase -, blue
