### Smart Camera
* [Omron FQ2-S15050F](https://www.ia.omron.com/products/family/3131/)
* [Catalog](https://www.ia.omron.com/data_pdf/cat/fq2_q193-e1_14_2_csm1006918.pdf?id=3131)

### Gripper
* [Festo HGPC-16-A](https://www.festo.com/net/hu_hu/SupportPortal/default.aspx?cat=1551)
* [Datasheet](https://www.festo.com/cat/en-gb_gb/data/doc_ENUS/PDF/US/HGPC_ENUS.PDF)
