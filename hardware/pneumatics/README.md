Pneumatic Information
====

Kuka Robot
----

Air is put into the robot through AIR1 (Fig. 6-10: Valve diagram / [KR AGILUS sixx](../hardware/datasheet/KUKA_KR_AGILUS_sixx.pdf)).

AIR1 outside diameter: 6mm
Max pressure 7 bar.


Connecting the two out/in lets of the Festo gripper to a A-B pair on the top of the wrist.
Silencer is mounted onto (1) according to Fig. 6-9: Connector bypack option ( [KR AGILUS sixx](../hardware/datasheet/KUKA_KR_AGILUS_sixx.pdf) ).
B of the robot is the default outlet on the wrist.
Size of the outlet on the wrist is M5.


The robot has a total of three 5/2 valves.
We use one of those to control the gripper.
The 5/2 valve is controlled from the robot and allows air to be pumped both ways through the system.
Thus the robot can actuate the gripper in both directions.



FESTO Gripper
----

The grippers connectors are size M5.

B of robot should be conneced to backside of gripper (side with NO Festo logo), this way the initial pose of the gripper should be open wrt internal gripping.

Gripper: Max 8 bar min 2 bar.

[Festo datasheet](../hardware/datasheet/HGPC_ENUS.PDF)


Pneumatics 'Diagram' and Mounting
----

Diagram
```
Air outlet      Robot Inlet      Robot Outlet      Gripper  
------------------------------------------------------------------------  
Robolab     ->     AIR1      ->       1A       ->   front (Festo logo)  
                             ->       1B       ->   back  
```

Furthermore remember to mount the silencer to the robot.

The valve on the gripper can be adjusted in the beginning to control the airflow.

No more than 7 bar should be supplied from the robolab outlet.



Random Notes
----
WorkVirtual